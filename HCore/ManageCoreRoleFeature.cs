﻿using HCore.Framework.RoleManagement;
using HCore.Helper;
using HCore.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore
{
    public class ManageCoreRoleFeature
    {
        FrameworkCoreRoleFeature? _FrameworkCoreRoleFeature;

        public async Task<OResponse> SaveRoleFeatures(ORoleManagement.RoleFeature.Request _Request)
        {
            _FrameworkCoreRoleFeature = new FrameworkCoreRoleFeature();
            return await _FrameworkCoreRoleFeature.SaveRoleFeatures(_Request);
        }

        public async Task<OResponse> UpdateRoleFeatures(ORoleManagement.RoleFeature.Request _Request)
        {
            _FrameworkCoreRoleFeature = new FrameworkCoreRoleFeature();
            return await _FrameworkCoreRoleFeature.UpdateRoleFeatures(_Request);
        }

        public async Task<OResponse> GetRoleFeature(OReference _Request)
        {
            _FrameworkCoreRoleFeature = new FrameworkCoreRoleFeature();
            return await _FrameworkCoreRoleFeature.GetRoleFeature(_Request);
        }

        public async Task<OResponse> GetRoleFeatures(OList.Request _Request)
        {
            _FrameworkCoreRoleFeature = new FrameworkCoreRoleFeature();
            return await _FrameworkCoreRoleFeature.GetRoleFeatures(_Request);
        }

        public async Task<OResponse> GetFeatures(OList.Request _Request)
        {
            _FrameworkCoreRoleFeature = new FrameworkCoreRoleFeature();
            return await _FrameworkCoreRoleFeature.GetFeatures(_Request);
        }
    }
}
