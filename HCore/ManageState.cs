﻿using HCore.Framework;
using HCore.Helper;

namespace HCore
{
    public class ManageState
    {
        FrameworkState? _FrameworkState;

        public async Task<OResponse> GetStates(OList.Request _Request)
        {
            _FrameworkState = new FrameworkState();
            return await _FrameworkState.GetStates(_Request);
        }
    }
}
