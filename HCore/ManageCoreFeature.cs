﻿using HCore.Framework.RoleManagement;
using HCore.Helper;
using HCore.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore
{
    public class ManageCoreFeature
    {
        FrameworkCoreFeature? _FrameworkCoreFeature;

        public async Task<OResponse> SaveFeature(ORoleManagement.Feature.Request _Request)
        {
            _FrameworkCoreFeature = new FrameworkCoreFeature();
            return await _FrameworkCoreFeature.SaveFeature(_Request);
        }

        public async Task<OResponse> GetFeature(OReference _Request)
        {
            _FrameworkCoreFeature = new FrameworkCoreFeature();
            return await _FrameworkCoreFeature.GetFeature(_Request);
        }

        public async Task<OResponse> GetFeatures(OList.Request _Request)
        {
            _FrameworkCoreFeature = new FrameworkCoreFeature();
            return await _FrameworkCoreFeature.GetFeatures(_Request);
        }
    }
}
