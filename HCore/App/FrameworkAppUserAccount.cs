﻿using HCore.Object;
using HCore.Data.Models;

using HCore.Helper;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;

namespace HCore.App.Framework
{
    public class FrameworkAppUserAccount
    {
        HCoreContext? _HCoreContext;
        AtAccountSession? _AtAccountSession;
        OAccount.UserProfileDetails? _OAccount;

        private async Task<OAccount.UserProfileDetails> GetUserProfile(long AccountId, OUserReference _UserReference)
        {
            try
            {
                _OAccount = new OAccount.UserProfileDetails();
                string? UserAccountSessionKey = HCoreHelper.GenerateGuid();
                using (_HCoreContext = new HCoreContext())
                {
                    var UserDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                      .Select(x => new
                                      {
                                          UserAccountId = x.Id,
                                          UserAccountKey = x.Guid,
                                          AccountTypeId = x.AccountTypeId,
                                          Name = x.FirstName + " " + x.LastName,
                                          DisplayName = x.DisplayName,
                                          EmailAddress = x.EmailAddress,
                                          MobileNumber = x.MobileNumber,
                                      }).FirstOrDefaultAsync();
                    if (UserDetails != null)
                    {
                        _OAccount.Name = UserDetails.Name;
                        _OAccount.DisplayName = UserDetails.DisplayName;
                        _OAccount.EmailAddress = UserDetails.EmailAddress;
                        long UserSessionId = 0;
                        DateTime _LogOutDate = HCoreHelper.GetDateTime();

                        var UserActiveSessions = await _HCoreContext.AtAccountSessions.Where(x => x.AccountId == AccountId && x.StatusId == HelperStatus.Default.Active && x.PlatformId == Helpers.Platform.Mobile).ToListAsync();
                        foreach (var UserActiveSession in UserActiveSessions)
                        {
                            UserActiveSession.LogoutDate = _LogOutDate;
                            UserActiveSession.StatusId = HelperStatus.Default.Inactive;
                        }

                        await _HCoreContext.SaveChangesAsync();

                        _AtAccountSession = new AtAccountSession();
                        _AtAccountSession.Guid = UserAccountSessionKey;
                        _AtAccountSession.AccountId = UserDetails.UserAccountId;
                        _AtAccountSession.AppVersionId = _UserReference.AppVersionId;
                        _AtAccountSession.PlatformId = Helpers.Platform.Mobile;
                        _AtAccountSession.LastActivityDate = HCoreHelper.GetDateTime();
                        _AtAccountSession.LoginDate = HCoreHelper.GetDateTime();
                        _AtAccountSession.IPAddress = _UserReference.RequestIpAddress;
                        _AtAccountSession.Latitude = (decimal?)_UserReference.RequestLatitude;
                        _AtAccountSession.Longitude = (decimal?)_UserReference.RequestLongitude;
                        _AtAccountSession.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.AtAccountSessions.AddAsync(_AtAccountSession);
                        await _HCoreContext.SaveChangesAsync();
                        UserSessionId = _AtAccountSession.Id;

                        _OAccount.AccountKey = UserDetails.UserAccountKey;
                        _OAccount.AccountId = UserDetails.UserAccountId;
                        _OAccount.Key = HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(UserSessionId + "|" + UserAccountSessionKey));

                        var UserAccountD = await _HCoreContext.AtAccounts.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefaultAsync();
                        if (UserAccountD != null)
                        {
                            UserAccountD.LastLoginDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.SaveChangesAsync();
                        }

                        await _HCoreContext.DisposeAsync();

                        _OAccount.Message = "Login successful";
                        return _OAccount;
                    }
                    else
                    {
                        _OAccount.Message = "Authentication failed";
                        return _OAccount;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserProfile", ex);
                _OAccount = new OAccount.UserProfileDetails();
                _OAccount.Message = "Authentication failed. An error occurred while fetching user profile details.";
                return _OAccount;
            }
        }

        internal async Task<OResponse> AppUserLogin(OAccount.UserAccount.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.UserName) || string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT023", "Username and Password required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    var UserDetails = await _HCoreContext.AtAccounts.Where(x => x.MobileNumber == _Request.UserName && x.AccessPin == Password && x.AccountTypeId == UserAccountType.User).FirstOrDefaultAsync();
                    if (UserDetails != null)
                    {
                        if (UserDetails.StatusId == HelperStatus.Default.Inactive)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0401", "Authentication failed");
                        }
                        _OAccount = new OAccount.UserProfileDetails();
                        _OAccount = await GetUserProfile(UserDetails.Id, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OAccount, "AT0200", "Login successful");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT023", "Invalid Username and Password");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("AppUserLogin", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> AppUserLogout(OAccount.UserAccount.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var SessionDetails = await _HCoreContext.AtAccountSessions.Where(x => x.Id == _Request.UserReference.AccountSessionId && x.PlatformId == Helpers.Platform.Mobile && x.AccountId == _Request.UserReference.AccountId).FirstOrDefaultAsync();
                    if (SessionDetails != null)
                    {
                        SessionDetails.LogoutDate = HCoreHelper.GetDateTime();
                        SessionDetails.StatusId = HCoreConstant.StatusBlocked;
                        await _HCoreContext.SaveChangesAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "HC131", "Log out successful");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "HC132", "Invalid user session");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("AppUserLogout", _Exception, _Request.UserReference);
            }
        }
    }
}
