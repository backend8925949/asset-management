﻿using HCore.Object;
using HCore.Data.Models;
using HCore.Helper;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkState
    {
        HCoreContext? _HCoreContext;
        AtState? _AtState;
        AtCity? _AtCity;

        internal async Task<OResponse> GetStates(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtStates
                                                .Select(x => new OState.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.StateName,
                                                }).Where(_Request.SearchCondition)
                                                 .CountAsync();

                    }
                    List<OState.List> Data = await _HCoreContext.AtStates
                                             .Select(x => new OState.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 Name = x.StateName,
                                             }).Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToListAsync();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "AT0200", "States loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStates", _Exception, _Request.UserReference);
            }
        }
    }
}
