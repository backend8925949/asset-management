﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using HCore.Data.Logging;
using HCore.Data.Logging.Models;

namespace HCore.Framework
{
    public class FrameworkSystemConfigurations
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        OSystemConfigurations.SystemConfigurations? _SystemConfigurations;

        internal async Task<OResponse> GetSystemConfigs(OReference _Request)
        {
            try
            {
                _SystemConfigurations = new OSystemConfigurations.SystemConfigurations();
                using (_HCoreContext = new HCoreContext())
                {
                    var OrganizationDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                    if (OrganizationDetails != null)
                    {
                        _SystemConfigurations.OrganizationId = OrganizationDetails.Id;
                        _SystemConfigurations.OrganizationKey = OrganizationDetails.Guid;
                        _SystemConfigurations.OrganizationName = OrganizationDetails.DisplayName;
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _SystemConfigurations, "HC0200", "Success");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0401", "Unauthorized");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSystemConfigs", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDownloads(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCoreStorages.Where(x => x.AccountId == _Request.UserReference.AccountId)
                                           .Select(x => new OSystemConfigurations.Downloads
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,
                                               File = x.Files,
                                               File1 = x.Files1,
                                               File2 = x.Files2,
                                               File3 = x.Files1,
                                               FileName = x.FileName,
                                               FileExtension = x.FileExtension,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OSystemConfigurations.Downloads> _Downloads = await _HCoreContext.AtCoreStorages.Where(x => x.AccountId == _Request.UserReference.AccountId)
                                       .Select(x => new OSystemConfigurations.Downloads
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           File = x.Files,
                                           File1 = x.Files1,
                                           File2 = x.Files2,
                                           File3 = x.Files1,
                                           FileName = x.FileName,
                                           FileExtension = x.FileExtension,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach(var File in _Downloads)
                    {
                        File.File = File.File + "" + File.File1 + "" + File.File2 + "" + File.File3;
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Downloads, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDownloads", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSystemLogs(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtCoreLogs
                                           .Select(x => new OSystemConfigurations.SystemLogs
                                           {
                                               ReferenceId = x.Id,
                                               Title = x.Title,
                                               Host = x.HostName,
                                               Message = x.Message,
                                               RequestReference = x.RequestReference,
                                               Data = x.Data,
                                               CreateDate = x.CreateDate,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OSystemConfigurations.SystemLogs> _SystemLogs = await _HCoreContextLogging.AtCoreLogs
                                       .Select(x => new OSystemConfigurations.SystemLogs
                                       {
                                           ReferenceId = x.Id,
                                           Title = x.Title,
                                           Host = x.HostName,
                                           Message = x.Message,
                                           RequestReference = x.RequestReference,
                                           Data = x.Data,
                                           CreateDate = x.CreateDate,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SystemLogs, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSystemLogs", _Exception, _Request.UserReference);
            }
        }
    }
}
