﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework.RoleManagement
{
    public class FrameworkCoreFeature
    {
        HCoreContext? _HCoreContext;
        AtCoreFeature? _AtCoreFeature;

        internal async Task<OResponse> SaveFeature(ORoleManagement.Feature.Request _Request)
        {
            try
            {
                if(string.IsNullOrEmpty(_Request.FeatureName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Feature name required.");
                }

                int? StatusId = 0;
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTATUS", "Please select status.");
                }
                else
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATINSTATUS", "Invalid status selected.");
                    }
                }

                int? FeatureTypeId = 0;
                if (string.IsNullOrEmpty(_Request.FeatureType))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTATUS", "Please select type.");
                }
                else
                {
                    FeatureTypeId = HCoreHelper.GetSystemHelperId(_Request.FeatureType, _Request.UserReference);
                    if (FeatureTypeId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATINSTATUS", "Invalid type selected.");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? SystemName = HCoreHelper.GenerateSystemName(_Request.FeatureName);
                    bool _IsExists = await _HCoreContext.AtCoreFeatures.AnyAsync(x => x.SystemName == SystemName);
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATSAVE", "Feature details are already present in the system.");
                    }

                    long? ParentId = 0;
                    if (!string.IsNullOrEmpty(_Request.ParentKey))
                    {
                        ParentId = await _HCoreContext.AtCoreFeatures.Where(x => x.Guid == _Request.ParentKey).Select(x => x.Id).FirstOrDefaultAsync();
                    }

                    _AtCoreFeature = new AtCoreFeature();
                    _AtCoreFeature.Guid = HCoreHelper.GenerateGuid();
                    _AtCoreFeature.TypeId = (int)FeatureTypeId;

                    if(ParentId > 0 && ParentId != null)
                    {
                        _AtCoreFeature.ParentId = ParentId;
                    }

                    _AtCoreFeature.Name = _Request.FeatureName;
                    _AtCoreFeature.SystemName = SystemName;
                    _AtCoreFeature.HelperId = 1;
                    _AtCoreFeature.StatusId = (int)StatusId;
                    _AtCoreFeature.CreatedDate = HCoreHelper.GetDateTime();
                    _AtCoreFeature.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContext.AtCoreFeatures.AddAsync(_AtCoreFeature);
                    await _HCoreContext.SaveChangesAsync();

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATSAVE", "Feature details saved successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveFeature", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetFeature(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFID", "Please select feature to get details");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFKEY", "Please select feature to get details");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var FeatureDetails = await _HCoreContext.AtCoreFeatures.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                         .Select(x => new ORoleManagement.Feature.Details
                                         {
                                             FeatureId = x.Id,
                                             FeatureKey = x.Guid,
                                             FeatureName = x.Name,

                                             FeatureTypeId = x.TypeId,
                                             FeatureType = x.Type.SystemName,
                                             FeatureTypeName = x.Type.Name,

                                             ParentId = x.ParentId,
                                             ParentKey = x.Parent.Guid,
                                             ParentName = x.Parent.Name,

                                             CreateDate = x.CreatedDate,
                                             CreatedById = (long)x.CreatedById,
                                             CreatedByKey = x.CreatedBy.Guid,
                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                             ModifyDate = x.ModifyDate,
                                             ModifyById = x.ModifiedById,
                                             ModifyByKey = x.ModifiedBy.Guid,
                                             ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                             StatusId = x.StatusId,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                         }).FirstOrDefaultAsync();

                    if(FeatureDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, FeatureDetails, "AT0200", "Feature details loaded successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, FeatureDetails, "AT0404", "Feature details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetFeature", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetFeatures(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "FeatureId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "FeatureId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCoreFeatures
                                                .Select(x => new ORoleManagement.Feature.List
                                                {
                                                    FeatureId = x.Id,
                                                    FeatureKey = x.Guid,
                                                    FeatureName = x.Name,

                                                    FeatureTypeId = x.TypeId,
                                                    FeatureType = x.Type.SystemName,
                                                    FeatureTypeName = x.Type.Name,

                                                    ParentId = x.ParentId,
                                                    ParentKey = x.Parent.Guid,
                                                    ParentName = x.Parent.Name,

                                                    CreateDate = x.CreatedDate,
                                                    CreatedById = (long)x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifiedById,
                                                    ModifyByKey = x.ModifiedBy.Guid,
                                                    ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<ORoleManagement.Feature.List> _Features = await _HCoreContext.AtCoreFeatures
                                                                   .Select(x => new ORoleManagement.Feature.List
                                                                   {
                                                                       FeatureId = x.Id,
                                                                       FeatureKey = x.Guid,
                                                                       FeatureName = x.Name,

                                                                       FeatureTypeId = x.TypeId,
                                                                       FeatureType = x.Type.SystemName,
                                                                       FeatureTypeName = x.Type.Name,

                                                                       ParentId = x.ParentId,
                                                                       ParentKey = x.Parent.Guid,
                                                                       ParentName = x.Parent.Name,

                                                                       CreateDate = x.CreatedDate,
                                                                       CreatedById = (long)x.CreatedById,
                                                                       CreatedByKey = x.CreatedBy.Guid,
                                                                       CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                       ModifyDate = x.ModifyDate,
                                                                       ModifyById = x.ModifiedById,
                                                                       ModifyByKey = x.ModifiedBy.Guid,
                                                                       ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                                       StatusId = x.StatusId,
                                                                       StatusCode = x.Status.SystemName,
                                                                       StatusName = x.Status.Name,
                                                                   }).Where(_Request.SearchCondition)
                                                                    .OrderBy(_Request.SortExpression)
                                                                    .Skip(_Request.Offset)
                                                                    .Take(_Request.Limit)
                                                                    .ToListAsync();

                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _Features, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "AT0200", "Features loaded successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetFeatures", _Exception, _Request.UserReference);
            }
        }
    }
}
