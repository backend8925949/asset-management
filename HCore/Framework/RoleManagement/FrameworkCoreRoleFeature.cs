﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.Object;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework.RoleManagement
{
    public class FrameworkCoreRoleFeature
    {
        HCoreContext? _HCoreContext;
        AtCoreRole? _AtCoreRole;
        AtCoreRoleFeature? _AtCoreRoleFeature;

        internal async Task<OResponse> SaveRoleFeatures(ORoleManagement.RoleFeature.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.RoleName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Role Name Requires.");
                }

                int? StatusId = 0;
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTATUS", "Please select status.");
                }
                else
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATINSTATUS", "Invalid status selected.");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? SystemName = HCoreHelper.GenerateSystemName(_Request.RoleName);
                    bool _IsExists = await _HCoreContext.AtCoreRoles.AnyAsync(x => x.SystemName == SystemName && (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1));
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATEXISTS", "Role details are already present in system");
                    }

                    _AtCoreRole = new AtCoreRole();
                    _AtCoreRole.Guid = HCoreHelper.GenerateGuid();
                    _AtCoreRole.OrganizationId = _Request.UserReference.OrganizationId;
                    _AtCoreRole.Name = _Request.RoleName;
                    _AtCoreRole.SystemName = SystemName;
                    _AtCoreRole.StatusId = (int)StatusId;
                    _AtCoreRole.CreatedDate = HCoreHelper.GetDateTime();
                    _AtCoreRole.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContext.AtCoreRoles.AddAsync(_AtCoreRole);
                    await _HCoreContext.SaveChangesAsync();

                    long? RoleId = _AtCoreRole.Id;

                    if (_Request.Features.Count > 0)
                    {
                        foreach (var Feature in _Request.Features)
                        {
                            if (Feature.IsView || Feature.IsAdd || Feature.IsUpdate || Feature.IsDelete)
                            {
                                var FeatureDetails = await _HCoreContext.AtCoreFeatures.Where(x => x.Id == Feature.FeatureId && x.Guid == Feature.FeatureKey).FirstOrDefaultAsync();
                                if (FeatureDetails != null)
                                {
                                    _AtCoreRoleFeature = new AtCoreRoleFeature();
                                    _AtCoreRoleFeature.Guid = HCoreHelper.GenerateGuid();
                                    _AtCoreRoleFeature.RoleId = RoleId;
                                    _AtCoreRoleFeature.FeatureId = FeatureDetails.Id;
                                    if (Feature.IsAdd == true)
                                    {
                                        _AtCoreRoleFeature.IsAdd = 1;
                                    }
                                    if (Feature.IsUpdate == true)
                                    {
                                        _AtCoreRoleFeature.IsUpdate = 1;
                                    }
                                    if (Feature.IsDelete == true)
                                    {
                                        _AtCoreRoleFeature.IsDelete = 1;
                                    }
                                    if (Feature.IsView == true)
                                    {
                                        _AtCoreRoleFeature.IsView = 1;
                                    }
                                    if (Feature.IsViewAll == true)
                                    {
                                        _AtCoreRoleFeature.IsViewAll = 1;
                                    }
                                    _AtCoreRoleFeature.StatusId = HelperStatus.Default.Active;
                                    _AtCoreRoleFeature.CreatedById = _Request.UserReference.AccountId;
                                    _AtCoreRoleFeature.CreatedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.AtCoreRoleFeatures.AddAsync(_AtCoreRoleFeature);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSAVE", "Role details saved successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRoleFeature", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateRoleFeatures(ORoleManagement.RoleFeature.Request _Request)
        {
            try
            {
                if (_Request.RoleId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFID", "Please select role to get details");
                }
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFKEY", "Please select role to get details");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATINSTATUS", "Invalid status selected.");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var RoleDetails = await _HCoreContext.AtCoreRoles.Where(x => x.Id == _Request.RoleId && x.Guid == _Request.RoleKey && (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1)).FirstOrDefaultAsync();
                    if (RoleDetails != null)
                    {
                        string? SystemName = HCoreHelper.GenerateSystemName(_Request.RoleName);
                        bool _IsExists = await _HCoreContext.AtCoreRoles.AnyAsync(x => x.SystemName == SystemName && x.Guid != RoleDetails.Guid && (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1));
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATEXISTS", "Role details are already present in system");
                        }

                        if((_Request.UserReference.EmailAddress != "omkar.anekar@rajkamalbarscan.com") && (RoleDetails.Id == UserRole.SuperAdmin || RoleDetails.Id == UserRole.Admin || RoleDetails.Id == UserRole.HR || RoleDetails.Id == UserRole.HeadOfDiv || RoleDetails.Id == UserRole.HeadOfDept || RoleDetails.Id == UserRole.Employee || RoleDetails.Id == UserRole.User))
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATPERDNY", "You don't have the permission to update the details of this role. Please contact support.");
                        }

                        if (!string.IsNullOrEmpty(_Request.RoleName) && RoleDetails.Name != _Request.RoleName)
                        {
                            RoleDetails.Name = _Request.RoleName;
                            RoleDetails.SystemName = SystemName;
                        }
                        if (StatusId > 0 && StatusId != RoleDetails.StatusId)
                        {
                            RoleDetails.StatusId = (int)StatusId;
                        }
                        RoleDetails.ModifyDate = HCoreHelper.GetDateTime();
                        RoleDetails.ModifiedById = _Request.UserReference.AccountId;
                        await _HCoreContext.SaveChangesAsync();

                        var RoleFeatures = await _HCoreContext.AtCoreRoleFeatures.Where(x => x.RoleId == RoleDetails.Id).ToListAsync();
                        if (RoleFeatures.Count > 0)
                        {
                            _HCoreContext.AtCoreRoleFeatures.RemoveRange(RoleFeatures);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        if (_Request.Features.Count > 0)
                        {
                            foreach (var Feature in _Request.Features)
                            {
                                if (Feature.IsView || Feature.IsAdd || Feature.IsUpdate || Feature.IsDelete)
                                {
                                    var FeatureDetails = await _HCoreContext.AtCoreFeatures.Where(x => x.Id == Feature.FeatureId && x.Guid == Feature.FeatureKey).FirstOrDefaultAsync();
                                    if (FeatureDetails != null)
                                    {
                                        _AtCoreRoleFeature = new AtCoreRoleFeature();
                                        _AtCoreRoleFeature.Guid = HCoreHelper.GenerateGuid();
                                        _AtCoreRoleFeature.RoleId = RoleDetails.Id;
                                        _AtCoreRoleFeature.FeatureId = FeatureDetails.Id;
                                        if (Feature.IsAdd == true)
                                        {
                                            _AtCoreRoleFeature.IsAdd = 1;
                                        }
                                        if (Feature.IsUpdate == true)
                                        {
                                            _AtCoreRoleFeature.IsUpdate = 1;
                                        }
                                        if (Feature.IsDelete == true)
                                        {
                                            _AtCoreRoleFeature.IsDelete = 1;
                                        }
                                        if (Feature.IsView == true)
                                        {
                                            _AtCoreRoleFeature.IsView = 1;
                                        }
                                        if (Feature.IsViewAll == true)
                                        {
                                            _AtCoreRoleFeature.IsViewAll = 1;
                                        }
                                        _AtCoreRoleFeature.StatusId = HelperStatus.Default.Active;
                                        _AtCoreRoleFeature.CreatedById = _Request.UserReference.AccountId;
                                        _AtCoreRoleFeature.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtCoreRoleFeatures.AddAsync(_AtCoreRoleFeature);
                                        await _HCoreContext.SaveChangesAsync();
                                    }
                                }
                            }
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSAVE", "Role details updated successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Role details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRoleFeature", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetRoleFeature(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFID", "Please select role to get details");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFKEY", "Please select role to get details");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var RoleDetails = await _HCoreContext.AtCoreRoles.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1))
                                      .Select(x => new ORoleManagement.RoleFeature.Details
                                      {
                                          RoleId = x.Id,
                                          RoleKey = x.Guid,
                                          RoleName = x.Name,

                                          CreateDate = x.CreatedDate,
                                          CreatedById = (long)x.CreatedById,
                                          CreatedByKey = x.CreatedBy.Guid,
                                          CreatedByDisplayName = x.CreatedBy.DisplayName,

                                          ModifyDate = x.ModifyDate,
                                          ModifyById = x.ModifiedById,
                                          ModifyByKey = x.ModifiedBy.Guid,
                                          ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                          StatusId = x.StatusId,
                                          StatusCode = x.Status.SystemName,
                                          StatusName = x.Status.Name,
                                      }).FirstOrDefaultAsync();
                    if (RoleDetails != null)
                    {
                        List<ORoleManagement.RoleFeature.RoleFeatures>? _Features = new List<ORoleManagement.RoleFeature.RoleFeatures>();
                        _Features = await _HCoreContext.AtCoreFeatures.Where(x => x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new ORoleManagement.RoleFeature.RoleFeatures
                                {
                                    FeatureId = x.Id,
                                    FeatureKey = x.Guid,
                                    FeatureName = x.Name,
                                    IsAdd = x.AtCoreRoleFeatures.Where(a => a.FeatureId == x.Id && a.RoleId == RoleDetails.RoleId).Select(a => a.IsAdd).FirstOrDefault(),
                                    IsUpdate = x.AtCoreRoleFeatures.Where(a => a.FeatureId == x.Id && a.RoleId == RoleDetails.RoleId).Select(a => a.IsUpdate).FirstOrDefault(),
                                    IsDelete = x.AtCoreRoleFeatures.Where(a => a.FeatureId == x.Id && a.RoleId == RoleDetails.RoleId).Select(a => a.IsDelete).FirstOrDefault(),
                                    IsView = x.AtCoreRoleFeatures.Where(a => a.FeatureId == x.Id && a.RoleId == RoleDetails.RoleId).Select(a => a.IsView).FirstOrDefault(),
                                    IsViewAll = x.AtCoreRoleFeatures.Where(a => a.FeatureId == x.Id && a.RoleId == RoleDetails.RoleId).Select(a => a.IsViewAll).FirstOrDefault(),
                                }).ToListAsync();

                        RoleDetails.Features = _Features;

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, RoleDetails, "AT0200", "Role details loaded successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Invalid role selected.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRoleFeature", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetRoleFeatures(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "RoleId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "RoleId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCoreRoles.Where(x => (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1))
                                                .Select(x => new ORoleManagement.RoleFeature.List
                                                {
                                                    RoleId = x.Id,
                                                    RoleKey = x.Guid,
                                                    RoleName = x.Name,
                                                    RoleAccess = x.AtCoreRoleFeatures.Where(a => a.RoleId == x.Id).Count(),

                                                    CreateDate = x.CreatedDate,
                                                    CreatedById = (long)x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifiedById,
                                                    ModifyByKey = x.ModifiedBy.Guid,
                                                    ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<ORoleManagement.RoleFeature.List> _Roles = await _HCoreContext.AtCoreRoles.Where(x => (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1))
                                                                    .Select(x => new ORoleManagement.RoleFeature.List
                                                                    {
                                                                        RoleId = x.Id,
                                                                        RoleKey = x.Guid,
                                                                        RoleName = x.Name,
                                                                        RoleAccess = x.AtCoreRoleFeatures.Where(a => a.RoleId == x.Id).Count(),

                                                                        CreateDate = x.CreatedDate,
                                                                        CreatedById = (long)x.CreatedById,
                                                                        CreatedByKey = x.CreatedBy.Guid,
                                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                        ModifyDate = x.ModifyDate,
                                                                        ModifyById = x.ModifiedById,
                                                                        ModifyByKey = x.ModifiedBy.Guid,
                                                                        ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                                        StatusId = x.StatusId,
                                                                        StatusCode = x.Status.SystemName,
                                                                        StatusName = x.Status.Name,
                                                                    }).Where(_Request.SearchCondition)
                                                                    .OrderBy(_Request.SortExpression)
                                                                    .Skip(_Request.Offset)
                                                                    .Take(_Request.Limit)
                                                                    .ToListAsync();

                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _Roles, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "AT0200", "Roles loaded successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRoleFeature", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetFeatures(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "FeatureId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "FeatureId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCoreFeatures.Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new ORoleManagement.Features
                                                {
                                                    FeatureId = x.Id,
                                                    FeatureKey = x.Guid,
                                                    FeatureName = x.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<ORoleManagement.Features> _Features = await _HCoreContext.AtCoreFeatures.Where(x => x.StatusId == HelperStatus.Default.Active)
                                                               .Select(x => new ORoleManagement.Features
                                                               {
                                                                   FeatureId = x.Id,
                                                                   FeatureKey = x.Guid,
                                                                   FeatureName = x.Name,
                                                               }).Where(_Request.SearchCondition)
                                                               .OrderBy(_Request.SortExpression)
                                                               .Skip(_Request.Offset)
                                                               .Take(_Request.Limit)
                                                               .ToListAsync();

                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _Features, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "AT0200", "Features loaded successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetFeatures", _Exception, _Request.UserReference);
            }
        }
    }
}
