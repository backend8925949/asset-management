﻿using HCore.Object;
using HCore.Data.Models;
using HCore.Helper;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.Framework
{
    public class FrameworkCity
    {
        HCoreContext? _HCoreContext;

        internal async Task<OResponse> GetCities(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCities.Where(x => x.StateId == _Request.ReferenceId && x.State.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCity.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.CityName,
                                                }).Where(_Request.SearchCondition)
                                                 .CountAsync();

                    }
                    List<OCity.List> Data = await _HCoreContext.AtCities.Where(x => x.StateId == _Request.ReferenceId && x.State.Guid == _Request.ReferenceKey)
                                            .Select(x => new OCity.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                Name = x.CityName,
                                            }).Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToListAsync();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "AT0200", "Cities loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCities", _Exception, _Request.UserReference);
            }
        }
    }
}
