﻿using HCore.Object;
using HCore.Data.Models;

using HCore.Helper;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
using System.Security.Principal;
using System.Text;

namespace HCore.Framework
{
    public class FrameworkUserAccount
    {
        HCoreContext? _HCoreContext;
        AtAccountSession? _AtAccountSession;
        AtAccount? _AtAccount;
        AtAddress? _AtAddress;
        AtAccountAuth? _AtAccountAuth;
        AtCoreRole? _AtCoreRole;
        AtCoreRoleFeature? _AtCoreRoleFeature;
        Random? _Random;
        OAccount.UserProfileDetails? _OAccount;
        List<OAccount.Features>? _Features;
        OAccount.RequisitionConfig? _RequisitionConfig;
        AtAssetRequisitionConfig? _AssetRequisitionConfig;

        private async Task<OAccount.UserProfileDetails> GetUserProfile(long AccountId, OUserReference _UserReference)
        {
            try
            {
                _OAccount = new OAccount.UserProfileDetails();
                _Features = new List<OAccount.Features>();
                string? UserAccountSessionKey = HCoreHelper.GenerateGuid();
                using (_HCoreContext = new HCoreContext())
                {
                    var UserDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                      .Select(x => new
                                      {
                                          UserAccountId = x.Id,
                                          UserAccountKey = x.Guid,
                                          Name = x.FirstName + " " + x.LastName,
                                          DisplayName = x.DisplayName,
                                          EmailAddress = x.EmailAddress,
                                          MobileNumber = x.MobileNumber,
                                          OrganizationId = x.OrganizationId,
                                          OrganizationKey = x.Organization.Guid,
                                          OrganizationName = x.Organization.DisplayName,
                                          AccountTypeId = x.AccountTypeId,
                                          AccountTypeCode = x.AccountType.SystemName,
                                          AccountTypeName = x.AccountType.Name,
                                          UserRoleId = x.RoleId,
                                          UserRoleKey = x.Role.SystemName,
                                          UserRoleName = x.Role.Name,
                                      }).FirstOrDefaultAsync();
                    if (UserDetails != null)
                    {
                        _OAccount.Name = UserDetails.Name;
                        _OAccount.DisplayName = UserDetails.DisplayName;
                        _OAccount.EmailAddress = UserDetails.EmailAddress;
                        _OAccount.OrganizationId = UserDetails.OrganizationId;
                        _OAccount.OrganizationKey = UserDetails.OrganizationKey;
                        _OAccount.OrganizationName = UserDetails.OrganizationName;
                        _OAccount.AccountTypeId = UserDetails.AccountTypeId;
                        _OAccount.AccountTypeCode = UserDetails.AccountTypeCode;
                        _OAccount.AccountTypeName = UserDetails.AccountTypeName;
                        _OAccount.UserRoleId = UserDetails.UserRoleId;
                        _OAccount.UserRoleKey = UserDetails.UserRoleKey;
                        _OAccount.UserRoleName = UserDetails.UserRoleName;
                        long UserSessionId = 0;
                        DateTime _LogOutDate = HCoreHelper.GetDateTime();

                        var UserActiveSessions = await _HCoreContext.AtAccountSessions.Where(x => x.AccountId == AccountId && x.PlatformId == Helpers.Platform.Web && x.StatusId == HCoreConstant.HelperStatus.Default.Active).ToListAsync();
                        foreach (var UserActiveSession in UserActiveSessions)
                        {
                            UserActiveSession.LogoutDate = _LogOutDate;
                            UserActiveSession.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                        }

                        await _HCoreContext.SaveChangesAsync();

                        _Features = await _HCoreContext.AtCoreRoleFeatures.Where(x => x.RoleId == UserDetails.UserRoleId)
                                    .Select(x => new OAccount.Features
                                    {
                                        FeatureId = x.FeatureId,
                                        FeatureKey = x.Feature.SystemName,
                                        FeatureName = x.Feature.Name,
                                        IsView = x.IsView,
                                        IsAdd = x.IsAdd,
                                        IsUpdate = x.IsUpdate,
                                        IsDelete = x.IsDelete,
                                        IsViewAll = x.IsViewAll,
                                    }).ToListAsync();

                        _OAccount.Features = _Features;

                        _RequisitionConfig = await _HCoreContext.AtAssetRequisitionConfigs.Where(x => x.RoleId == UserDetails.UserRoleId && x.OrganizationId == UserDetails.OrganizationId)
                                             .Select(x => new OAccount.RequisitionConfig
                                             {
                                                 RoleId = x.RoleId,
                                                 RoleKey = x.Role.Guid,
                                                 RoleName = x.Role.Name,
                                                 IsIssueRequest = x.IssueRequest,
                                                 IsIssueApproval = x.IssueApproval,
                                                 IsTransferRequest = x.TransferRequest,
                                                 IsTransferApproval = x.TransferApproval,
                                                 IsReturnRequest = x.ReturnRequest,
                                                 IsReturnApproval = x.ReturnApproval,
                                                 IsMaintenanceRequest = x.MaintenanceRequest,
                                                 IsMaintenanceApproval = x.MaintenanceApproval,
                                                 IsPORequest = x.PoRequest,
                                                 IsPOApproval = x.PoApproval,
                                             }).FirstOrDefaultAsync();
                        if (_RequisitionConfig != null)
                        {
                            if (_RequisitionConfig.IsIssueRequest == 1)
                            {
                                _RequisitionConfig.IssueRequest = true;
                            }
                            else
                            {
                                _RequisitionConfig.IssueRequest = false;
                            }

                            if (_RequisitionConfig.IsIssueApproval == 1)
                            {
                                _RequisitionConfig.IssueApproval = true;
                            }
                            else
                            {
                                _RequisitionConfig.IssueApproval = false;
                            }

                            if (_RequisitionConfig.IsTransferRequest == 1)
                            {
                                _RequisitionConfig.TransferRequest = true;
                            }
                            else
                            {
                                _RequisitionConfig.TransferRequest = false;
                            }

                            if (_RequisitionConfig.IsTransferApproval == 1)
                            {
                                _RequisitionConfig.TransferApproval = true;
                            }
                            else
                            {
                                _RequisitionConfig.TransferApproval = false;
                            }

                            if (_RequisitionConfig.IsReturnRequest == 1)
                            {
                                _RequisitionConfig.ReturnRequest = true;
                            }
                            else
                            {
                                _RequisitionConfig.ReturnRequest = false;
                            }

                            if (_RequisitionConfig.IsReturnApproval == 1)
                            {
                                _RequisitionConfig.ReturnApproval = true;
                            }
                            else
                            {
                                _RequisitionConfig.ReturnApproval = false;
                            }

                            if (_RequisitionConfig.IsMaintenanceRequest == 1)
                            {
                                _RequisitionConfig.MaintenanceRequest = true;
                            }
                            else
                            {
                                _RequisitionConfig.MaintenanceRequest = false;
                            }

                            if (_RequisitionConfig.IsMaintenanceApproval == 1)
                            {
                                _RequisitionConfig.MaintenanceApproval = true;
                            }
                            else
                            {
                                _RequisitionConfig.MaintenanceApproval = false;
                            }

                            if (_RequisitionConfig.IsPORequest == 1)
                            {
                                _RequisitionConfig.PORequest = true;
                            }
                            else
                            {
                                _RequisitionConfig.PORequest = false;
                            }

                            if (_RequisitionConfig.IsPOApproval == 1)
                            {
                                _RequisitionConfig.POApproval = true;
                            }
                            else
                            {
                                _RequisitionConfig.POApproval = false;
                            }

                            _OAccount.RequisitionConfig = _RequisitionConfig;
                        }

                        _AtAccountSession = new AtAccountSession();
                        _AtAccountSession.Guid = UserAccountSessionKey;
                        _AtAccountSession.AccountId = UserDetails.UserAccountId;
                        _AtAccountSession.AppVersionId = _UserReference.AppVersionId;
                        _AtAccountSession.PlatformId = Helpers.Platform.Web;
                        _AtAccountSession.LastActivityDate = HCoreHelper.GetDateTime();
                        _AtAccountSession.LoginDate = HCoreHelper.GetDateTime();
                        _AtAccountSession.IPAddress = _UserReference.RequestIpAddress;
                        _AtAccountSession.Latitude = (decimal?)_UserReference.RequestLatitude;
                        _AtAccountSession.Longitude = (decimal?)_UserReference.RequestLongitude;
                        _AtAccountSession.StatusId = HCoreConstant.HelperStatus.Default.Active;
                        await _HCoreContext.AtAccountSessions.AddAsync(_AtAccountSession);
                        await _HCoreContext.SaveChangesAsync();
                        UserSessionId = _AtAccountSession.Id;

                        _OAccount.AccountKey = UserDetails.UserAccountKey;
                        _OAccount.AccountId = UserDetails.UserAccountId;
                        _OAccount.Key = HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(UserSessionId + "|" + UserAccountSessionKey));

                        var UserAccountD = await _HCoreContext.AtAccounts.Where(x => x.Id == UserDetails.UserAccountId).FirstOrDefaultAsync();
                        if (UserAccountD != null)
                        {
                            UserAccountD.LastLoginDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.SaveChangesAsync();
                        }

                        await _HCoreContext.DisposeAsync();

                        _OAccount.Message = "Login successful";
                        return _OAccount;
                    }
                    else
                    {
                        _OAccount.Message = "Authentication failed";
                        return _OAccount;
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("GetUserProfile", ex);
                _OAccount = new OAccount.UserProfileDetails();
                _OAccount.Message = "Authentication failed. An error occurred while fetching user profile details.";
                return _OAccount;
            }
        }

        internal async Task<OResponse> Login(OAccount.UserAccount.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.UserName) || string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT023", "Username and Password required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? Password = HCoreEncrypt.EncryptHash(_Request.Password.Trim());
                    var UserDetails = await _HCoreContext.AtAccountAuths.Where(x => x.UserName == _Request.UserName.Trim() && x.Password == Password).FirstOrDefaultAsync();
                    if (UserDetails != null)
                    {
                        if (UserDetails.StatusId == HelperStatus.Default.Inactive)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0401", "Authentication failed");
                        }
                        bool _IsInactiveUser = await _HCoreContext.AtAccounts.AnyAsync(x => x.Id == UserDetails.UserId && x.StatusId == HelperStatus.Default.Inactive);
                        if (_IsInactiveUser)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0401", "Authentication failed");
                        }
                        _OAccount = new OAccount.UserProfileDetails();
                        _OAccount = await GetUserProfile(UserDetails.UserId, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OAccount, "AT0200", "Login successful");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT023", "Invalid Username and Password");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("Login", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> Logout(OAccount.UserAccount.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var SessionDetails = await _HCoreContext.AtAccountSessions.Where(x => x.Id == _Request.UserReference.AccountSessionId && x.AccountId == _Request.UserReference.AccountId && x.PlatformId == Helpers.Platform.Web).FirstOrDefaultAsync();
                    if (SessionDetails != null)
                    {
                        SessionDetails.LogoutDate = HCoreHelper.GetDateTime();
                        SessionDetails.StatusId = HCoreConstant.StatusBlocked;
                        await _HCoreContext.SaveChangesAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "HC131", "Log out successful");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "HC132", "Invalid user session");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("Logout", _Exception, _Request.UserReference);
            }
        }

        //internal async Task<OResponse> ForgotPassword(OAccount.UserAccount.Request _Request)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.EmailAddress))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "First name required");
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        return HCoreHelper.LogException("Logout", _Exception, _Request.UserReference);
        //    }
        //}

        internal async Task<OResponse> CreateUserAccount(OAccount.UserAccount.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "First name required");
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Last name required");
                }
                if (string.IsNullOrEmpty(_Request.CompanyName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Company name required");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Email address required");
                }
                if (string.IsNullOrEmpty(_Request.ContactNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Contact number required");
                }
                if (string.IsNullOrEmpty(_Request.UserName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Username required");
                }
                if (string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Password required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? SystemName = HCoreHelper.GenerateSystemName(_Request.CompanyName);

                    bool _IsExists = await _HCoreContext.AtAccounts.AnyAsync(x => x.SystemName == SystemName && x.AccountTypeId == UserAccountType.Organization);
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Company details already exists");
                    }

                    bool _IsExist = await _HCoreContext.AtAccountAuths.AnyAsync(x => x.UserName == _Request.UserName);
                    if (_IsExist)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCU001", "Username already exists");
                    }

                    _Random = new Random();

                    string AccountCode = HCoreHelper.GenerateAccountCode(6);

                    _AtAccount = new AtAccount();
                    _AtAccount.Guid = HCoreHelper.GenerateGuid();
                    _AtAccount.AccountTypeId = UserAccountType.Organization;
                    _AtAccount.SourceId = RegistrationSource.WebApplication;
                    _AtAccount.OwnerId = _Request.UserReference.OrganizationId;
                    _AtAccount.MobileNumber = _Request.ContactNumber;
                    _AtAccount.DisplayName = _Request.CompanyName;
                    _AtAccount.EmailAddress = _Request.EmailAddress;
                    _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                    _AtAccount.CpName = _Request.FirstName + " " + _Request.LastName;
                    _AtAccount.CpEmailAddress = _Request.EmailAddress;
                    _AtAccount.CpMobileNumber = _AtAccount.MobileNumber;
                    _AtAccount.AccountCode = AccountCode;
                    _AtAccount.CreatedById = 1;
                    _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAccount.StatusId = HelperStatus.Default.Active;
                    await _HCoreContext.AtAccounts.AddAsync(_AtAccount);
                    await _HCoreContext.SaveChangesAsync();

                    long? OrganizationId = _AtAccount.Id;
                    AccountCode = HCoreHelper.GenerateAccountCode(6);
                    string? AccessPin = HCoreHelper.GenerateAccountCode(4);

                    _AtAccount = new AtAccount();
                    _AtAccount.Guid = HCoreHelper.GenerateGuid();
                    _AtAccount.AccountTypeId = UserAccountType.User;
                    _AtAccount.RoleId = UserRole.SuperAdmin;
                    _AtAccount.SourceId = RegistrationSource.WebApplication;
                    _AtAccount.OwnerId = OrganizationId;
                    _AtAccount.OrganizationId = OrganizationId;
                    _AtAccount.MobileNumber = _Request.ContactNumber;
                    _AtAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                    _AtAccount.DisplayName = _Request.FirstName + " " + _Request.LastName;
                    _AtAccount.FirstName = _Request.FirstName;
                    _AtAccount.LastName = _Request.LastName;
                    _AtAccount.EmailAddress = _Request.EmailAddress;
                    _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                    _AtAccount.AccountCode = AccountCode;
                    _AtAccount.CreatedById = 1;
                    _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAccount.StatusId = HelperStatus.Default.Active;
                    await _HCoreContext.AtAccounts.AddAsync(_AtAccount);
                    await _HCoreContext.SaveChangesAsync();

                    _AtAccountAuth = new AtAccountAuth();
                    _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _AtAccountAuth.UserId = _AtAccount.Id;
                    _AtAccountAuth.UserName = _Request.UserName;
                    _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                    _AtAccountAuth.CreatedById = 1;
                    await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                    await _HCoreContext.SaveChangesAsync();

                    string? Body = "<div>";
                    Body += "<hr>";
                    Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                    Body += "<label> Please use the below credentials to log-in on asset management web application using given link(URL).</label>";
                    Body += "</div>";
                    Body += "<hr>";
                    Body += "<br>";
                    Body += "<div>Username: - {{UserName}}</div>";
                    Body += "<br>";
                    Body += "<div>Password: - {{Password}}</div>";
                    Body += "<br>";
                    Body += "<div>URL: - <a href=\"{{URL}}\">{{URL}}</a></div>";
                    Body += "<br>";
                    Body += "<hr>";
                    Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                    Body += "<label> Please use the below credentials to log-in on asset management mobile application.</label>";
                    Body += "</div>";
                    Body += "<hr>";
                    Body += "<br>";
                    Body += "<div>Username: - {{MobileNumber}}</div>";
                    Body += "<br>";
                    Body += "<div>Password: - {{AuthPin}}</div>";
                    Body += "</div>";
                    StringBuilder? EmailBody = new StringBuilder(Body);
                    EmailBody.Replace("{{UserName}}", _AtAccountAuth.UserName);
                    EmailBody.Replace("{{Password}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));
                    EmailBody.Replace("{{MobileNumber}}", _AtAccount.MobileNumber);
                    EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(_AtAccount.AccessPin));
                    EmailBody.Replace("{{URL}}", "http://asset-management.rajkamalbarscan.in/");
                    await HCoreHelper.BroadCastEmail(_AtAccount.EmailAddress, "Asset Management Log-In Credentials", EmailBody.ToString(), _Request.UserReference);

                    _OAccount = new OAccount.UserProfileDetails();
                    _OAccount = await GetUserProfile(_AtAccount.Id, _Request.UserReference);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OAccount, "HCV036", "Registration successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CreateUserAccount", _Exception, _Request.UserReference);
            }
        }
    }
}
