﻿using HCore.Framework;
using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore
{
    public class ManageSystemConfigurations
    {
        FrameworkSystemConfigurations? _FrameworkSystemConfigurations;

        public async Task<OResponse> GetDownloads(OList.Request _Request)
        {
            _FrameworkSystemConfigurations = new FrameworkSystemConfigurations();
            return await _FrameworkSystemConfigurations.GetDownloads(_Request);
        }

        public async Task<OResponse> GetSystemLogs(OList.Request _Request)
        {
            _FrameworkSystemConfigurations = new FrameworkSystemConfigurations();
            return await _FrameworkSystemConfigurations.GetSystemLogs(_Request);
        }
    }
}
