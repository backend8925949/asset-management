﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.Object
{
    public class OSystemConfigurations
    {
        public class SystemConfigurations
        {
            public string? AccessToken { get; set; }
            public long? OrganizationId { get; set; }
            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public string? IconUrl { get; set; }
        }

        public class Downloads
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? File { get; set; }
            public string? File1 { get; set; }
            public string? File2 { get; set; }
            public string? File3 { get; set; }
            public string? FileName { get; set; }
            public string? FileExtension { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class SystemLogs
        {
            public long? ReferenceId { get; set; }
            public string? Title { get; set; }
            public string? Host { get; set; }
            public string? Message { get; set; }
            public string? RequestReference { get; set; }
            public string? Data { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
        }
    }
}
