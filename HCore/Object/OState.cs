﻿namespace HCore.Object
{
    public class OState
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
        }
    }
}
