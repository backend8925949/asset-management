﻿
using HCore.Helper;

namespace HCore.Object
{
    public class OAccount
    {
        public class UserAccount
        {
            public class Request
            {
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? CompanyName { get; set; }
                public string? EmailAddress { get; set; }
                public string? ContactNumber { get; set; }
                public string? UserName { get; set; }
                public string? Password { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class UserProfileDetails
        {
            public string? Message { get; set; }
            public string? Key { get; set; }
            public string? AccountKey { get; set; }
            public long? AccountId { get; set; }
            public string? DisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? Name { get; set; }
            public long? OrganizationId { get; set; }
            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public long? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }
            public long? UserRoleId { get; set; }
            public string? UserRoleKey { get; set; }
            public string? UserRoleName { get; set; }
            public List<Features>? Features { get; set; }
            public RequisitionConfig? RequisitionConfig { get; set; }
        }

        public class Features
        {
            public long? FeatureId { get; set; }
            public string? FeatureKey { get; set; }
            public string? FeatureName { get; set; }
            public byte? IsAdd { get; set; }
            public byte? IsUpdate { get; set; }
            public byte? IsDelete { get; set; }
            public byte? IsView { get; set; }
            public byte? IsViewAll { get; set; }
        }

        public class RequisitionConfig
        {
            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }
            public byte? IsIssueRequest { get; set; }
            public byte? IsIssueApproval { get; set; }
            public byte? IsTransferRequest { get; set; }
            public byte? IsTransferApproval { get; set; }
            public byte? IsReturnRequest { get; set; }
            public byte? IsReturnApproval { get; set; }
            public byte? IsMaintenanceRequest { get; set; }
            public byte? IsMaintenanceApproval { get; set; }
            public byte? IsPORequest { get; set; }
            public byte? IsPOApproval { get; set; }
            public bool IssueRequest { get; set; }
            public bool IssueApproval { get; set; }
            public bool TransferRequest { get; set; }
            public bool TransferApproval { get; set; }
            public bool ReturnRequest { get; set; }
            public bool ReturnApproval { get; set; }
            public bool MaintenanceRequest { get; set; }
            public bool MaintenanceApproval { get; set; }
            public bool PORequest { get; set; }
            public bool POApproval { get; set; }
        }
    }
}
