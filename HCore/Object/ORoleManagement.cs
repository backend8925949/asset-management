﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.Object
{
    public class ORoleManagement
    {
        public class RoleFeature
        {
            public class Request
            {
                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }
                public List<Features>? Features { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Features
            {
                public long? RoleFeatureId { get; set; }
                public string? RoleFeatureKey { get; set; }
                public long? FeatureId { get; set; }
                public string? FeatureKey { get; set; }
                public string? FeatureName { get; set; }
                public bool IsAdd { get; set; }
                public bool IsUpdate { get; set; }
                public bool IsDelete { get; set; }
                public bool IsView { get; set; }
                public bool IsViewAll { get; set; }
                public List<SubFeatures>? SubFeatures { get; set; }
            }

            public class SubFeatures
            {
                public long? RoleSubFeatureId { get; set; }
                public string? RoleSubFeatureKey { get; set; }
                public long? SubFeatureId { get; set; }
                public string? SubFeatureKey { get; set; }
                public string? SubFeatureName { get; set; }
                public bool IsAdd { get; set; }
                public bool IsUpdate { get; set; }
                public bool IsDelete { get; set; }
                public bool IsView { get; set; }
                public bool IsTab { get; set; }
                public bool IsViewAll { get; set; }
                public List<SubFeatures>? SubFeature { get; set; }
            }

            public class Details
            {
                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public List<RoleFeatures>? Features { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }

            public class List
            {
                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }
                public long? RoleAccess { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }

            public class RoleFeatures
            {
                public long? FeatureId { get; set; }
                public string? FeatureKey { get; set; }
                public string? FeatureName { get; set; }
                public byte? IsAdd { get; set; }
                public byte? IsUpdate { get; set; }
                public byte? IsDelete { get; set; }
                public byte? IsView { get; set; }
                public byte? IsViewAll { get; set; }
                public List<RoleSubFeatures>? SubFeatures { get; set; }
            }

            public class RoleSubFeatures
            {
                public long? SubFeatureId { get; set; }
                public string? SubFeatureKey { get; set; }
                public string? SubFeatureName { get; set; }
                public byte? IsAdd { get; set; }
                public byte? IsUpdate { get; set; }
                public byte? IsDelete { get; set; }
                public byte? IsView { get; set; }
                public byte? IsTab { get; set; }
                public byte? IsViewAll { get; set; }
                public List<RoleSubFeatures>? SubFeature { get; set; }
            }
        }

        public class Feature
        {
            public class Request
            {
                public long? FeatureId { get; set; }
                public string? FeatureKey { get; set; }
                public string? FeatureName { get; set; }
                public string? FeatureType { get; set; }
                public string? ParentKey { get; set; }
                public string? StatusCode { get; set;}
                public OUserReference? UserReference { get; set; }
            }

            public class List
            {
                public long? FeatureId { get; set; }
                public string? FeatureKey { get; set; }
                public string? FeatureName { get; set; }
                public int? FeatureTypeId { get; set; }
                public string? FeatureType { get; set; }
                public string? FeatureTypeName { get; set; }
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }

            public class  Details
            {
                public long? FeatureId { get; set; }
                public string? FeatureKey { get; set; }
                public string? FeatureName { get; set; }
                public int? FeatureTypeId { get; set; }
                public string? FeatureType { get; set; }
                public string? FeatureTypeName { get; set; }
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Features
        {
            public long? FeatureId { get; set; }
            public string? FeatureKey { get; set; }
            public string? FeatureName { get; set; }
            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }
            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentName { get; set; }
            public List<SubFeature>? SubFeatures { get; set; }
        }

        public class SubFeature
        {
            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }
            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentName { get; set; }
            public long? SubFeatureId { get; set; }
            public string? SubFeatureKey { get; set; }
            public string? SubFeatureName { get; set; }
            public List<SubFeature>? SubFeatures { get; set; }
        }
    }
}
