﻿using HCore.Framework;
using HCore.Helper;

namespace HCore
{
    public class ManageCity
    {
        FrameworkCity? _FrameworkCity;

        public async Task<OResponse> GetCities(OList.Request _Request)
        {
            _FrameworkCity = new FrameworkCity();
            return await _FrameworkCity.GetCities(_Request);
        }
    }
}
