﻿using HCore.Framework;
using HCore.Object;
using HCore.Helper;
using HCore.App.Framework;

namespace HCore
{
    public class ManageUserAccount
    {
        FrameworkUserAccount? _FrameworkUserAccount;
        FrameworkAppUserAccount? _FrameworkAppUserAccount;

        public async Task<OResponse> Login(OAccount.UserAccount.Request _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.Login(_Request);
        }

        public async Task<OResponse> Logout(OAccount.UserAccount.Request _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.Logout(_Request);
        }

        public async Task<OResponse> AppUserLogin(OAccount.UserAccount.Request _Request)
        {
            _FrameworkAppUserAccount = new FrameworkAppUserAccount();
            return await _FrameworkAppUserAccount.AppUserLogin(_Request);
        }

        public async Task<OResponse> AppUserLogout(OAccount.UserAccount.Request _Request)
        {
            _FrameworkAppUserAccount = new FrameworkAppUserAccount();
            return await _FrameworkAppUserAccount.AppUserLogout(_Request);
        }

        public async Task<OResponse> CreateUserAccount(OAccount.UserAccount.Request _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.CreateUserAccount(_Request);
        }
    }
}
