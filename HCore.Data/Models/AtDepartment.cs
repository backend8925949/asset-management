﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtDepartment
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OrganizationId { get; set; }

    public long? DivisionId { get; set; }

    public string DepartmentName { get; set; } = null!;

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

   public virtual ICollection<AtAccount> AtAccounts { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAddress> AtAddresses { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetails { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetails { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtLocationAccount> AtLocationAccounts { get; set; } = new List<AtLocationAccount>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtDivision? Division { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount Organization { get; set; } = null!;

    public virtual AtCore? Status { get; set; }
}
