﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtSubCategory
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public int CategoryId { get; set; }

    public string SubCategoryName { get; set; } = null!;

    public int? StatusId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

   public virtual ICollection<AtAssetRegister> AtAssetRegisters { get; set; } = new List<AtAssetRegister>();

    public virtual AtCategory Category { get; set; } = null!;

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }
}
