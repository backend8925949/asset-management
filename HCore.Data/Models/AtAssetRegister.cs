﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetRegister
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int AssetTypeId { get; set; }

    public int AssetClassId { get; set; }

    public int AssetCategoryId { get; set; }

    public int AssetSubCategoryId { get; set; }

    public string? AssetBarcode { get; set; }

    public string? RfId { get; set; }

    public string? EpcId { get; set; }

    public string AssetSerialNumber { get; set; } = null!;

    public int? SerialNumber { get; set; }

    public int? TotalQuantity { get; set; }

    public string AssetName { get; set; } = null!;

    public string? ExAssetId { get; set; }

    public string? AssetMake { get; set; }

    public string? AssetModel { get; set; }

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? OrganizationId { get; set; }

    public int? RemainingQuantity { get; set; }

    public decimal? SalvageValue { get; set; }

    public virtual AtCategory AssetCategory { get; set; } = null!;

    public virtual AtClass AssetClass { get; set; } = null!;

    public virtual AtSubCategory AssetSubCategory { get; set; } = null!;

    public virtual AtCore AssetType { get; set; } = null!;

   public virtual ICollection<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurations { get; set; } = new List<AtAssetDepreciationConfiguration>();

   public virtual ICollection<AtAssetDepreciation> AtAssetDepreciations { get; set; } = new List<AtAssetDepreciation>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetails { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetLocation> AtAssetLocations { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenances { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetRequisition> AtAssetRequisitions { get; set; } = new List<AtAssetRequisition>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetails { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetails { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetVendorDetail> AtAssetVendorDetails { get; set; } = new List<AtAssetVendorDetail>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtCore? Status { get; set; }
}
