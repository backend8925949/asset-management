﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetRequisition
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AssetId { get; set; }

    public byte? IsDivHeadApproved { get; set; }

    public byte? IsDeptHeadApproved { get; set; }

    public byte? IsHrApproved { get; set; }

    public byte? IsAdminApproved { get; set; }

    public byte? IsSuperAdminApproved { get; set; }

    public long? CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public int? ApprovalTypeId { get; set; }

    public virtual AtCore? ApprovalType { get; set; }

    public virtual AtAssetRegister Asset { get; set; } = null!;

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }
}
