﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreRole
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? Name { get; set; }

    public string? SystemName { get; set; }

    public int StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifiedById { get; set; }

    public long? OrganizationId { get; set; }

   public virtual ICollection<AtAccount> AtAccounts { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAssetRequisitionConfig> AtAssetRequisitionConfigs { get; set; } = new List<AtAssetRequisitionConfig>();

   public virtual ICollection<AtCoreRoleFeature> AtCoreRoleFeatures { get; set; } = new List<AtCoreRoleFeature>();

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
