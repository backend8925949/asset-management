﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetAmcDetail
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long VendorId { get; set; }

    public long VendorDetailsId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public decimal? Price { get; set; }

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtAccount Vendor { get; set; } = null!;

    public virtual AtAssetVendorDetail VendorDetails { get; set; } = null!;
}
