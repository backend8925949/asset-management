﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreStorage
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public string? Files { get; set; }

    public string? Files1 { get; set; }

    public string? Files2 { get; set; }

    public string? Files3 { get; set; }

    public string FileName { get; set; } = null!;

    public string FilePath { get; set; } = null!;

    public string FileExtension { get; set; } = null!;

    public decimal? FileSize { get; set; }

    public int StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public virtual AtAccount? Account { get; set; }

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
