﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreAccountDevice
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AccountId { get; set; }

    public string SerialNumber { get; set; } = null!;

    public long AppId { get; set; }

    public long AppVersionId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public int StatusId { get; set; }

    public virtual AtAccount Account { get; set; } = null!;

    public virtual AtCoreApp App { get; set; } = null!;

    public virtual AtCoreAppVersion AppVersion { get; set; } = null!;

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
