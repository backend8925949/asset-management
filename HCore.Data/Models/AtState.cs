﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtState
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int CountryId { get; set; }

    public string StateName { get; set; } = null!;

    public int? StatusId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

   public virtual ICollection<AtAddress> AtAddresses { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtCity> AtCities { get; set; } = new List<AtCity>();

    public virtual AtCountry Country { get; set; } = null!;

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }
}
