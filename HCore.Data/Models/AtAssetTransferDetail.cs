﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetTransferDetail
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AssetId { get; set; }

    public long AssetIssueId { get; set; }

    public long OrganizationId { get; set; }

    public long DivisionId { get; set; }

    public long? DepartmentId { get; set; }

    public long LocationId { get; set; }

    public long SubLocationId { get; set; }

    public long? EmployeeId { get; set; }

    public string? Remark { get; set; }

    public int? Quantity { get; set; }

    public string? OsName { get; set; }

    public string? HostName { get; set; }

    public string? IpAddress { get; set; }

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public int? SourceId { get; set; }

    public long? ApprovedById { get; set; }

    public DateTime? ApprovedDate { get; set; }

    public string? ReceiptNo { get; set; }

    public DateTime? ReceiptDate { get; set; }

    public string? Comment { get; set; }

    public virtual AtAccount? ApprovedBy { get; set; }

    public virtual AtAssetRegister Asset { get; set; } = null!;

    public virtual AtAssetIssueDetail AssetIssue { get; set; } = null!;

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtDepartment? Department { get; set; }

    public virtual AtDivision Division { get; set; } = null!;

    public virtual AtAccount? Employee { get; set; }

    public virtual AtLocation Location { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount Organization { get; set; } = null!;

    public virtual AtCore? Source { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtLocation SubLocation { get; set; } = null!;
}
