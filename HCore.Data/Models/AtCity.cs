﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCity
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long StateId { get; set; }

    public string CityName { get; set; } = null!;

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

   public virtual ICollection<AtAddress> AtAddresses { get; set; } = new List<AtAddress>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtState State { get; set; } = null!;

    public virtual AtCore? Status { get; set; }
}
