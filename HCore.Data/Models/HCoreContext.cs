﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace HCore.Data.Models;

public partial class HCoreContext : DbContext
{
    private readonly string ConnectionString;
    public HCoreContext()
    {
        this.ConnectionString = Helper.HostHelper.Host;
    }
    public HCoreContext(string ConnectionString) : base()
    {
        this.ConnectionString = ConnectionString;
    }
    public HCoreContext(DbContextOptions<HCoreContext> options) : base(options)
    {

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
    }

    public virtual DbSet<AtAccount> AtAccounts { get; set; }

    public virtual DbSet<AtAccountAuth> AtAccountAuths { get; set; }

    public virtual DbSet<AtAccountEmailConfiguration> AtAccountEmailConfigurations { get; set; }

    public virtual DbSet<AtAccountSession> AtAccountSessions { get; set; }

    public virtual DbSet<AtAddress> AtAddresses { get; set; }

    public virtual DbSet<AtAssetAmcDetail> AtAssetAmcDetails { get; set; }

    public virtual DbSet<AtAssetDepreciation> AtAssetDepreciations { get; set; }

    public virtual DbSet<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurations { get; set; }

    public virtual DbSet<AtAssetInsuranceDetail> AtAssetInsuranceDetails { get; set; }

    public virtual DbSet<AtAssetIssueDetail> AtAssetIssueDetails { get; set; }

    public virtual DbSet<AtAssetLocation> AtAssetLocations { get; set; }

    public virtual DbSet<AtAssetMaintenance> AtAssetMaintenances { get; set; }

    public virtual DbSet<AtAssetRegister> AtAssetRegisters { get; set; }

    public virtual DbSet<AtAssetRequisition> AtAssetRequisitions { get; set; }

    public virtual DbSet<AtAssetRequisitionConfig> AtAssetRequisitionConfigs { get; set; }

    public virtual DbSet<AtAssetReturnDetail> AtAssetReturnDetails { get; set; }

    public virtual DbSet<AtAssetTransferDetail> AtAssetTransferDetails { get; set; }

    public virtual DbSet<AtAssetVendorDetail> AtAssetVendorDetails { get; set; }

    public virtual DbSet<AtCategory> AtCategories { get; set; }

    public virtual DbSet<AtCity> AtCities { get; set; }

    public virtual DbSet<AtClass> AtClasses { get; set; }

    public virtual DbSet<AtCore> AtCores { get; set; }

    public virtual DbSet<AtCoreAccountDevice> AtCoreAccountDevices { get; set; }

    public virtual DbSet<AtCoreApp> AtCoreApps { get; set; }

    public virtual DbSet<AtCoreAppVersion> AtCoreAppVersions { get; set; }

    public virtual DbSet<AtCoreFeature> AtCoreFeatures { get; set; }

    public virtual DbSet<AtCoreRole> AtCoreRoles { get; set; }

    public virtual DbSet<AtCoreRoleFeature> AtCoreRoleFeatures { get; set; }

    public virtual DbSet<AtCoreStorage> AtCoreStorages { get; set; }

    public virtual DbSet<AtCountry> AtCountries { get; set; }

    public virtual DbSet<AtDepartment> AtDepartments { get; set; }

    public virtual DbSet<AtDepreciationConfiguration> AtDepreciationConfigurations { get; set; }

    public virtual DbSet<AtDivision> AtDivisions { get; set; }

    public virtual DbSet<AtLocation> AtLocations { get; set; }

    public virtual DbSet<AtLocationAccount> AtLocationAccounts { get; set; }

    public virtual DbSet<AtPrinterDetail> AtPrinterDetails { get; set; }

    public virtual DbSet<AtState> AtStates { get; set; }

    public virtual DbSet<AtSubCategory> AtSubCategories { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AtAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ACCOUNT_ID");

            entity.ToTable("at_account");

            entity.HasIndex(e => e.Guid, "UQ_AT_ACCOUNT").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccessPin)
                .HasMaxLength(64)
                .HasColumnName("access_pin");
            entity.Property(e => e.AccountCode)
                .HasMaxLength(64)
                .HasColumnName("account_code");
            entity.Property(e => e.AccountTypeId).HasColumnName("account_type_id");
            entity.Property(e => e.Address)
                .HasMaxLength(2024)
                .HasColumnName("address");
            entity.Property(e => e.ContactNumber)
                .HasMaxLength(64)
                .HasColumnName("contact_number");
            entity.Property(e => e.CpEmailAddress)
                .HasMaxLength(256)
                .HasColumnName("cp_email_address");
            entity.Property(e => e.CpMobileNumber)
                .HasMaxLength(64)
                .HasColumnName("cp_mobile_number");
            entity.Property(e => e.CpName)
                .HasMaxLength(256)
                .HasColumnName("cp_name");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepartmentId).HasColumnName("department_id");
            entity.Property(e => e.Designation)
                .HasMaxLength(256)
                .HasColumnName("designation");
            entity.Property(e => e.DisplayName)
                .HasMaxLength(256)
                .HasColumnName("display_name");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(256)
                .HasColumnName("email_address");
            entity.Property(e => e.FirstName)
                .HasMaxLength(256)
                .HasColumnName("first_name");
            entity.Property(e => e.GstNumber)
                .HasMaxLength(256)
                .HasColumnName("gst_number");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.LastActivityDate)
                .HasColumnType("datetime")
                .HasColumnName("last_activity_date");
            entity.Property(e => e.LastLoginDate)
                .HasColumnType("datetime")
                .HasColumnName("last_login_date");
            entity.Property(e => e.LastName)
                .HasMaxLength(256)
                .HasColumnName("last_name");
            entity.Property(e => e.MobileNumber)
                .HasMaxLength(64)
                .HasColumnName("mobile_number");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.OwnerId).HasColumnName("owner_id");
            entity.Property(e => e.PanNumber)
                .HasMaxLength(256)
                .HasColumnName("pan_number");
            entity.Property(e => e.RoleId).HasColumnName("role_id");
            entity.Property(e => e.SourceId).HasColumnName("source_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubOwnerId).HasColumnName("sub_owner_id");
            entity.Property(e => e.SystemName)
                .HasMaxLength(256)
                .HasColumnName("system_name");

            entity.HasOne(d => d.AccountType).WithMany(p => p.AtAccountAccountTypes)
                .HasForeignKey(d => d.AccountTypeId)
                .HasConstraintName("FK_ACC_ACCTYPE_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.InverseCreatedBy)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ACC_CREATEDBY");

            entity.HasOne(d => d.Department).WithMany(p => p.AtAccounts)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("FK_ACC_DEPT_ID");

            entity.HasOne(d => d.Division).WithMany(p => p.AtAccounts)
                .HasForeignKey(d => d.DivisionId)
                .HasConstraintName("FK_ACC_DIV_ID");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.InverseModifiedBy)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ACC_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.InverseOrganization)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_ACC_ORG_ID");

            entity.HasOne(d => d.Owner).WithMany(p => p.InverseOwner)
                .HasForeignKey(d => d.OwnerId)
                .HasConstraintName("FK_ACC_OWNER_ID");

            entity.HasOne(d => d.Role).WithMany(p => p.AtAccounts)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("FK_ACC_ROLE_ID");

            entity.HasOne(d => d.Source).WithMany(p => p.AtAccountSources)
                .HasForeignKey(d => d.SourceId)
                .HasConstraintName("FK_ACC_SOURCE_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAccountStatuses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ACC_STATUS");

            entity.HasOne(d => d.SubOwner).WithMany(p => p.InverseSubOwner)
                .HasForeignKey(d => d.SubOwnerId)
                .HasConstraintName("FK_ACC_SUBOWNER_ID");
        });

        modelBuilder.Entity<AtAccountAuth>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_ACC_AUTH_ID");

            entity.ToTable("at_account_auth");

            entity.HasIndex(e => e.Guid, "UQ_ACC_AUTH").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Password)
                .HasMaxLength(24)
                .HasColumnName("password");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.UserId).HasColumnName("user_id");
            entity.Property(e => e.UserName)
                .HasMaxLength(64)
                .HasColumnName("user_name");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAccountAuthCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ACC_AUTH_CREATED_BY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAccountAuthModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ACC_AUTH_MODIFIED_BY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAccountAuths)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ACC_AUTH_STATUS");

            entity.HasOne(d => d.User).WithMany(p => p.AtAccountAuthUsers)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ACC_AUTH_USER_ID");
        });

        modelBuilder.Entity<AtAccountEmailConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_accou__3213E83F8ED04865");

            entity.ToTable("at_account_email_configuration");

            entity.HasIndex(e => e.Guid, "UQ__at_accou__497F6CB59E65E466").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(256)
                .HasColumnName("email_address");
            entity.Property(e => e.Guid)
                .HasMaxLength(45)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Password)
                .HasMaxLength(256)
                .HasColumnName("password");
            entity.Property(e => e.Port).HasColumnName("port");
            entity.Property(e => e.SmtpServer)
                .HasMaxLength(256)
                .HasColumnName("smtp_server");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtAccountEmailConfigurationAccounts)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("ACC_EMAIL_CONFIG_ACC");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAccountEmailConfigurationCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("ACC_EMAIL_CONFIG_CREATED_BY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAccountEmailConfigurationModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("ACC_EMAIL_CONFIG_MODIFIED_BY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAccountEmailConfigurations)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("ACC_EMAIL_CONFIG_STATUS");
        });

        modelBuilder.Entity<AtAccountSession>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_accou__3213E83F81C6D01F");

            entity.ToTable("at_account_session");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.AppVersionId).HasColumnName("app_version_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.IPAddress)
                .HasMaxLength(50)
                .HasColumnName("iP_address");
            entity.Property(e => e.LastActivityDate)
                .HasColumnType("datetime")
                .HasColumnName("last_activity_date");
            entity.Property(e => e.Latitude)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("latitude");
            entity.Property(e => e.LoginDate)
                .HasColumnType("datetime")
                .HasColumnName("login_date");
            entity.Property(e => e.LogoutDate)
                .HasColumnType("datetime")
                .HasColumnName("logout_date");
            entity.Property(e => e.Longitude)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("longitude");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("modify_date");
            entity.Property(e => e.ModifybyId).HasColumnName("modifyby_id");
            entity.Property(e => e.NotificationUrl)
                .HasMaxLength(2048)
                .HasColumnName("notification_url");
            entity.Property(e => e.PlatformId).HasColumnName("platform_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtAccountSessionAccounts)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_HCSUS_UserAccountId_HCUserAccount_Id");

            entity.HasOne(d => d.Modifyby).WithMany(p => p.AtAccountSessionModifybies)
                .HasForeignKey(d => d.ModifybyId)
                .HasConstraintName("FK_HCSUS_ModifyById_UserAccount_Id");

            entity.HasOne(d => d.Platform).WithMany(p => p.AtAccountSessionPlatforms)
                .HasForeignKey(d => d.PlatformId)
                .HasConstraintName("FK_HCSUS_PlatformId_Core");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAccountSessionStatuses)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HCSUS_StatusId_CoreHelper_Id");
        });

        modelBuilder.Entity<AtAddress>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ADDRESS_ID");

            entity.ToTable("at_address");

            entity.HasIndex(e => e.Guid, "UQ_AT_ADDRESS").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.Address)
                .HasMaxLength(2024)
                .HasColumnName("address");
            entity.Property(e => e.CityId).HasColumnName("city_id");
            entity.Property(e => e.CountryId).HasColumnName("country_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepartmentId).HasColumnName("department_id");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.EmailAddress)
                .HasMaxLength(64)
                .HasColumnName("email_address");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Latitude)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("latitude");
            entity.Property(e => e.Longitude)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("longitude");
            entity.Property(e => e.MobileNumber)
                .HasMaxLength(64)
                .HasColumnName("mobile_number");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.ParentId).HasColumnName("parent_id");
            entity.Property(e => e.StateId).HasColumnName("state_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubParentId).HasColumnName("sub_parent_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtAddressAccounts)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ADDRESS_ACC_ID");

            entity.HasOne(d => d.City).WithMany(p => p.AtAddresses)
                .HasForeignKey(d => d.CityId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ADDRESS_CITY_ID");

            entity.HasOne(d => d.Country).WithMany(p => p.AtAddresses)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ADDRESS_COUNTRY_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAddressCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ADDRESS_CREATEDBY");

            entity.HasOne(d => d.Department).WithMany(p => p.AtAddresses)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("FK_ADDRESS_DEPT_ID");

            entity.HasOne(d => d.Division).WithMany(p => p.AtAddresses)
                .HasForeignKey(d => d.DivisionId)
                .HasConstraintName("FK_ADDRESS_DIV_ID");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAddressModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ADDRESS_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAddressOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_ADDRESS_ORG_ID");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_ADDRESS_PARENT_ID");

            entity.HasOne(d => d.State).WithMany(p => p.AtAddresses)
                .HasForeignKey(d => d.StateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ADDRESS_STATE_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAddresses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ADDRESS_STATUS");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_ADDRESS_SUBPARENT_ID");
        });

        modelBuilder.Entity<AtAssetAmcDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_AMC_ID");

            entity.ToTable("at_asset_amc_details");

            entity.HasIndex(e => e.Guid, "UQ_AT_ASSET_AMC").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.EndDate)
                .HasColumnType("datetime")
                .HasColumnName("end_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Price)
                .HasColumnType("money")
                .HasColumnName("price");
            entity.Property(e => e.StartDate)
                .HasColumnType("datetime")
                .HasColumnName("start_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.VendorDetailsId).HasColumnName("vendor_details_id");
            entity.Property(e => e.VendorId).HasColumnName("vendor_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetAmcDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_AMC_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetAmcDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_AMC_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetAmcDetails)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_AMC_STATUS");

            entity.HasOne(d => d.VendorDetails).WithMany(p => p.AtAssetAmcDetails)
                .HasForeignKey(d => d.VendorDetailsId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_AMC_VENDOR_DETAILS");

            entity.HasOne(d => d.Vendor).WithMany(p => p.AtAssetAmcDetailVendors)
                .HasForeignKey(d => d.VendorId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_AMC_VENDOR");
        });

        modelBuilder.Entity<AtAssetDepreciation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83FB38EAF2C");

            entity.ToTable("at_asset_depreciation");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB571BC3277").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccumulatedDepreciation)
                .HasColumnType("money")
                .HasColumnName("accumulated_depreciation");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.AssetValue)
                .HasColumnType("money")
                .HasColumnName("asset_value");
            entity.Property(e => e.BookValue)
                .HasColumnType("money")
                .HasColumnName("book_value");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");
            entity.Property(e => e.DepreciationExpense)
                .HasColumnType("money")
                .HasColumnName("depreciation_expense");
            entity.Property(e => e.DepreciationTypeId).HasColumnName("depreciation_type_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Month).HasColumnName("month");
            entity.Property(e => e.OpeningBookValue)
                .HasColumnType("money")
                .HasColumnName("opening_book_value");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.Year).HasColumnName("year");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetDepreciations)
                .HasForeignKey(d => d.AssetId)
                .HasConstraintName("FK_ASSET_DEPR_ASSET");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetDepreciationCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_ASSET_DEP_CREATED_BY");

            entity.HasOne(d => d.DepreciationType).WithMany(p => p.AtAssetDepreciationDepreciationTypes)
                .HasForeignKey(d => d.DepreciationTypeId)
                .HasConstraintName("FK_ASSET_DEP_TYPE");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetDepreciationModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_DEP_MODIFIED_BY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetDepreciationOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_DEP_ORG");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetDepreciationStatuses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_DEP_STATUS");
        });

        modelBuilder.Entity<AtAssetDepreciationConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83FB058E562");

            entity.ToTable("at_asset_depreciation_configuration");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB593728491").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccumulatedDepreciation)
                .HasColumnType("money")
                .HasColumnName("accumulated_depreciation");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.AssetPurchaseDate)
                .HasColumnType("datetime")
                .HasColumnName("asset_purchase_date");
            entity.Property(e => e.BookValue)
                .HasColumnType("money")
                .HasColumnName("book_value");
            entity.Property(e => e.CostOfAcquisition)
                .HasColumnType("money")
                .HasColumnName("cost_of_acquisition");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepreciationExpense)
                .HasColumnType("money")
                .HasColumnName("depreciation_expense");
            entity.Property(e => e.DepreciationMethodId).HasColumnName("depreciation_method_id");
            entity.Property(e => e.DepreciationRate)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("depreciation_rate");
            entity.Property(e => e.DepreciationStartDate)
                .HasColumnType("datetime")
                .HasColumnName("depreciation_start_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.NetBookValue)
                .HasColumnType("money")
                .HasColumnName("net_book_value");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.SalvageValue)
                .HasColumnType("money")
                .HasColumnName("salvage_value");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.UsefulLife).HasColumnName("useful_life");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetDepreciationConfigurations)
                .HasForeignKey(d => d.AssetId)
                .HasConstraintName("FK_ASSET_DEPR_CONFIG_ASSET");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetDepreciationConfigurationCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_ASSET_DEPR_CONFIG_CREATED_BY");

            entity.HasOne(d => d.DepreciationMethod).WithMany(p => p.AtAssetDepreciationConfigurationDepreciationMethods)
                .HasForeignKey(d => d.DepreciationMethodId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_DEPR_CONFIG_METHOD");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetDepreciationConfigurationModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_DEPR_CONFIG_MODIFIED_BY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetDepreciationConfigurationOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_DEPR_CONFIG_ORG");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetDepreciationConfigurationStatuses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_DEPR_CONFIG_STATUS");
        });

        modelBuilder.Entity<AtAssetInsuranceDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_INSURANCE_ID");

            entity.ToTable("at_asset_insurance_details");

            entity.HasIndex(e => e.Guid, "UQ_AT_ASSET_INSURANCE").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.EndDate)
                .HasColumnType("datetime")
                .HasColumnName("end_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Price)
                .HasColumnType("money")
                .HasColumnName("price");
            entity.Property(e => e.StartDate)
                .HasColumnType("datetime")
                .HasColumnName("start_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.VendorDetailsId).HasColumnName("vendor_details_id");
            entity.Property(e => e.VendorId).HasColumnName("vendor_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetInsuranceDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_INSURANCE_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetInsuranceDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_INSURANCE_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetInsuranceDetails)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_INSURANCE_STATUS");

            entity.HasOne(d => d.VendorDetails).WithMany(p => p.AtAssetInsuranceDetails)
                .HasForeignKey(d => d.VendorDetailsId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_INSURANCE_VENDOR_DETAILS");

            entity.HasOne(d => d.Vendor).WithMany(p => p.AtAssetInsuranceDetailVendors)
                .HasForeignKey(d => d.VendorId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_INSURANCE_VENDOR");
        });

        modelBuilder.Entity<AtAssetIssueDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_ISSUE_ID");

            entity.ToTable("at_asset_issue_details");

            entity.HasIndex(e => new { e.Guid, e.TransferReference }, "UQ_AT_ASSET_ISSUE").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ApprovedById).HasColumnName("approved_by_id");
            entity.Property(e => e.ApprovedDate)
                .HasColumnType("datetime")
                .HasColumnName("approved_date");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepartmentId).HasColumnName("department_id");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.EmployeeId).HasColumnName("employee_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.HostName)
                .HasMaxLength(256)
                .HasColumnName("host_name");
            entity.Property(e => e.IpAddress)
                .HasMaxLength(256)
                .HasColumnName("ip_address");
            entity.Property(e => e.IsTransfered).HasColumnName("is_transfered");
            entity.Property(e => e.IssueReceiptDate)
                .HasColumnType("datetime")
                .HasColumnName("issue_receipt_date");
            entity.Property(e => e.IssueReceiptNo)
                .HasMaxLength(64)
                .HasColumnName("issue_receipt_no");
            entity.Property(e => e.LocationId).HasColumnName("location_id");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.OsName)
                .HasMaxLength(256)
                .HasColumnName("os_name");
            entity.Property(e => e.Quantity).HasColumnName("quantity");
            entity.Property(e => e.Remark)
                .HasMaxLength(1024)
                .HasColumnName("remark");
            entity.Property(e => e.SourceId).HasColumnName("source_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubLocationId).HasColumnName("sub_location_id");
            entity.Property(e => e.TransferReceiptDate)
                .HasColumnType("datetime")
                .HasColumnName("transfer_receipt_date");
            entity.Property(e => e.TransferReceiptNo)
                .HasMaxLength(64)
                .HasColumnName("transfer_receipt_no");
            entity.Property(e => e.TransferReference)
                .HasMaxLength(64)
                .HasColumnName("transfer_reference");
            entity.Property(e => e.TransferedBy).HasColumnName("transfered_by");
            entity.Property(e => e.TransferedDate)
                .HasColumnType("datetime")
                .HasColumnName("transfered_date");
            entity.Property(e => e.TransferedTo).HasColumnName("transfered_to");

            entity.HasOne(d => d.ApprovedBy).WithMany(p => p.AtAssetIssueDetailApprovedBies)
                .HasForeignKey(d => d.ApprovedById)
                .HasConstraintName("FK_ASSET_ISSUE_APPROVED_BY");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetIssueDetails)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ISSUE_ASSET_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetIssueDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ISSUE_CREATEDBY");

            entity.HasOne(d => d.Department).WithMany(p => p.AtAssetIssueDetails)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("FK_ASSET_ISSUE_DEPT_ID");

            entity.HasOne(d => d.Division).WithMany(p => p.AtAssetIssueDetails)
                .HasForeignKey(d => d.DivisionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ISSUE_DIV_ID");

            entity.HasOne(d => d.Employee).WithMany(p => p.AtAssetIssueDetailEmployees)
                .HasForeignKey(d => d.EmployeeId)
                .HasConstraintName("FK_ASSET_ISSUE_EMP_ID");

            entity.HasOne(d => d.Location).WithMany(p => p.AtAssetIssueDetailLocations)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ISSUE_LOC_ID");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetIssueDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_ISSUE_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetIssueDetailOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ISSUE_ORG_ID");

            entity.HasOne(d => d.Source).WithMany(p => p.AtAssetIssueDetailSources)
                .HasForeignKey(d => d.SourceId)
                .HasConstraintName("FK_ASSET_ISSUE_SOURCE_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetIssueDetailStatuses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_ISSUE_STATUS");

            entity.HasOne(d => d.SubLocation).WithMany(p => p.AtAssetIssueDetailSubLocations)
                .HasForeignKey(d => d.SubLocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ISSUE_SUBLOC_ID");

            entity.HasOne(d => d.TransferedByNavigation).WithMany(p => p.AtAssetIssueDetailTransferedByNavigations)
                .HasForeignKey(d => d.TransferedBy)
                .HasConstraintName("FK_ASSET_ISSUE_TRANSFER_BY_ID");

            entity.HasOne(d => d.TransferedToNavigation).WithMany(p => p.AtAssetIssueDetailTransferedToNavigations)
                .HasForeignKey(d => d.TransferedTo)
                .HasConstraintName("FK_ASSET_ISSUE_TRANSFER_TO_ID");
        });

        modelBuilder.Entity<AtAssetLocation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F555C1A06");

            entity.ToTable("at_asset_location");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5A5B9BB71").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.LocationId).HasColumnName("location_id");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubLocationId).HasColumnName("sub_location_id");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetLocations)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Asset_Location_Asset");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetLocationCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_Asset_Location_Created_By");

            entity.HasOne(d => d.Division).WithMany(p => p.AtAssetLocations)
                .HasForeignKey(d => d.DivisionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Asset_Location_Division");

            entity.HasOne(d => d.Location).WithMany(p => p.AtAssetLocationLocations)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Asset_Location_Location");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetLocationModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_Asset_Location_Modified_By");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetLocations)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Asset_Location_Status");

            entity.HasOne(d => d.SubLocation).WithMany(p => p.AtAssetLocationSubLocations)
                .HasForeignKey(d => d.SubLocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Asset_Location_Sub_Location");
        });

        modelBuilder.Entity<AtAssetMaintenance>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83FE03391B8");

            entity.ToTable("at_asset_maintenance");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5F0463F9B").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Amount)
                .HasColumnType("money")
                .HasColumnName("amount");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Description)
                .HasMaxLength(2048)
                .HasColumnName("description");
            entity.Property(e => e.ExpectedDate)
                .HasColumnType("datetime")
                .HasColumnName("expected_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.MaintenanceType).HasColumnName("maintenance_type");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.ReceiptDate)
                .HasColumnType("datetime")
                .HasColumnName("receipt_date");
            entity.Property(e => e.ReceiptNo)
                .HasMaxLength(16)
                .HasColumnName("receipt_no");
            entity.Property(e => e.ReceivedDate)
                .HasColumnType("datetime")
                .HasColumnName("received_date");
            entity.Property(e => e.Remark)
                .HasMaxLength(2048)
                .HasColumnName("remark");
            entity.Property(e => e.RequestDate)
                .HasColumnType("datetime")
                .HasColumnName("request_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.VendorId).HasColumnName("vendor_id");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetMaintenances)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MAINTENANCE_ASSET");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetMaintenanceCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MAINTENANCE_CREATED_BY");

            entity.HasOne(d => d.MaintenanceTypeNavigation).WithMany(p => p.AtAssetMaintenanceMaintenanceTypeNavigations)
                .HasForeignKey(d => d.MaintenanceType)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MAINTENANCE_TYPE");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetMaintenanceModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_MAINTENANCE_MODIFIED_BY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetMaintenanceOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_MAINTENANCE_ORG");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetMaintenanceStatuses)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MAINTENANCE_STATUS");

            entity.HasOne(d => d.Vendor).WithMany(p => p.AtAssetMaintenanceVendors)
                .HasForeignKey(d => d.VendorId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MAINTENANCE_VENDOR");
        });

        modelBuilder.Entity<AtAssetRegister>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_REGISTER_ID");

            entity.ToTable("at_asset_register");

            entity.HasIndex(e => new { e.Guid, e.AssetBarcode }, "UQ_AT_ASSET_REGISTER").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AssetBarcode)
                .HasMaxLength(64)
                .HasColumnName("asset_barcode");
            entity.Property(e => e.AssetCategoryId).HasColumnName("asset_category_id");
            entity.Property(e => e.AssetClassId).HasColumnName("asset_class_id");
            entity.Property(e => e.AssetMake)
                .HasMaxLength(256)
                .HasColumnName("asset_make");
            entity.Property(e => e.AssetModel)
                .HasMaxLength(256)
                .HasColumnName("asset_model");
            entity.Property(e => e.AssetName)
                .HasMaxLength(256)
                .HasColumnName("asset_name");
            entity.Property(e => e.AssetSerialNumber)
                .HasMaxLength(50)
                .HasColumnName("asset_serial_number");
            entity.Property(e => e.AssetSubCategoryId).HasColumnName("asset_sub_category_id");
            entity.Property(e => e.AssetTypeId).HasColumnName("asset_type_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.EpcId)
                .HasMaxLength(64)
                .HasColumnName("epc_id");
            entity.Property(e => e.ExAssetId)
                .HasMaxLength(64)
                .HasColumnName("ex_asset_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.RemainingQuantity).HasColumnName("remaining_quantity");
            entity.Property(e => e.RfId)
                .HasMaxLength(64)
                .HasColumnName("rf_id");
            entity.Property(e => e.SalvageValue)
                .HasColumnType("smallmoney")
                .HasColumnName("salvage_value");
            entity.Property(e => e.SerialNumber).HasColumnName("serial_number");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.TotalQuantity).HasColumnName("total_quantity");

            entity.HasOne(d => d.AssetCategory).WithMany(p => p.AtAssetRegisters)
                .HasForeignKey(d => d.AssetCategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_CAT_ID");

            entity.HasOne(d => d.AssetClass).WithMany(p => p.AtAssetRegisters)
                .HasForeignKey(d => d.AssetClassId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_CLASS_ID");

            entity.HasOne(d => d.AssetSubCategory).WithMany(p => p.AtAssetRegisters)
                .HasForeignKey(d => d.AssetSubCategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_SUBCAT_ID");

            entity.HasOne(d => d.AssetType).WithMany(p => p.AtAssetRegisterAssetTypes)
                .HasForeignKey(d => d.AssetTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_ASSET_TYPE_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetRegisterCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetRegisterModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetRegisterOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_ASSET_REG_ORG_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetRegisterStatuses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_STATUS");
        });

        modelBuilder.Entity<AtAssetRequisition>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83FBE0C6906");

            entity.ToTable("at_asset_requisition");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB545110461").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ApprovalTypeId).HasColumnName("approval_type_id");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.IsAdminApproved)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_admin_approved");
            entity.Property(e => e.IsDeptHeadApproved)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_dept_head_approved");
            entity.Property(e => e.IsDivHeadApproved)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_div_head_approved");
            entity.Property(e => e.IsHrApproved)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_hr_approved");
            entity.Property(e => e.IsSuperAdminApproved)
                .HasDefaultValueSql("((0))")
                .HasColumnName("is_super_admin_approved");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");

            entity.HasOne(d => d.ApprovalType).WithMany(p => p.AtAssetRequisitions)
                .HasForeignKey(d => d.ApprovalTypeId)
                .HasConstraintName("FK_ASST_REQ_APPROVAL_TYPE");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetRequisitions)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASST_REQ_ASSET");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetRequisitionCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_ASST_REQ_CON_CREATED_BY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetRequisitionModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASST_REQ_CON_MODIFIED_BY");
        });

        modelBuilder.Entity<AtAssetRequisitionConfig>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F46748F60");

            entity.ToTable("at_asset_requisition_config");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5ACEE6C89").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.IssueApproval)
                .HasDefaultValueSql("((0))")
                .HasColumnName("issue_approval");
            entity.Property(e => e.IssueRequest)
                .HasDefaultValueSql("((0))")
                .HasColumnName("issue_request");
            entity.Property(e => e.MaintenanceApproval)
                .HasDefaultValueSql("((0))")
                .HasColumnName("maintenance_approval");
            entity.Property(e => e.MaintenanceRequest)
                .HasDefaultValueSql("((0))")
                .HasColumnName("maintenance_request");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.PoApproval)
                .HasDefaultValueSql("((0))")
                .HasColumnName("po_approval");
            entity.Property(e => e.PoRequest)
                .HasDefaultValueSql("((0))")
                .HasColumnName("po_request");
            entity.Property(e => e.ReturnApproval)
                .HasDefaultValueSql("((0))")
                .HasColumnName("return_approval");
            entity.Property(e => e.ReturnRequest)
                .HasDefaultValueSql("((0))")
                .HasColumnName("return_request");
            entity.Property(e => e.RoleId).HasColumnName("role_id");
            entity.Property(e => e.TransferApproval)
                .HasDefaultValueSql("((0))")
                .HasColumnName("transfer_approval");
            entity.Property(e => e.TransferRequest)
                .HasDefaultValueSql("((0))")
                .HasColumnName("transfer_request");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetRequisitionConfigCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_ASST_REQU_CON_CREATED_BY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetRequisitionConfigModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASST_REQU_CON_MODIFIED_BY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetRequisitionConfigOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_ASST_REQU_CON_ORG");

            entity.HasOne(d => d.Role).WithMany(p => p.AtAssetRequisitionConfigs)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASST_REQU_CON_ROLE");
        });

        modelBuilder.Entity<AtAssetReturnDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_RETURN_ID");

            entity.ToTable("at_asset_return_details");

            entity.HasIndex(e => new { e.Guid, e.ReturnReference }, "UQ_AT_ASSET_RETURN").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.ReturnApprovedBy).HasColumnName("return_approved_by");
            entity.Property(e => e.ReturnApprovedDate)
                .HasColumnType("datetime")
                .HasColumnName("return_approved_date");
            entity.Property(e => e.ReturnBy).HasColumnName("return_by");
            entity.Property(e => e.ReturnCondition)
                .HasMaxLength(256)
                .HasColumnName("return_condition");
            entity.Property(e => e.ReturnDate)
                .HasColumnType("datetime")
                .HasColumnName("return_date");
            entity.Property(e => e.ReturnNotes)
                .HasMaxLength(256)
                .HasColumnName("return_notes");
            entity.Property(e => e.ReturnQuantity).HasColumnName("return_quantity");
            entity.Property(e => e.ReturnReference)
                .HasMaxLength(64)
                .HasColumnName("return_reference");
            entity.Property(e => e.ReturnTo).HasColumnName("return_to");
            entity.Property(e => e.ReturnValue)
                .HasColumnType("money")
                .HasColumnName("return_value");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetReturnDetails)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RETURN_ASSET_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetReturnDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RETURN_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetReturnDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_RETURN_MODIFIEDBY");

            entity.HasOne(d => d.ReturnApprovedByNavigation).WithMany(p => p.AtAssetReturnDetailReturnApprovedByNavigations)
                .HasForeignKey(d => d.ReturnApprovedBy)
                .HasConstraintName("FK_RETURN_APPROVE_BY_ID");

            entity.HasOne(d => d.ReturnByNavigation).WithMany(p => p.AtAssetReturnDetailReturnByNavigations)
                .HasForeignKey(d => d.ReturnBy)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RETURN_RETURN_BY_ID");

            entity.HasOne(d => d.ReturnToNavigation).WithMany(p => p.AtAssetReturnDetailReturnToNavigations)
                .HasForeignKey(d => d.ReturnTo)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RETURN_RETURN_TO_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetReturnDetails)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_RETURN_STATUS");
        });

        modelBuilder.Entity<AtAssetTransferDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_TRANS_ID");

            entity.ToTable("at_asset_transfer_details");

            entity.HasIndex(e => e.Guid, "UQ_AT_ASSET_TRANS").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ApprovedById).HasColumnName("approved_by_id");
            entity.Property(e => e.ApprovedDate)
                .HasColumnType("datetime")
                .HasColumnName("approved_date");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.AssetIssueId).HasColumnName("asset_issue_id");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepartmentId).HasColumnName("department_id");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.EmployeeId).HasColumnName("employee_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.HostName)
                .HasMaxLength(256)
                .HasColumnName("host_name");
            entity.Property(e => e.IpAddress)
                .HasMaxLength(256)
                .HasColumnName("ip_address");
            entity.Property(e => e.LocationId).HasColumnName("location_id");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.OsName)
                .HasMaxLength(256)
                .HasColumnName("os_name");
            entity.Property(e => e.Quantity).HasColumnName("quantity");
            entity.Property(e => e.ReceiptDate)
                .HasColumnType("datetime")
                .HasColumnName("receipt_date");
            entity.Property(e => e.ReceiptNo)
                .HasMaxLength(64)
                .HasColumnName("receipt_no");
            entity.Property(e => e.Remark)
                .HasMaxLength(1024)
                .HasColumnName("remark");
            entity.Property(e => e.SourceId).HasColumnName("source_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubLocationId).HasColumnName("sub_location_id");

            entity.HasOne(d => d.ApprovedBy).WithMany(p => p.AtAssetTransferDetailApprovedBies)
                .HasForeignKey(d => d.ApprovedById)
                .HasConstraintName("FK_ASSET_TRANS_APPROVED_BY");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetTransferDetails)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_ASSET_ID");

            entity.HasOne(d => d.AssetIssue).WithMany(p => p.AtAssetTransferDetails)
                .HasForeignKey(d => d.AssetIssueId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_ASSET_ISSUE_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetTransferDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_CREATEDBY");

            entity.HasOne(d => d.Department).WithMany(p => p.AtAssetTransferDetails)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("FK_ASSET_TRANS_DEPT_ID");

            entity.HasOne(d => d.Division).WithMany(p => p.AtAssetTransferDetails)
                .HasForeignKey(d => d.DivisionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_DIV_ID");

            entity.HasOne(d => d.Employee).WithMany(p => p.AtAssetTransferDetailEmployees)
                .HasForeignKey(d => d.EmployeeId)
                .HasConstraintName("FK_ASSET_TRANS_EMP_ID");

            entity.HasOne(d => d.Location).WithMany(p => p.AtAssetTransferDetailLocations)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_LOC_ID");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetTransferDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_ASSET_TRANS_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtAssetTransferDetailOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_ORG_ID");

            entity.HasOne(d => d.Source).WithMany(p => p.AtAssetTransferDetailSources)
                .HasForeignKey(d => d.SourceId)
                .HasConstraintName("FK_ASSET_TRANS_SOURCE_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetTransferDetailStatuses)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_ASSET_TRANS_STATUS");

            entity.HasOne(d => d.SubLocation).WithMany(p => p.AtAssetTransferDetailSubLocations)
                .HasForeignKey(d => d.SubLocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ASSET_TRANS_SUBLOC_ID");
        });

        modelBuilder.Entity<AtAssetVendorDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_ASSET_VENDOR_ID");

            entity.ToTable("at_asset_vendor_details");

            entity.HasIndex(e => e.Guid, "UQ_AT_ASSET_VENDOR").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AdditionalPrice)
                .HasColumnType("money")
                .HasColumnName("additional_price");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Currency)
                .HasMaxLength(64)
                .HasColumnName("currency");
            entity.Property(e => e.ExpiresIn).HasColumnName("expires_in");
            entity.Property(e => e.ExpiryDate)
                .HasColumnType("datetime")
                .HasColumnName("expiry_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.InvoiceNumber)
                .HasMaxLength(16)
                .HasColumnName("invoice_number");
            entity.Property(e => e.IsAmcApplicable).HasColumnName("is_amc_applicable");
            entity.Property(e => e.IsInsurance).HasColumnName("is_insurance");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.PoNumber)
                .HasMaxLength(16)
                .HasColumnName("po_number");
            entity.Property(e => e.PurchasePrice)
                .HasColumnType("money")
                .HasColumnName("purchase_price");
            entity.Property(e => e.RecievedDate)
                .HasColumnType("datetime")
                .HasColumnName("recieved_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.VendorId).HasColumnName("vendor_id");
            entity.Property(e => e.WarrantyEndDate)
                .HasColumnType("datetime")
                .HasColumnName("warranty_end_date");
            entity.Property(e => e.WarrantyStartDate)
                .HasColumnType("datetime")
                .HasColumnName("warranty_start_date");

            entity.HasOne(d => d.Asset).WithMany(p => p.AtAssetVendorDetails)
                .HasForeignKey(d => d.AssetId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_VENDOR_ASSET_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtAssetVendorDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_VENDOR_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtAssetVendorDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_VENDOR_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtAssetVendorDetails)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_VENDOR_STATUS");

            entity.HasOne(d => d.Vendor).WithMany(p => p.AtAssetVendorDetailVendors)
                .HasForeignKey(d => d.VendorId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_VENDOR_VENDOR_ID");
        });

        modelBuilder.Entity<AtCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_CATEGORY_ID");

            entity.ToTable("at_category");

            entity.HasIndex(e => e.Guid, "UQ_AT_CATEGORY").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CategoryName)
                .HasMaxLength(256)
                .HasColumnName("category_name");
            entity.Property(e => e.ClassId).HasColumnName("class_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Class).WithMany(p => p.AtCategories)
                .HasForeignKey(d => d.ClassId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CATEGORY_CLASS_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCategoryCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CATEGORY_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCategoryModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CATEGORY_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtCategoryOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_CAT_ORG_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCategories)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_CATEGORY_STATUS");
        });

        modelBuilder.Entity<AtCity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_CITY_ID");

            entity.ToTable("at_city");

            entity.HasIndex(e => e.Guid, "UQ_AT_CITY").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CityName)
                .HasMaxLength(256)
                .HasColumnName("city_name");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StateId).HasColumnName("state_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCityCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CITY_CREATED_BY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCityModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CITY_MODIFIEDBY");

            entity.HasOne(d => d.State).WithMany(p => p.AtCities)
                .HasForeignKey(d => d.StateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CITY_STATE_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCities)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_CITY_STATUS");
        });

        modelBuilder.Entity<AtClass>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_CLASS_ID");

            entity.ToTable("at_class");

            entity.HasIndex(e => e.Guid, "UQ_AT_CLASS").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ClassName)
                .HasMaxLength(256)
                .HasColumnName("class_name");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepreciationRate)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("depreciation_rate");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtClassCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CLASS_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtClassModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CLASS_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtClassOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_CLASS_ORG_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtClasses)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CLASS_STATUS");
        });

        modelBuilder.Entity<AtCore>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_CORE_ID");

            entity.ToTable("at_core");

            entity.HasIndex(e => e.Guid, "UQ_AT_CORE").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.Name)
                .HasMaxLength(256)
                .HasColumnName("name");
            entity.Property(e => e.ParentId).HasColumnName("parent_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubParentId).HasColumnName("sub_parent_id");
            entity.Property(e => e.SystemName)
                .HasMaxLength(256)
                .HasColumnName("system_name");
            entity.Property(e => e.TypeId).HasColumnName("type_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCoreCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CORE_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCoreModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CORE_MODIFIEDBY");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_CORE_PARENT_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.InverseStatus)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_CORE_STATUS");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_CORE_SUBPARENT_ID");

            entity.HasOne(d => d.Type).WithMany(p => p.InverseType)
                .HasForeignKey(d => d.TypeId)
                .HasConstraintName("FK_CORE_TYPE_ID");
        });

        modelBuilder.Entity<AtCoreAccountDevice>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83FEBBEDC92");

            entity.ToTable("at_core_account_device");

            entity.HasIndex(e => e.Guid, "UQ__at_core___497F6CB58FC8A7D5").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.AppId).HasColumnName("app_id");
            entity.Property(e => e.AppVersionId).HasColumnName("app_version_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.SerialNumber)
                .HasMaxLength(1024)
                .HasColumnName("serial_number");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtCoreAccountDeviceAccounts)
                .HasForeignKey(d => d.AccountId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Core_Account_Device_Account");

            entity.HasOne(d => d.App).WithMany(p => p.AtCoreAccountDevices)
                .HasForeignKey(d => d.AppId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Core_Account_Device_App");

            entity.HasOne(d => d.AppVersion).WithMany(p => p.AtCoreAccountDevices)
                .HasForeignKey(d => d.AppVersionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Core_Account_Device_App_Version");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCoreAccountDeviceCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK__Core_Account_Device_Created_By");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCoreAccountDeviceModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK__Core_Account_Device_Modified_By");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreAccountDevices)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Core_Account_Device_Status");
        });

        modelBuilder.Entity<AtCoreApp>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83F7887F830");

            entity.ToTable("at_core_app");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.AllowLogging)
                .HasDefaultValueSql("('1')")
                .HasColumnName("allow_logging");
            entity.Property(e => e.AppKey)
                .HasMaxLength(64)
                .IsUnicode(false)
                .HasColumnName("app_key");
            entity.Property(e => e.CreateDate)
                .HasColumnType("datetime")
                .HasColumnName("create_date");
            entity.Property(e => e.CreatedbyId).HasColumnName("createdby_id");
            entity.Property(e => e.Description)
                .HasMaxLength(256)
                .IsUnicode(false)
                .HasColumnName("description");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.IPAddress)
                .HasMaxLength(50)
                .HasColumnName("iP_address");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("modify_date");
            entity.Property(e => e.ModifybyId).HasColumnName("modifyby_id");
            entity.Property(e => e.Name)
                .HasMaxLength(128)
                .IsUnicode(false)
                .HasColumnName("name");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SystemName)
                .HasMaxLength(128)
                .IsUnicode(false)
                .HasColumnName("system_name");

            entity.HasOne(d => d.Account).WithMany(p => p.AtCoreAppAccounts)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("fk_app_statusid_core");

            entity.HasOne(d => d.Createdby).WithMany(p => p.AtCoreAppCreatedbies)
                .HasForeignKey(d => d.CreatedbyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_app_createdbyid_account");

            entity.HasOne(d => d.Modifyby).WithMany(p => p.AtCoreAppModifybies)
                .HasForeignKey(d => d.ModifybyId)
                .HasConstraintName("fk_app_accountid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreApps)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_app_modifybyid_account");
        });

        modelBuilder.Entity<AtCoreAppVersion>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83F1029BB7D");

            entity.ToTable("at_core_app_version");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.ApiKey)
                .HasMaxLength(128)
                .HasColumnName("api_key");
            entity.Property(e => e.AppId).HasColumnName("app_id");
            entity.Property(e => e.Comment)
                .HasMaxLength(256)
                .HasColumnName("comment");
            entity.Property(e => e.CreateDate)
                .HasColumnType("datetime")
                .HasColumnName("create_date");
            entity.Property(e => e.CreatedbyId).HasColumnName("createdby_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.LastRequestTime)
                .HasColumnType("datetime")
                .HasColumnName("last_request_time");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("modify_date");
            entity.Property(e => e.ModifybyId).HasColumnName("modifyby_id");
            entity.Property(e => e.PrivateKey)
                .HasColumnType("ntext")
                .HasColumnName("private_key");
            entity.Property(e => e.PublicKey)
                .HasColumnType("ntext")
                .HasColumnName("public_key");
            entity.Property(e => e.RequestCount)
                .HasDefaultValueSql("('0')")
                .HasColumnName("request_count");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SystemPrivateKey)
                .HasColumnType("ntext")
                .HasColumnName("system_private_key");
            entity.Property(e => e.SystemPublicKey)
                .HasColumnType("ntext")
                .HasColumnName("system_public_key");
            entity.Property(e => e.VersionId)
                .HasMaxLength(8)
                .HasColumnName("version_id");

            entity.HasOne(d => d.App).WithMany(p => p.AtCoreAppVersions)
                .HasForeignKey(d => d.AppId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_appv_appid_app");

            entity.HasOne(d => d.Createdby).WithMany(p => p.AtCoreAppVersionCreatedbies)
                .HasForeignKey(d => d.CreatedbyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_appv_createdbyid_account");

            entity.HasOne(d => d.Modifyby).WithMany(p => p.AtCoreAppVersionModifybies)
                .HasForeignKey(d => d.ModifybyId)
                .HasConstraintName("fk_appv_modifybyid_account");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreAppVersions)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_appv_statusid_core");
        });

        modelBuilder.Entity<AtCoreFeature>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83F6A128257");

            entity.ToTable("at_core_feature");

            entity.HasIndex(e => e.Guid, "UQ__at_core___497F6CB54FE46BBA").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Add)
                .HasMaxLength(256)
                .HasColumnName("add");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Delete)
                .HasMaxLength(256)
                .HasColumnName("delete");
            entity.Property(e => e.Edit)
                .HasMaxLength(256)
                .HasColumnName("edit");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .IsUnicode(false)
                .HasColumnName("guid");
            entity.Property(e => e.HelperId).HasColumnName("helper_id");
            entity.Property(e => e.IsTab)
                .HasDefaultValueSql("('0')")
                .HasColumnName("is_tab");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("modify_date");
            entity.Property(e => e.Name)
                .HasMaxLength(256)
                .HasColumnName("name");
            entity.Property(e => e.ParentId).HasColumnName("parent_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SystemName)
                .HasMaxLength(256)
                .HasColumnName("system_name");
            entity.Property(e => e.TypeId).HasColumnName("type_id");
            entity.Property(e => e.View)
                .HasMaxLength(256)
                .HasColumnName("view");
            entity.Property(e => e.ViewAll)
                .HasMaxLength(256)
                .HasColumnName("view_all");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCoreFeatureCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CoreFeature_CREATEDBY");

            entity.HasOne(d => d.Helper).WithMany(p => p.AtCoreFeatureHelpers)
                .HasForeignKey(d => d.HelperId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Fk_CoreFeature_HelperId_CoreHelper_Id");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCoreFeatureModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CoreFeature_MODIFIEDBY");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_CoreFeature_ParentId_CoreFeature_Id");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreFeatureStatuses)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreFeature_StatusId_Core");

            entity.HasOne(d => d.Type).WithMany(p => p.AtCoreFeatureTypes)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreFeature_TypeId_CoreHelper_Id");
        });

        modelBuilder.Entity<AtCoreRole>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83F0E35382B");

            entity.ToTable("at_core_role");

            entity.HasIndex(e => e.Guid, "UQ__at_core___497F6CB5FE362D60").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .IsUnicode(false)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("modify_date");
            entity.Property(e => e.Name)
                .HasMaxLength(256)
                .HasColumnName("name");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SystemName)
                .HasMaxLength(256)
                .HasColumnName("system_name");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCoreRoleCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CoreRole_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCoreRoleModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CoreRole_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtCoreRoleOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_CORE_ROLE_ORGANIZATION");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreRoles)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreRole_StatusId_Core");
        });

        modelBuilder.Entity<AtCoreRoleFeature>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83FF27D666C");

            entity.ToTable("at_core_role_feature");

            entity.HasIndex(e => e.Guid, "UQ__at_core___497F6CB5A64900B9").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.FeatureId).HasColumnName("feature_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .IsUnicode(false)
                .HasColumnName("guid");
            entity.Property(e => e.IsAdd)
                .HasDefaultValueSql("('0')")
                .HasColumnName("is_add");
            entity.Property(e => e.IsDelete)
                .HasDefaultValueSql("('0')")
                .HasColumnName("is_delete");
            entity.Property(e => e.IsUpdate)
                .HasDefaultValueSql("('0')")
                .HasColumnName("is_update");
            entity.Property(e => e.IsView)
                .HasDefaultValueSql("('0')")
                .HasColumnName("is_view");
            entity.Property(e => e.IsViewAll)
                .HasDefaultValueSql("('0')")
                .HasColumnName("is_view_all");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifyDate)
                .HasColumnType("datetime")
                .HasColumnName("modify_date");
            entity.Property(e => e.ParentId).HasColumnName("parent_id");
            entity.Property(e => e.RoleId).HasColumnName("role_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubParentId).HasColumnName("sub_parent_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCoreRoleFeatureCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CoreRoleFeature_CREATEDBY");

            entity.HasOne(d => d.Feature).WithMany(p => p.AtCoreRoleFeatures)
                .HasForeignKey(d => d.FeatureId)
                .HasConstraintName("FK_CoreRoleFeature_FeatureId_CoreFeature");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCoreRoleFeatureModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CoreRoleFeature_MODIFIEDBY");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_COREROLEFEATURE_PARENT");

            entity.HasOne(d => d.Role).WithMany(p => p.AtCoreRoleFeatures)
                .HasForeignKey(d => d.RoleId)
                .HasConstraintName("FK_CoreRoleFeature_RoleId_CoreRole");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreRoleFeatures)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CoreRoleFeature_StatusId_Core");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_COREROLEFEATURE_SUB_PARENT");
        });

        modelBuilder.Entity<AtCoreStorage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_core___3213E83FC2D51114");

            entity.ToTable("at_core_storage");

            entity.HasIndex(e => e.Guid, "UQ__at_core___497F6CB5CAF59AD9").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.FileExtension)
                .HasMaxLength(50)
                .HasColumnName("file_extension");
            entity.Property(e => e.FileName)
                .HasMaxLength(256)
                .HasColumnName("file_name");
            entity.Property(e => e.FilePath)
                .HasMaxLength(1024)
                .HasColumnName("file_path");
            entity.Property(e => e.FileSize)
                .HasColumnType("decimal(9, 6)")
                .HasColumnName("file_size");
            entity.Property(e => e.Files).HasColumnName("files");
            entity.Property(e => e.Files1).HasColumnName("files_1");
            entity.Property(e => e.Files2).HasColumnName("files_2");
            entity.Property(e => e.Files3).HasColumnName("files_3");
            entity.Property(e => e.Guid)
                .HasMaxLength(45)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtCoreStorageAccounts)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_CORE_STORAGE_ACCOUNT_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCoreStorageCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("FK_CORE_STORAGE_Created_By");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCoreStorageModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_CORE_STORAGE_Modified_By");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCoreStorages)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CORE_STORAGE_Status");
        });

        modelBuilder.Entity<AtCountry>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_COUNTRY_ID");

            entity.ToTable("at_country");

            entity.HasIndex(e => e.Guid, "UQ_AT_COUNTRY").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CapitalName)
                .HasMaxLength(256)
                .HasColumnName("capital_name");
            entity.Property(e => e.CountryName)
                .HasMaxLength(256)
                .HasColumnName("country_name");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.CurrencyNotation)
                .HasMaxLength(256)
                .HasColumnName("currency_notation");
            entity.Property(e => e.CurrencySymbol)
                .HasMaxLength(256)
                .HasColumnName("currency_symbol");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Isd)
                .HasMaxLength(64)
                .HasColumnName("isd");
            entity.Property(e => e.Iso)
                .HasMaxLength(64)
                .HasColumnName("iso");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtCountryCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_COUNTRY_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtCountryModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_COUNTRY_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtCountries)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_COUNTRY_STATUS");
        });

        modelBuilder.Entity<AtDepartment>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_DEPARTMENT_ID");

            entity.ToTable("at_department");

            entity.HasIndex(e => e.Guid, "UQ_AT_DEPARTMENT").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepartmentName)
                .HasMaxLength(256)
                .HasColumnName("department_name");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtDepartmentCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DEPARTMENT_CREATEDBY");

            entity.HasOne(d => d.Division).WithMany(p => p.AtDepartments)
                .HasForeignKey(d => d.DivisionId)
                .HasConstraintName("FK_DEPT_DIV_ID");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtDepartmentModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_DEPARTMENT_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtDepartmentOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DEPT_ORG_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtDepartments)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_DEPARTMENT_STATUS");
        });

        modelBuilder.Entity<AtDepreciationConfiguration>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_depre__3213E83F5C88AA8E");

            entity.ToTable("at_depreciation_configuration");

            entity.HasIndex(e => e.Guid, "UQ__at_depre__497F6CB57AAC7D47").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepreciationMethodId).HasColumnName("depreciation_method_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(45)
                .HasColumnName("guid");
            entity.Property(e => e.IsFromIssuedDate).HasColumnName("is_from_issued_date");
            entity.Property(e => e.IsFromReceivedDate).HasColumnName("is_from_received_date");
            entity.Property(e => e.IsWithActualValue).HasColumnName("is_with_actual_value");
            entity.Property(e => e.IsWithSalvageValue).HasColumnName("is_with_salvage_value");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtDepreciationConfigurationAccounts)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("DEPRECIATION_CONFIG_ACC");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtDepreciationConfigurationCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .HasConstraintName("DEPRECIATION_CONFIG_CREATED_BY");

            entity.HasOne(d => d.DepreciationMethod).WithMany(p => p.AtDepreciationConfigurationDepreciationMethods)
                .HasForeignKey(d => d.DepreciationMethodId)
                .HasConstraintName("DEPRECIATION_CONFIG_DEP_METH_ID");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtDepreciationConfigurationModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("DEPRECIATION_CONFIG_MODIFIED_BY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtDepreciationConfigurationStatuses)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("DEPRECIATION_CONFIG_STATUS");
        });

        modelBuilder.Entity<AtDivision>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_DIVISION_ID");

            entity.ToTable("at_division");

            entity.HasIndex(e => e.Guid, "UQ_AT_DIVISION").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DivisionName)
                .HasMaxLength(256)
                .HasColumnName("division_name");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.ParentId).HasColumnName("parent_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubParentId).HasColumnName("sub_parent_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtDivisionCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DIVISION_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtDivisionModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_DIVISION_MODIFIEDBY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtDivisionOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DIVISION_ORG_ID");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_DIVISION_PARENT_ID");

            entity.HasOne(d => d.Status).WithMany(p => p.AtDivisions)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_DIVISION_STATUS");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_DIVISION_SUBPARENT_ID");
        });

        modelBuilder.Entity<AtLocation>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_LOC_ID");

            entity.ToTable("at_location");

            entity.HasIndex(e => e.Guid, "UQ_LOC").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DivisionId).HasColumnName("division_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.LocationName)
                .HasMaxLength(256)
                .HasColumnName("location_name");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.ParentId).HasColumnName("parent_id");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubParentId).HasColumnName("sub_parent_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtLocationCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LOC_CREATED_BY");

            entity.HasOne(d => d.Division).WithMany(p => p.AtLocations)
                .HasForeignKey(d => d.DivisionId)
                .HasConstraintName("FK_LOC_DIVISION");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtLocationModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_LOC_MODIFIED_BY");

            entity.HasOne(d => d.Organization).WithMany(p => p.AtLocationOrganizations)
                .HasForeignKey(d => d.OrganizationId)
                .HasConstraintName("FK_LOC_ORG");

            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_LOC_PARENT");

            entity.HasOne(d => d.Status).WithMany(p => p.AtLocations)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_LOC_STATUS");

            entity.HasOne(d => d.SubParent).WithMany(p => p.InverseSubParent)
                .HasForeignKey(d => d.SubParentId)
                .HasConstraintName("FK_LOC_SUB_PARENT");
        });

        modelBuilder.Entity<AtLocationAccount>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_LOC_ACC_ID");

            entity.ToTable("at_location_account");

            entity.HasIndex(e => e.Guid, "UQ_LOC_ACC").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AccountId).HasColumnName("account_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.DepartmentId).HasColumnName("department_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.LocationId).HasColumnName("location_id");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubLocationId).HasColumnName("sub_location_id");

            entity.HasOne(d => d.Account).WithMany(p => p.AtLocationAccountAccounts)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_LOC_ACC");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtLocationAccountCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LOC_ACC_CREATED_BY");

            entity.HasOne(d => d.Department).WithMany(p => p.AtLocationAccounts)
                .HasForeignKey(d => d.DepartmentId)
                .HasConstraintName("FK_LOC_ACC_DEPT");

            entity.HasOne(d => d.Location).WithMany(p => p.AtLocationAccountLocations)
                .HasForeignKey(d => d.LocationId)
                .HasConstraintName("FK_LOC_ACC_LOCATION");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtLocationAccountModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_LOC_ACC_MODIFIED_BY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtLocationAccounts)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_LOC_ACC_STATUS");

            entity.HasOne(d => d.SubLocation).WithMany(p => p.AtLocationAccountSubLocations)
                .HasForeignKey(d => d.SubLocationId)
                .HasConstraintName("FK_LOC_ACC_SUB_LOC");
        });

        modelBuilder.Entity<AtPrinterDetail>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_PRINTER_ID");

            entity.ToTable("at_printer_details");

            entity.HasIndex(e => e.Guid, "UQ_AT_PRINTER").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.PrinterName)
                .HasMaxLength(256)
                .HasColumnName("printer_name");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtPrinterDetailCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PRINTER_CREATED_BY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtPrinterDetailModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_PRINTER_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtPrinterDetails)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_PRINTER_STATUS");
        });

        modelBuilder.Entity<AtState>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_STATE_ID");

            entity.ToTable("at_state");

            entity.HasIndex(e => e.Guid, "UQ_AT_STATE").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CountryId).HasColumnName("country_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StateName)
                .HasMaxLength(256)
                .HasColumnName("state_name");
            entity.Property(e => e.StatusId).HasColumnName("status_id");

            entity.HasOne(d => d.Country).WithMany(p => p.AtStates)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_STATE_COUNTRY_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtStateCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_STATE_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtStateModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_STATE_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtStates)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_STATE_STATUS");
        });

        modelBuilder.Entity<AtSubCategory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_AT_SUB_CAT_ID");

            entity.ToTable("at_sub_category");

            entity.HasIndex(e => e.Guid, "UQ_AT_SUB_CAT").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CategoryId).HasColumnName("category_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.ModifiedById).HasColumnName("modified_by_id");
            entity.Property(e => e.ModifiedDate)
                .HasColumnType("datetime")
                .HasColumnName("modified_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubCategoryName)
                .HasMaxLength(256)
                .HasColumnName("sub_category_name");

            entity.HasOne(d => d.Category).WithMany(p => p.AtSubCategories)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SUBCAT_CAT_ID");

            entity.HasOne(d => d.CreatedBy).WithMany(p => p.AtSubCategoryCreatedBies)
                .HasForeignKey(d => d.CreatedById)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SUBCAT_CREATEDBY");

            entity.HasOne(d => d.ModifiedBy).WithMany(p => p.AtSubCategoryModifiedBies)
                .HasForeignKey(d => d.ModifiedById)
                .HasConstraintName("FK_SUBCAT_MODIFIEDBY");

            entity.HasOne(d => d.Status).WithMany(p => p.AtSubCategories)
                .HasForeignKey(d => d.StatusId)
                .HasConstraintName("FK_SUBCAT_STATUS");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
