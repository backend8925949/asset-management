﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreFeature
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string? Name { get; set; }

    public int TypeId { get; set; }

    public int HelperId { get; set; }

    public long? ParentId { get; set; }

    public string? SystemName { get; set; }

    public string? Add { get; set; }

    public string? Edit { get; set; }

    public string? Delete { get; set; }

    public string? View { get; set; }

    public byte? IsTab { get; set; }

    public string? ViewAll { get; set; }

    public int StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifiedById { get; set; }

   public virtual ICollection<AtCoreRoleFeature> AtCoreRoleFeatures { get; set; } = new List<AtCoreRoleFeature>();

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtCore Helper { get; set; } = null!;

   public virtual ICollection<AtCoreFeature> InverseParent { get; set; } = new List<AtCoreFeature>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCoreFeature? Parent { get; set; }

    public virtual AtCore Status { get; set; } = null!;

    public virtual AtCore Type { get; set; } = null!;
}
