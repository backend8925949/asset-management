﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetVendorDetail
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AssetId { get; set; }

    public long VendorId { get; set; }

    public string? PoNumber { get; set; }

    public string? InvoiceNumber { get; set; }

    public DateTime? RecievedDate { get; set; }

    public DateTime? WarrantyStartDate { get; set; }

    public DateTime? WarrantyEndDate { get; set; }

    public DateTime? ExpiryDate { get; set; }

    public int? ExpiresIn { get; set; }

    public string? Currency { get; set; }

    public decimal? PurchasePrice { get; set; }

    public byte? IsAmcApplicable { get; set; }

    public byte? IsInsurance { get; set; }

    public int? StatusId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long CreatedById { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public decimal? AdditionalPrice { get; set; }

    public virtual AtAssetRegister Asset { get; set; } = null!;

   public virtual ICollection<AtAssetAmcDetail> AtAssetAmcDetails { get; set; } = new List<AtAssetAmcDetail>();

   public virtual ICollection<AtAssetInsuranceDetail> AtAssetInsuranceDetails { get; set; } = new List<AtAssetInsuranceDetail>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtAccount Vendor { get; set; } = null!;
}
