﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCountry
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public string CountryName { get; set; } = null!;

    public string? Iso { get; set; }

    public string? Isd { get; set; }

    public string? CurrencyNotation { get; set; }

    public string? CurrencySymbol { get; set; }

    public string? CapitalName { get; set; }

    public int? StatusId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

   public virtual ICollection<AtAddress> AtAddresses { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtState> AtStates { get; set; } = new List<AtState>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }
}
