﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetMaintenance
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? OrganizationId { get; set; }

    public int MaintenanceType { get; set; }

    public long VendorId { get; set; }

    public long AssetId { get; set; }

    public string Description { get; set; } = null!;

    public string? Remark { get; set; }

    public DateTime RequestDate { get; set; }

    public DateTime? ExpectedDate { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public decimal? Amount { get; set; }

    public int StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public string? ReceiptNo { get; set; }

    public DateTime? ReceiptDate { get; set; }

    public string? Comment { get; set; }

    public virtual AtAssetRegister Asset { get; set; } = null!;

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtCore MaintenanceTypeNavigation { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtCore Status { get; set; } = null!;

    public virtual AtAccount Vendor { get; set; } = null!;
}
