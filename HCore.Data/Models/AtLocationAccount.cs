﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtLocationAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? LocationId { get; set; }

    public long? SubLocationId { get; set; }

    public long? AccountId { get; set; }

    public long? DepartmentId { get; set; }

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public virtual AtAccount? Account { get; set; }

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtDepartment? Department { get; set; }

    public virtual AtLocation? Location { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtLocation? SubLocation { get; set; }
}
