﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtClass
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public string ClassName { get; set; } = null!;

    public long? OrganizationId { get; set; }

    public int StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public decimal? DepreciationRate { get; set; }

   public virtual ICollection<AtAssetRegister> AtAssetRegisters { get; set; } = new List<AtAssetRegister>();

   public virtual ICollection<AtCategory> AtCategories { get; set; } = new List<AtCategory>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
