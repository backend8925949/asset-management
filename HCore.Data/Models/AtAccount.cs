﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAccount
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? OwnerId { get; set; }

    public long? SubOwnerId { get; set; }

    public long? OrganizationId { get; set; }

    public long? DepartmentId { get; set; }

    public long? DivisionId { get; set; }

    public int? AccountTypeId { get; set; }

    public long? RoleId { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string EmailAddress { get; set; } = null!;

    public string? Address { get; set; }

    public string ContactNumber { get; set; } = null!;

    public string? MobileNumber { get; set; }

    public string DisplayName { get; set; } = null!;

    public string? AccountCode { get; set; }

    public string? GstNumber { get; set; }

    public string? PanNumber { get; set; }

    public string? CpName { get; set; }

    public string? CpMobileNumber { get; set; }

    public string? CpEmailAddress { get; set; }

    public string? Designation { get; set; }

    public DateTime? LastLoginDate { get; set; }

    public DateTime? LastActivityDate { get; set; }

    public int? StatusId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public int? SourceId { get; set; }

    public string? AccessPin { get; set; }

    public string? SystemName { get; set; }

    public virtual AtCore? AccountType { get; set; }

   public virtual ICollection<AtAccountAuth> AtAccountAuthCreatedBies { get; set; } = new List<AtAccountAuth>();

   public virtual ICollection<AtAccountAuth> AtAccountAuthModifiedBies { get; set; } = new List<AtAccountAuth>();

   public virtual ICollection<AtAccountAuth> AtAccountAuthUsers { get; set; } = new List<AtAccountAuth>();

   public virtual ICollection<AtAccountEmailConfiguration> AtAccountEmailConfigurationAccounts { get; set; } = new List<AtAccountEmailConfiguration>();

   public virtual ICollection<AtAccountEmailConfiguration> AtAccountEmailConfigurationCreatedBies { get; set; } = new List<AtAccountEmailConfiguration>();

   public virtual ICollection<AtAccountEmailConfiguration> AtAccountEmailConfigurationModifiedBies { get; set; } = new List<AtAccountEmailConfiguration>();

   public virtual ICollection<AtAccountSession> AtAccountSessionAccounts { get; set; } = new List<AtAccountSession>();

   public virtual ICollection<AtAccountSession> AtAccountSessionModifybies { get; set; } = new List<AtAccountSession>();

   public virtual ICollection<AtAddress> AtAddressAccounts { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAddress> AtAddressCreatedBies { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAddress> AtAddressModifiedBies { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAddress> AtAddressOrganizations { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAssetAmcDetail> AtAssetAmcDetailCreatedBies { get; set; } = new List<AtAssetAmcDetail>();

   public virtual ICollection<AtAssetAmcDetail> AtAssetAmcDetailModifiedBies { get; set; } = new List<AtAssetAmcDetail>();

   public virtual ICollection<AtAssetAmcDetail> AtAssetAmcDetailVendors { get; set; } = new List<AtAssetAmcDetail>();

   public virtual ICollection<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurationCreatedBies { get; set; } = new List<AtAssetDepreciationConfiguration>();

   public virtual ICollection<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurationModifiedBies { get; set; } = new List<AtAssetDepreciationConfiguration>();

   public virtual ICollection<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurationOrganizations { get; set; } = new List<AtAssetDepreciationConfiguration>();

   public virtual ICollection<AtAssetDepreciation> AtAssetDepreciationCreatedBies { get; set; } = new List<AtAssetDepreciation>();

   public virtual ICollection<AtAssetDepreciation> AtAssetDepreciationModifiedBies { get; set; } = new List<AtAssetDepreciation>();

   public virtual ICollection<AtAssetDepreciation> AtAssetDepreciationOrganizations { get; set; } = new List<AtAssetDepreciation>();

   public virtual ICollection<AtAssetInsuranceDetail> AtAssetInsuranceDetailCreatedBies { get; set; } = new List<AtAssetInsuranceDetail>();

   public virtual ICollection<AtAssetInsuranceDetail> AtAssetInsuranceDetailModifiedBies { get; set; } = new List<AtAssetInsuranceDetail>();

   public virtual ICollection<AtAssetInsuranceDetail> AtAssetInsuranceDetailVendors { get; set; } = new List<AtAssetInsuranceDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailApprovedBies { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailCreatedBies { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailEmployees { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailModifiedBies { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailOrganizations { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailTransferedByNavigations { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailTransferedToNavigations { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetLocation> AtAssetLocationCreatedBies { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetLocation> AtAssetLocationModifiedBies { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenanceCreatedBies { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenanceModifiedBies { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenanceOrganizations { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenanceVendors { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetRegister> AtAssetRegisterCreatedBies { get; set; } = new List<AtAssetRegister>();

   public virtual ICollection<AtAssetRegister> AtAssetRegisterModifiedBies { get; set; } = new List<AtAssetRegister>();

   public virtual ICollection<AtAssetRegister> AtAssetRegisterOrganizations { get; set; } = new List<AtAssetRegister>();

   public virtual ICollection<AtAssetRequisitionConfig> AtAssetRequisitionConfigCreatedBies { get; set; } = new List<AtAssetRequisitionConfig>();

   public virtual ICollection<AtAssetRequisitionConfig> AtAssetRequisitionConfigModifiedBies { get; set; } = new List<AtAssetRequisitionConfig>();

   public virtual ICollection<AtAssetRequisitionConfig> AtAssetRequisitionConfigOrganizations { get; set; } = new List<AtAssetRequisitionConfig>();

   public virtual ICollection<AtAssetRequisition> AtAssetRequisitionCreatedBies { get; set; } = new List<AtAssetRequisition>();

   public virtual ICollection<AtAssetRequisition> AtAssetRequisitionModifiedBies { get; set; } = new List<AtAssetRequisition>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetailCreatedBies { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetailModifiedBies { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetailReturnApprovedByNavigations { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetailReturnByNavigations { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetailReturnToNavigations { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailApprovedBies { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailCreatedBies { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailEmployees { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailModifiedBies { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailOrganizations { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetVendorDetail> AtAssetVendorDetailCreatedBies { get; set; } = new List<AtAssetVendorDetail>();

   public virtual ICollection<AtAssetVendorDetail> AtAssetVendorDetailModifiedBies { get; set; } = new List<AtAssetVendorDetail>();

   public virtual ICollection<AtAssetVendorDetail> AtAssetVendorDetailVendors { get; set; } = new List<AtAssetVendorDetail>();

   public virtual ICollection<AtCategory> AtCategoryCreatedBies { get; set; } = new List<AtCategory>();

   public virtual ICollection<AtCategory> AtCategoryModifiedBies { get; set; } = new List<AtCategory>();

   public virtual ICollection<AtCategory> AtCategoryOrganizations { get; set; } = new List<AtCategory>();

   public virtual ICollection<AtCity> AtCityCreatedBies { get; set; } = new List<AtCity>();

   public virtual ICollection<AtCity> AtCityModifiedBies { get; set; } = new List<AtCity>();

   public virtual ICollection<AtClass> AtClassCreatedBies { get; set; } = new List<AtClass>();

   public virtual ICollection<AtClass> AtClassModifiedBies { get; set; } = new List<AtClass>();

   public virtual ICollection<AtClass> AtClassOrganizations { get; set; } = new List<AtClass>();

   public virtual ICollection<AtCoreAccountDevice> AtCoreAccountDeviceAccounts { get; set; } = new List<AtCoreAccountDevice>();

   public virtual ICollection<AtCoreAccountDevice> AtCoreAccountDeviceCreatedBies { get; set; } = new List<AtCoreAccountDevice>();

   public virtual ICollection<AtCoreAccountDevice> AtCoreAccountDeviceModifiedBies { get; set; } = new List<AtCoreAccountDevice>();

   public virtual ICollection<AtCoreApp> AtCoreAppAccounts { get; set; } = new List<AtCoreApp>();

   public virtual ICollection<AtCoreApp> AtCoreAppCreatedbies { get; set; } = new List<AtCoreApp>();

   public virtual ICollection<AtCoreApp> AtCoreAppModifybies { get; set; } = new List<AtCoreApp>();

   public virtual ICollection<AtCoreAppVersion> AtCoreAppVersionCreatedbies { get; set; } = new List<AtCoreAppVersion>();

   public virtual ICollection<AtCoreAppVersion> AtCoreAppVersionModifybies { get; set; } = new List<AtCoreAppVersion>();

   public virtual ICollection<AtCore> AtCoreCreatedBies { get; set; } = new List<AtCore>();

   public virtual ICollection<AtCoreFeature> AtCoreFeatureCreatedBies { get; set; } = new List<AtCoreFeature>();

   public virtual ICollection<AtCoreFeature> AtCoreFeatureModifiedBies { get; set; } = new List<AtCoreFeature>();

   public virtual ICollection<AtCore> AtCoreModifiedBies { get; set; } = new List<AtCore>();

   public virtual ICollection<AtCoreRole> AtCoreRoleCreatedBies { get; set; } = new List<AtCoreRole>();

   public virtual ICollection<AtCoreRoleFeature> AtCoreRoleFeatureCreatedBies { get; set; } = new List<AtCoreRoleFeature>();

   public virtual ICollection<AtCoreRoleFeature> AtCoreRoleFeatureModifiedBies { get; set; } = new List<AtCoreRoleFeature>();

   public virtual ICollection<AtCoreRole> AtCoreRoleModifiedBies { get; set; } = new List<AtCoreRole>();

   public virtual ICollection<AtCoreRole> AtCoreRoleOrganizations { get; set; } = new List<AtCoreRole>();

   public virtual ICollection<AtCoreStorage> AtCoreStorageAccounts { get; set; } = new List<AtCoreStorage>();

   public virtual ICollection<AtCoreStorage> AtCoreStorageCreatedBies { get; set; } = new List<AtCoreStorage>();

   public virtual ICollection<AtCoreStorage> AtCoreStorageModifiedBies { get; set; } = new List<AtCoreStorage>();

   public virtual ICollection<AtCountry> AtCountryCreatedBies { get; set; } = new List<AtCountry>();

   public virtual ICollection<AtCountry> AtCountryModifiedBies { get; set; } = new List<AtCountry>();

   public virtual ICollection<AtDepartment> AtDepartmentCreatedBies { get; set; } = new List<AtDepartment>();

   public virtual ICollection<AtDepartment> AtDepartmentModifiedBies { get; set; } = new List<AtDepartment>();

   public virtual ICollection<AtDepartment> AtDepartmentOrganizations { get; set; } = new List<AtDepartment>();

   public virtual ICollection<AtDepreciationConfiguration> AtDepreciationConfigurationAccounts { get; set; } = new List<AtDepreciationConfiguration>();

   public virtual ICollection<AtDepreciationConfiguration> AtDepreciationConfigurationCreatedBies { get; set; } = new List<AtDepreciationConfiguration>();

   public virtual ICollection<AtDepreciationConfiguration> AtDepreciationConfigurationModifiedBies { get; set; } = new List<AtDepreciationConfiguration>();

   public virtual ICollection<AtDivision> AtDivisionCreatedBies { get; set; } = new List<AtDivision>();

   public virtual ICollection<AtDivision> AtDivisionModifiedBies { get; set; } = new List<AtDivision>();

   public virtual ICollection<AtDivision> AtDivisionOrganizations { get; set; } = new List<AtDivision>();

   public virtual ICollection<AtLocationAccount> AtLocationAccountAccounts { get; set; } = new List<AtLocationAccount>();

   public virtual ICollection<AtLocationAccount> AtLocationAccountCreatedBies { get; set; } = new List<AtLocationAccount>();

   public virtual ICollection<AtLocationAccount> AtLocationAccountModifiedBies { get; set; } = new List<AtLocationAccount>();

   public virtual ICollection<AtLocation> AtLocationCreatedBies { get; set; } = new List<AtLocation>();

   public virtual ICollection<AtLocation> AtLocationModifiedBies { get; set; } = new List<AtLocation>();

   public virtual ICollection<AtLocation> AtLocationOrganizations { get; set; } = new List<AtLocation>();

   public virtual ICollection<AtPrinterDetail> AtPrinterDetailCreatedBies { get; set; } = new List<AtPrinterDetail>();

   public virtual ICollection<AtPrinterDetail> AtPrinterDetailModifiedBies { get; set; } = new List<AtPrinterDetail>();

   public virtual ICollection<AtState> AtStateCreatedBies { get; set; } = new List<AtState>();

   public virtual ICollection<AtState> AtStateModifiedBies { get; set; } = new List<AtState>();

   public virtual ICollection<AtSubCategory> AtSubCategoryCreatedBies { get; set; } = new List<AtSubCategory>();

   public virtual ICollection<AtSubCategory> AtSubCategoryModifiedBies { get; set; } = new List<AtSubCategory>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtDepartment? Department { get; set; }

    public virtual AtDivision? Division { get; set; }

   public virtual ICollection<AtAccount> InverseCreatedBy { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAccount> InverseModifiedBy { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAccount> InverseOrganization { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAccount> InverseOwner { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAccount> InverseSubOwner { get; set; } = new List<AtAccount>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtAccount? Owner { get; set; }

    public virtual AtCoreRole? Role { get; set; }

    public virtual AtCore? Source { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtAccount? SubOwner { get; set; }
}
