﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreAppVersion
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AppId { get; set; }

    public string VersionId { get; set; } = null!;

    public string ApiKey { get; set; } = null!;

    public string PublicKey { get; set; } = null!;

    public string PrivateKey { get; set; } = null!;

    public string SystemPublicKey { get; set; } = null!;

    public string SystemPrivateKey { get; set; } = null!;

    public long RequestCount { get; set; }

    public DateTime? LastRequestTime { get; set; }

    public string? Comment { get; set; }

    public int StatusId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedbyId { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifybyId { get; set; }

    public virtual AtCoreApp App { get; set; } = null!;

   public virtual ICollection<AtCoreAccountDevice> AtCoreAccountDevices { get; set; } = new List<AtCoreAccountDevice>();

    public virtual AtAccount Createdby { get; set; } = null!;

    public virtual AtAccount? Modifyby { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
