﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetReturnDetail
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AssetId { get; set; }

    public DateTime ReturnDate { get; set; }

    public long ReturnBy { get; set; }

    public string? ReturnCondition { get; set; }

    public string? ReturnNotes { get; set; }

    public long ReturnTo { get; set; }

    public long? ReturnApprovedBy { get; set; }

    public DateTime? ReturnApprovedDate { get; set; }

    public int ReturnQuantity { get; set; }

    public decimal? ReturnValue { get; set; }

    public string ReturnReference { get; set; } = null!;

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public string? Comment { get; set; }

    public virtual AtAssetRegister Asset { get; set; } = null!;

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? ReturnApprovedByNavigation { get; set; }

    public virtual AtAccount ReturnByNavigation { get; set; } = null!;

    public virtual AtAccount ReturnToNavigation { get; set; } = null!;

    public virtual AtCore? Status { get; set; }
}
