﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetLocation
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long AssetId { get; set; }

    public long DivisionId { get; set; }

    public long LocationId { get; set; }

    public long SubLocationId { get; set; }

    public int StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public virtual AtAssetRegister Asset { get; set; } = null!;

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtDivision Division { get; set; } = null!;

    public virtual AtLocation Location { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore Status { get; set; } = null!;

    public virtual AtLocation SubLocation { get; set; } = null!;
}
