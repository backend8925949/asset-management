﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAccountAuth
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long UserId { get; set; }

    public string UserName { get; set; } = null!;

    public string Password { get; set; } = null!;

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtAccount User { get; set; } = null!;
}
