﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetDepreciation
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OrganizationId { get; set; }

    public int? Year { get; set; }

    public decimal? AssetValue { get; set; }

    public decimal? DepreciationExpense { get; set; }

    public decimal? AccumulatedDepreciation { get; set; }

    public decimal? BookValue { get; set; }

    public int? StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public int? DepreciationTypeId { get; set; }

    public int? Month { get; set; }

    public DateTime? Date { get; set; }

    public decimal? OpeningBookValue { get; set; }

    public long? AssetId { get; set; }

    public virtual AtAssetRegister? Asset { get; set; }

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtCore? DepreciationType { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount Organization { get; set; } = null!;

    public virtual AtCore? Status { get; set; }
}
