﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtDepreciationConfiguration
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public int? DepreciationMethodId { get; set; }

    public byte? IsFromReceivedDate { get; set; }

    public byte? IsFromIssuedDate { get; set; }

    public byte? IsWithSalvageValue { get; set; }

    public byte? IsWithActualValue { get; set; }

    public int StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public virtual AtAccount? Account { get; set; }

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtCore? DepreciationMethod { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
