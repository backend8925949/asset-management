﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAccountSession
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public int? PlatformId { get; set; }

    public long? AccountId { get; set; }

    public long AppVersionId { get; set; }

    public DateTime LoginDate { get; set; }

    public DateTime LastActivityDate { get; set; }

    public DateTime? LogoutDate { get; set; }

    public string? IPAddress { get; set; }

    public decimal? Latitude { get; set; }

    public decimal? Longitude { get; set; }

    public int StatusId { get; set; }

    public string? NotificationUrl { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifybyId { get; set; }

    public virtual AtAccount? Account { get; set; }

    public virtual AtAccount? Modifyby { get; set; }

    public virtual AtCore? Platform { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
