﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetDepreciationConfiguration
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OrganizationId { get; set; }

    public int DepreciationMethodId { get; set; }

    public DateTime? AssetPurchaseDate { get; set; }

    public decimal? CostOfAcquisition { get; set; }

    public int? UsefulLife { get; set; }

    public decimal? SalvageValue { get; set; }

    public decimal? DepreciationRate { get; set; }

    public decimal? AccumulatedDepreciation { get; set; }

    public decimal? BookValue { get; set; }

    public decimal? DepreciationExpense { get; set; }

    public decimal? NetBookValue { get; set; }

    public DateTime? DepreciationStartDate { get; set; }

    public int? StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public long? AssetId { get; set; }

    public virtual AtAssetRegister? Asset { get; set; }

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtCore DepreciationMethod { get; set; } = null!;

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount Organization { get; set; } = null!;

    public virtual AtCore? Status { get; set; }
}
