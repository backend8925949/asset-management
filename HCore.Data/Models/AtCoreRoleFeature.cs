﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreRoleFeature
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? RoleId { get; set; }

    public long? FeatureId { get; set; }

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public byte? IsViewAll { get; set; }

    public byte? IsAdd { get; set; }

    public byte? IsView { get; set; }

    public byte? IsDelete { get; set; }

    public byte? IsUpdate { get; set; }

    public int StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifiedById { get; set; }

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtCoreFeature? Feature { get; set; }

   public virtual ICollection<AtCoreRoleFeature> InverseParent { get; set; } = new List<AtCoreRoleFeature>();

   public virtual ICollection<AtCoreRoleFeature> InverseSubParent { get; set; } = new List<AtCoreRoleFeature>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCoreRoleFeature? Parent { get; set; }

    public virtual AtCoreRole? Role { get; set; }

    public virtual AtCore Status { get; set; } = null!;

    public virtual AtCoreRoleFeature? SubParent { get; set; }
}
