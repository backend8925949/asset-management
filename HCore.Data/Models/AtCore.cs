﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCore
{
    public int Id { get; set; }

    public string? Guid { get; set; }

    public string Name { get; set; } = null!;

    public int? ParentId { get; set; }

    public int? SubParentId { get; set; }

    public string SystemName { get; set; } = null!;

    public int? TypeId { get; set; }

    public int? StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

   public virtual ICollection<AtAccount> AtAccountAccountTypes { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAccountAuth> AtAccountAuths { get; set; } = new List<AtAccountAuth>();

   public virtual ICollection<AtAccountEmailConfiguration> AtAccountEmailConfigurations { get; set; } = new List<AtAccountEmailConfiguration>();

   public virtual ICollection<AtAccountSession> AtAccountSessionPlatforms { get; set; } = new List<AtAccountSession>();

   public virtual ICollection<AtAccountSession> AtAccountSessionStatuses { get; set; } = new List<AtAccountSession>();

   public virtual ICollection<AtAccount> AtAccountSources { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAccount> AtAccountStatuses { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAddress> AtAddresses { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAssetAmcDetail> AtAssetAmcDetails { get; set; } = new List<AtAssetAmcDetail>();

   public virtual ICollection<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurationDepreciationMethods { get; set; } = new List<AtAssetDepreciationConfiguration>();

   public virtual ICollection<AtAssetDepreciationConfiguration> AtAssetDepreciationConfigurationStatuses { get; set; } = new List<AtAssetDepreciationConfiguration>();

   public virtual ICollection<AtAssetDepreciation> AtAssetDepreciationDepreciationTypes { get; set; } = new List<AtAssetDepreciation>();

   public virtual ICollection<AtAssetDepreciation> AtAssetDepreciationStatuses { get; set; } = new List<AtAssetDepreciation>();

   public virtual ICollection<AtAssetInsuranceDetail> AtAssetInsuranceDetails { get; set; } = new List<AtAssetInsuranceDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailSources { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailStatuses { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetLocation> AtAssetLocations { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenanceMaintenanceTypeNavigations { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetMaintenance> AtAssetMaintenanceStatuses { get; set; } = new List<AtAssetMaintenance>();

   public virtual ICollection<AtAssetRegister> AtAssetRegisterAssetTypes { get; set; } = new List<AtAssetRegister>();

   public virtual ICollection<AtAssetRegister> AtAssetRegisterStatuses { get; set; } = new List<AtAssetRegister>();

   public virtual ICollection<AtAssetRequisition> AtAssetRequisitions { get; set; } = new List<AtAssetRequisition>();

   public virtual ICollection<AtAssetReturnDetail> AtAssetReturnDetails { get; set; } = new List<AtAssetReturnDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailSources { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailStatuses { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetVendorDetail> AtAssetVendorDetails { get; set; } = new List<AtAssetVendorDetail>();

   public virtual ICollection<AtCategory> AtCategories { get; set; } = new List<AtCategory>();

   public virtual ICollection<AtCity> AtCities { get; set; } = new List<AtCity>();

   public virtual ICollection<AtClass> AtClasses { get; set; } = new List<AtClass>();

   public virtual ICollection<AtCoreAccountDevice> AtCoreAccountDevices { get; set; } = new List<AtCoreAccountDevice>();

   public virtual ICollection<AtCoreAppVersion> AtCoreAppVersions { get; set; } = new List<AtCoreAppVersion>();

   public virtual ICollection<AtCoreApp> AtCoreApps { get; set; } = new List<AtCoreApp>();

   public virtual ICollection<AtCoreFeature> AtCoreFeatureHelpers { get; set; } = new List<AtCoreFeature>();

   public virtual ICollection<AtCoreFeature> AtCoreFeatureStatuses { get; set; } = new List<AtCoreFeature>();

   public virtual ICollection<AtCoreFeature> AtCoreFeatureTypes { get; set; } = new List<AtCoreFeature>();

   public virtual ICollection<AtCoreRoleFeature> AtCoreRoleFeatures { get; set; } = new List<AtCoreRoleFeature>();

   public virtual ICollection<AtCoreRole> AtCoreRoles { get; set; } = new List<AtCoreRole>();

   public virtual ICollection<AtCoreStorage> AtCoreStorages { get; set; } = new List<AtCoreStorage>();

   public virtual ICollection<AtCountry> AtCountries { get; set; } = new List<AtCountry>();

   public virtual ICollection<AtDepartment> AtDepartments { get; set; } = new List<AtDepartment>();

   public virtual ICollection<AtDepreciationConfiguration> AtDepreciationConfigurationDepreciationMethods { get; set; } = new List<AtDepreciationConfiguration>();

   public virtual ICollection<AtDepreciationConfiguration> AtDepreciationConfigurationStatuses { get; set; } = new List<AtDepreciationConfiguration>();

   public virtual ICollection<AtDivision> AtDivisions { get; set; } = new List<AtDivision>();

   public virtual ICollection<AtLocationAccount> AtLocationAccounts { get; set; } = new List<AtLocationAccount>();

   public virtual ICollection<AtLocation> AtLocations { get; set; } = new List<AtLocation>();

   public virtual ICollection<AtPrinterDetail> AtPrinterDetails { get; set; } = new List<AtPrinterDetail>();

   public virtual ICollection<AtState> AtStates { get; set; } = new List<AtState>();

   public virtual ICollection<AtSubCategory> AtSubCategories { get; set; } = new List<AtSubCategory>();

    public virtual AtAccount? CreatedBy { get; set; }

   public virtual ICollection<AtCore> InverseParent { get; set; } = new List<AtCore>();

   public virtual ICollection<AtCore> InverseStatus { get; set; } = new List<AtCore>();

   public virtual ICollection<AtCore> InverseSubParent { get; set; } = new List<AtCore>();

   public virtual ICollection<AtCore> InverseType { get; set; } = new List<AtCore>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtCore? Parent { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtCore? SubParent { get; set; }

    public virtual AtCore? Type { get; set; }
}
