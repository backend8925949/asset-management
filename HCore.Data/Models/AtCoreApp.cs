﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtCoreApp
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? AccountId { get; set; }

    public string Name { get; set; } = null!;

    public string SystemName { get; set; } = null!;

    public string? Description { get; set; }

    public string? AppKey { get; set; }

    public string? IPAddress { get; set; }

    public byte AllowLogging { get; set; }

    public int StatusId { get; set; }

    public DateTime CreateDate { get; set; }

    public long CreatedbyId { get; set; }

    public DateTime? ModifyDate { get; set; }

    public long? ModifybyId { get; set; }

    public virtual AtAccount? Account { get; set; }

   public virtual ICollection<AtCoreAccountDevice> AtCoreAccountDevices { get; set; } = new List<AtCoreAccountDevice>();

   public virtual ICollection<AtCoreAppVersion> AtCoreAppVersions { get; set; } = new List<AtCoreAppVersion>();

    public virtual AtAccount Createdby { get; set; } = null!;

    public virtual AtAccount? Modifyby { get; set; }

    public virtual AtCore Status { get; set; } = null!;
}
