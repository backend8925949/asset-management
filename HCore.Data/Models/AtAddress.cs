﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAddress
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public long AccountId { get; set; }

    public long? OrganizationId { get; set; }

    public long? DepartmentId { get; set; }

    public long? DivisionId { get; set; }

    public string? MobileNumber { get; set; }

    public string? EmailAddress { get; set; }

    public string Address { get; set; } = null!;

    public int CountryId { get; set; }

    public long StateId { get; set; }

    public long CityId { get; set; }

    public decimal? Latitude { get; set; }

    public decimal? Longitude { get; set; }

    public int? StatusId { get; set; }

    public DateTime CreatedDate { get; set; }

    public long CreatedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? ModifiedById { get; set; }

    public virtual AtAccount Account { get; set; } = null!;

    public virtual AtCity City { get; set; } = null!;

    public virtual AtCountry Country { get; set; } = null!;

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtDepartment? Department { get; set; }

    public virtual AtDivision? Division { get; set; }

   public virtual ICollection<AtAddress> InverseParent { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAddress> InverseSubParent { get; set; } = new List<AtAddress>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtAddress? Parent { get; set; }

    public virtual AtState State { get; set; } = null!;

    public virtual AtCore? Status { get; set; }

    public virtual AtAddress? SubParent { get; set; }
}
