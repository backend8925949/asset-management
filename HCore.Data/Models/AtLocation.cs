﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtLocation
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public string LocationName { get; set; } = null!;

    public long? OrganizationId { get; set; }

    public long? DivisionId { get; set; }

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailLocations { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetailSubLocations { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetLocation> AtAssetLocationLocations { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetLocation> AtAssetLocationSubLocations { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailLocations { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetailSubLocations { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtLocationAccount> AtLocationAccountLocations { get; set; } = new List<AtLocationAccount>();

   public virtual ICollection<AtLocationAccount> AtLocationAccountSubLocations { get; set; } = new List<AtLocationAccount>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

    public virtual AtDivision? Division { get; set; }

   public virtual ICollection<AtLocation> InverseParent { get; set; } = new List<AtLocation>();

   public virtual ICollection<AtLocation> InverseSubParent { get; set; } = new List<AtLocation>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtLocation? Parent { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtLocation? SubParent { get; set; }
}
