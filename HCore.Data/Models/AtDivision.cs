﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtDivision
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long OrganizationId { get; set; }

    public long? ParentId { get; set; }

    public long? SubParentId { get; set; }

    public string DivisionName { get; set; } = null!;

    public int? StatusId { get; set; }

    public long CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

   public virtual ICollection<AtAccount> AtAccounts { get; set; } = new List<AtAccount>();

   public virtual ICollection<AtAddress> AtAddresses { get; set; } = new List<AtAddress>();

   public virtual ICollection<AtAssetIssueDetail> AtAssetIssueDetails { get; set; } = new List<AtAssetIssueDetail>();

   public virtual ICollection<AtAssetLocation> AtAssetLocations { get; set; } = new List<AtAssetLocation>();

   public virtual ICollection<AtAssetTransferDetail> AtAssetTransferDetails { get; set; } = new List<AtAssetTransferDetail>();

   public virtual ICollection<AtDepartment> AtDepartments { get; set; } = new List<AtDepartment>();

   public virtual ICollection<AtLocation> AtLocations { get; set; } = new List<AtLocation>();

    public virtual AtAccount CreatedBy { get; set; } = null!;

   public virtual ICollection<AtDivision> InverseParent { get; set; } = new List<AtDivision>();

   public virtual ICollection<AtDivision> InverseSubParent { get; set; } = new List<AtDivision>();

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount Organization { get; set; } = null!;

    public virtual AtDivision? Parent { get; set; }

    public virtual AtCore? Status { get; set; }

    public virtual AtDivision? SubParent { get; set; }
}
