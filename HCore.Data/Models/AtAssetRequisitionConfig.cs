﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Models;

public partial class AtAssetRequisitionConfig
{
    public int Id { get; set; }

    public string Guid { get; set; } = null!;

    public long RoleId { get; set; }

    public byte? IssueRequest { get; set; }

    public byte? IssueApproval { get; set; }

    public byte? TransferRequest { get; set; }

    public byte? TransferApproval { get; set; }

    public byte? ReturnRequest { get; set; }

    public byte? ReturnApproval { get; set; }

    public byte? MaintenanceRequest { get; set; }

    public byte? MaintenanceApproval { get; set; }

    public byte? PoRequest { get; set; }

    public byte? PoApproval { get; set; }

    public long? CreatedById { get; set; }

    public DateTime CreatedDate { get; set; }

    public long? ModifiedById { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public long? OrganizationId { get; set; }

    public virtual AtAccount? CreatedBy { get; set; }

    public virtual AtAccount? ModifiedBy { get; set; }

    public virtual AtAccount? Organization { get; set; }

    public virtual AtCoreRole Role { get; set; } = null!;
}
