﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetDisposeLog
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public string? AssetBarcode { get; set; }

    public string? Comment { get; set; }

    public DateTime? DisposedDate { get; set; }

    public long? DisposedById { get; set; }
}
