﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetHistory
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public string? AssetBarcode { get; set; }

    public string? Status { get; set; }

    public DateTime? ActivityDate { get; set; }

    public string? Activity { get; set; }

    public long? UserId { get; set; }

    public long? OrganizationId { get; set; }
}
