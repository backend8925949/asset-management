﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetMappingLog
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Organization { get; set; } = null!;

    public string Asset { get; set; } = null!;

    public string RefId { get; set; } = null!;

    public string EpcId { get; set; } = null!;

    public long CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }
}
