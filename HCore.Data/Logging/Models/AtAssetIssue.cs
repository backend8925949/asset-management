﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetIssue
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Asset { get; set; } = null!;

    public string Organization { get; set; } = null!;

    public string Division { get; set; } = null!;

    public string? Department { get; set; }

    public string? Location { get; set; }

    public string? SubLocation { get; set; }

    public string? Employee { get; set; }

    public string? Remark { get; set; }

    public int? Quantity { get; set; }

    public int? Status { get; set; }

    public long CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string? ReceiptNo { get; set; }

    public DateTime? ReceiptDate { get; set; }

    public string? Comment { get; set; }
}
