﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtCoreLog
{
    public long Id { get; set; }

    public string Title { get; set; } = null!;

    public string Message { get; set; } = null!;

    public string HostName { get; set; } = null!;

    public string? RequestReference { get; set; }

    public string? Data { get; set; }

    public DateTime? CreateDate { get; set; }
}
