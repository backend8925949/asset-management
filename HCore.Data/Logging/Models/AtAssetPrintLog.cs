﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetPrintLog
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public long? OrganizationId { get; set; }

    public long AssetId { get; set; }

    public string AssetBarcode { get; set; } = null!;

    public string? ExAssetId { get; set; }

    public DateTime PrintDate { get; set; }

    public DateTime? ReprintDate { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public string? Status { get; set; }
}
