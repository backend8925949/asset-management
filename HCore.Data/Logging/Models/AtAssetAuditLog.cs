﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetAuditLog
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public string? AssetBarcode { get; set; }

    public string? Employee { get; set; }

    public string? Department { get; set; }

    public string? SubLocation { get; set; }

    public string? Location { get; set; }

    public string? Division { get; set; }

    public string? AssetStatus { get; set; }

    public string? AuditStatus { get; set; }

    public DateTime? AuditDate { get; set; }

    public long? UserId { get; set; }

    public long? OrganizationId { get; set; }

    public string? AuditDivision { get; set; }

    public string? AuditLocation { get; set; }

    public string? AuditSubLocation { get; set; }
}
