﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetUpload
{
    public long Id { get; set; }

    public string? Guid { get; set; }

    public long? OrganizationId { get; set; }

    public string? AssetType { get; set; }

    public string? AssetBarcode { get; set; }

    public string? AssetClass { get; set; }

    public string? AssetCategory { get; set; }

    public string? AssetSubCategory { get; set; }

    public string? AssetName { get; set; }

    public string? ExAssetId { get; set; }

    public string? AssetMake { get; set; }

    public string? AssetModel { get; set; }

    public string? AssetSerialNo { get; set; }

    public int? TotalQuantity { get; set; }

    public string? Division { get; set; }

    public string? Location { get; set; }

    public string? SubLocation { get; set; }

    public string? AssetVendor { get; set; }

    public string? PoNumber { get; set; }

    public string? InvoiceNumber { get; set; }

    public DateTime? ReceivedDate { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public DateTime? ExpiryDate { get; set; }

    public string? Currency { get; set; }

    public decimal? PurchasePrice { get; set; }

    public byte? IsAmc { get; set; }

    public byte? IsInsurance { get; set; }

    public int? StatusId { get; set; }

    public DateTime? CreatedDate { get; set; }

    public long? CreatedById { get; set; }

    public int? ExpiresIn { get; set; }

    public decimal? AdditionalPrice { get; set; }

    public decimal? SalvageValue { get; set; }

    public string? Comment { get; set; }
}
