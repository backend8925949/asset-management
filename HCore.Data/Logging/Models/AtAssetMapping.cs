﻿using System;
using System.Collections.Generic;

namespace HCore.Data.Logging.Models;

public partial class AtAssetMapping
{
    public long Id { get; set; }

    public string Guid { get; set; } = null!;

    public string Asset { get; set; } = null!;

    public string RefId { get; set; } = null!;

    public string EpcId { get; set; } = null!;

    public int? Status { get; set; }

    public long CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string Organization { get; set; } = null!;
}
