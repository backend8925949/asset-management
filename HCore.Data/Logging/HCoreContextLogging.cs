﻿using System;
using System.Collections.Generic;
using HCore.Data.Logging.Models;
using Microsoft.EntityFrameworkCore;

namespace HCore.Data.Logging;

public partial class HCoreContextLogging : DbContext
{
    private readonly string ConnectionString;
    public HCoreContextLogging()
    {
        this.ConnectionString = Helper.HostHelper.HostLogging;
    }
    public HCoreContextLogging(string ConnectionString) : base()
    {
        this.ConnectionString = ConnectionString;
    }
    public HCoreContextLogging(DbContextOptions<HCoreContextLogging> options) : base(options)
    {
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
    }

    public virtual DbSet<AtAssetAuditLog> AtAssetAuditLogs { get; set; }

    public virtual DbSet<AtAssetDisposeLog> AtAssetDisposeLogs { get; set; }

    public virtual DbSet<AtAssetHistory> AtAssetHistories { get; set; }

    public virtual DbSet<AtAssetIssue> AtAssetIssues { get; set; }

    public virtual DbSet<AtAssetMapping> AtAssetMappings { get; set; }

    public virtual DbSet<AtAssetMappingLog> AtAssetMappingLogs { get; set; }

    public virtual DbSet<AtAssetPrintLog> AtAssetPrintLogs { get; set; }

    public virtual DbSet<AtAssetTransfer> AtAssetTransfers { get; set; }

    public virtual DbSet<AtAssetUpload> AtAssetUploads { get; set; }

    public virtual DbSet<AtCoreLog> AtCoreLogs { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AtAssetAuditLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83FCFCBD177");

            entity.ToTable("at_asset_audit_log");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5A877F99C").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AssetBarcode)
                .HasMaxLength(64)
                .HasColumnName("asset_barcode");
            entity.Property(e => e.AssetStatus)
                .HasMaxLength(256)
                .HasColumnName("asset_status");
            entity.Property(e => e.AuditDate)
                .HasColumnType("datetime")
                .HasColumnName("audit_date");
            entity.Property(e => e.AuditDivision)
                .HasMaxLength(256)
                .HasColumnName("audit_division");
            entity.Property(e => e.AuditLocation)
                .HasMaxLength(256)
                .HasColumnName("audit_location");
            entity.Property(e => e.AuditStatus)
                .HasMaxLength(1024)
                .HasColumnName("audit_status");
            entity.Property(e => e.AuditSubLocation)
                .HasMaxLength(256)
                .HasColumnName("audit_sub_location");
            entity.Property(e => e.Department)
                .HasMaxLength(256)
                .HasColumnName("department");
            entity.Property(e => e.Division)
                .HasMaxLength(256)
                .HasColumnName("division");
            entity.Property(e => e.Employee)
                .HasMaxLength(256)
                .HasColumnName("employee");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Location)
                .HasMaxLength(256)
                .HasColumnName("location");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.SubLocation)
                .HasMaxLength(256)
                .HasColumnName("sub_location");
            entity.Property(e => e.UserId).HasColumnName("user_id");
        });

        modelBuilder.Entity<AtAssetDisposeLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F36BCB2B3");

            entity.ToTable("at_asset_dispose_log");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB58C08EFD0").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AssetBarcode)
                .HasMaxLength(64)
                .HasColumnName("asset_barcode");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.DisposedById).HasColumnName("disposed_by_id");
            entity.Property(e => e.DisposedDate)
                .HasColumnType("datetime")
                .HasColumnName("disposed_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
        });

        modelBuilder.Entity<AtAssetHistory>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F45013B60");

            entity.ToTable("at_asset_history");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB541925FB5").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Activity).HasColumnName("activity");
            entity.Property(e => e.ActivityDate)
                .HasColumnType("datetime")
                .HasColumnName("activity_date");
            entity.Property(e => e.AssetBarcode)
                .HasMaxLength(64)
                .HasColumnName("asset_barcode");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.Status)
                .HasMaxLength(1024)
                .HasColumnName("status");
            entity.Property(e => e.UserId).HasColumnName("user_id");
        });

        modelBuilder.Entity<AtAssetIssue>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F86B4D993");

            entity.ToTable("at_asset_issue");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5A03D6DE2").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Asset)
                .HasMaxLength(64)
                .HasColumnName("asset");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedBy).HasColumnName("created_by");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Department)
                .HasMaxLength(64)
                .HasColumnName("department");
            entity.Property(e => e.Division)
                .HasMaxLength(64)
                .HasColumnName("division");
            entity.Property(e => e.Employee)
                .HasMaxLength(64)
                .HasColumnName("employee");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Location)
                .HasMaxLength(64)
                .HasColumnName("location");
            entity.Property(e => e.Organization)
                .HasMaxLength(64)
                .HasColumnName("organization");
            entity.Property(e => e.Quantity).HasColumnName("quantity");
            entity.Property(e => e.ReceiptDate)
                .HasColumnType("datetime")
                .HasColumnName("receipt_date");
            entity.Property(e => e.ReceiptNo)
                .HasMaxLength(64)
                .HasColumnName("receipt_no");
            entity.Property(e => e.Remark)
                .HasMaxLength(1024)
                .HasColumnName("remark");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.SubLocation)
                .HasMaxLength(64)
                .HasColumnName("sub_location");
        });

        modelBuilder.Entity<AtAssetMapping>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83FE021982C");

            entity.ToTable("at_asset_mapping");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5CC59429B").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Asset)
                .HasMaxLength(64)
                .HasColumnName("asset");
            entity.Property(e => e.CreatedBy).HasColumnName("created_by");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.EpcId)
                .HasMaxLength(64)
                .HasColumnName("epc_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Organization)
                .HasMaxLength(64)
                .HasColumnName("organization");
            entity.Property(e => e.RefId)
                .HasMaxLength(64)
                .HasColumnName("ref_id");
            entity.Property(e => e.Status).HasColumnName("status");
        });

        modelBuilder.Entity<AtAssetMappingLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F9CEE809A");

            entity.ToTable("at_asset_mapping_log");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5E476DD61").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Asset)
                .HasMaxLength(64)
                .HasColumnName("asset");
            entity.Property(e => e.CreatedBy).HasColumnName("created_by");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.EpcId)
                .HasMaxLength(64)
                .HasColumnName("epc_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Organization)
                .HasMaxLength(64)
                .HasColumnName("organization");
            entity.Property(e => e.RefId)
                .HasMaxLength(64)
                .HasColumnName("ref_id");
        });

        modelBuilder.Entity<AtAssetPrintLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F52181FC8");

            entity.ToTable("at_asset_print_log");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5D1AC6E06").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AssetBarcode)
                .HasMaxLength(64)
                .HasColumnName("asset_barcode");
            entity.Property(e => e.AssetId).HasColumnName("asset_id");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.ExAssetId)
                .HasMaxLength(64)
                .HasColumnName("ex_asset_id");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.PrintDate)
                .HasColumnType("datetime")
                .HasColumnName("print_date");
            entity.Property(e => e.ReprintDate)
                .HasColumnType("datetime")
                .HasColumnName("reprint_date");
            entity.Property(e => e.Status)
                .HasMaxLength(64)
                .HasColumnName("status");
        });

        modelBuilder.Entity<AtAssetTransfer>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F16B8E6FE");

            entity.ToTable("at_asset_transfer");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB5AD4E9ADA").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Asset)
                .HasMaxLength(64)
                .HasColumnName("asset");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedBy).HasColumnName("created_by");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Department)
                .HasMaxLength(64)
                .HasColumnName("department");
            entity.Property(e => e.Division)
                .HasMaxLength(64)
                .HasColumnName("division");
            entity.Property(e => e.Employee)
                .HasMaxLength(64)
                .HasColumnName("employee");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.Location)
                .HasMaxLength(64)
                .HasColumnName("location");
            entity.Property(e => e.Organization)
                .HasMaxLength(64)
                .HasColumnName("organization");
            entity.Property(e => e.ReceiptDate)
                .HasColumnType("datetime")
                .HasColumnName("receipt_date");
            entity.Property(e => e.ReceiptNo)
                .HasMaxLength(64)
                .HasColumnName("receipt_no");
            entity.Property(e => e.Remark)
                .HasMaxLength(1024)
                .HasColumnName("remark");
            entity.Property(e => e.Status).HasColumnName("status");
            entity.Property(e => e.SubLocation)
                .HasMaxLength(64)
                .HasColumnName("sub_location");
        });

        modelBuilder.Entity<AtAssetUpload>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__at_asset__3213E83F05419995");

            entity.ToTable("at_asset_upload");

            entity.HasIndex(e => e.Guid, "UQ__at_asset__497F6CB563AD4557").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.AdditionalPrice)
                .HasColumnType("money")
                .HasColumnName("additional_price");
            entity.Property(e => e.AssetBarcode)
                .HasMaxLength(64)
                .HasColumnName("asset_barcode");
            entity.Property(e => e.AssetCategory)
                .HasMaxLength(64)
                .HasColumnName("asset_category");
            entity.Property(e => e.AssetClass)
                .HasMaxLength(64)
                .HasColumnName("asset_class");
            entity.Property(e => e.AssetMake)
                .HasMaxLength(256)
                .HasColumnName("asset_make");
            entity.Property(e => e.AssetModel)
                .HasMaxLength(256)
                .HasColumnName("asset_model");
            entity.Property(e => e.AssetName)
                .HasMaxLength(256)
                .HasColumnName("asset_name");
            entity.Property(e => e.AssetSerialNo)
                .HasMaxLength(24)
                .HasColumnName("asset_serial_no");
            entity.Property(e => e.AssetSubCategory)
                .HasMaxLength(64)
                .HasColumnName("asset_sub_category");
            entity.Property(e => e.AssetType)
                .HasMaxLength(64)
                .HasColumnName("asset_type");
            entity.Property(e => e.AssetVendor)
                .HasMaxLength(64)
                .HasColumnName("asset_vendor");
            entity.Property(e => e.Comment)
                .HasMaxLength(1024)
                .HasColumnName("comment");
            entity.Property(e => e.CreatedById).HasColumnName("created_by_id");
            entity.Property(e => e.CreatedDate)
                .HasColumnType("datetime")
                .HasColumnName("created_date");
            entity.Property(e => e.Currency)
                .HasMaxLength(64)
                .HasColumnName("currency");
            entity.Property(e => e.Division)
                .HasMaxLength(64)
                .HasColumnName("division");
            entity.Property(e => e.EndDate)
                .HasColumnType("datetime")
                .HasColumnName("end_date");
            entity.Property(e => e.ExAssetId)
                .HasMaxLength(64)
                .HasColumnName("ex_asset_id");
            entity.Property(e => e.ExpiresIn).HasColumnName("expires_in");
            entity.Property(e => e.ExpiryDate)
                .HasColumnType("datetime")
                .HasColumnName("expiry_date");
            entity.Property(e => e.Guid)
                .HasMaxLength(64)
                .HasColumnName("guid");
            entity.Property(e => e.InvoiceNumber)
                .HasMaxLength(64)
                .HasColumnName("invoice_number");
            entity.Property(e => e.IsAmc).HasColumnName("is_amc");
            entity.Property(e => e.IsInsurance).HasColumnName("is_insurance");
            entity.Property(e => e.Location)
                .HasMaxLength(64)
                .HasColumnName("location");
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id");
            entity.Property(e => e.PoNumber)
                .HasMaxLength(64)
                .HasColumnName("po_number");
            entity.Property(e => e.PurchasePrice)
                .HasColumnType("money")
                .HasColumnName("purchase_price");
            entity.Property(e => e.ReceivedDate)
                .HasColumnType("datetime")
                .HasColumnName("received_date");
            entity.Property(e => e.SalvageValue)
                .HasColumnType("money")
                .HasColumnName("salvage_value");
            entity.Property(e => e.StartDate)
                .HasColumnType("datetime")
                .HasColumnName("start_date");
            entity.Property(e => e.StatusId).HasColumnName("status_id");
            entity.Property(e => e.SubLocation)
                .HasMaxLength(64)
                .HasColumnName("sub_location");
            entity.Property(e => e.TotalQuantity).HasColumnName("total_quantity");
        });

        modelBuilder.Entity<AtCoreLog>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK_CORE_LOG_ID");

            entity.ToTable("at_core_log");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreateDate)
                .HasColumnType("datetime")
                .HasColumnName("create_date");
            entity.Property(e => e.Data).HasColumnName("data");
            entity.Property(e => e.HostName)
                .HasMaxLength(128)
                .HasColumnName("host_name");
            entity.Property(e => e.Message)
                .HasMaxLength(1024)
                .HasColumnName("message");
            entity.Property(e => e.RequestReference).HasColumnName("request_reference");
            entity.Property(e => e.Title)
                .HasMaxLength(512)
                .HasColumnName("title");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
