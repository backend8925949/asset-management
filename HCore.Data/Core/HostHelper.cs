using Microsoft.Extensions.Options;

namespace HCore.Data.Helper
{
    public class HostHelper
    {
        public static string Host = "";
        public static string HostLogging = "";
        public static string HostOperations = "";
        public static string HostIntegration = "";

    }
}

#region Master
// MASTER
// dotnet ef dbcontext scaffold "Server=LAPTOP-KPCBADOI;User Id=sa;Password=rbs1;Database=asset_tracking_master;Trusted_Connection=True;TrustServerCertificate=True;" Microsoft.EntityFrameworkCore.SqlServer -c HCoreContext --output-dir Models
// dotnet ef dbcontext scaffold "Server=P3NWPLSK12SQL-v02.shr.prod.phx3.secureserver.net;User Id=Rajkamal;Password=S7c350bk_;Database=core_master;TrustServerCertificate=True;" Microsoft.EntityFrameworkCore.SqlServer -c HCoreContext --output-dir Models

//private readonly string ConnectionString;
//public HCoreContext()
//{
//    this.ConnectionString = Helper.HostHelper.Host;
//}
//public HCoreContext(string ConnectionString) : base()
//        {
//    this.ConnectionString = ConnectionString;
//}
//public HCoreContext(DbContextOptions<HCoreContext> options) : base(options)
//        {
//
//        }
//
//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//{
//    if (!optionsBuilder.IsConfigured)
//    {
//              optionsBuilder.UseSqlServer(ConnectionString);
//    }
//}
#endregion

#region Logging
// LOGGING
// dotnet ef dbcontext scaffold "Server=LAPTOP-KPCBADOI;User Id=sa;Password=rbs1;Database=asset_tracking_logging;Trusted_Connection=True;TrustServerCertificate=True;" Microsoft.EntityFrameworkCore.SqlServer -c HCoreContextLogging --context-dir Logging --output-dir Logging/Models
// dotnet ef dbcontext scaffold "Server=P3NWPLSK12SQL-v02.shr.prod.phx3.secureserver.net;User Id=Rajkamal_1;Password=S7c350bk_;Database=core_logging;TrustServerCertificate=True;" Microsoft.EntityFrameworkCore.SqlServer -c HCoreContextLogging --context-dir Logging --output-dir Logging/Models

//private readonly string ConnectionString;
//public HCoreContextLogging()
//{
//    this.ConnectionString = Helper.HostHelper.HostLogging;
//}
//public HCoreContextLogging(string ConnectionString) : base()
//    {
//    this.ConnectionString = ConnectionString;
//}
//public HCoreContextLogging(DbContextOptions<HCoreContextLogging> options) : base(options)
//    {
//}
//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//{
//    if (!optionsBuilder.IsConfigured)
//    {
//              optionsBuilder.UseSqlServer(ConnectionString);
//    }
//}
#endregion