//==================================================================================
// FileName: HCoreResponseCode.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

namespace HCore.Helper
{
    public class HCoreResponseCode
    {
        public static class System
        {
            ///<summary>Details already present</summary>
            public static string HC498_DetailsAlreadyPresent = "HC498";
            ///<summary>Invalid Status Code</summary>
            public static string HC499_InvalidStatusCode = "HC499";
            /// <summary>Server error occured</summary>
            public static string HC500 = "HC500";
            /// <summary> Details loaded successfully </summary>
            public static string HC000_DetailsLoaded = "HC000";
            ///<summary>Reference missing </summary>
            public static string HCREF = "HCREF";
            ///<summary>Results loaded </summary>
            public static string HC001_ListLoaded = "HC001";
            /// <summary>
            /// Details not found. It might be removed or not available at the moment. Please try after some time or contact support.
            /// </summary>
            public static string HC002_DetailsNotFound = "HC002";
            /// <summary>
            /// Details saved successfully
            /// </summary>
            public static string HC003_DetailsSaved = "HC003";
            /// <summary>
            /// Details updated successfully
            /// </summary>
            public static string HC004_DetailsUpdated = "HC004";
            /// <summary>
            /// Details deleted successfully
            /// </summary>
            public static string HC005_DetailsDeleted = "HC005";
            /// <summary>
            /// Status updated successfully
            /// </summary>
            public static string HC006_DetailsUpdate = "HC006";
            /// <summary>
            /// Name required
            /// </summary>
            public static string HC007_NameRequired = "HC007";
            /// <summary>
            ///Username required
            /// </summary>
            public static string HC008 = "HC008";
            /// <summary>
            ///Password required
            /// </summary>
            public static string HC009 = "HC009";
            /// <summary>
            ///Username / Account already exits
            /// </summary>
            public static string HC010 = "HC010";
            /// <summary>
            ///Account type code missing
            /// </summary>
            public static string HC011 = "HC011";
            /// <summary>
            /// Logout successfull
            /// </summary>
            public static string HC012 = "HC012";
            /// <summary>
            ///Account Created Successfully
            /// </summary>
            public static string HC013 = "HC013";
            /// <summary>
            ///Account status code missing
            /// </summary>
            public static string HC014 = "HC014";
            /// <summary>
            ///Registration source code missing
            /// </summary>
            public static string HC015 = "HC015";
            /// <summary>
            ///Session details not found
            /// </summary>
            public static string HC016 = "HC016";
            /// <summary>
            ///Account type code missing
            /// </summary>
            public static string HC017 = "HC017";
            /// <summary>
            ///Username missing
            /// </summary>
            public static string HC018 = "HC018";
            /// <summary>
            ///Password missing
            /// </summary>
            public static string HC019 = "HC019";
            /// <summary>
            ///Invalid account type
            /// </summary>
            public static string HC020 = "HC020";
            ///<summary>Invalid organization key</summary>
            public static string HC021 = "HC021";
            ///<summary> Invalid username or password</summary>
            public static string HC022 = "HC022";
            ///<summary>Your account is inactive or blocked. Please contact administrator</summary>
            public static string HC023 = "HC023";
            ///<summary> Invalid username or password</summary>
            public static string HC024 = "HC024";
            ///<summary> Profile section error. Please contact support</summary>
            public static string HC025 = "HC025";
            ///<summary> Login successful</summary>
            public static string HC026 = "HC026";
            ///<summary> Account Details Not Found</summary>
            public static string HC027 = "HC027";
            ///<summary> Uername required</summary>
            public static string HC028 = "HC028";
            ///<summary> Username / login id already present</summary>
            public static string HC029 = "HC029";
            ///<summary> Auth details not found</summary>
            public static string HC030 = "HC030";
            ///<summary> Username / login id updated successfully</summary>
            public static string HC031 = "HC031";
            ///<summary> Account Reference Id</summary>
            public static string HC032 = "HC032";
            ///<summary> Reference id missing</summary>
            public static string HC033 = "HC033";
            ///<summary> Invalid account</summary>
            public static string HC034 = "HC034";
            ///<summary> Invalid account reference</summary>
            public static string HC035 = "HC035";
            ///<summary> Account details not found</summary>
            public static string HC036 = "HC036";
            ///<summary> Account details updated successfully</summary>
            public static string HC037 = "HC037";
            ///<summary> Account key missing</summary>
            public static string HC038 = "HC038";
            ///<summary> App name already present for the account</summary>
            public static string HC039 = "HC039";
            ///<summary> Invalid organization key</summary>
            public static string HC040 = "HC040";
            ///<summary> Invalid platform code</summary>
            public static string HC041 = "HC041";
            ///<summary> App created successfully</summary>
            public static string HC042 = "HC042";
            ///<summary> App details updated successfully</summary>
            public static string HC043 = "HC043";
            ///<summary> Operation failed. App usage detected, Contact support to delete app</summary>
            public static string HC044 = "HC044";
            ///<summary> App deleted successfully</summary>
            public static string HC045 = "HC045";
            ///<summary> App key missing</summary>
            public static string HC046 = "HC046";
            ///<summary> App details not found. It might be removed or not available at the moment</summary>
            public static string HC047 = "HC047";
            ///<summary> App version updated</summary>
            public static string HC048 = "HC048";
            ///<summary> Operation failed. App version usage detected, Contact support to delete app</summary>
            public static string HC049 = "HC049";
            ///<summary> App version deleted</summary>
            public static string HC050 = "HC050";
            ///<summary>Account key missing</summary>
            public static string HC051 = "HC051";
            //<summary>Session key missing</summary>
            public static string HC052 = "HC052";
            //<summary>Status code missing</summary>
            public static string HC054 = "HC054";
            ///<summary> Session details not found</summary>
            public static string HC055 = "HC055";
            ///<summary>Account session updated successfully</summary>
            public static string HC056 = "HC056";
            ///<summary> Invalid account key</summary>
            public static string HC057 = "HC057";
            ///<summary> Mobile number required</summary>
            public static string HC058 = "HC058";
            ///<summary> Account available</summary>
            public static string HC059 = "HC059";
            ///<summary> Account not available</summary>
            public static string HC060 = "HC060";
            ///<summary> Device serial number missing</summary>
            public static string HC061 = "HC061";
            ///<summary> Pin missing</summary>
            public static string HC062 = "HC062";
            ///<summary> Invalid pin</summary>
            public static string HC063 = "HC063";
            ///<summary> Verification details not found</summary>
            public static string HC064 = "HC064";
            ///<summary> RoleName not found</summary>
            public static string HC065 = "HC065";
            ///<summary> CountryName is not valid</summary>
            public static string HC066 = "HC066";
            ///<summary> Region Name is not valid</summary>
            public static string HC067 = "HC067";
            ///<summary> RegionArea Name is not valid</summary>
            public static string HC068 = "HC068";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";
            ///<summary> xxx</summary>
            //public static string HC054 = "HC054";


            public const string HCC100 = "Invalid status code";
            public const string HCC101 = "Configuration name required";
            public const string HCC102 = "Configuration system name required";
            public const string HCC103 = "Configuration description required";
            public const string HCC104 = "Configuration value required";
            public const string HCC105 = "Configuration data type required";
            public const string HCC106 = "Configuration already exists";
            public const string HCC107 = "Configuration added successfully";
            public const string HCC108 = "Configuration key missing";
            public const string HCC109 = "Configuration details not found";
            public const string HCC110 = "Configuration details  updated successfully";
            public const string HCC111 = "Configuration deleted successfully";
            public const string HCC112 = "Configuration value updated successfully";
            public const string HCC113 = "Configuration loaded";
            public const string HCC114 = "Account type required";
            public const string HCC115 = "Account type added to configuration";
            public const string HCC116 = "Account type removed from configuration";
            public const string HCC117 = "Account type not found for configuration";
            public const string HCC118 = "Could Not Update Due To Dependency";
        }
    }
}
