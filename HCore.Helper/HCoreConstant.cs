using DocumentFormat.OpenXml.Bibliography;
using HCore.Data.Models;

namespace HCore.Helper
{
    public static class HCoreConstant
    {
        public static Globals _AppConfig = new Globals();
        public static string HostName = "";
        public static HostEnvironmentType HostEnvironment = HostEnvironmentType.Local;
        public const string StatusSuccess = "Success";
        public const string StatusError = "Error";
        public const int DefaultLimit = 10;
        public const int StatusActive = 2;
        public const int StatusInactive = 1;
        public const int StatusBlocked = 3;
        public const int StatusArchived = 4;

        public enum HostEnvironmentType
        {
            Local = 1,
            Live = 2,
            Test = 3,
            Management = 4,
            Tech = 5,
            Dev = 6
        }

        public enum ResponseStatus
        {
            Error = 2,
            Success = 1,
        }

        public enum LogType
        {
            Exception = 89,
            Log = 90,
            Alert = 91,
            Security = 92,
        }

        public enum LogLevel
        {
            High = 89,
            Moderate = 90,
            Low = 91,
        }

        public enum ActivityType
        {
            Add = 93,
            Update = 94,
            Delete = 95,
            View = 96,
        }

        public static class Helpers
        {
            public class Platform
            {
                public const int Mobile = 59;
                public const int Web = 60;
            }

            public class Depreciation_Methods
            {
                public const int Straight_line = 61;
                public const int Double_Declining_Balance = 62;
                public const int Units_Of_Production = 63;
                public const int Sum_Of_Years_Digits = 64;
                public const int Written_Down_Value = 65;
            }

            public class DepreciationTypes
            {
                public const int Daily = 69;
                public const int Monthly = 68;
                public const int Yearly = 67;
            }
        }

        public class HelperStatus
        {
            public static class Default
            {
                public const int Active = 1;
                public const int Inactive = 2;
                public const int Blocked = 3;
                public const int Dead = 4;
            }
        }

        public static class UserAccountType
        {
            public const int SuperAdmin = 20;
            public const int Admin = 21;
            public const int AccountManager = 22;
            public const int Manager = 23;
            public const int Accountant = 24;
            public const int CustomerSupport = 25;
            public const int Organization = 26;
            public const int Employee = 27;
            public const int GuestUser = 28;
            public const int Vendor = 29;
            public const int OperationsManager = 32;
            public const int User = 50;
            public const int Client = 54;
        }

        public static class UserRole
        {
            public const int SuperAdmin = 1;
            public const int Admin = 2;
            public const int HeadOfDept = 3;
            public const int HeadOfDiv = 4;
            public const int HR = 5;
            public const int Employee = 6;
            public const int User = 7;
        }

        public static class RegistrationSource
        {
            public const int WebApplication = 49;
            public const int MobileApp = 48;
        }

        public static class AssetType
        {
            public const int WithBarcode = 15;
            public const int WithoutBarcode = 16;
        }

        public static class VendorType
        {
            public const int InsuranceVendor = 28;
            public const int AssetVendor = 29;
        }

        public static class AssetStatus
        {
            public const int Issued = 7;
            public const int Returned = 8;
            public const int Created = 9;
            public const int Transfered = 10;
            public const int Damaged = 11;
            public const int Missing = 12;
            public const int Repair = 13;
            public const int Expired = 14;
            public const int WarrentyExpired = 53;
            public const int InProcess = 31;
            public const int Processed = 42;
            public const int Failed = 43;
            public const int Disposed = 41;
            public const int ApprovalPending = 51;
            public const int Approved = 52;

            public static class AssetReturn
            {
                public const int ApprovalPending = 39;
                public const int Approved = 40;
            }

            public static class AssetApproval
            {
                public const int PendingDepartmentHeadApproval = 71;
                public const int PendingDivisionHeadApproval = 72;
                public const int PendingHRApproval = 73;
                public const int PendingAdminApproval = 74;
                public const int PendingSuperAdminApproval = 75;
                public const int Failed = 69;
                public const int Rejected = 70;
            }
        }
        public static class MaintenanceType
        {
            public const int Repaire = 33;
            public const string RepaireS = "Repair";
            public const int Maintenance = 34;
            public const string MaintenanceS = "Maintenance";
        }

        public static class MaintenanceStatus
        {
            public const int Assigned = 36;
            public const int Completed = 37;
        }

        public static class FeatureType
        {
            public const int Feature = 45;
            public const int FeatureOption = 46;
        }

        public static class AssetApproval
        {
            public const int IssueApproval = 77;
            public const int TransferApproval = 78;
            public const int ReturnApproval = 79;
            public const int MaintenanceApproval = 80;
            public const int POApproval = 81;
        }
    }
}
