using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HCore.Helper
{
    public static class HCoreEncrypt
    {
        public static string EncryptHash(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                byte[] passwordKey = encodeDigest(PASSWORD);
                RijndaelManaged _RijndaelManaged = new RijndaelManaged();
                _RijndaelManaged.IV = _rawSecretKey;
                _RijndaelManaged.Key = passwordKey;
                ICryptoTransform _Encryptor = _RijndaelManaged.CreateEncryptor(passwordKey, _rawSecretKey);
                byte[] plainText = Encoding.UTF8.GetBytes(Content);
                byte[] cipherText = _Encryptor.TransformFinalBlock(plainText, 0, plainText.Length);
                return Convert.ToBase64String(cipherText);
            }
            else
            {
                return null;
            }
        }

        public static string DecryptHash(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                byte[] passwordKey = encodeDigest(PASSWORD);
                RijndaelManaged _RijndaelManaged = new RijndaelManaged();
                _RijndaelManaged.IV = _rawSecretKey;
                _RijndaelManaged.Key = passwordKey;
                ICryptoTransform _Decryptor = _RijndaelManaged.CreateDecryptor(passwordKey, _rawSecretKey);
                byte[] cipherText = Convert.FromBase64String(Content);
                byte[] decryptedText = _Decryptor.TransformFinalBlock(cipherText, 0, cipherText.Length);
                return Encoding.UTF8.GetString(decryptedText);
            }
            else
            {
                return null;
            }

        }

        public static string EncodeText(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(Content);
                return System.Convert.ToBase64String(plainTextBytes);
            }
            else
            {
                return null;
            }
        }

        public static string DecodeText(string Content)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                var base64EncodedBytes = System.Convert.FromBase64String(Content);
                var decText = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            else
            {
                return null;
            }
        }

        private static readonly string PASSWORD = "4047E98985014A32BFDE0862C8D480B945AAB975C85D4997B6DB8AC61F244646";
        private static byte[] _rawSecretKey =
          {
            0x12, 0x00, 0x22, 0x55, 0x33, 0x78, 0x25, 0x11,
            0x33, 0x45, 0x00, 0x00, 0x34, 0x00, 0x23, 0x28
        };

        private static byte[] encodeDigest(string passPhrase)
        {
            MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = Encoding.UTF8.GetBytes(passPhrase);
            return x.ComputeHash(data);
        }
    }
}
