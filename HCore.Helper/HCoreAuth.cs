using HCore.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;

namespace HCore.Helper
{
    public static class HCoreAuth
    {
        public static object Auth_ResponseDefaultObject(OResponse _OResponse)
        {
            if (_OResponse.Status == HCoreConstant.StatusSuccess)
            {
                _OResponse.ResponseCode = "HCG200";
            }
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            return JObject.Parse(JsonString);
        }

        public static string Auth_ResponseDefault(OResponse _OResponse)
        {
            if (_OResponse.Status == StatusSuccess)
            {
                _OResponse.ResponseCode = "HCG200";
            }
            string JsonString = JsonConvert.SerializeObject(_OResponse,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       NullValueHandling = NullValueHandling.Ignore
                   });
            return JsonString;
        }

        public static OAuth.Response Auth_App_Core(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            try
            {
                HCoreContext? _CoreContext = new HCoreContext();
                string? Temp_Header_ApiKey = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
                string? ApiKey = null;
                string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                OAuth.Response? _OAuth = new OAuth.Response();
                OUserReference? _OUserReference = new OUserReference();
                _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
                _OUserReference.RequestIpAddress = IpAddress;
                _OUserReference.RequestTime = RequestTime;
                _OUserReference.Request = RequestBody;
                _OUserReference.ResponseType = "json";
                _OUserReference.ResponseData = null;
                _OUserReference.Task = ApiName;
                _OAuth.UserReference = _OUserReference;
                _OAuth.Status = StatusError;
                try
                {
                    Temp_Header_ApiKey = Temp_Header_ApiKey.Replace("Bearer", "").Trim();
                    ApiKey = HCoreEncrypt.DecodeText(Temp_Header_ApiKey);
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("Auth_Key_Request_ApiKeyError : " + Temp_Header_ApiKey, _Exception, _OUserReference);
                    #region Set response
                    _OAuth.UserReference = null;
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCAK002", "Authentication failed");
                    return _OAuth;
                    #endregion
                }
                using (_CoreContext = new HCoreContext())
                {
                    var AppKeyDetails = _CoreContext.AtCoreAppVersions.Where(x => x.ApiKey == ApiKey && x.StatusId == HelperStatus.Default.Active)
                                                 .Select(x => new
                                                 {
                                                     AppId = x.AppId,
                                                     AppVersionId = x.Id,
                                                     LogRequests = x.App.AllowLogging,
                                                     AppStatusId = x.App.StatusId,
                                                     AppVersionStatusId = x.StatusId,
                                                     UserAccountId = x.App.AccountId,
                                                     UserAccountKey = x.App.Account.Guid,
                                                     UserAccountTypeId = x.App.Account.AccountTypeId,
                                                     UserAccountDisplayName = x.App.Account.DisplayName,
                                                     OrganizationId = x.App.Account.OrganizationId,
                                                 }).FirstOrDefault();
                    if (AppKeyDetails != null)
                    {
                        string? HAuthKey = Request.Headers.Where(x => x.Key == "ask").FirstOrDefault().Value;
                        if (!string.IsNullOrEmpty(HAuthKey))
                        {
                            string SessionKeyDetails = HCoreEncrypt.DecryptHash(HCoreEncrypt.DecodeText(HAuthKey));
                            string[] Sd = SessionKeyDetails.Split("|");
                            long SessionId = Convert.ToInt64(Sd[0]);
                            string SessionKey = Sd[1];
                            using (_CoreContext = new HCoreContext())
                            {
                                var SessionDetails = _CoreContext.AtAccountSessions.Where(x => x.Id == SessionId && x.Guid == SessionKey)
                                    .Select(x => new
                                    {
                                        StatusId = x.StatusId,
                                        AccountId = x.AccountId,
                                        AccountKey = x.Account.Guid,
                                        AccountTypeId = x.Account.AccountTypeId,
                                        AccountDisplayName = x.Account.DisplayName,
                                        AccountEmailAddress = x.Account.EmailAddress,
                                        AccountCode = x.Account.AccountCode,
                                        AccountOwnerKey = x.Account.Owner.Guid,
                                        AccountOwnerId = x.Account.OwnerId,
                                        AccountOwnerDisplayName = x.Account.DisplayName,
                                        UserRoleId = x.Account.RoleId,
                                        UserRoleKey = x.Account.Role.SystemName,
                                        UserRoleName = x.Account.Role.Name,
                                    }).FirstOrDefault();
                                if (SessionDetails != null)
                                {
                                    if (SessionDetails.StatusId == HelperStatus.Default.Active)
                                    {
                                        _OUserReference.AccountId = (long)SessionDetails.AccountId;
                                        _OUserReference.AccountKey = SessionDetails.AccountKey;
                                        _OUserReference.AccountTypeId = (long)SessionDetails.AccountTypeId;
                                        _OUserReference.DisplayName = SessionDetails.AccountDisplayName;
                                        _OUserReference.EmailAddress = SessionDetails.AccountEmailAddress;
                                        _OUserReference.AccountCode = SessionDetails.AccountCode;
                                        _OUserReference.AccountOwnerKey = SessionDetails.AccountOwnerKey;
                                        _OUserReference.AccountOwnerId = SessionDetails.AccountOwnerId;
                                        _OUserReference.AccountOwnerDisplayName = SessionDetails.AccountOwnerDisplayName;
                                        _OUserReference.AppVersionId = AppKeyDetails.AppVersionId;
                                        _OUserReference.LogRequest = AppKeyDetails.LogRequests;
                                        _OUserReference.ResponseData = null;
                                        _OUserReference.AppId = AppKeyDetails.AppId;
                                        _OUserReference.AccountSessionId = SessionId;
                                        _OUserReference.OrganizationId = (long)AppKeyDetails.OrganizationId;
                                        _OUserReference.UserRoleId = (int?)SessionDetails.UserRoleId;
                                        _OUserReference.UserRoleKey = SessionDetails.UserRoleKey;
                                        _OUserReference.UserRoleName = SessionDetails.UserRoleName;
                                        _OAuth.AccountId = (long)SessionDetails.AccountId;
                                        _OAuth.Status = StatusSuccess;
                                        _OAuth.UserReference = _OUserReference;
                                        return _OAuth;
                                    }
                                    else
                                    {
                                        _OAuth.Status = StatusError;
                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Session expired. Please login again");
                                        return _OAuth;
                                    }
                                }
                                else
                                {
                                    _OAuth.Status = StatusError;
                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Session invalidated. Please login again");
                                    return _OAuth;
                                }
                            }

                        }
                        else
                        {
                            _OUserReference.AccountId = (long)AppKeyDetails.UserAccountId;
                            _OUserReference.AccountKey = AppKeyDetails.UserAccountKey;
                            _OUserReference.AccountTypeId = (long)AppKeyDetails.UserAccountTypeId;
                            _OUserReference.DisplayName = AppKeyDetails.UserAccountDisplayName;
                            _OUserReference.AppVersionId = AppKeyDetails.AppVersionId;
                            _OUserReference.LogRequest = AppKeyDetails.LogRequests;
                            _OUserReference.ResponseData = null;
                            _OUserReference.AppId = AppKeyDetails.AppId;
                            _OUserReference.OrganizationId = (long)AppKeyDetails.OrganizationId;
                            _OAuth.Status = StatusSuccess;
                            _OAuth.UserReference = _OUserReference;
                            return _OAuth;
                        }
                    }
                    else
                    {
                        _OAuth.Status = HCoreConstant.StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                        return _OAuth;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Invoke", _Exception);
                OAuth.Response? _OAuth = new OAuth.Response();
                _OAuth.UserReference = null;
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                return _OAuth;
            }
        }

        public static async Task<OAuth.Response> Auth_App(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            try
            {
                HCoreContext? _CoreContext;
                string? Temp_Header_ApiKey = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
                string? ApiKey = null;
                string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                OAuth.Response? _OAuth = new OAuth.Response();
                OUserReference? _OUserReference = new OUserReference();
                _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
                _OUserReference.RequestIpAddress = IpAddress;
                _OUserReference.RequestTime = RequestTime;
                _OUserReference.Request = RequestBody;
                _OUserReference.ResponseType = "json";
                _OUserReference.ResponseData = null;
                _OUserReference.Task = ApiName;
                _OAuth.UserReference = _OUserReference;
                _OAuth.Status = StatusError;
                try
                {
                    Temp_Header_ApiKey = Temp_Header_ApiKey.Replace("Bearer", "").Trim();
                    ApiKey = HCoreEncrypt.DecodeText(Temp_Header_ApiKey);
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("Auth_Key_Request_ApiKeyError : " + Temp_Header_ApiKey, _Exception, _OUserReference);
                    _OAuth.UserReference = null;
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                using (_CoreContext = new HCoreContext())
                {
                    var AppKeyDetails = await _CoreContext.AtCoreAppVersions.Where(x => x.ApiKey == ApiKey && x.StatusId == HelperStatus.Default.Active)
                                        .Select(x => new
                                        {
                                            AppId = x.AppId,
                                            AppVersionId = x.Id,
                                            IpAddress = x.App.IPAddress,
                                            LogRequests = x.App.AllowLogging,
                                            AppStatusId = x.App.StatusId,
                                            AppVersionStatusId = x.StatusId,
                                            OrganizationId = x.App.AccountId,
                                            OrganizationKey = x.App.Account.Guid,
                                            OrganizationCode = x.App.Account.AccountCode,
                                            OrganizationTypeId = x.App.Account.AccountTypeId,
                                            OrganizationDisplayName = x.App.Account.DisplayName,
                                        }).FirstOrDefaultAsync();
                    if (AppKeyDetails != null)
                    {
                        _OUserReference.OrganizationId = (long)AppKeyDetails.OrganizationId;
                        _OUserReference.OrganizationKey = AppKeyDetails.OrganizationKey;
                        _OUserReference.OrganizationCode = AppKeyDetails.OrganizationCode;
                        _OUserReference.OrganizationTypeId = (int)AppKeyDetails.OrganizationTypeId;
                        _OUserReference.OrganizationDisplayName = AppKeyDetails.OrganizationDisplayName;
                        _OUserReference.AppVersionId = AppKeyDetails.AppVersionId;
                        _OUserReference.LogRequest = AppKeyDetails.LogRequests;
                        _OUserReference.ResponseData = null;
                        _OUserReference.AppId = AppKeyDetails.AppId;
                        _OAuth.Status = StatusSuccess;
                        _OAuth.UserReference = _OUserReference;
                        return _OAuth;
                    }
                    else
                    {
                        _OAuth.Status = HCoreConstant.StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                        return _OAuth;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Invoke", _Exception);
                OAuth.Response? _OAuth = new OAuth.Response();
                _OAuth.UserReference = null;
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                return _OAuth;
            }
        }

        public static async Task<OAuth.Response> Auth_App_Request(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            try
            {
                HCoreContext? _CoreContext = new HCoreContext();
                string? Temp_Header_ApiKey = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value;
                string? ApiKey = null;
                string IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                OAuth.Response? _OAuth = new OAuth.Response();
                OUserReference? _OUserReference = new OUserReference();
                _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
                _OUserReference.RequestIpAddress = IpAddress;
                _OUserReference.RequestTime = RequestTime;
                _OUserReference.Request = RequestBody;
                _OUserReference.ResponseType = "json";
                _OUserReference.ResponseData = null;
                _OUserReference.Task = ApiName;
                _OAuth.UserReference = _OUserReference;
                _OAuth.Status = StatusError;
                try
                {
                    Temp_Header_ApiKey = Temp_Header_ApiKey.Replace("Bearer", "").Trim();
                    ApiKey = HCoreEncrypt.DecodeText(Temp_Header_ApiKey);
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("Auth_Key_Request_ApiKeyError : " + Temp_Header_ApiKey, _Exception, _OUserReference);
                    _OAuth.UserReference = null;
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                using (_CoreContext = new HCoreContext())
                {
                    var AppKeyDetails = await _CoreContext.AtCoreAppVersions.Where(x => x.ApiKey == ApiKey && x.StatusId == HelperStatus.Default.Active)
                                        .Select(x => new
                                        {
                                            AppId = x.AppId,
                                            AppVersionId = x.Id,
                                            IpAddress = x.App.IPAddress,
                                            LogRequests = x.App.AllowLogging,
                                            AppStatusId = x.App.StatusId,
                                            AppVersionStatusId = x.StatusId,
                                        }).FirstOrDefaultAsync();
                    if (AppKeyDetails != null)
                    {
                        string? HAuthKey = Request.Headers.Where(x => x.Key == "ask").FirstOrDefault().Value;
                        if (!string.IsNullOrEmpty(HAuthKey))
                        {
                            string SessionKeyDetails = HCoreEncrypt.DecryptHash(HCoreEncrypt.DecodeText(HAuthKey));
                            string[] Sd = SessionKeyDetails.Split("|");
                            long SessionId = Convert.ToInt64(Sd[0]);
                            string SessionKey = Sd[1];
                            using (_CoreContext = new HCoreContext())
                            {
                                var SessionDetails = _CoreContext.AtAccountSessions.Where(x => x.Id == SessionId && x.Guid == SessionKey)
                                    .Select(x => new
                                    {
                                        StatusId = x.StatusId,
                                        AccountId = x.AccountId,
                                        AccountKey = x.Account.Guid,
                                        AccountTypeId = x.Account.AccountTypeId,
                                        AccountDisplayName = x.Account.DisplayName,
                                        AccountEmailAddress = x.Account.EmailAddress,
                                        AccountCode = x.Account.AccountCode,
                                        AccountOwnerKey = x.Account.Owner.Guid,
                                        AccountOwnerId = x.Account.OwnerId,
                                        AccountOwnerDisplayName = x.Account.DisplayName,
                                        OrganizationId = x.Account.OrganizationId,
                                        OrganizationKey = x.Account.Organization.Guid,
                                        OrganizationCode = x.Account.Organization.AccountCode,
                                        OrganizationTypeId = x.Account.Organization.AccountTypeId,
                                        OrganizationDisplayName = x.Account.Organization.DisplayName,
                                        UserRoleId = x.Account.RoleId,
                                        UserRoleKey = x.Account.Role.SystemName,
                                        UserRoleName = x.Account.Role.Name,
                                    }).FirstOrDefault();
                                if (SessionDetails != null)
                                {
                                    if (SessionDetails.StatusId == HelperStatus.Default.Active)
                                    {
                                        _OUserReference.AccountId = (long)SessionDetails.AccountId;
                                        _OUserReference.AccountKey = SessionDetails.AccountKey;
                                        _OUserReference.AccountTypeId = (long)SessionDetails.AccountTypeId;
                                        _OUserReference.DisplayName = SessionDetails.AccountDisplayName;
                                        _OUserReference.EmailAddress = SessionDetails.AccountEmailAddress;
                                        _OUserReference.AccountCode = SessionDetails.AccountCode;
                                        _OUserReference.AccountOwnerKey = SessionDetails.AccountOwnerKey;
                                        _OUserReference.AccountOwnerId = SessionDetails.AccountOwnerId;
                                        _OUserReference.AccountOwnerDisplayName = SessionDetails.AccountOwnerDisplayName;
                                        _OUserReference.OrganizationId = (long)SessionDetails.OrganizationId;
                                        _OUserReference.OrganizationKey = SessionDetails.OrganizationKey;
                                        _OUserReference.OrganizationCode = SessionDetails.OrganizationCode;
                                        _OUserReference.OrganizationTypeId = (int)SessionDetails.OrganizationTypeId;
                                        _OUserReference.OrganizationDisplayName = SessionDetails.OrganizationDisplayName;
                                        _OUserReference.AppVersionId = AppKeyDetails.AppVersionId;
                                        _OUserReference.LogRequest = AppKeyDetails.LogRequests;
                                        _OUserReference.ResponseData = null;
                                        _OUserReference.AppId = AppKeyDetails.AppId;
                                        _OUserReference.AccountSessionId = SessionId;
                                        _OUserReference.UserRoleId = (int?)SessionDetails.UserRoleId;
                                        _OUserReference.UserRoleKey = SessionDetails.UserRoleKey;
                                        _OUserReference.UserRoleName = SessionDetails.UserRoleName;
                                        _OAuth.AccountId = (long)SessionDetails.AccountId;
                                        _OAuth.Status = StatusSuccess;
                                        _OAuth.UserReference = _OUserReference;
                                        return _OAuth;
                                    }
                                    else
                                    {
                                        _OAuth.Status = StatusError;
                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Session expired. Please login again");
                                        return _OAuth;
                                    }
                                }
                                else
                                {
                                    _OAuth.Status = StatusError;
                                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Session invalidated. Please login again");
                                    return _OAuth;
                                }
                            }

                        }
                        else
                        {
                            _OAuth.Status = HCoreConstant.StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                            return _OAuth;
                        }
                    }
                    else
                    {
                        _OAuth.Status = HCoreConstant.StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                        return _OAuth;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Invoke", _Exception);
                OAuth.Response? _OAuth = new OAuth.Response();
                _OAuth.UserReference = null;
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                return _OAuth;
            }
        }

        public static async Task<OAuth.Response> Auth_Mobile_App(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            try
            {
                HCoreContext? _HCoreContext;
                string? Temp_Header_ak = Request.Headers.Where(x => x.Key == "ak").FirstOrDefault().Value;
                string? Temp_Header_avk = Request.Headers.Where(x => x.Key == "avk").FirstOrDefault().Value;
                string? Temp_Header_audk = Request.Headers.Where(x => x.Key == "audk").FirstOrDefault().Value;
                string? Temp_Header_audlt = Request.Headers.Where(x => x.Key == "audlt").FirstOrDefault().Value;
                string? Temp_Header_audln = Request.Headers.Where(x => x.Key == "audln").FirstOrDefault().Value;
                string? IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                string? Header_AppKey;
                string? Header_AppVersionKey;
                string? Header_DeviceKey;
                double? Header_Latitude = 0;
                double? Header_Longitude = 0;
                OAuth.Response? _OAuth = new OAuth.Response();
                OUserReference? _OUserReference = new OUserReference();
                _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
                _OUserReference.RequestIpAddress = IpAddress;
                _OUserReference.RequestTime = RequestTime;
                _OUserReference.Request = RequestBody;
                _OUserReference.ResponseType = "json";
                _OUserReference.ResponseData = null;
                _OUserReference.Task = ApiName;
                _OAuth.UserReference = _OUserReference;
                _OAuth.Status = StatusError;
                if (!string.IsNullOrEmpty(Temp_Header_audlt))
                {
                    try
                    {
                        Header_Latitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_audlt));
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Request_DeviceLatitude" + Temp_Header_audlt, _Exception, null);
                        Header_Latitude = 0;
                    }
                }
                if (!string.IsNullOrEmpty(Temp_Header_audln))
                {
                    try
                    {
                        Header_Longitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_audln));
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Request_DeviceLongitude" + Temp_Header_audln, _Exception, null);
                        Header_Longitude = 0;
                    }
                }
                if (string.IsNullOrEmpty(Temp_Header_ak))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else if (string.IsNullOrEmpty(Temp_Header_avk))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else if (string.IsNullOrEmpty(Temp_Header_audk))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else
                {
                    try
                    {
                        Header_AppKey = HCoreEncrypt.DecodeText(Temp_Header_ak);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_App_AppKey " + Temp_Header_ak, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    try
                    {
                        Header_AppVersionKey = HCoreEncrypt.DecodeText(Temp_Header_avk);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_App_AppVersionKey " + Temp_Header_avk, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    try
                    {
                        Header_DeviceKey = HCoreEncrypt.DecodeText(Temp_Header_audk);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Request_DeviceKey" + Temp_Header_audk, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AppKeyDetails = await _HCoreContext.AtCoreApps.Where(x => x.AppKey == Header_AppKey && x.StatusId == HelperStatus.Default.Active).FirstOrDefaultAsync();
                        if (AppKeyDetails != null)
                        {
                            var ApiKeyDetails = await _HCoreContext.AtCoreAppVersions.Where(x => x.ApiKey == Header_AppVersionKey && x.AppId == AppKeyDetails.Id && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new
                                                {
                                                    AppId = x.AppId,
                                                    AppVersionId = x.Id,
                                                    IpAddress = x.App.IPAddress,
                                                    LogRequests = x.App.AllowLogging,
                                                    AppStatusId = x.App.StatusId,
                                                    AppVersionStatusId = x.StatusId,
                                                    OrganizationId = x.App.AccountId,
                                                    OrganizationKey = x.App.Account.Guid,
                                                    OrganizationCode = x.App.Account.AccountCode,
                                                    OrganizationTypeId = x.App.Account.AccountTypeId,
                                                    OrganizationDisplayName = x.App.Account.DisplayName,
                                                }).FirstOrDefaultAsync();
                            if (ApiKeyDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Header_DeviceKey))
                                {
                                    var DeviceDetails = await _HCoreContext.AtCoreAccountDevices.Where(x => x.SerialNumber == Header_DeviceKey && x.AppId == ApiKeyDetails.AppId && x.AppVersionId == ApiKeyDetails.AppVersionId).FirstOrDefaultAsync();
                                    if (DeviceDetails != null)
                                    {
                                        _OUserReference.DeviceId = (long)DeviceDetails.Id;
                                        _OUserReference.DeviceKey = DeviceDetails.Guid;
                                        _OUserReference.DeviceSerialNumber = Header_DeviceKey;
                                    }
                                    else
                                    {
                                        AtCoreAccountDevice? _AtCoreAccountDevices = new AtCoreAccountDevice();
                                        _AtCoreAccountDevices.Guid = HCoreHelper.GenerateGuid();
                                        _AtCoreAccountDevices.AccountId = (long)ApiKeyDetails.OrganizationId;
                                        _AtCoreAccountDevices.AppId = ApiKeyDetails.AppId;
                                        _AtCoreAccountDevices.AppVersionId = ApiKeyDetails.AppVersionId;
                                        _AtCoreAccountDevices.SerialNumber = Header_DeviceKey;
                                        _AtCoreAccountDevices.StatusId = HelperStatus.Default.Active;
                                        _AtCoreAccountDevices.CreatedById = ApiKeyDetails.OrganizationId;
                                        _AtCoreAccountDevices.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtCoreAccountDevices.AddAsync(_AtCoreAccountDevices);
                                        await _HCoreContext.SaveChangesAsync();

                                        _OUserReference.DeviceId = (long)_AtCoreAccountDevices.Id;
                                        _OUserReference.DeviceKey = _AtCoreAccountDevices.Guid;
                                        _OUserReference.DeviceSerialNumber = Header_DeviceKey;
                                    }
                                }
                                _OUserReference.OrganizationId = (long)ApiKeyDetails.OrganizationId;
                                _OUserReference.OrganizationKey = ApiKeyDetails.OrganizationKey;
                                _OUserReference.OrganizationCode = ApiKeyDetails.OrganizationCode;
                                _OUserReference.OrganizationTypeId = (int)ApiKeyDetails.OrganizationTypeId;
                                _OUserReference.OrganizationDisplayName = ApiKeyDetails.OrganizationDisplayName;
                                _OUserReference.AppId = ApiKeyDetails.AppId;
                                _OUserReference.AppVersionId = ApiKeyDetails.AppVersionId;
                                _OUserReference.LogRequest = ApiKeyDetails.LogRequests;
                                _OUserReference.ResponseData = null;
                                _OAuth.Status = StatusSuccess;
                                _OAuth.UserReference = _OUserReference;
                                return _OAuth;
                            }
                            else
                            {
                                _OAuth.Status = HCoreConstant.StatusError;
                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                                return _OAuth;
                            }
                        }
                        else
                        {
                            _OAuth.Status = HCoreConstant.StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                            return _OAuth;
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Invoke", _Exception);
                OAuth.Response? _OAuth = new OAuth.Response();
                _OAuth.UserReference = null;
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                return _OAuth;
            }
        }

        public static async Task<OAuth.Response> Auth_Mobile_App_Request(HttpRequest Request, string? RequestBody, DateTime RequestTime, string? ApiName)
        {
            try
            {
                HCoreContext? _HCoreContext = new HCoreContext();
                string? Temp_Header_ask = Request.Headers.Where(x => x.Key == "ask").FirstOrDefault().Value;
                string? Temp_Header_ak = Request.Headers.Where(x => x.Key == "ak").FirstOrDefault().Value;
                string? Temp_Header_avk = Request.Headers.Where(x => x.Key == "avk").FirstOrDefault().Value;
                string? Temp_Header_audk = Request.Headers.Where(x => x.Key == "audk").FirstOrDefault().Value;
                string? Temp_Header_audlt = Request.Headers.Where(x => x.Key == "audlt").FirstOrDefault().Value;
                string? Temp_Header_audln = Request.Headers.Where(x => x.Key == "audln").FirstOrDefault().Value;
                string? IpAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                string? HAuthKey;
                string? Header_AppKey;
                string? Header_AppVersionKey;
                string? Header_DeviceKey;
                double? Header_Latitude = 0;
                double? Header_Longitude = 0;
                OAuth.Response? _OAuth = new OAuth.Response();
                OUserReference? _OUserReference = new OUserReference();
                _OUserReference.RequestKey = HCoreHelper.GenerateGuid();
                _OUserReference.RequestIpAddress = IpAddress;
                _OUserReference.RequestTime = RequestTime;
                _OUserReference.Request = RequestBody;
                _OUserReference.ResponseType = "json";
                _OUserReference.ResponseData = null;
                _OUserReference.Task = ApiName;
                _OAuth.UserReference = _OUserReference;
                _OAuth.Status = StatusError;
                if (!string.IsNullOrEmpty(Temp_Header_audlt))
                {
                    try
                    {
                        Header_Latitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_audlt));
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Request_DeviceLatitude" + Temp_Header_audlt, _Exception, null);
                        Header_Latitude = 0;
                    }
                }
                if (!string.IsNullOrEmpty(Temp_Header_audln))
                {
                    try
                    {
                        Header_Longitude = Convert.ToDouble(HCoreEncrypt.DecodeText(Temp_Header_audln));
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Request_DeviceLongitude" + Temp_Header_audln, _Exception, null);
                        Header_Longitude = 0;
                    }
                }
                if (string.IsNullOrEmpty(Temp_Header_ask))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else if (string.IsNullOrEmpty(Temp_Header_ak))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else if (string.IsNullOrEmpty(Temp_Header_avk))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else if (string.IsNullOrEmpty(Temp_Header_audk))
                {
                    _OAuth.Status = StatusError;
                    _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                    return _OAuth;
                }
                else
                {
                    try
                    {
                        HAuthKey = HCoreEncrypt.DecodeText(Temp_Header_ask);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_App_AppKey " + Temp_Header_ask, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    try
                    {
                        Header_AppKey = HCoreEncrypt.DecodeText(Temp_Header_ak);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_App_AppKey " + Temp_Header_ak, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    try
                    {
                        Header_AppVersionKey = HCoreEncrypt.DecodeText(Temp_Header_avk);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_App_AppVersionKey " + Temp_Header_avk, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    try
                    {
                        Header_DeviceKey = HCoreEncrypt.DecodeText(Temp_Header_audk);
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("Auth_Request_DeviceKey" + Temp_Header_audk, _Exception, null);
                        _OAuth.Status = StatusError;
                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                        return _OAuth;
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AppKeyDetails = await _HCoreContext.AtCoreApps.Where(x => x.AppKey == Header_AppKey && x.StatusId == HelperStatus.Default.Active).FirstOrDefaultAsync();
                        if (AppKeyDetails != null)
                        {
                            var ApiKeyDetails = await _HCoreContext.AtCoreAppVersions.Where(x => x.ApiKey == Header_AppVersionKey && x.AppId == AppKeyDetails.Id && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new
                                                {
                                                    AppId = x.AppId,
                                                    AppVersionId = x.Id,
                                                    IpAddress = x.App.IPAddress,
                                                    LogRequests = x.App.AllowLogging,
                                                    AppStatusId = x.App.StatusId,
                                                    AppVersionStatusId = x.StatusId,
                                                }).FirstOrDefaultAsync();
                            if (ApiKeyDetails != null)
                            {
                                var DeviceDetails = await _HCoreContext.AtCoreAccountDevices.Where(x => x.SerialNumber == Header_DeviceKey && x.AppId == ApiKeyDetails.AppId && x.AppVersionId == ApiKeyDetails.AppVersionId).FirstOrDefaultAsync();
                                if (DeviceDetails != null)
                                {
                                    _OUserReference.DeviceId = DeviceDetails.Id;
                                    _OUserReference.DeviceKey = DeviceDetails.Guid;
                                    _OUserReference.DeviceSerialNumber = Header_DeviceKey;
                                }

                                string SessionKeyDetails = HCoreEncrypt.DecryptHash(HAuthKey);
                                string[] Sd = SessionKeyDetails.Split("|");
                                long SessionId = Convert.ToInt64(Sd[0]);
                                string SessionKey = Sd[1];
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var SessionDetails = _HCoreContext.AtAccountSessions.Where(x => x.Id == SessionId && x.Guid == SessionKey)
                                        .Select(x => new
                                        {
                                            StatusId = x.StatusId,
                                            AccountId = x.AccountId,
                                            AccountKey = x.Account.Guid,
                                            AccountTypeId = x.Account.AccountTypeId,
                                            AccountDisplayName = x.Account.DisplayName,
                                            AccountEmailAddress = x.Account.EmailAddress,
                                            AccountCode = x.Account.AccountCode,
                                            AccountOwnerKey = x.Account.Owner.Guid,
                                            AccountOwnerId = x.Account.OwnerId,
                                            AccountOwnerDisplayName = x.Account.DisplayName,
                                            OrganizationId = x.Account.OrganizationId,
                                            OrganizationKey = x.Account.Organization.Guid,
                                            OrganizationCode = x.Account.Organization.AccountCode,
                                            OrganizationTypeId = x.Account.Organization.AccountTypeId,
                                            OrganizationDisplayName = x.Account.Organization.DisplayName,
                                            UserRoleId = x.Account.RoleId,
                                            UserRoleKey = x.Account.Role.SystemName,
                                            UserRoleName = x.Account.Role.Name,
                                        }).FirstOrDefault();
                                    if (SessionDetails != null)
                                    {
                                        if (SessionDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            _OUserReference.AccountId = (long)SessionDetails.AccountId;
                                            _OUserReference.AccountKey = SessionDetails.AccountKey;
                                            _OUserReference.AccountTypeId = (long)SessionDetails.AccountTypeId;
                                            _OUserReference.DisplayName = SessionDetails.AccountDisplayName;
                                            _OUserReference.EmailAddress = SessionDetails.AccountEmailAddress;
                                            _OUserReference.AccountCode = SessionDetails.AccountCode;
                                            _OUserReference.AccountOwnerKey = SessionDetails.AccountOwnerKey;
                                            _OUserReference.AccountOwnerId = SessionDetails.AccountOwnerId;
                                            _OUserReference.AccountOwnerDisplayName = SessionDetails.AccountOwnerDisplayName;
                                            _OUserReference.OrganizationId = (long)SessionDetails.OrganizationId;
                                            _OUserReference.OrganizationKey = SessionDetails.OrganizationKey;
                                            _OUserReference.OrganizationCode = SessionDetails.OrganizationCode;
                                            _OUserReference.OrganizationTypeId = (int)SessionDetails.OrganizationTypeId;
                                            _OUserReference.OrganizationDisplayName = SessionDetails.OrganizationDisplayName;
                                            _OUserReference.AppVersionId = ApiKeyDetails.AppVersionId;
                                            _OUserReference.LogRequest = ApiKeyDetails.LogRequests;
                                            _OUserReference.ResponseData = null;
                                            _OUserReference.AppId = ApiKeyDetails.AppId;
                                            _OUserReference.AccountSessionId = SessionId;
                                            _OUserReference.UserRoleId = (int?)SessionDetails.UserRoleId;
                                            _OUserReference.UserRoleKey = SessionDetails.UserRoleKey;
                                            _OUserReference.UserRoleName = SessionDetails.UserRoleName;
                                            _OAuth.AccountId = (long)SessionDetails.AccountId;
                                            _OAuth.Status = StatusSuccess;
                                            _OAuth.UserReference = _OUserReference;
                                            return _OAuth;
                                        }
                                        else
                                        {
                                            _OAuth.Status = StatusError;
                                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Session expired. Please login again");
                                            return _OAuth;
                                        }
                                    }
                                    else
                                    {
                                        _OAuth.Status = StatusError;
                                        _OAuth.UserResponse = HCoreHelper.SendResponse(_OUserReference, ResponseStatus.Error, null, "HCA132", "Session invalidated. Please login again");
                                        return _OAuth;
                                    }
                                }
                            }
                            else
                            {
                                _OAuth.Status = HCoreConstant.StatusError;
                                _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                                return _OAuth;
                            }
                        }
                        else
                        {
                            _OAuth.Status = HCoreConstant.StatusError;
                            _OAuth.UserResponse = HCoreHelper.SendResponse(_OAuth.UserReference, ResponseStatus.Error, null, "HCA132", "System not available at the moment. Please try after some time");
                            return _OAuth;
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("Invoke", _Exception);
                OAuth.Response? _OAuth = new OAuth.Response();
                _OAuth.UserReference = null;
                _OAuth.Status = StatusError;
                _OAuth.UserResponse = HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "HCA132", "Authentication failed");
                return _OAuth;
            }
        }
    }
}
