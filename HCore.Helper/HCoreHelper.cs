using HCore.Data.Logging;
using HCore.Data.Logging.Models;
using HCore.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using System.Net;
using System.Net.Mail;
using ClosedXML.Excel;
using System.Reflection;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections;
using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using OfficeOpenXml;
using System.Data;

namespace HCore.Helper
{
    public static class HCoreHelper
    {
        public static DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var random = new Random(); var range = to - from;
            var randTimeSpan = new TimeSpan((long)(random.NextDouble() * range.Ticks));
            return from + randTimeSpan;
        }

        public static double GetPercentage(double Amount, double Percentage, int Round, bool AllowNegative = false)
        {
            #region Code Block
            double Value = (Amount * Percentage) / 100;
            if (Value > 0 && Value < 10000000000)
            {
                double PercentageValue = (double)Math.Round(Value, Round);
                if (PercentageValue > 0)
                {
                    return PercentageValue;
                }
                else
                {
                    if (AllowNegative)
                    {
                        return PercentageValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
            #endregion
        }

        private static readonly Random _Random = new();

        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[_Random.Next(s.Length)]).ToArray());
        }

        public static string GenerateFixedString(long? Number, int? Length = 6)
        {
            if (Number > 0)
            {
                return string.Format("{0:00000000}", Number);
            }

            return "00000000";
        }

        public static string GenerateAccountCode(int length = 10, string? Prefix = null, string? Sufix = null)
        {
            string Code = GenerateAccountCodeX(length, Prefix, Sufix);
            if (Code.Length != length)
            {
                Code = GenerateAccountCodeX(length, Prefix, Sufix);
            }
            if (Code.Length != length)
            {
                Code = GenerateAccountCodeX(length, Prefix, Sufix);
            }
            return Code;
        }

        private static string GenerateAccountCodeX(int length = 10, string? Prefix = null, string? Sufix = null)
        {
            if (!string.IsNullOrEmpty(Prefix))
            {
                length = length - Prefix.Length;
            }
            if (!string.IsNullOrEmpty(Sufix))
            {
                length = length - Sufix.Length;
            }
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random _Random = new Random(BitConverter.ToInt32(seed, 0));
            int _RandomNumber = 0;
            String _GeneratedRandomNumber = "";
            for (int i = 0; i < length; i++)
            {
                _RandomNumber = _Random.Next(48, 58);
                _GeneratedRandomNumber = _GeneratedRandomNumber + (char)_RandomNumber;
            }
            if (!string.IsNullOrEmpty(Prefix) && !string.IsNullOrEmpty(Sufix))
            {
                return Prefix + "" + _GeneratedRandomNumber + "" + Sufix;
            }
            else if (!string.IsNullOrEmpty(Prefix))
            {
                return Prefix + "" + _GeneratedRandomNumber;
            }
            else if (!string.IsNullOrEmpty(Sufix))
            {
                return _GeneratedRandomNumber + "" + Sufix;
            }
            else
            {
                return _GeneratedRandomNumber;
            }
        }

        public static double RoundNumber(double Value, int Round, bool AllowNegative = false)
        {
            #region Code Block
            return (double)Math.Round(Value, Round);
            #endregion
        }

        public static double GetPercentage(double Amount, double Percentage, bool AllowNegative = false)
        {
            #region Code Block
            double Value = (Amount * Percentage) / 100;
            if (Value > 0 && Value < 10000000000)
            {
                double PercentageValue = (double)Math.Round(Value, 2);
                if (PercentageValue > 0)
                {
                    return PercentageValue;
                }
                else
                {
                    if (AllowNegative)
                    {
                        return PercentageValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
            #endregion
        }

        public static double GetAmountPercentage(double SmallAmount, double BigAmount, bool AllowNegative = false)
        {
            #region Code Block
            double Value = (SmallAmount / BigAmount) * 100;
            if (Value > 0 && Value < 10000000000)
            {
                double PercentageValue = Math.Round(Value, 3);
                if (PercentageValue > 0)
                {
                    return PercentageValue;
                }
                else
                {
                    if (AllowNegative)
                    {
                        return PercentageValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            else
            {
                return 0;
            }
            #endregion
        }

        public static string FormatMobileNumber(string CountryIsd, string MobileNumber)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                CountryIsd = CountryIsd.Trim();
                CountryIsd = CountryIsd.Replace("+", "");
                CountryIsd = CountryIsd.Replace(" ", "");
                string VMobileNumber = MobileNumber.Trim();
                VMobileNumber = VMobileNumber.Replace("+", "");
                VMobileNumber = VMobileNumber.Replace(" ", "");
                string ExPrefix = CountryIsd + "0";
                if (VMobileNumber.Length == 10)
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else if (VMobileNumber.Length > 10)
                {
                    int tail_length = 10;
                    VMobileNumber = VMobileNumber.Substring(VMobileNumber.Length - tail_length);
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                return VMobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }

        public static string CleanMobileNumber(string MobileNumber, int Length)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                string VMobileNumber = MobileNumber.Trim();
                VMobileNumber = VMobileNumber.Replace("+", "");
                VMobileNumber = VMobileNumber.Replace(" ", "");
                return VMobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }

        public static string FormatMobileNumber(string CountryIsd, string MobileNumber, int Length)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                string VMobileNumber = MobileNumber.Trim();
                VMobileNumber = VMobileNumber.Replace("+", "");
                VMobileNumber = VMobileNumber.Replace(" ", "");
                string ExPrefix = CountryIsd + "0";
                if (VMobileNumber.Length == Length)
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else if (VMobileNumber.Length > Length)
                {
                    int tail_length = Length;
                    VMobileNumber = VMobileNumber.Substring(VMobileNumber.Length - tail_length);
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                else
                {
                    VMobileNumber = CountryIsd + VMobileNumber;
                }
                return VMobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }

        public static string GenerateGuid()
        {
            #region Code Block
            Guid _Guid = Guid.NewGuid();
            string TGuid = _Guid.ToString("N");
            return TGuid;
            #endregion
        }

        public static bool ValidateEmailAddress(this string email)
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(email);
        }

        public static string Substring(string Content, int End)
        {
            if (!string.IsNullOrEmpty(Content))
            {
                if (Content.Length <= End)
                {
                    return Content;
                }
                else
                {
                    return Content.Substring(0, End);
                }
            }
            else
            {
                return Content;
            }
        }

        public static string GenerateSystemName(string Value)
        {
            if (!string.IsNullOrEmpty(Value))
            {
                #region Code Block
                return Value.Replace(" ", String.Empty).ToLower();
                #endregion
            }
            else
            {

                return null;
            }

        }

        public static string CleanString(string Value)
        {
            return Regex.Replace(Value, @"\s", "");
        }

        public static string GenerateRandomNumber()
        {
            DateTime _DateTime = DateTime.Now;
            string Year = Convert.ToString(_DateTime.Year);
            string Month = Convert.ToString(_DateTime.Month);
            string Day = Convert.ToString(_DateTime.Day);
            string Minute = Convert.ToString(_DateTime.Minute);
            string Second = Convert.ToString(_DateTime.Second);
            string Millisecond = Convert.ToString(_DateTime.Millisecond);
            Random _Random = new Random();
            string RandomNo = Convert.ToString(_Random.Next(00, 99));
            return Year + Month + Day + Minute + Second + Millisecond + RandomNo;
        }

        public static string GenerateDateString()
        {
            DateTime GMTTime = DateTime.UtcNow;

            return GMTTime.ToString("ms") +
                  GMTTime.ToString("ss") +
                  GMTTime.ToString("mm") +
                  GMTTime.ToString("HH") +
                  GMTTime.ToString("dd") +
                  GMTTime.ToString("MM") +
                  GMTTime.ToString("yy");
        }

        public static DateTime GetDateTime()
        {
            TimeZoneInfo TimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime ActiveDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZone);
            return ActiveDate;
        }

        public static string GenerateRandomNumber(int length)
        {
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random _Random = new Random(BitConverter.ToInt32(seed, 0));
            int _RandomNumber = 0;
            String _GeneratedRandomNumber = "";
            for (int i = 0; i < length; i++)
            {
                _RandomNumber = _Random.Next(48, 58);
                _GeneratedRandomNumber = _GeneratedRandomNumber + (char)_RandomNumber;
            }
            return _GeneratedRandomNumber;
        }

        public static OList.Request GetSearchCondition(OList.Request _Request, string SearchColumnName, string SearchColumnValue, string SearchConditions)
        {
            string SearchExpression = "";
            if (!string.IsNullOrEmpty(SearchColumnName))
            {
                SearchExpression += string.Format(SearchColumnName + " " + SearchConditions + " {0} ", SearchColumnValue);

                _Request.SearchCondition += !string.IsNullOrEmpty(_Request.SearchCondition) ? " OR " : "";
                _Request.SearchCondition += SearchExpression;

                return _Request;
            }
            else
            {
                return _Request;
            }
        }

        public static string GetSearchCondition(string SearchColumnName, string SearchColumnValue, string SearchConditions)
        {
            if (!string.IsNullOrEmpty(SearchColumnName))
            {
                return string.Format(SearchColumnName + " " + SearchConditions + " {0} ", SearchColumnValue);
            }
            else
            {
                return null;
            }
        }

        public static OList.Request GetSortCondition(OList.Request _Request, string FieldName, string SortOrder)
        {
            #region Sort Order
            string SortCondition = "";
            if (!string.IsNullOrEmpty(FieldName))
            {
                if (!string.IsNullOrEmpty(SortOrder))
                {
                    SortCondition = FieldName + " " + SortOrder;
                }
            }
            _Request.SortExpression = SortCondition;
            return _Request;
            #endregion
        }

        public static OList.Response GetListResponse(int TotalRecords, object Data, int Offset, int Limit)
        {
            OList.Response _OListResponse = new OList.Response();
            _OListResponse.TotalRecords = TotalRecords;
            _OListResponse.Data = Data;
            _OListResponse.Offset = Offset;
            _OListResponse.Limit = Limit;
            return _OListResponse;
        }

        public static bool WriteFile(string FileName, string Content)
        {
            try
            {
                string ItemUrl = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"files/" + FileName);
                if (System.IO.File.Exists(ItemUrl))
                {
                    System.IO.File.WriteAllText(ItemUrl, Content);
                }
                else
                {
                    System.IO.File.Create(ItemUrl).Close();
                    System.IO.File.WriteAllText(ItemUrl, Content);
                }
                return true;
            }
            catch (Exception _Exception)
            {

                LogException("CORE-WriteFile", _Exception);
                return false;
            }
        }

        public static string ReadFile(string FileName)
        {
            try
            {
                string ItemUrl = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"files/" + FileName);
                if (File.Exists(ItemUrl))
                {
                    return System.IO.File.ReadAllText(ItemUrl);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {

                LogException("CORE-ReadFile", _Exception);
                return null;
            }
        }

        public static OResponse SendResponse(OUserReference? UserReference, HCore.Helper.HCoreConstant.ResponseStatus Status, object? Result, string ResponseCode, string Message)
        {

            string StatusText = Status == HCore.Helper.HCoreConstant.ResponseStatus.Success ? "Success" : "Error";
            OResponse _OResponse = new OResponse();
            _OResponse.Message = Message;
            _OResponse.Status = StatusText;
            _OResponse.Result = Result;
            _OResponse.ResponseCode = ResponseCode;
            switch (HostEnvironment)
            {
                case HostEnvironmentType.Management:
                    _OResponse.Mode = "bot";
                    break;
                case HostEnvironmentType.Test:
                    _OResponse.Mode = "test";
                    break;
                case HostEnvironmentType.Tech:
                    _OResponse.Mode = "tech";
                    break;
                case HostEnvironmentType.Dev:
                    _OResponse.Mode = "dev";
                    break;
                case HostEnvironmentType.Local:
                    _OResponse.Mode = "local";
                    break;
                case HostEnvironmentType.Live:
                    _OResponse.Mode = "play";
                    break;
                default:
                    break;
            }
            if (UserReference != null && !string.IsNullOrEmpty(UserReference.RequestKey))
            {
                try
                {
                    UserReference.ResponseData = _OResponse;
                    UserReference.ResponseStatus = StatusText;
                    //UserReference.ResponseTime = GetDateTime();
                    //var system = ActorSystem.Create("ActorCoreUsageHandlerUSAGE");
                    //var greeter = system.ActorOf<ActorCoreUsageHandler>("ActorCoreUsageHandlerUSAGE");
                    //greeter.Tell(UserReference);
                }
                catch (Exception _Exception)
                {

                }
            }
            return _OResponse;
        }

        public static void SaveLog(string Name, string Comment)
        {
            try
            {
                using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
                {
                    AtCoreLog _AtCoreLog = new AtCoreLog();
                    _AtCoreLog.Title = Name;
                    _AtCoreLog.HostName = HostName;
                    _AtCoreLog.Message = Comment;
                    _AtCoreLog.CreateDate = GetDateTime();
                    _HCoreContextLogging.AtCoreLogs.Add(_AtCoreLog);
                    _HCoreContextLogging.SaveChanges();
                    _HCoreContextLogging.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                LogException("SaveLogAsync", _Exception);
            }
        }

        public static OResponse LogException(string Title, Exception _Exception, OUserReference UserReference)
        {
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                AtCoreLog _AtCoreLog = new AtCoreLog();
                _AtCoreLog.Title = Title;
                _AtCoreLog.Message = _Exception.Message;
                _AtCoreLog.HostName = HostName;
                _AtCoreLog.Data = _Exception.ToString();
                if (UserReference != null)
                {
                    _AtCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _AtCoreLog.CreateDate = GetDateTime();
                _HCoreContextLogging.AtCoreLogs.Add(_AtCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
            }
            return SendResponse(UserReference, HCore.Helper.HCoreConstant.ResponseStatus.Error, null, "ATCL0500", "Internal server error");
        }

        public static void LogException(string Title, Exception _Exception)
        {
            #region Code Block
            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                AtCoreLog _AtCoreLog = new AtCoreLog();
                _AtCoreLog.Title = Title;
                _AtCoreLog.HostName = HostName;
                _AtCoreLog.Message = _Exception.Message;
                _AtCoreLog.Data = _Exception.ToString();
                _AtCoreLog.CreateDate = GetDateTime();
                _HCoreContextLogging.AtCoreLogs.Add(_AtCoreLog);
                _HCoreContextLogging.SaveChanges();
                _HCoreContextLogging.Dispose();
            }
            #endregion
        }

        public static async void LogData(LogType LogType, string Title, string Message, string Data, OUserReference? UserReference)
        {
            #region Code Block

            using (HCoreContextLogging _HCoreContextLogging = new HCoreContextLogging())
            {
                AtCoreLog _AtCoreLog = new AtCoreLog();
                _AtCoreLog.Title = Title;
                _AtCoreLog.Message = Message;
                _AtCoreLog.Data = Data;
                _AtCoreLog.HostName = HostName;
                _AtCoreLog.CreateDate = GetDateTime();
                if (UserReference is not null)
                {
                    _AtCoreLog.RequestReference = JsonConvert.SerializeObject(UserReference);
                }
                _HCoreContextLogging.AtCoreLogs.Add(_AtCoreLog);
                await _HCoreContextLogging.SaveChangesAsync();
            }
            #endregion
        }

        public static int? GetSystemHelperId(string ReferenceCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ReferenceCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    int? Value = _CoreContext.AtCores.Where(x => x.SystemName == ReferenceCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static string GenerateSlug(string Data)
        {
            if (!string.IsNullOrEmpty(Data))
            {
                string Slug = Data.RemoveAccents().ToLower();

                Slug = Regex.Replace(Slug, @"[^A-Za-z0-9%\s-]", "");

                Slug = Regex.Replace(Slug, @"\s+", " ").Trim();

                Slug = Regex.Replace(Slug, @"\s", "-");

                return Slug;
            }
            else
            {
                return Data;
            }
        }

        private static string RemoveAccents(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            char[] chars = text
                .Where(c => CharUnicodeInfo.GetUnicodeCategory(c)
                != UnicodeCategory.NonSpacingMark).ToArray();

            return new string(chars).Normalize(NormalizationForm.FormC);
        }

        public static long? GetAccountId(string AccountCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(AccountCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtAccounts.Where(x => x.Guid == AccountCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetOrganizationId(string OrganizationCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(OrganizationCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtAccounts.Where(x => x.Guid == OrganizationCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetDepartmentId(string DepartmentCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(DepartmentCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtDepartments.Where(x => x.Guid == DepartmentCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetDivisionId(string DivisionCode, long OrganizationId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(DivisionCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtDivisions.Where(x => x.Guid == DivisionCode && x.OrganizationId == OrganizationId).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetSubDivisionId(string SubDivisionCode, long DivisionId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(SubDivisionCode) && DivisionId > 0)
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtDivisions.Where(x => x.Guid == SubDivisionCode && x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetLocationId(string LocationCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(LocationCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtLocations.Where(x => x.Guid == LocationCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetSubLocationId(string SubLocationCode, long LocationId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(SubLocationCode) && LocationId > 0)
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtLocations.Where(x => x.Guid == SubLocationCode && x.ParentId == LocationId).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetCategoryId(string CategoryCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(CategoryCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtCategories.Where(x => x.Guid == CategoryCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetSubCategoryId(string SubCategoryCode, long CategoryId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(SubCategoryCode) && CategoryId > 0)
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtSubCategories.Where(x => x.Guid == SubCategoryCode && x.CategoryId == CategoryId).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetClassId(string ClassCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(ClassCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtClasses.Where(x => x.Guid == ClassCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static int? GetCountryId(string CountryCode, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(CountryCode))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    int? Value = _CoreContext.AtCountries.Where(x => x.Guid == CountryCode).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetStateId(string StateCode, int CountryId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(StateCode) && CountryId > 0)
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtStates.Where(x => x.Guid == StateCode && x.CountryId == CountryId).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static long? GetCityId(string CityCode, long StateId, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(CityCode) && StateId > 0)
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = _CoreContext.AtCities.Where(x => x.Guid == CityCode && x.StateId == StateId).Select(x => x.Id).FirstOrDefault();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public async static Task<long?> GetUserRole(string RoleKey, OUserReference UserReference)
        {
            if (!string.IsNullOrEmpty(RoleKey))
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    long? Value = await _CoreContext.AtCoreRoles.Where(x => x.Guid == RoleKey && (x.OrganizationId == UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1)).Select(x => x.Id).FirstOrDefaultAsync();
                    if (Value != null)
                    {
                        if (Value != 0)
                        {
                            return Value;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public static string? GenerateBarcode(string? SerialNumber)
        {
            try
            {
                if (!string.IsNullOrEmpty(SerialNumber))
                {
                    string? Date = GetDateTime().ToString("yyyy-MM");
                    string? Barcode = Date + "-" + SerialNumber;
                    return Barcode;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                LogException("GenerateBarcode", _Exception, null);
                return null;
            }
        }

        public static async Task<string?> GenerateSerialNumber(long? OrganizationId)
        {
            try
            {
                using (HCoreContext _CoreContext = new HCoreContext())
                {
                    DateTime TodayDate = GetDateTime();
                    int? Year = TodayDate.Date.Year;
                    int? Month = TodayDate.Date.Month;
                    string? SerialNumber = "";
                    var Assets = await _CoreContext.AtAssetRegisters.Where(x => x.CreatedDate.Value.Date.Year == Year && x.CreatedDate.Value.Date.Month == Month && x.OrganizationId == OrganizationId).OrderByDescending(x => x.SerialNumber).ToListAsync();
                    if (Assets.Count > 0)
                    {
                        int? MaxSerialNo = Assets.Select(x => x.SerialNumber).FirstOrDefault();
                        if (MaxSerialNo > 0 && MaxSerialNo != null)
                        {
                            int? NewSerialNo = MaxSerialNo + 1;
                            SerialNumber = NewSerialNo.Value.ToString("000000");
                        }
                    }
                    else
                    {
                        SerialNumber = "000001";
                    }

                    return SerialNumber;
                }
            }
            catch (Exception _Exception)
            {
                LogException("GenerateBarcode", _Exception, null);
                return null;
            }
        }

        public static async Task BroadCastEmail(string? ToAddress, string? Subject, string? EmailBody, OUserReference UserReference)
        {
            try
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var EmailConfig = await _HCoreContext.AtAccountEmailConfigurations.Where(x => x.AccountId == UserReference.OrganizationId).FirstOrDefaultAsync();
                    if (EmailConfig != null)
                    {
                        using (SmtpClient smtpClient = new SmtpClient(EmailConfig.SmtpServer))
                        {
                            if (HostEnvironment == HostEnvironmentType.Dev)
                            {
                                smtpClient.Port = (int)EmailConfig.Port;
                            }
                            smtpClient.Credentials = new NetworkCredential(EmailConfig.EmailAddress, HCoreEncrypt.DecryptHash(EmailConfig.Password));
                            smtpClient.EnableSsl = false;

                            using (MailMessage mailMessage = new MailMessage())
                            {
                                mailMessage.From = new MailAddress(EmailConfig.EmailAddress);
                                mailMessage.Subject = Subject;
                                mailMessage.IsBodyHtml = true;
                                mailMessage.Body = EmailBody;

                                mailMessage.To.Add(ToAddress);

                                smtpClient.Send(mailMessage);
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                LogException("BroadCastEmail", _Exception, UserReference);
            }
        }

        public static async Task CreateDownload(string? Title, IEnumerable<object> Data, OUserReference UserReference)
        {
            try
            {
                using (var _XLWorkbook = new XLWorkbook())
                {
                    var _WorkSheet = _XLWorkbook.Worksheets.Add(Title);
                    PropertyInfo[] properties = Data.First().GetType().GetProperties();
                    List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                    for (int i = 0; i < headerNames.Count; i++)
                    {
                        _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                    }
                    _WorkSheet.Cell(2, 1).InsertData(Data);
                    //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    //response.Clear();
                    //response.Charset = "";
                    //response.ContentEncoding = System.Text.Encoding.Default;
                    //response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //response.AddHeader("content-disposition", "attachment; filename=DynamicExcel.xlsx");
                    //using (MemoryStream stream = new MemoryStream())
                    //{
                    _XLWorkbook.SaveAs(Title + ".xlsx");
                    //    stream.WriteTo(response.OutputStream);
                    //}
                    //response.Flush();
                    //response.End();
                }
                //using (var _XLWorkbook = new XLWorkbook())
                //{
                //    var _WorkSheet = _XLWorkbook.Worksheets.Add(Title);
                //    PropertyInfo[] properties = Data.First().GetType().GetProperties();
                //    List<string> headerNames = properties.Select(prop => prop.Name).ToList();
                //    for (int i = 0; i < headerNames.Count; i++)
                //    {
                //        _WorkSheet.Cell(1, i + 1).Value = headerNames[i];
                //    }
                //    _WorkSheet.Cell(2, 1).InsertData(Data);
                //    MemoryStream _MemoryStream = new MemoryStream();
                //    _XLWorkbook.SaveAs(_MemoryStream);
                //    byte[] byteArray = _MemoryStream.ToArray();
                //    string dataUri = "data:application/octet-stream;base64," + Convert.ToBase64String(byteArray);
                //    await SaveStorage(Title, "xlsx", _MemoryStream, dataUri, UserReference);
                //}
            }
            catch (Exception _Exception)
            {
                LogException("CreateDownload", _Exception);
            }
        }

        public static async Task SaveStorage(string Name, string Extension, MemoryStream _MemoryStream, string Data, OUserReference UserReference)
        {
            try
            {
                if (Data != null)
                {
                    using (HCoreContext _HCoreContext = new HCoreContext())
                    {
                        bool IsFileSaved = false;
                        string FileServerName = GenerateGuid() + "." + Extension;
                        string FileDirectory = GetDateTime().Year + "/" + GetDateTime().Month + "/";
                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            FileDirectory = "l/" + FileDirectory;
                        }
                        else
                        {
                            FileDirectory = "o/" + FileDirectory;
                        }
                        string FilePath = FileDirectory + FileServerName;

                        GoogleCredential _GoogleCredential = null;
                        using (var jsonStream = new FileStream("skey.json", FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            _GoogleCredential = GoogleCredential.FromStream(jsonStream);
                        }
                        var _StorageClient = StorageClient.Create(_GoogleCredential);
                        var _UploadObject = new Google.Apis.Storage.v1.Data.Object()
                        {
                            Bucket = "rajkamal",
                            Name = FilePath,
                        };
                        try
                        {
                            _StorageClient.UploadObject(_UploadObject, _MemoryStream, null, null);
                            IsFileSaved = true;
                        }
                        catch (Exception _Exception)
                        {
                            IsFileSaved = false;
                            LogException("GCloud-Save", _Exception);
                        }

                        List<string> splitParts = await SplitString(Data);

                        AtCoreStorage? _HCCoreStorage = new AtCoreStorage();
                        _HCCoreStorage.Guid = GenerateGuid();
                        _HCCoreStorage.AccountId = UserReference.AccountId;
                        _HCCoreStorage.Files = splitParts[0];
                        if (splitParts.Count == 2)
                        {
                            if (splitParts[1] != null)
                            {
                                _HCCoreStorage.Files1 = splitParts[1];
                            }
                        }
                        if (splitParts.Count == 3)
                        {
                            if (splitParts[2] != null)
                            {
                                _HCCoreStorage.Files2 = splitParts[2];
                            }
                        }
                        if (splitParts.Count == 4)
                        {
                            if (splitParts[3] != null)
                            {
                                _HCCoreStorage.Files3 = splitParts[3];
                            }
                        }
                        _HCCoreStorage.FileName = Name;
                        _HCCoreStorage.FilePath = FilePath;
                        _HCCoreStorage.FileExtension = Extension;
                        _HCCoreStorage.FileSize = 0;
                        _HCCoreStorage.CreatedDate = GetDateTime();
                        _HCCoreStorage.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.AtCoreStorages.AddAsync(_HCCoreStorage);
                        await _HCoreContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                LogException("SaveStorage", _Exception, UserReference);
            }
        }

        public static async Task<List<string>> SplitString(string Data)
        {
            List<string> parts = new List<string>();

            for (int i = 0; i < Data.Length; i += 10000000)
            {
                int remainingLength = Math.Min(2500000, Data.Length - i);
                parts.Add(Data.Substring(i, remainingLength));
            }

            return parts;
        }
    }
}
