using HCore.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
namespace HCore.App.RKBS.WebConnect.Core
{
    public class HCoreXAuthConnector
    {
        public static List<string> openApi = new List<string>();
        private readonly RequestDelegate _next;
        public HCoreXAuthConnector(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext _HttpContext)
        {
            var _Request = _HttpContext.Request;
            string _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
            try
            {
                DateTime RequestTime = HCoreHelper.GetDateTime();
                var Url = _Request.Path.Value;
                if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 3)
                {
                    string ApiName = Url.Split("/")[3];
                    string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                    if (ApiName == "userlogin")
                    {
                        OAuth.Response? _OAuthApp = await HCoreAuth.Auth_App(_Request, SubJsonContent, RequestTime, ApiName);
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            JObject _JObject = JObject.Parse(_RequestBody);
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                    }
                    else if (ApiName == "userlogout")
                    {
                        OAuth.Response? _OAuthApp = await HCoreAuth.Auth_App_Request(_Request, SubJsonContent, RequestTime, ApiName);
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            JObject _JObject = JObject.Parse(_RequestBody);
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                    }
                    else if (ApiName == "register")
                    {
                        OAuth.Response? _OAuthApp = await HCoreAuth.Auth_App(_Request, SubJsonContent, RequestTime, ApiName);
                        if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                        {
                            JObject _JObject = JObject.Parse(_RequestBody);
                            _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                            var Newjson = JsonConvert.SerializeObject(_JObject);
                            var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                            Stream stream = await requestContent.ReadAsStreamAsync();
                            _HttpContext.Request.Body = stream;
                        }
                        else
                        {
                            string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                            var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                            var json = JsonConvert.SerializeObject(Data);
                            _HttpContext.Response.StatusCode = 401;
                            await _HttpContext.Response.WriteAsync(json);
                            return;
                        }
                    }
                    else
                    {
                        _HttpContext.Response.StatusCode = 401;
                        return;
                    }
                }
                else if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 4)
                {
                    string ApiName = Url.Split("/")[4];
                    var _Headers = _Request.Headers;
                    string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                    OAuth.Response? _OAuthApp = await HCoreAuth.Auth_App_Request(_Request, SubJsonContent, RequestTime, ApiName);
                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                    {
                        JObject _JObject = JObject.Parse(_RequestBody);
                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                        var Newjson = JsonConvert.SerializeObject(_JObject);
                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                        Stream stream = await requestContent.ReadAsStreamAsync();
                        _HttpContext.Request.Body = stream;
                    }
                    else
                    {
                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                        var json = JsonConvert.SerializeObject(Data);
                        _HttpContext.Response.StatusCode = 401;
                        await _HttpContext.Response.WriteAsync(json);
                        return;
                    }
                }
                else
                {
                    _HttpContext.Response.StatusCode = 401;
                    return;
                }

                await _next.Invoke(_HttpContext);
            }
            catch (Exception _Exception)
            {
                if (!string.IsNullOrEmpty(_RequestBody))
                {
                    HCoreHelper.LogData(HCoreConstant.LogType.Exception, "INVOCE-EX-BODY", _RequestBody, _Exception.ToString(), null);
                }
                HCoreHelper.LogException("Invoke", _Exception);
                _HttpContext.Response.StatusCode = 401;
                return;
            }
        }
    }
    public static class HCoreXAuthConnectorExtension
    {
        public static IApplicationBuilder HCoreXAuth(this IApplicationBuilder app)
        {
            app.UseMiddleware<HCoreXAuthConnector>();
            return app;
        }
    }
}

