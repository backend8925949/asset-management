﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Helpers;
using HCore.RKBS.Core.Operations.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Helpers
{
    [Produces("application/json")]
    [Route("api/core/helpers/[action]")]
    public class ATHelpersController : Controller
    {
        ManageHelpers? _ManageHelpers;

        [HttpPost]
        [ActionName("getdivisions")]
        public async Task<object> GetDivisions([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetDivisions(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdepartments")]
        public async Task<object> GetDepartments([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetDepartments(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getlocations")]
        public async Task<object> GetLocations([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetLocations(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getsublocations")]
        public async Task<object> GetSubLocations([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetSubLocations(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getvendors")]
        public async Task<object> GetVendors([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetVendors(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getemployees")]
        public async Task<object> GetEmployees([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetEmployees(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getclasses")]
        public async Task<object> GetClasses([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetClasses(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getcategories")]
        public async Task<object> GetCategories([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetCategories(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getsubcategories")]
        public async Task<object> GetSubCategories([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetSubCategories(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getemployee")]
        public async Task<object> GetEmployee([FromBody] OHelpers.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetEmployee(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdepartment")]
        public async Task<object> GetDepartment([FromBody] OReference _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetDepartment(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getuserroles")]
        public async Task<object> GetUserRoles([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetUserRoles(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getusers")]
        public async Task<object> GetUsers([FromBody] OList.Request _Request)
        {
            _ManageHelpers = new ManageHelpers();
            OResponse _Response = await _ManageHelpers.GetUsers(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
