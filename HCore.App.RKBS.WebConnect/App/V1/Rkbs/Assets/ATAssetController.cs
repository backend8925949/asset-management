﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Assets;
using HCore.RKBS.Core.Object.BackgroundProcessor;
using HCore.RKBS.Core.Operations.Assets;
using HCore.RKBS.Core.Operations.BackgroundProcessor;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Assets
{
    [Produces("application/json")]
    [Route("api/asset/op/[action]")]
    public class ATAssetController
    {
        ManageAssets? _ManageAssets;

        [HttpPost]
        [ActionName("getasset")]
        public async Task<object> GetAsset([FromBody] OReference _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassets")]
        public async Task<object> GetAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdisposedassets")]
        public async Task<object> GetDisposedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetDisposedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getexpiredassets")]
        public async Task<object> GetExpiredAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetExpiredAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetbarcode")]
        public async Task<object> GetAssetBarcode([FromBody] OReference _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssetBarcode(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getbulkassets")]
        public async Task<object> GetBulkAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetBulkAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getissuedasset")]
        public async Task<object> GetIssuedAsset([FromBody] OReference _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetIssuedAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getissuedassets")]
        public async Task<object> GetIssuedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetIssuedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getunissuedassets")]
        public async Task<object> GetUnIssuedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetUnIssuedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("gettransferedassets")]
        public async Task<object> GetTransferedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetTransferedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getreturnedasset")]
        public async Task<object> GetReturnedAsset([FromBody] OReference _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetReturnedAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getreturnedassets")]
        public async Task<object> GetReturnedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetReturnedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getmaintenanceasset")]
        public async Task<object> GetMaintenanceAsset([FromBody] OReference _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetMaintenanceAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getmaintenanceassets")]
        public async Task<object> GetMaintenanceAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetMaintenanceAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetauditreport")]
        public async Task<object> GetAssetAuditReport([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssetAuditReport(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassethistory")]
        public async Task<object> GetAssetHistory([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssetHistory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetdepreciation")]
        public async Task<object> GetAssetDepreciation([FromBody] OReference _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssetDepreciation(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetdepreciations")]
        public async Task<object> GetAssetDepreciation([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssetDepreciation(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
