﻿using HCore.Helper;
using HCore.RKBS.Core.Operations.Assets;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Assets
{
    [Produces("application/json")]
    [Route("api/asset/report/[action]")]
    public class ATAssetReportsController
    {
        ManageAssetReport? _ManageAssetReport;

        [HttpPost]
        [ActionName("getassetsoverview")]
        public async Task<object> GetAssetsOverview([FromBody] OReference _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetAssetsOverview(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetissuereport")]
        public async Task<object> GetAssetIssueReport([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetAssetIssueReport(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetmaintenancereport")]
        public async Task<object> GetAssetMaintenanceReport([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetAssetMaintenanceReport(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassetreturnreport")]
        public async Task<object> GetAssetReturnReport([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetAssetReturnReport(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getwithbarcodeassetsstock")]
        public async Task<object> GetWithBarcodeAssetsStock([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetWithBarcodeAssetsStock(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getwithoutbarcodeassetsstock")]
        public async Task<object> GetWithoutBarcodeAssetsStock([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetWithoutBarcodeAssetsStock(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getissuedassets")]
        public async Task<object> GetIssuedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetIssuedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("gettransferredassets")]
        public async Task<object> GetTransferredAssets([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetTransferredAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getmappedassets")]
        public async Task<object> GetMappedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetMappedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassets")]
        public async Task<object> GetAssets([FromBody] OList.Request _Request)
        {
            _ManageAssetReport = new ManageAssetReport();
            OResponse _Response = await _ManageAssetReport.GetAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
