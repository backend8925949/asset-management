﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Assets;
using HCore.RKBS.Core.Operations.Assets;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Assets
{
    [Produces("application/json")]
    [Route("api/asset/operations/[action]")]
    public class ATAssetOperationsController
    {
        ManageAssetOperations? _ManageAssetOperations;

        [HttpPost]
        [ActionName("registerasset")]
        public async Task<object> RegisterAsset([FromBody] OAssetOperations.Register _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.RegisterAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updateasset")]
        public async Task<object> UpdateAsset([FromBody] OAssetOperations.Register _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.UpdateAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("returnasset")]
        public async Task<object> ReturnAsset([FromBody] OAssetOperations.RetrurnAsset.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ReturnAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("approvereturn")]
        public async Task<object> ApproveAssetReturn([FromBody] OReference _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ApproveAssetReturn(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("issueasset")]
        public async Task<object> IssueAsset([FromBody] OAssetOperations.IssueAsset.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.IssueAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("approveissue")]
        public async Task<object> ApproveAssetIssue([FromBody] OReference _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ApproveAssetIssue(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("transferasset")]
        public async Task<object> TransferAsset([FromBody] OAssetOperations.TransferAsset.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.TransferAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("approvetransfer")]
        public async Task<object> ApproveAssetTransfer([FromBody] OReference _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ApproveAssetTransfer(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("issuetomaintenance")]
        public async Task<object> IssueAssetToMaintenance([FromBody] OAssetOperations.Maintenance.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.IssueAssetToMaintenance(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("issuefrommaintenance")]
        public async Task<object> ReturnAssetFromMaintenance([FromBody] OAssetOperations.Maintenance.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ReturnAssetFromMaintenance(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("printbarcodes")]
        public async Task<object> MultipleBarcodePrint([FromBody] OAssetOperations.MultipleBarcodePrinting.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.MultipleBarcodePrint(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("reprintbarcode")]
        public async Task<object> RePrintBarcode([FromBody] OAssetOperations.RePrintBarcode _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.RePrintBarcode(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("disposeasset")]
        public async Task<object> DisposeAsset([FromBody] OAssetOperations.Dispose _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.DisposeAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("validatebarcode")]
        public async Task<object> ValidateBarcode([FromBody] OReference _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ValidateBarcode(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("registerassets")]
        public async Task<object> RegisterAssets([FromBody] OAssetOperations.BulkUpload.Request _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ManageAssetUpload(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
