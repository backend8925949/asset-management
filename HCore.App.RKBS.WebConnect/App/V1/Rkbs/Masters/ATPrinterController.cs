﻿using HCore.Helper;
using Microsoft.AspNetCore.Mvc;
using HCore.RKBS.Core.Operations.Master;
using HCore.RKBS.Core.Object.Master;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/printers/[action]")]
    public class ATPrinterController
    {
        ManagePrinters? _ManagePrinters;

        [HttpPost]
        [ActionName("getprinters")]
        public async Task<object> GetPrinters([FromBody] OList.Request _Request)
        {
            _ManagePrinters = new ManagePrinters();
            OResponse _Response = await _ManagePrinters.GetPrinters(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("saveprintdata")]
        public async Task<object> SavePrintData([FromBody] OPrinter.Request _Request)
        {
            _ManagePrinters = new ManagePrinters();
            OResponse _Response = await _ManagePrinters.SavePrintData(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("saveprintsdata")]
        public async Task<object> SavePrintsData([FromBody] OPrinter.ListRequest _Request)
        {
            _ManagePrinters = new ManagePrinters();
            OResponse _Response = await _ManagePrinters.SavePrintsData(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
