﻿using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/department/[action]")]
    public class ATDepartmentController
    {
        ManageDepartment? _ManageDepartment;

        [HttpPost]
        [ActionName("savedepartment")]
        public async Task<object> SaveDepartment([FromBody] ODepartment.Save.Request _Request)
        {
            _ManageDepartment = new ManageDepartment();
            OResponse _Response = await _ManageDepartment.SaveDepartment(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatedepartment")]
        public async Task<object> UpdateDepartment([FromBody] ODepartment.Update.Request _Request)
        {
            _ManageDepartment = new ManageDepartment();
            OResponse _Response = await _ManageDepartment.UpdateDepartment(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deletedepartment")]
        public async Task<object> DeleteDepartment([FromBody] OReference _Request)
        {
            _ManageDepartment = new ManageDepartment();
            OResponse _Response = await _ManageDepartment.DeleteDepartment(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdepartment")]
        public async Task<object> GetDepartment([FromBody] OReference _Request)
        {
            _ManageDepartment = new ManageDepartment();
            OResponse _Response = await _ManageDepartment.GetDepartment(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdepartments")]
        public async Task<object> GetDepartments([FromBody] OList.Request _Request)
        {
            _ManageDepartment = new ManageDepartment();
            OResponse _Response = await _ManageDepartment.GetDepartments(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
