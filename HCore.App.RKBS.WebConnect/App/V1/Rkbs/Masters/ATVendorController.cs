﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/vendor/[action]")]
    public class ATVendorController
    {
        ManageVendor? _ManageVendor;

        [HttpPost]
        [ActionName("savevendor")]
        public async Task<object> SaveVendor([FromBody] OVendor.Save.Request _Request)
        {
            _ManageVendor = new ManageVendor();
            OResponse _Response = await _ManageVendor.SaveVendor(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatevendor")]
        public async Task<object> UpdateVendor([FromBody] OVendor.Update.Request _Request)
        {
            _ManageVendor = new ManageVendor();
            OResponse _Response = await _ManageVendor.UpdateVendor(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deletevendor")]
        public async Task<object> DeleteVendor([FromBody] OReference _Request)
        {
            _ManageVendor = new ManageVendor();
            OResponse _Response = await _ManageVendor.DeleteVendor(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getvendor")]
        public async Task<object> GetVendor([FromBody] OReference _Request)
        {
            _ManageVendor = new ManageVendor();
            OResponse _Response = await _ManageVendor.GetVendor(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getvendors")]
        public async Task<object> GetVendors([FromBody] OList.Request _Request)
        {
            _ManageVendor = new ManageVendor();
            OResponse _Response = await _ManageVendor.GetVendors(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
