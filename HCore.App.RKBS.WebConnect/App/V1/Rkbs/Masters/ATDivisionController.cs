﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/division/[action]")]
    public class ATDivisionController
    {
        ManageDivision? _ManageDivision;

        [HttpPost]
        [ActionName("savedivision")]
        public async Task<object> SaveDivision([FromBody] ODivision.Save.Request _Request)
        {
            _ManageDivision = new ManageDivision();
            OResponse _Response = await _ManageDivision.SaveDivision(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatedivision")]
        public async Task<object> UpdateDivision([FromBody] ODivision.Update.Request _Request)
        {
            _ManageDivision = new ManageDivision();
            OResponse _Response = await _ManageDivision.UpdateDivision(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deletedivision")]
        public async Task<object> DeleteDivision([FromBody] OReference _Request)
        {
            _ManageDivision = new ManageDivision();
            OResponse _Response = await _ManageDivision.DeleteDivision(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdivision")]
        public async Task<object> GetDivision([FromBody] OReference _Request)
        {
            _ManageDivision = new ManageDivision();
            OResponse _Response = await _ManageDivision.GetDivision(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdivisions")]
        public async Task<object> GetDivisions([FromBody] OList.Request _Request)
        {
            _ManageDivision = new ManageDivision();
            OResponse _Response = await _ManageDivision.GetDivisions(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
