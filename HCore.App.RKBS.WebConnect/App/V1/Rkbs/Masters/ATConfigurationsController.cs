﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/config/[action]")]
    public class ATConfigurationsController
    {
        ManageConfigurations? _ManageConfigurations;

        [HttpPost]
        [ActionName("saveemailconfiguration")]
        public async Task<object> SaveEmailConfiguration([FromBody] OConfigurations.EmailConfiguration.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.SaveEmailConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updateemailconfiguration")]
        public async Task<object> UpdateEmailConfiguration([FromBody] OConfigurations.EmailConfiguration.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.UpdateEmailConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getemailconfiguration")]
        public async Task<object> GetEmailConfiguration([FromBody] OReference _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.GetEmailConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("savedepreciationconfiguration")]
        public async Task<object> SaveDepreciationConfiguration([FromBody] OConfigurations.DepreciationConfiguration.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.SaveDepreciationConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatedepreciationconfiguration")]
        public async Task<object> UpdateDepreciationConfiguration([FromBody] OConfigurations.DepreciationConfiguration.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.UpdateDepreciationConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getdepreciationconfiguration")]
        public async Task<object> GetDepreciationConfiguration([FromBody] OReference _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.GetDepreciationConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("saverequisitionconfig")]
        public async Task<object> SaveRequisitionConfig([FromBody] OConfigurations.RequisitionConfig.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.SaveRequisitionConfig(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updaterequisitionconfig")]
        public async Task<object> UpdateRequisitionConfig([FromBody] OConfigurations.RequisitionConfig.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.UpdateRequisitionConfig(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getrequisitionconfig")]
        public async Task<object> GetRequisitionConfig([FromBody] OReference _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.GetRequisitionConfig(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getrequisitionconfigs")]
        public async Task<object> GetRequisitionConfig([FromBody] OList.Request _Request)
        {
            _ManageConfigurations = new ManageConfigurations();
            OResponse _Response = await _ManageConfigurations.GetRequisitionConfig(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
