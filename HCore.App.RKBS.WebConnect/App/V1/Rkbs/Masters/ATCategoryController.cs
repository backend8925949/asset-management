﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/cat/[action]")]
    public class ATCategoryController
    {
        ManageCategory? _ManageCategory;

        [HttpPost]
        [ActionName("savecategory")]
        public async Task<object> SaveCategory([FromBody] OCategory.Save.Request _Request)
        {
            _ManageCategory = new ManageCategory();
            OResponse _Response = await _ManageCategory.SaveCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatecategory")]
        public async Task<object> UpdateCategory([FromBody] OCategory.Update.Request _Request)
        {
            _ManageCategory = new ManageCategory();
            OResponse _Response = await _ManageCategory.UpdateCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deletecategory")]
        public async Task<object> DeleteCategory([FromBody] OReference _Request)
        {
            _ManageCategory = new ManageCategory();
            OResponse _Response = await _ManageCategory.DeleteCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getcategory")]
        public async Task<object> GetCategory([FromBody] OReference _Request)
        {
            _ManageCategory = new ManageCategory();
            OResponse _Response = await _ManageCategory.GetCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getcategories")]
        public async Task<object> GetCategories([FromBody] OList.Request _Request)
        {
            _ManageCategory = new ManageCategory();
            OResponse _Response = await _ManageCategory.GetCategories(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
