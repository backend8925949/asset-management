﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/subcat/[action]")]
    public class ATSubCategoryController
    {
        ManageSubCategory? _ManageSubCategory;

        [HttpPost]
        [ActionName("savesubcategory")]
        public async Task<object> SaveSubCategory([FromBody] OSubCategory.Save.Request _Request)
        {
            _ManageSubCategory = new ManageSubCategory();
            OResponse _Response = await _ManageSubCategory.SaveSubCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatesubcategory")]
        public async Task<object> UpdateSubCategory([FromBody] OSubCategory.Update.Request _Request)
        {
            _ManageSubCategory = new ManageSubCategory();
            OResponse _Response = await _ManageSubCategory.UpdateSubCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deletesubcategory")]
        public async Task<object> DeleteSubCategory([FromBody] OReference _Request)
        {
            _ManageSubCategory = new ManageSubCategory();
            OResponse _Response = await _ManageSubCategory.DeleteSubCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getsubcategory")]
        public async Task<object> GetSubCategory([FromBody] OReference _Request)
        {
            _ManageSubCategory = new ManageSubCategory();
            OResponse _Response = await _ManageSubCategory.GetSubCategory(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getsubcategories")]
        public async Task<object> GetSubCategories([FromBody] OList.Request _Request)
        {
            _ManageSubCategory = new ManageSubCategory();
            OResponse _Response = await _ManageSubCategory.GetSubCategories(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
