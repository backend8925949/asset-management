﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/class/[action]")]
    public class ATClassController
    {
        ManageClass? _ManageClass;

        [HttpPost]
        [ActionName("saveclass")]
        public async Task<object> SaveClass([FromBody] OClass.Save.Request _Request)
        {
            _ManageClass = new ManageClass();
            OResponse _Response = await _ManageClass.SaveClass(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updateclass")]
        public async Task<object> UpdateClass([FromBody] OClass.Update.Request _Request)
        {
            _ManageClass = new ManageClass();
            OResponse _Response = await _ManageClass.UpdateClass(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deleteclass")]
        public async Task<object> DeleteClass([FromBody] OReference _Request)
        {
            _ManageClass = new ManageClass();
            OResponse _Response = await _ManageClass.DeleteClass(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getclass")]
        public async Task<object> GetClass([FromBody] OReference _Request)
        {
            _ManageClass = new ManageClass();
            OResponse _Response = await _ManageClass.GetClass(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getclasses")]
        public async Task<object> GetClasses([FromBody] OList.Request _Request)
        {
            _ManageClass = new ManageClass();
            OResponse _Response = await _ManageClass.GetClasses(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
