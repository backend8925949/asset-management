﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/location/[action]")]
    public class ATLocationController
    {
        ManageLocation? _ManageLocation;

        [HttpPost]
        [ActionName("savelocation")]
        public async Task<object> SaveLocation([FromBody] OLocation.Save.Request _Request)
        {
            _ManageLocation = new ManageLocation();
            OResponse _Response = await _ManageLocation.SaveLocation(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updatelocation")]
        public async Task<object> UpdateLocation([FromBody] OLocation.Update.Request _Request)
        {
            _ManageLocation = new ManageLocation();
            OResponse _Response = await _ManageLocation.UpdateLocation(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deletelocation")]
        public async Task<object> DeleteLocation([FromBody] OReference _Request)
        {
            _ManageLocation = new ManageLocation();
            OResponse _Response = await _ManageLocation.DeleteLocation(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getlocation")]
        public async Task<object> GetLocation([FromBody] OReference _Request)
        {
            _ManageLocation = new ManageLocation();
            OResponse _Response = await _ManageLocation.GetLocation(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getlocations")]
        public async Task<object> GetLocations([FromBody] OList.Request _Request)
        {
            _ManageLocation = new ManageLocation();
            OResponse _Response = await _ManageLocation.GetLocations(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getsublocations")]
        public async Task<object> GetSubLocations([FromBody] OList.Request _Request)
        {
            _ManageLocation = new ManageLocation();
            OResponse _Response = await _ManageLocation.GetSubLocations(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
