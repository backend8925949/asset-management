﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/bulkupload/[action]")]
    public class ATBulkUploadController : Controller
    {
        ManageBulkUpload? _ManageBulkUpload;

        [HttpPost]
        [ActionName("saveemployees")]
        public async Task<object> SaveEmployees([FromBody] OBulkUpload.Request _Request)
        {
            _ManageBulkUpload = new ManageBulkUpload();
            OResponse _Response = await _ManageBulkUpload.SaveEmployees(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("saveusers")]
        public async Task<object> SaveUsers([FromBody] OBulkUpload.Request _Request)
        {
            _ManageBulkUpload = new ManageBulkUpload();
            OResponse _Response = await _ManageBulkUpload.SaveUsers(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
