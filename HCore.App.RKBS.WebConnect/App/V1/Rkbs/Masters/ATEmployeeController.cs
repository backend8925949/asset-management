﻿using Microsoft.AspNetCore.Mvc;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/emp/[action]")]
    public class ATEmployeeController
    {
        ManageEmployee? _ManageEmployee;

        [HttpPost]
        [ActionName("saveemployee")]
        public async Task<object> SaveEmployee([FromBody] OEmployee.Save.Request _Request)
        {
            _ManageEmployee = new ManageEmployee();
            OResponse _Response = await _ManageEmployee.SaveEmployee(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updateemployee")]
        public async Task<object> UpdateEmployee([FromBody] OEmployee.Update.Request _Request)
        {
            _ManageEmployee = new ManageEmployee();
            OResponse _Response = await _ManageEmployee.UpdateEmployee(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deleteemployee")]
        public async Task<object> DeleteEmployee([FromBody] OReference _Request)
        {
            _ManageEmployee = new ManageEmployee();
            OResponse _Response = await _ManageEmployee.DeleteEmployee(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getemployee")]
        public async Task<object> GetEmployee([FromBody] OReference _Request)
        {
            _ManageEmployee = new ManageEmployee();
            OResponse _Response = await _ManageEmployee.GetEmployee(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getemployees")]
        public async Task<object> GetEmployees([FromBody] OList.Request _Request)
        {
            _ManageEmployee = new ManageEmployee();
            OResponse _Response = await _ManageEmployee.GetEmployees(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
