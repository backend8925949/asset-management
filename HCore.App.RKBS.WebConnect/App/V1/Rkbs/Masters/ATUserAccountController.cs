﻿using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Masters
{
    [Produces("application/json")]
    [Route("api/master/user/[action]")]
    public class ATUserAccountController
    {
        HCore.RKBS.Core.Operations.Master.ManageUserAccount? _ManageUserAccount;

        [HttpPost]
        [ActionName("saveuser")]
        public async Task<object> SaveUser([FromBody] OUserAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new HCore.RKBS.Core.Operations.Master.ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.SaveUser(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updateuser")]
        public async Task<object> UpdateUser([FromBody] OUserAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new HCore.RKBS.Core.Operations.Master.ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.UpdateUser(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("deleteuser")]
        public async Task<object> DeleteUser([FromBody] OReference _Request)
        {
            _ManageUserAccount = new HCore.RKBS.Core.Operations.Master.ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.DeleteUser(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getuser")]
        public async Task<object> GetUser([FromBody] OReference _Request)
        {
            _ManageUserAccount = new HCore.RKBS.Core.Operations.Master.ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.GetUser(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getusers")]
        public async Task<object> GetUsers([FromBody] OList.Request _Request)
        {
            _ManageUserAccount = new HCore.RKBS.Core.Operations.Master.ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.GetUsers(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
