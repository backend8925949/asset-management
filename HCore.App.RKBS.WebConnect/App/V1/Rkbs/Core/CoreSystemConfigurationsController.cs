﻿using HCore.Helper;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/sys/[action]")]
    public class CoreSystemConfigurationsController
    {
        ManageSystemConfigurations? _ManageSystemConfigurations;

        [HttpPost]
        [ActionName("getdownloads")]
        public async Task<object> GetDownloads([FromBody] OList.Request _Request)
        {
            _ManageSystemConfigurations = new ManageSystemConfigurations();
            OResponse _Response = await _ManageSystemConfigurations.GetDownloads(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getsystemlogs")]
        public async Task<object> GetSystemLogs([FromBody] OList.Request _Request)
        {
            _ManageSystemConfigurations = new ManageSystemConfigurations();
            OResponse _Response = await _ManageSystemConfigurations.GetSystemLogs(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
