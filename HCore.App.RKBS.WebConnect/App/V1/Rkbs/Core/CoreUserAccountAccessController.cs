﻿using HCore;
using HCore.Object;
using HCore.Helper;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/[action]")]
    public class CoreUserAccountAccessController
    {
        ManageUserAccount? _ManageUserAccount;

        [HttpPost]
        [ActionName("userlogin")]
        public async Task<object> UserLogin([FromBody] OAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.Login(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("userlogout")]
        public async Task<object> UserLogout([FromBody] OAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.Logout(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("register")]
        public async Task<object> CreateUserAccount([FromBody] OAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.CreateUserAccount(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
