﻿using HCore;
using HCore.Helper;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/city/[action]")]
    public class CoreCityController
    {
        ManageCity? _ManageCity;

        [HttpPost]
        [ActionName("getcities")]
        public async Task<object> GetCities([FromBody] OList.Request _Request)
        {
            _ManageCity = new ManageCity();
            OResponse _Response = await _ManageCity.GetCities(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
