﻿using HCore.Helper;
using HCore.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/access/[action]")]
    public class CoreRoleFeatureController
    {
        ManageCoreRoleFeature? _ManageCoreRoleFeature;

        [HttpPost]
        [ActionName("saverolefeature")]
        public async Task<object> SaveRoleFeatures([FromBody] ORoleManagement.RoleFeature.Request _Request)
        {
            _ManageCoreRoleFeature = new ManageCoreRoleFeature();
            OResponse _Response = await _ManageCoreRoleFeature.SaveRoleFeatures(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("updaterolefeature")]
        public async Task<object> UpdateRoleFeatures([FromBody] ORoleManagement.RoleFeature.Request _Request)
        {
            _ManageCoreRoleFeature = new ManageCoreRoleFeature();
            OResponse _Response = await _ManageCoreRoleFeature.UpdateRoleFeatures(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getrolefeature")]
        public async Task<object> GetRoleFeature([FromBody] OReference _Request)
        {
            _ManageCoreRoleFeature = new ManageCoreRoleFeature();
            OResponse _Response = await _ManageCoreRoleFeature.GetRoleFeature(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getrolefeatures")]
        public async Task<object> GetRoleFeatures([FromBody] OList.Request _Request)
        {
            _ManageCoreRoleFeature = new ManageCoreRoleFeature();
            OResponse _Response = await _ManageCoreRoleFeature.GetRoleFeatures(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getfeatures")]
        public async Task<object> GetFeatures([FromBody] OList.Request _Request)
        {
            _ManageCoreRoleFeature = new ManageCoreRoleFeature();
            OResponse _Response = await _ManageCoreRoleFeature.GetFeatures(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
