﻿using HCore;
using HCore.Helper;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/state/[action]")]
    public class CoreStateController
    {
        ManageState? _ManageState;

        [HttpPost]
        [ActionName("getstates")]
        public async Task<object> GetStates([FromBody] OList.Request _Request)
        {
            _ManageState = new ManageState();
            OResponse _Response = await _ManageState.GetStates(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
