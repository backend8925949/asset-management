﻿using HCore.Helper;
using HCore.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/feature/[action]")]
    public class CoreFeatureController
    {
        ManageCoreFeature? _ManageCoreFeature;

        [HttpPost]
        [ActionName("savefeature")]
        public async Task<object> SaveFeatures([FromBody] ORoleManagement.Feature.Request _Request)
        {
            _ManageCoreFeature = new ManageCoreFeature();
            OResponse _Response = await _ManageCoreFeature.SaveFeature(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getfeature")]
        public async Task<object> GetFeature([FromBody] OReference _Request)
        {
            _ManageCoreFeature = new ManageCoreFeature();
            OResponse _Response = await _ManageCoreFeature.GetFeature(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getfeatures")]
        public async Task<object> GetFeatures([FromBody] OList.Request _Request)
        {
            _ManageCoreFeature = new ManageCoreFeature();
            OResponse _Response = await _ManageCoreFeature.GetFeatures(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
