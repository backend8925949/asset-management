﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Helpers
{
    public class FrameworkHelpers
    {
        HCoreContext? _HCoreContext;

        internal async Task<OResponse> GetDivisions(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "DivisionId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "DivisionId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtDivisions.Where(x => (x.ParentId < 1 || x.ParentId == null) && x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Division
                                                {
                                                    DivisionId = x.Id,
                                                    DivisionKey = x.Guid,
                                                    DivisionName = x.DivisionName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Division> _Divisions = await _HCoreContext.AtDivisions.Where(x => (x.ParentId < 1 || x.ParentId == null) && x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                         .Select(x => new OHelpers.Division
                                                         {
                                                             DivisionId = x.Id,
                                                             DivisionKey = x.Guid,
                                                             DivisionName = x.DivisionName,
                                                         }).OrderBy(_Request.SortExpression)
                                                         .Where(_Request.SearchCondition)
                                                         .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Divisions, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDivisions", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDepartments(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "DepartmentId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "DepartmentId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtDepartments.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Department
                                                {
                                                    DepartmentId = x.Id,
                                                    DepartmentKey = x.Guid,
                                                    DepartmentName = x.DepartmentName,
                                                    DivisionId = x.DivisionId,
                                                    DivisionKey = x.Division.Guid,
                                                    DivisionName = x.Division.DivisionName,
                                                    LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                                    SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Department> _Departments = await _HCoreContext.AtDepartments.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                           .Select(x => new OHelpers.Department
                                                           {
                                                               DepartmentId = x.Id,
                                                               DepartmentKey = x.Guid,
                                                               DepartmentName = x.DepartmentName,
                                                               DivisionId = x.DivisionId,
                                                               DivisionKey = x.Division.Guid,
                                                               DivisionName = x.Division.DivisionName,
                                                               LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                               LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                               LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                                               SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                               SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                               SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                                           }).OrderBy(_Request.SortExpression)
                                                           .Where(_Request.SearchCondition)
                                                           .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Departments, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDepartments", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetLocations(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "LocationId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "LocationId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtLocations.Where(x => x.StatusId == HelperStatus.Default.Active && (x.ParentId == null || x.ParentId < 1) && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Location
                                                {
                                                    LocationId = x.Id,
                                                    LocationKey = x.Guid,
                                                    LocationName = x.LocationName,
                                                    DivisionId = x.DivisionId,
                                                    DivisionKey = x.Division.Guid,
                                                    DivisionName = x.Division.DivisionName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Location> _Locations = await _HCoreContext.AtLocations.Where(x => x.StatusId == HelperStatus.Default.Active && (x.ParentId == null || x.ParentId < 1) && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                         .Select(x => new OHelpers.Location
                                                         {
                                                             LocationId = x.Id,
                                                             LocationKey = x.Guid,
                                                             LocationName = x.LocationName,
                                                             DivisionId = x.DivisionId,
                                                             DivisionKey = x.Division.Guid,
                                                             DivisionName = x.Division.DivisionName,
                                                         }).OrderBy(_Request.SortExpression)
                                                         .Where(_Request.SearchCondition)
                                                         .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Locations, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetLocations", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSubLocations(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "SubLocationId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "SubLocationId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtLocations.Where(x => x.StatusId == HelperStatus.Default.Active && x.ParentId > 0 && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.SubLocation
                                                {
                                                    SubLocationId = x.Id,
                                                    SubLocationKey = x.Guid,
                                                    SubLocationName = x.LocationName,
                                                    LocationId = x.ParentId,
                                                    LocationKey = x.Parent.Guid,
                                                    LocationName = x.Parent.LocationName,
                                                    DivisionId = x.Parent.DivisionId,
                                                    DivisionKey = x.Parent.Division.Guid,
                                                    DivisionName = x.Parent.Division.DivisionName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.SubLocation> _SubLocations = await _HCoreContext.AtLocations.Where(x => x.StatusId == HelperStatus.Default.Active && x.ParentId > 0 && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                               .Select(x => new OHelpers.SubLocation
                                                               {
                                                                   SubLocationId = x.Id,
                                                                   SubLocationKey = x.Guid,
                                                                   SubLocationName = x.LocationName,
                                                                   LocationId = x.ParentId,
                                                                   LocationKey = x.Parent.Guid,
                                                                   LocationName = x.Parent.LocationName,
                                                                   DivisionId = x.Parent.DivisionId,
                                                                   DivisionKey = x.Parent.Division.Guid,
                                                                   DivisionName = x.Parent.Division.DivisionName,
                                                               }).OrderBy(_Request.SortExpression)
                                                               .Where(_Request.SearchCondition)
                                                               .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SubLocations, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSubLocations", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetVendors(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "VendorId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "VendorId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId && (x.AccountTypeId == VendorType.InsuranceVendor || x.AccountTypeId == VendorType.AssetVendor))
                                                .Select(x => new OHelpers.Vendor
                                                {
                                                    VendorId = x.Id,
                                                    VendorKey = x.Guid,
                                                    Name = x.DisplayName,
                                                    TypeId = (int)x.AccountTypeId,
                                                    TypeCode = x.AccountType.SystemName,
                                                    TypeName = x.AccountType.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Vendor> _Vendors = await _HCoreContext.AtAccounts.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId && (x.AccountTypeId == VendorType.InsuranceVendor || x.AccountTypeId == VendorType.AssetVendor))
                                                     .Select(x => new OHelpers.Vendor
                                                     {
                                                         VendorId = x.Id,
                                                         VendorKey = x.Guid,
                                                         Name = x.DisplayName,
                                                         TypeId = (int)x.AccountTypeId,
                                                         TypeCode = x.AccountType.SystemName,
                                                         TypeName = x.AccountType.Name,
                                                     }).OrderBy(_Request.SortExpression)
                                                     .Where(_Request.SearchCondition)
                                                     .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Vendors, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVendors", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetEmployees(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "EmployeeId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "EmployeeId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.StatusId == HelperStatus.Default.Active && x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Employee
                                                {
                                                    EmployeeId = x.Id,
                                                    EmployeeKey = x.Guid,
                                                    Name = x.DisplayName,
                                                    EmployeeCode = x.AccountCode,
                                                    DepartmentId = x.DepartmentId,
                                                    DepartmentKey = x.Department.Guid,
                                                    DepartmentName = x.Department.DepartmentName,
                                                    SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),
                                                    LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),
                                                    DivisionId = x.DivisionId,
                                                    DivisionKey = x.Division.Guid,
                                                    DivisionName = x.Division.DivisionName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Employee> _Employees = await _HCoreContext.AtAccounts.Where(x => x.StatusId == HelperStatus.Default.Active && x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                         .Select(x => new OHelpers.Employee
                                                         {
                                                             EmployeeId = x.Id,
                                                             EmployeeKey = x.Guid,
                                                             Name = x.DisplayName,
                                                             EmployeeCode = x.AccountCode,
                                                             DepartmentId = x.DepartmentId,
                                                             DepartmentKey = x.Department.Guid,
                                                             DepartmentName = x.Department.DepartmentName,
                                                             SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                                             SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                                             SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),
                                                             LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                                             LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                                             LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),
                                                             DivisionId = x.DivisionId,
                                                             DivisionKey = x.Division.Guid,
                                                             DivisionName = x.Division.DivisionName,
                                                         }).OrderBy(_Request.SortExpression)
                                                         .Where(_Request.SearchCondition)
                                                         .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Employees, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetEmployees", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetClasses(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ClassId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ClassId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtClasses.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Class
                                                {
                                                    ClassId = x.Id,
                                                    ClassKey = x.Guid,
                                                    ClassName = x.ClassName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Class> _Classes = await _HCoreContext.AtClasses.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                         .Select(x => new OHelpers.Class
                                                         {
                                                             ClassId = x.Id,
                                                             ClassKey = x.Guid,
                                                             ClassName = x.ClassName,
                                                         }).OrderBy(_Request.SortExpression)
                                                         .Where(_Request.SearchCondition)
                                                         .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Classes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetClasses", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetCategories(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "CategoryId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CategoryId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCategories.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Category
                                                {
                                                    CategoryId = x.Id,
                                                    CategoryKey = x.Guid,
                                                    CategoryName = x.CategoryName,
                                                    ClassId = x.ClassId,
                                                    ClassKey = x.Class.Guid,
                                                    ClassName = x.Class.ClassName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Category> _Categories = await _HCoreContext.AtCategories.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                         .Select(x => new OHelpers.Category
                                                         {
                                                             CategoryId = x.Id,
                                                             CategoryKey = x.Guid,
                                                             CategoryName = x.CategoryName,
                                                             ClassId = x.ClassId,
                                                             ClassKey = x.Class.Guid,
                                                             ClassName = x.Class.ClassName,
                                                         }).OrderBy(_Request.SortExpression)
                                                         .Where(_Request.SearchCondition)
                                                         .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categories, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSubCategories(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "SubCategoryId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "SubCategoryId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtSubCategories.Where(x => x.StatusId == HelperStatus.Default.Active && x.Category.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.SubCategory
                                                {
                                                    SubCategoryId = x.Id,
                                                    SubCategoryKey = x.Guid,
                                                    SubCategoryName = x.SubCategoryName,
                                                    CategoryId = x.CategoryId,
                                                    CategoryKey = x.Category.Guid,
                                                    CategoryName = x.Category.CategoryName,
                                                    ClassId = x.Category.ClassId,
                                                    ClassKey = x.Category.Class.Guid,
                                                    ClassName = x.Category.Class.ClassName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.SubCategory> _SubCategories = await _HCoreContext.AtSubCategories.Where(x => x.StatusId == HelperStatus.Default.Active && x.Category.OrganizationId == _Request.UserReference.OrganizationId)
                                                         .Select(x => new OHelpers.SubCategory
                                                         {
                                                             SubCategoryId = x.Id,
                                                             SubCategoryKey = x.Guid,
                                                             SubCategoryName = x.SubCategoryName,
                                                             CategoryId = x.CategoryId,
                                                             CategoryKey = x.Category.Guid,
                                                             CategoryName = x.Category.CategoryName,
                                                             ClassId = x.Category.ClassId,
                                                             ClassKey = x.Category.Class.Guid,
                                                             ClassName = x.Category.Class.ClassName,
                                                         }).OrderBy(_Request.SortExpression)
                                                         .Where(_Request.SearchCondition)
                                                         .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SubCategories, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSubCategories", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetEmployee(OHelpers.Request _Request)
        {
            try
            {
                if(string.IsNullOrEmpty(_Request.EmployeeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMPCODE", "Please enter employee code");
                }

                using(_HCoreContext = new HCoreContext())
                {
                    var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.AccountCode == _Request.EmployeeCode && x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                          .Select(x => new
                                          {
                                              EmployeeId = x.Id,
                                              EmployeeKey = x.Guid,
                                              EmployeeName = x.DisplayName,
                                              EmployeeCode = x.AccountCode,
                                              DivisionId = x.DivisionId,
                                              DivisionKey = x.Division.Guid,
                                              DivisionName = x.Division.DivisionName,
                                              DepartmentId = x.DepartmentId,
                                              DepartmentKey = x.Department.Guid,
                                              DepartmentName = x.Department.DepartmentName,
                                              LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                              LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                              LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                              SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                              SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                              SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                          }).FirstOrDefaultAsync();
                    if(EmployeeDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMPCODEINV", "Please enter correct employee code");
                    }

                    OHelpers.Employee? _Employee = new OHelpers.Employee();
                    _Employee.EmployeeId = EmployeeDetails.EmployeeId;
                    _Employee.EmployeeKey = EmployeeDetails.EmployeeKey;
                    _Employee.Name = EmployeeDetails.EmployeeName;
                    _Employee.DivisionId = EmployeeDetails.DivisionId;
                    _Employee.DivisionKey = EmployeeDetails.DivisionKey;
                    _Employee.DivisionName = EmployeeDetails.DivisionName;
                    _Employee.DepartmentId = EmployeeDetails.DepartmentId;
                    _Employee.DepartmentKey = EmployeeDetails.DepartmentKey;
                    _Employee.DepartmentName = EmployeeDetails.DepartmentName;
                    _Employee.LocationId = EmployeeDetails.LocationId;
                    _Employee.LocationKey = EmployeeDetails.LocationKey;
                    _Employee.LocationName = EmployeeDetails.LocationName;
                    _Employee.SubLocationId = EmployeeDetails.SubLocationId;
                    _Employee.SubLocationKey = EmployeeDetails.SubLocationKey;
                    _Employee.SubLocationName = EmployeeDetails.SubLocationName;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Employee, "AT0200", "Employee details loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetEmployee", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDepartment(OReference _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey) || _Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMPCODE", "Please select department");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepartmentDetails = await _HCoreContext.AtDepartments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                          .Select(x => new
                                          {
                                              DepartmentId = x.Id,
                                              DepartmentKey = x.Guid,
                                              DepartmentName = x.DepartmentName,
                                              LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                              LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                              LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                              SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                              SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                              SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                          }).FirstOrDefaultAsync();
                    if (DepartmentDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMPCODEINV", "Please select valid department");
                    }

                    OHelpers.Department? _Department = new OHelpers.Department();
                    _Department.DepartmentId = DepartmentDetails.DepartmentId;
                    _Department.DepartmentKey = DepartmentDetails.DepartmentKey;
                    _Department.DepartmentName = DepartmentDetails.DepartmentName;
                    _Department.LocationId = DepartmentDetails.LocationId;
                    _Department.LocationKey = DepartmentDetails.LocationKey;
                    _Department.LocationName = DepartmentDetails.LocationName;
                    _Department.SubLocationId = DepartmentDetails.SubLocationId;
                    _Department.SubLocationKey = DepartmentDetails.SubLocationKey;
                    _Department.SubLocationName = DepartmentDetails.SubLocationName;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Department, "AT0200", "Department details loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDepartment", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetUserRoles(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "RoleId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "RoleId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCoreRoles.Where(x => x.StatusId == HelperStatus.Default.Active && (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1))
                                                .Select(x => new OHelpers.Roles
                                                {
                                                    RoleId = x.Id,
                                                    RoleKey = x.Guid,
                                                    RoleName = x.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Roles> _Roles = await _HCoreContext.AtCoreRoles.Where(x => x.StatusId == HelperStatus.Default.Active && (x.OrganizationId == _Request.UserReference.OrganizationId || x.OrganizationId == null || x.OrganizationId < 1))
                                                  .Select(x => new OHelpers.Roles
                                                  {
                                                      RoleId = x.Id,
                                                      RoleKey = x.Guid,
                                                      RoleName = x.Name,
                                                  }).OrderBy(_Request.SortExpression)
                                                  .Where(_Request.SearchCondition)
                                                  .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Roles, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetUserRoles", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetUsers(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "UserId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "UserId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.User && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OHelpers.Users
                                                {
                                                    UserId = x.Id,
                                                    UserKey = x.Guid,
                                                    UserName = x.DisplayName,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OHelpers.Users> _Roles = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.User && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                  .Select(x => new OHelpers.Users
                                                  {
                                                      UserId = x.Id,
                                                      UserKey = x.Guid,
                                                      UserName = x.DisplayName,
                                                  }).OrderBy(_Request.SortExpression)
                                                  .Where(_Request.SearchCondition)
                                                  .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Roles, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "Data loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetUsers", _Exception, _Request.UserReference);
            }
        }
    }
}
