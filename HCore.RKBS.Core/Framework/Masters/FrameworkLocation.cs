﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace HCore.RKBS.Core.Framework.Masters
{
    internal class FrameworkLocation
    {
        HCoreContext? _HCoreContext;
        AtLocation? _AtLocation;

        internal async Task<OResponse> SaveLocation(OLocation.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.LocationName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location name required");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? ParentId = 0;
                if (!string.IsNullOrEmpty(_Request.ParentKey))
                {
                    ParentId = HCoreHelper.GetLocationId(_Request.ParentKey, _Request.UserReference);
                    if (ParentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Parent Location");
                    }
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Division required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (ParentId > 0)
                    {
                        bool _IsExists = await _HCoreContext.AtLocations.AnyAsync(x => x.LocationName == _Request.LocationName && x.ParentId == ParentId);
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Location name already exists");
                        }
                    }
                    else
                    {
                        bool _IsExist = await _HCoreContext.AtLocations.AnyAsync(x => x.LocationName == _Request.LocationName && x.DivisionId == DivisionId);
                        if (_IsExist)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Location already exists");
                        }
                    }
                    _AtLocation = new AtLocation();
                    _AtLocation.Guid = "LOC-" + HCoreHelper.GenerateGuid();
                    _AtLocation.LocationName = _Request.LocationName;
                    if (ParentId > 0)
                    {
                        _AtLocation.ParentId = ParentId;
                        _AtLocation.Guid = "SUBLOC-" + HCoreHelper.GenerateGuid();
                        var ParentDetails = await _HCoreContext.AtLocations.Where(x => x.Id == ParentId)
                                            .Select(x => new
                                            {
                                                DivisionId = x.DivisionId,
                                            }).FirstOrDefaultAsync();
                        if (ParentDetails != null)
                        {
                            if (ParentDetails.DivisionId > 0)
                            {
                                _AtLocation.DivisionId = ParentDetails.DivisionId;
                            }
                        }
                    }
                    else
                    {
                        _AtLocation.DivisionId = DivisionId;
                    }
                    _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                    _AtLocation.StatusId = StatusId;
                    _AtLocation.LocationName = _Request.LocationName;
                    _AtLocation.CreatedById = _Request.UserReference.AccountId;
                    _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();

                    var _Response = new
                    {
                        LocationId = _AtLocation.Id,
                        LocationKey = _AtLocation.Guid,
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Location created successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateLocation(OLocation.Update.Request _Request)
        {
            try
            {
                if (_Request.LocationId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location id required");
                }
                if (string.IsNullOrEmpty(_Request.LocationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? ParentId = 0;
                if (!string.IsNullOrEmpty(_Request.ParentKey))
                {
                    ParentId = HCoreHelper.GetLocationId(_Request.ParentKey, _Request.UserReference);
                    if (ParentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Parent Location");
                    }
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Division");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (ParentId > 0)
                    {
                        bool _IsExists = await _HCoreContext.AtLocations.AnyAsync(x => x.LocationName == _Request.LocationName && x.Guid != _Request.LocationKey && (x.ParentId == ParentId));
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Location name already exists");
                        }
                    }
                    else
                    {
                        bool _IsExist = await _HCoreContext.AtLocations.AnyAsync(x => x.LocationName == _Request.LocationName && x.Guid != _Request.LocationKey && x.DivisionId == DivisionId);
                        if (_IsExist)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Location name already exists");
                        }
                    }
                    var LocationDetails = await _HCoreContext.AtLocations.Where(x => x.Guid == _Request.LocationKey && x.Id == _Request.LocationId).FirstOrDefaultAsync();
                    if (LocationDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.LocationName))
                        {
                            LocationDetails.LocationName = _Request.LocationName;
                        }
                        var ParentDetails = await _HCoreContext.AtLocations.Where(x => x.Id == ParentId)
                                            .Select(x => new
                                            {
                                                DivisionId = x.DivisionId,
                                            }).FirstOrDefaultAsync();
                        if (ParentDetails != null)
                        {
                            if (ParentDetails.DivisionId > 0)
                            {
                                LocationDetails.DivisionId = ParentDetails.DivisionId;
                            }
                        }
                        else
                        {
                            LocationDetails.DivisionId = DivisionId;
                        }
                        if (ParentId > 0 && ParentId != LocationDetails.ParentId)
                        {
                            LocationDetails.ParentId = ParentId;
                        }
                        if (StatusId > 0 && StatusId != LocationDetails.StatusId)
                        {
                            LocationDetails.StatusId = StatusId;
                        }
                        LocationDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        LocationDetails.ModifiedById = _Request.UserReference.AccountId;
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Location details updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Location details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteLocation(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var LocationDetails = await _HCoreContext.AtLocations.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (LocationDetails != null)
                    {
                        bool _IsAsset = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.LocationId == LocationDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Location as it has some assets assigned");
                        }
                        _HCoreContext.AtLocations.Remove(LocationDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Location removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Location details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetLocation(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var LocationDetails = await _HCoreContext.AtLocations.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OLocation.Details.Response
                                       {
                                           LocationId = x.Id,
                                           LocationKey = x.Guid,
                                           LocationName = x.LocationName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           ParentId = x.ParentId,
                                           ParentKey = x.Parent.Guid,
                                           ParentName = x.Parent.LocationName,

                                           SubParentId = x.SubParentId,
                                           SubParentKey = x.SubParent.Guid,
                                           SubParentName = x.SubParent.LocationName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (LocationDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, LocationDetails, "ATCLDELETE", "Location details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Location details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetLocations(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "LocationId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtLocations.Where(x => x.ParentId < 1 || x.ParentId == null && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OLocation.List.Response
                                           {
                                               LocationId = x.Id,
                                               LocationKey = x.Guid,
                                               LocationName = x.LocationName,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OLocation.List.Response> _Categoryes = await _HCoreContext.AtLocations.Where(x => x.ParentId < 1 || x.ParentId == null && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OLocation.List.Response
                                       {
                                           LocationId = x.Id,
                                           LocationKey = x.Guid,
                                           LocationName = x.LocationName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Location list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLocations", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSubLocations(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "LocationId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtLocations.Where(x => (x.ParentId > 0 && x.ParentId != null) && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OLocation.List.Response
                                           {
                                               LocationId = x.Id,
                                               LocationKey = x.Guid,
                                               LocationName = x.LocationName,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               ParentId = x.ParentId,
                                               ParentKey = x.Parent.Guid,
                                               ParentName = x.Parent.LocationName,

                                               SubParentId = x.SubParentId,
                                               SubParentKey = x.SubParent.Guid,
                                               SubParentName = x.SubParent.LocationName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OLocation.List.Response> _Categoryes = await _HCoreContext.AtLocations.Where(x => (x.ParentId > 0 && x.ParentId != null) && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OLocation.List.Response
                                       {
                                           LocationId = x.Id,
                                           LocationKey = x.Guid,
                                           LocationName = x.LocationName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           ParentId = x.ParentId,
                                           ParentKey = x.Parent.Guid,
                                           ParentName = x.Parent.LocationName,

                                           SubParentId = x.SubParentId,
                                           SubParentKey = x.SubParent.Guid,
                                           SubParentName = x.SubParent.LocationName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Location list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLocations", _Exception, _Request.UserReference);
            }
        }
    }
}
