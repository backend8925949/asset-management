﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace HCore.RKBS.Core.Framework.Masters
{
    internal class FrameworkSubCategory
    {
        HCoreContext? _HCoreContext;
        AtSubCategory? _AtSubCategory;

        internal async Task<OResponse> SaveSubCategory(OSubCategory.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category required");
                }
                if (string.IsNullOrEmpty(_Request.SubCategoryName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Sub category name required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var CategoryDetails = await _HCoreContext.AtCategories.Where(x => x.Guid == _Request.CategoryKey).FirstOrDefaultAsync();
                    if (CategoryDetails != null)
                    {
                        var SubCategoryDetails = await _HCoreContext.AtSubCategories.Where(x => x.SubCategoryName == _Request.SubCategoryName && x.CategoryId == CategoryDetails.Id).FirstOrDefaultAsync();
                        if (SubCategoryDetails != null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Sub category details already exists");
                        }
                        else
                        {
                            _AtSubCategory = new AtSubCategory();
                            _AtSubCategory.Guid = HCoreHelper.GenerateGuid();
                            _AtSubCategory.CategoryId = CategoryDetails.Id;
                            _AtSubCategory.SubCategoryName = _Request.SubCategoryName;
                            _AtSubCategory.StatusId = StatusId;
                            _AtSubCategory.CreatedById = _Request.UserReference.AccountId;
                            _AtSubCategory.CreatedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.AtSubCategories.AddAsync(_AtSubCategory);
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            var _Response = new
                            {
                                CategoryId = _AtSubCategory.Id,
                                CategoryKey = _AtSubCategory.Guid,
                            };

                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Sub category created successfully");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveSubCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateSubCategory(OSubCategory.Update.Request _Request)
        {
            try
            {
                if (_Request.SubCategoryId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Sub category id required");
                }
                if (string.IsNullOrEmpty(_Request.SubCategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Sub category key required");
                }
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var CategoryDetails = await _HCoreContext.AtCategories.Where(x => x.Guid == _Request.CategoryKey).FirstOrDefaultAsync();
                    if (CategoryDetails != null)
                    {
                        var SubCategoryDetails = await _HCoreContext.AtSubCategories.Where(x => x.Guid == _Request.SubCategoryKey && x.Id == _Request.SubCategoryId).FirstOrDefaultAsync();
                        if (SubCategoryDetails != null)
                        {
                            bool _Exists = await _HCoreContext.AtSubCategories.AnyAsync(x => x.SubCategoryName == _Request.SubCategoryName && x.Guid != SubCategoryDetails.Guid && x.CategoryId == CategoryDetails.Id);
                            if (!_Exists)
                            {
                                if (!string.IsNullOrEmpty(_Request.SubCategoryName))
                                {
                                    SubCategoryDetails.SubCategoryName = _Request.SubCategoryName;
                                }
                                if (CategoryDetails.Id != SubCategoryDetails.CategoryId)
                                {
                                    SubCategoryDetails.CategoryId = CategoryDetails.Id;
                                }
                                if (StatusId > 0 && StatusId != SubCategoryDetails.StatusId)
                                {
                                    SubCategoryDetails.StatusId = StatusId;
                                }
                                SubCategoryDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                SubCategoryDetails.ModifiedById = _Request.UserReference.AccountId;
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Sub category details updated successfully");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Sub category name already exists");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Sub category details not found");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateSubCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteSubCategory(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Sub category id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Sub category key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var SubCategoryDetails = await _HCoreContext.AtSubCategories.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (SubCategoryDetails != null)
                    {
                        bool _IsAsset = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.AssetSubCategoryId == SubCategoryDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this sub category as it is assigned to some assets");
                        }
                        _HCoreContext.AtSubCategories.Remove(SubCategoryDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Sub category removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Sub category details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteSubCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSubCategory(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "SubCategory id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "SubCategory key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var SubCategoryDetails = await _HCoreContext.AtSubCategories.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OSubCategory.Details.Response
                                       {
                                           SubCategoryId = x.Id,
                                           SubCategoryKey = x.Guid,
                                           SubCategoryName = x.SubCategoryName,

                                           CategoryId = x.CategoryId,
                                           CategoryKey = x.Category.Guid,
                                           CategoryName = x.Category.CategoryName,

                                           ClassId = x.Category.ClassId,
                                           ClassKey = x.Category.Class.Guid,
                                           ClassName = x.Category.Class.ClassName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (SubCategoryDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, SubCategoryDetails, "ATCLDELETE", "Sub category details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Sub category details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSubCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSubCategories(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "SubCategoryId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtSubCategories.Where(x => x.Category.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OSubCategory.List.Response
                                           {
                                               SubCategoryId = x.Id,
                                               SubCategoryKey = x.Guid,
                                               SubCategoryName = x.SubCategoryName,

                                               CategoryId = x.CategoryId,
                                               CategoryKey = x.Category.Guid,
                                               CategoryName = x.Category.CategoryName,

                                               ClassId = x.Category.ClassId,
                                               ClassKey = x.Category.Class.Guid,
                                               ClassName = x.Category.Class.ClassName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OSubCategory.List.Response> _Categoryes = await _HCoreContext.AtSubCategories.Where(x => x.Category.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OSubCategory.List.Response
                                       {
                                           SubCategoryId = x.Id,
                                           SubCategoryKey = x.Guid,
                                           SubCategoryName = x.SubCategoryName,

                                           CategoryId = x.CategoryId,
                                           CategoryKey = x.Category.Guid,
                                           CategoryName = x.Category.CategoryName,

                                           ClassId = x.Category.ClassId,
                                           ClassKey = x.Category.Class.Guid,
                                           ClassName = x.Category.Class.ClassName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Sub category list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSubCategories", _Exception, _Request.UserReference);
            }
        }
    }
}
