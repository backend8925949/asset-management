﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkDivision
    {
        HCoreContext? _HCoreContext;
        AtDivision? _AtDivision;

        internal async Task<OResponse> SaveDivision(ODivision.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.DivisionName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division name required");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool _IsExists = await _HCoreContext.AtDivisions.AnyAsync(x => x.DivisionName == _Request.DivisionName && x.OrganizationId == _Request.UserReference.OrganizationId);
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Division name already exists");
                    }

                    _AtDivision = new AtDivision();
                    _AtDivision.Guid = "DEV-" + HCoreHelper.GenerateGuid();
                    _AtDivision.OrganizationId = _Request.UserReference.OrganizationId;
                    _AtDivision.DivisionName = _Request.DivisionName;
                    if (StatusId > 0)
                    {
                        _AtDivision.StatusId = StatusId;
                    }
                    _AtDivision.CreatedById = _Request.UserReference.AccountId;
                    _AtDivision.CreatedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.AtDivisions.AddAsync(_AtDivision);
                    await _HCoreContext.SaveChangesAsync();

                    var _Response = new
                    {
                        DivisionId = _AtDivision.Id,
                        DivisionKey = _AtDivision.Guid,
                    };

                    await _HCoreContext.DisposeAsync();

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Division created successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDivision", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateDivision(ODivision.Update.Request _Request)
        {
            try
            {
                if (_Request.DivisionId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division id required");
                }
                if (string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? OrganizationId = 0;
                if (!string.IsNullOrEmpty(_Request.OrganizationKey))
                {
                    OrganizationId = HCoreHelper.GetOrganizationId(_Request.OrganizationKey, _Request.UserReference);
                    if (OrganizationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Organization");
                    }
                }
                else
                {
                    OrganizationId = _Request.UserReference.OrganizationId;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DivisionDetails = await _HCoreContext.AtDivisions.Where(x => x.Guid == _Request.DivisionKey && x.Id == _Request.DivisionId).FirstOrDefaultAsync();
                    if (DivisionDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.DivisionName))
                        {
                            DivisionDetails.DivisionName = _Request.DivisionName;
                        }
                        if (OrganizationId > 0 && OrganizationId != DivisionDetails.OrganizationId)
                        {
                            DivisionDetails.OrganizationId = (long)OrganizationId;
                        }
                        if (StatusId > 0 && StatusId != DivisionDetails.StatusId)
                        {
                            DivisionDetails.StatusId = StatusId;
                        }
                        DivisionDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        DivisionDetails.ModifiedById = _Request.UserReference.AccountId;
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Division details updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Division details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateDivision", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteDivision(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DivisionDetails = await _HCoreContext.AtDivisions.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (DivisionDetails != null)
                    {
                        bool _IsAccount = await _HCoreContext.AtAccounts.AnyAsync(x => x.DivisionId == DivisionDetails.Id);
                        if (_IsAccount)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Division as it has some accounts assigned");
                        }
                        bool _IsAddress = await _HCoreContext.AtAddresses.AnyAsync(x => x.DivisionId == DivisionDetails.Id);
                        if (_IsAddress)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Division as it has some addresses present on the system");
                        }
                        bool _IsAsset = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.DivisionId == DivisionDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Division as it has some assets assigned");
                        }
                        bool _IsDepartment = await _HCoreContext.AtDepartments.AnyAsync(x => x.DivisionId == DivisionDetails.Id);
                        if (_IsDepartment)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Division as it has some departments assigned");
                        }
                        _HCoreContext.AtDivisions.Remove(DivisionDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Division removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Division details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteDivision", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDivision(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Division key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DivisionDetails = await _HCoreContext.AtDivisions.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new ODivision.Details.Response
                                       {
                                           DivisionId = x.Id,
                                           DivisionKey = x.Guid,
                                           DivisionName = x.DivisionName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (DivisionDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, DivisionDetails, "ATCLDELETE", "Division details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Division details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDivision", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDivisions(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "DivisionId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtDivisions.Where(x => x.ParentId < 1 || x.ParentId == null && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new ODivision.List.Response
                                           {
                                               DivisionId = x.Id,
                                               DivisionKey = x.Guid,
                                               DivisionName = x.DivisionName,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<ODivision.List.Response> _Categoryes = await _HCoreContext.AtDivisions.Where(x => x.ParentId < 1 || x.ParentId == null && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new ODivision.List.Response
                                       {
                                           DivisionId = x.Id,
                                           DivisionKey = x.Guid,
                                           DivisionName = x.DivisionName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Division list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDivisions", _Exception, _Request.UserReference);
            }
        }
    }
}
