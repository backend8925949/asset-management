﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace HCore.RKBS.Core.Framework.Masters
{
    internal class FrameworkCategory
    {
        HCoreContext? _HCoreContext;
        AtCategory? _AtCategory;

        internal async Task<OResponse> SaveCategory(OCategory.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ClassKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class required");
                }
                if (string.IsNullOrEmpty(_Request.CategoryName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category name required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.Guid == _Request.ClassKey).FirstOrDefaultAsync();
                    if (ClassDetails != null)
                    {
                        var CategoryDetails = await _HCoreContext.AtCategories.Where(x => x.CategoryName == _Request.CategoryName && x.ClassId == ClassDetails.Id).FirstOrDefaultAsync();
                        if (CategoryDetails != null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category details already exists");
                        }
                        else
                        {
                            _AtCategory = new AtCategory();
                            _AtCategory.Guid = HCoreHelper.GenerateGuid();
                            _AtCategory.ClassId = ClassDetails.Id;
                            _AtCategory.OrganizationId = _Request.UserReference.OrganizationId;
                            _AtCategory.CategoryName = _Request.CategoryName;
                            _AtCategory.StatusId = StatusId;
                            _AtCategory.CreatedById = _Request.UserReference.AccountId;
                            _AtCategory.CreatedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.AtCategories.AddAsync(_AtCategory);
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            var _Response = new
                            {
                                ClassId = _AtCategory.Id,
                                ClassKey = _AtCategory.Guid,
                            };

                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Category created successfully");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateCategory(OCategory.Update.Request _Request)
        {
            try
            {
                if (_Request.CategoryId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category id required");
                }
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category key required");
                }
                if (string.IsNullOrEmpty(_Request.ClassKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.Guid == _Request.ClassKey).FirstOrDefaultAsync();
                    if (ClassDetails != null)
                    {
                        var CategoryDetails = await _HCoreContext.AtCategories.Where(x => x.Guid == _Request.CategoryKey && x.Id == _Request.CategoryId).FirstOrDefaultAsync();
                        if (CategoryDetails != null)
                        {
                            bool _Exists = await _HCoreContext.AtCategories.AnyAsync(x => x.CategoryName == _Request.CategoryName && x.Guid != CategoryDetails.Guid && x.ClassId == ClassDetails.Id);
                            if (!_Exists)
                            {
                                if (!string.IsNullOrEmpty(_Request.CategoryName))
                                {
                                    CategoryDetails.CategoryName = _Request.CategoryName;
                                }
                                if (ClassDetails.Id != CategoryDetails.ClassId)
                                {
                                    CategoryDetails.ClassId = ClassDetails.Id;
                                }
                                if (StatusId > 0 && StatusId != CategoryDetails.StatusId)
                                {
                                    CategoryDetails.StatusId = StatusId;
                                }
                                CategoryDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                CategoryDetails.ModifiedById = _Request.UserReference.AccountId;
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Category details updated successfully");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category name already exists");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category details not found");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteCategory(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var CategoryDetails = await _HCoreContext.AtCategories.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (CategoryDetails != null)
                    {
                        bool _IsSubCategory = await _HCoreContext.AtSubCategories.AnyAsync(x => x.CategoryId == CategoryDetails.Id);
                        if (_IsSubCategory)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Category as it already has some sub categories");
                        }
                        bool _IsAsset = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.AssetCategoryId == CategoryDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Category as it is assigned to some assets");
                        }
                        _HCoreContext.AtCategories.Remove(CategoryDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Category removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetCategory(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Category key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var CategoryDetails = await _HCoreContext.AtCategories.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OCategory.Details.Response
                                       {
                                           CategoryId = x.Id,
                                           CategoryKey = x.Guid,
                                           CategoryName = x.CategoryName,

                                           ClassId = x.ClassId,
                                           ClassKey = x.Class.Guid,
                                           ClassName = x.Class.ClassName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (CategoryDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, CategoryDetails, "ATCLDELETE", "Category details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Category details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetCategories(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "CategoryId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtCategories.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OCategory.List.Response
                                           {
                                               CategoryId = x.Id,
                                               CategoryKey = x.Guid,
                                               CategoryName = x.CategoryName,

                                               ClassId = x.ClassId,
                                               ClassKey = x.Class.Guid,
                                               ClassName = x.Class.ClassName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OCategory.List.Response> _Classes = await _HCoreContext.AtCategories.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OCategory.List.Response
                                       {
                                           CategoryId = x.Id,
                                           CategoryKey = x.Guid,
                                           CategoryName = x.CategoryName,

                                           ClassId = x.ClassId,
                                           ClassKey = x.Class.Guid,
                                           ClassName = x.Class.ClassName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Classes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Category list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference);
            }
        }
    }
}
