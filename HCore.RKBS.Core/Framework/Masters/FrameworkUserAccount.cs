﻿

using DocumentFormat.OpenXml.Spreadsheet;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System.Text;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkUserAccount
    {
        HCoreContext? _HCoreContext;
        AtAccount? _AtAccount;
        AtAddress? _AtAddress;
        AtAccountAuth? _AtAccountAuth;
        Random? _Random;
        List<OUserAccount.UserListDownload>? _UserDownload;

        internal async Task<OResponse> SaveUser(OUserAccount.UserAccount.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Please enter first name");
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Please enter last name");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Please enter email address");
                }
                if (string.IsNullOrEmpty(_Request.UserRole))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Please select user role");
                }
                if (string.IsNullOrEmpty(_Request.UserName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Please enter username");
                }
                if (string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Please enter password");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Invalid division selected");
                    }
                }

                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Invalid department selected");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    long? RoleId = await _HCoreContext.AtCoreRoles.Where(x => x.Guid == _Request.UserRole).Select(x => x.Id).FirstOrDefaultAsync();
                    bool _IsExists = await _HCoreContext.AtAccounts.AnyAsync(x => x.RoleId == RoleId && x.OrganizationId == _Request.UserReference.OrganizationId && (x.MobileNumber == _Request.MobileNumber || x.EmailAddress == _Request.EmailAddress));
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "User already exists in the system with mobile number or email address");
                    }

                    _IsExists = await _HCoreContext.AtAccountAuths.AnyAsync(x => x.UserName == _Request.UserName && x.User.OrganizationId == _Request.UserReference.OrganizationId && x.User.RoleId == RoleId);
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "User name already taken");
                    }

                    long? SubDivisionId = await _HCoreContext.AtDivisions.Where(x => x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefaultAsync();

                    _Random = new Random();
                    string AccountCode = HCoreHelper.GenerateAccountCode(6);
                    string? AccessPin = HCoreHelper.GenerateAccountCode(4);

                    _AtAccount = new AtAccount();
                    _AtAccount.Guid = HCoreHelper.GenerateGuid();
                    _AtAccount.AccountTypeId = UserAccountType.User;
                    _AtAccount.RoleId = RoleId;
                    _AtAccount.SourceId = RegistrationSource.WebApplication;
                    _AtAccount.OrganizationId = _Request.UserReference.OrganizationId;
                    if (DivisionId > 0)
                    {
                        _AtAccount.DivisionId = DivisionId;
                    }
                    if (DepartmentId > 0)
                    {
                        _AtAccount.DepartmentId = DepartmentId;
                    }
                    _AtAccount.MobileNumber = _Request.MobileNumber;
                    _AtAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                    _AtAccount.DisplayName = _Request.FirstName + " " + _Request.LastName;
                    _AtAccount.FirstName = _Request.FirstName;
                    _AtAccount.LastName = _Request.LastName;
                    _AtAccount.EmailAddress = _Request.EmailAddress;
                    _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                    _AtAccount.Designation = _Request.Designation;
                    _AtAccount.AccountCode = AccountCode;
                    _AtAccount.CreatedById = _Request.UserReference.AccountId;
                    _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAccount.StatusId = StatusId;
                    await _HCoreContext.AtAccounts.AddAsync(_AtAccount);
                    await _HCoreContext.SaveChangesAsync();

                    _AtAccountAuth = new AtAccountAuth();
                    _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                    _AtAccountAuth.UserId = _AtAccount.Id;
                    _AtAccountAuth.UserName = _Request.UserName;
                    _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                    _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                    await _HCoreContext.SaveChangesAsync();

                    if (_Request.Address != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Address.Address))
                        {
                            long? StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.Address.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                            long? CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.Address.CityKey).Select(x => x.Id).FirstOrDefaultAsync();

                            _AtAddress = new AtAddress();
                            _AtAddress.Guid = HCoreHelper.GenerateGuid();
                            _AtAddress.AccountId = _AtAccount.Id;
                            _AtAddress.Address = _Request.Address.Address;
                            _AtAddress.CountryId = 1;
                            _AtAddress.StateId = (long)StateId;
                            _AtAddress.CityId = (long)CityId;
                            _AtAddress.StatusId = StatusId;
                            _AtAddress.CreatedById = _Request.UserReference.AccountId;
                            _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                            await _HCoreContext.SaveChangesAsync();
                        }
                    }

                    var _Response = new
                    {
                        UserId = _AtAccount.Id,
                        UserKey = _AtAccount.Guid,
                    };

                    string? Body = "<div>";
                    Body += "<hr>";
                    Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                    Body += "<label> Please use the below credentials to log-in on asset management web application using given link(URL).</label>";
                    Body += "</div>";
                    Body += "<hr>";
                    Body += "<br>";
                    Body += "<div>Username: - {{UserName}}</div>";
                    Body += "<br>";
                    Body += "<div>Password: - {{Password}}</div>";
                    Body += "<br>";
                    Body += "<div>URL: - <a href=\"{{URL}}\">{{URL}}</a></div>";
                    Body += "<br>";
                    Body += "<hr>";
                    Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                    Body += "<label> Please use the below credentials to log-in on asset management mobile application.</label>";
                    Body += "</div>";
                    Body += "<hr>";
                    Body += "<br>";
                    Body += "<div>Username: - {{MobileNumber}}</div>";
                    Body += "<br>";
                    Body += "<div>Password: - {{AuthPin}}</div>";
                    Body += "</div>";
                    StringBuilder? EmailBody = new StringBuilder(Body);
                    EmailBody.Replace("{{UserName}}", _AtAccountAuth.UserName);
                    EmailBody.Replace("{{Password}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));
                    EmailBody.Replace("{{MobileNumber}}", _AtAccount.MobileNumber);
                    EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(_AtAccount.AccessPin));
                    EmailBody.Replace("{{URL}}", "http://asset-management.rajkamalbarscan.in/");
                    await HCoreHelper.BroadCastEmail(_AtAccount.EmailAddress, "Asset Management Log-In Credentials", EmailBody.ToString(), _Request.UserReference);

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "User registered successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveUser", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateUser(OUserAccount.UserAccount.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.UserKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "User key required");
                }
                if (_Request.UserId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "User id required");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Invalid division selected");
                    }
                }

                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Invalid department selected");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.UserId && x.OrganizationId == _Request.UserReference.OrganizationId && x.Guid == _Request.UserKey).FirstOrDefaultAsync();
                    if (UserAccountDetails != null)
                    {
                        long? RoleId = await _HCoreContext.AtCoreRoles.Where(x => x.Guid == _Request.UserRole).Select(x => x.Id).FirstOrDefaultAsync();

                        bool _IsExists = await _HCoreContext.AtAccounts.AnyAsync(x => x.Guid != UserAccountDetails.Guid && x.RoleId == RoleId && x.OrganizationId == _Request.UserReference.OrganizationId && (x.MobileNumber == _Request.MobileNumber || x.EmailAddress == _Request.EmailAddress));
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "User already exists in the system with mobile number or email address");
                        }

                        _IsExists = await _HCoreContext.AtAccountAuths.AnyAsync(x => x.UserName == _Request.UserName && x.User.RoleId == RoleId && x.UserId != UserAccountDetails.Id && x.User.OrganizationId == _Request.UserReference.OrganizationId);
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "User name already exists");
                        }

                        long? SubDivisionId = await _HCoreContext.AtDivisions.Where(x => x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefaultAsync();

                        if (RoleId > 0 && UserAccountDetails.RoleId != RoleId)
                        {
                            UserAccountDetails.RoleId = RoleId;
                        }
                        if (DivisionId > 0 && UserAccountDetails.DivisionId != DivisionId)
                        {
                            UserAccountDetails.DivisionId = DivisionId;
                        }
                        if (DepartmentId > 0 && UserAccountDetails.DepartmentId != DepartmentId)
                        {
                            UserAccountDetails.DepartmentId = DepartmentId;
                        }
                        if (!string.IsNullOrEmpty(_Request.FirstName) && UserAccountDetails.FirstName != _Request.FirstName)
                        {
                            UserAccountDetails.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.LastName) && UserAccountDetails.LastName != _Request.LastName)
                        {
                            UserAccountDetails.LastName = _Request.LastName;
                        }
                        if ((!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName)) && UserAccountDetails.DisplayName != _Request.FirstName + " " + _Request.LastName)
                        {
                            UserAccountDetails.DisplayName = _Request.FirstName + " " + _Request.LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && UserAccountDetails.MobileNumber != _Request.MobileNumber)
                        {
                            UserAccountDetails.MobileNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && UserAccountDetails.EmailAddress != _Request.EmailAddress)
                        {
                            UserAccountDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && UserAccountDetails.ContactNumber != _Request.MobileNumber)
                        {
                            UserAccountDetails.ContactNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Designation) && UserAccountDetails.Designation != _Request.Designation)
                        {
                            UserAccountDetails.Designation = _Request.Designation;
                        }
                        if (StatusId > 0 && UserAccountDetails.StatusId != StatusId)
                        {
                            UserAccountDetails.StatusId = StatusId;
                        }
                        UserAccountDetails.ModifiedById = _Request.UserReference.AccountId;
                        UserAccountDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.SaveChangesAsync();

                        string? Body = "<div>";
                        Body += "<hr>";
                        Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                        Body += "<label> Please use the below updated credentials to log-in on asset management web application using given link(URL).</label>";
                        Body += "</div>";
                        Body += "<hr>";
                        Body += "<br>";
                        Body += "<div>Username: - {{UserName}}</div>";
                        Body += "<br>";
                        Body += "<div>Password: - {{Password}}</div>";
                        Body += "<br>";
                        Body += "<div>URL: - <a href=\"{{URL}}\">{{URL}}</a></div>";
                        Body += "<br>";
                        Body += "<hr>";
                        Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                        Body += "<label> Please use the below credentials to log-in on asset management mobile application.</label>";
                        Body += "</div>";
                        Body += "<hr>";
                        Body += "<br>";
                        Body += "<div>Username: - {{MobileNumber}}</div>";
                        Body += "<br>";
                        Body += "<div>Password: - {{AuthPin}}</div>";
                        Body += "</div>";
                        StringBuilder? EmailBody = new StringBuilder(Body);

                        var User = await _HCoreContext.AtAccountAuths.Where(x => x.UserId == UserAccountDetails.Id).FirstOrDefaultAsync();
                        if (User != null)
                        {
                            string? Password = HCoreEncrypt.EncryptHash(_Request.Password);
                            if (!string.IsNullOrEmpty(_Request.UserName) && User.UserName != _Request.UserName)
                            {
                                User.UserName = _Request.UserName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Password))
                            {
                                if (User.Password != Password)
                                {
                                    User.Password = Password;
                                }
                            }

                            User.ModifiedById = _Request.UserReference.AccountId;
                            User.ModifiedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.SaveChangesAsync();

                            EmailBody.Replace("{{UserName}}", _Request.UserName);
                            EmailBody.Replace("{{Password}}", HCoreEncrypt.DecryptHash(Password));
                        }

                        if (_Request.Address != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.Address.Address))
                            {
                                long? StateId = 0;
                                long? CityId = 0;

                                var UserAddressDetails = await _HCoreContext.AtAddresses.Where(x => x.AccountId == UserAccountDetails.Id).FirstOrDefaultAsync();
                                if (UserAddressDetails != null)
                                {
                                    if (!string.IsNullOrEmpty(_Request.Address.StateKey))
                                    {
                                        StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.Address.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    }
                                    if (!string.IsNullOrEmpty(_Request.Address.CityKey))
                                    {
                                        CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.Address.CityKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    }

                                    if (!string.IsNullOrEmpty(_Request.Address.Address) && UserAddressDetails.Address != _Request.Address.Address)
                                    {
                                        UserAddressDetails.Address = _Request.Address.Address;
                                    }
                                    if (StateId > 0 && UserAddressDetails.StateId != (long)StateId)
                                    {
                                        UserAddressDetails.StateId = (long)StateId;
                                    }
                                    if (CityId > 0 && UserAddressDetails.CityId != (long)CityId)
                                    {
                                        UserAddressDetails.CityId = (long)CityId;
                                    }
                                    if (StatusId > 0 && UserAddressDetails.StatusId != StatusId)
                                    {
                                        UserAddressDetails.StatusId = StatusId;
                                    }
                                    UserAddressDetails.ModifiedById = _Request.UserReference.AccountId;
                                    UserAddressDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                else
                                {
                                    StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.Address.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.Address.CityKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    _AtAddress = new AtAddress();
                                    _AtAddress.Guid = HCoreHelper.GenerateGuid();
                                    _AtAddress.AccountId = UserAccountDetails.Id;
                                    _AtAddress.Address = _Request.Address.Address;
                                    _AtAddress.CountryId = 1;
                                    _AtAddress.StateId = (long)StateId;
                                    _AtAddress.CityId = (long)CityId;
                                    _AtAddress.StatusId = HelperStatus.Default.Active;
                                    _AtAddress.CreatedById = _Request.UserReference.AccountId;
                                    _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                            }
                        }

                        await _HCoreContext.DisposeAsync();

                        var _Response = new
                        {
                            UserId = UserAccountDetails.Id,
                            UserKey = UserAccountDetails.Guid,
                        };

                        EmailBody.Replace("{{MobileNumber}}", UserAccountDetails.MobileNumber);
                        EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(UserAccountDetails.AccessPin));
                        EmailBody.Replace("{{URL}}", "http://asset-management.rajkamalbarscan.in/");

                        await HCoreHelper.BroadCastEmail(UserAccountDetails.EmailAddress, "Asset Management Log-In Credentials Update", EmailBody.ToString(), _Request.UserReference);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "User registered successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0400", "User details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateUser", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteUser(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "User id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "User key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        var Addresses = await _HCoreContext.AtAddresses.Where(x => x.AccountId == AccountDetails.Id).ToListAsync();
                        if (Addresses.Count > 0)
                        {
                            _HCoreContext.AtAddresses.RemoveRange(Addresses);
                            await _HCoreContext.SaveChangesAsync();
                        }
                        var Users = await _HCoreContext.AtAccountAuths.Where(x => x.UserId == AccountDetails.Id).ToListAsync();
                        if (Users.Count > 0)
                        {
                            _HCoreContext.AtAccountAuths.RemoveRange(Users);
                            await _HCoreContext.SaveChangesAsync();
                        }
                        _HCoreContext.AtAccounts.Remove(AccountDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "User removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "User details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteUser", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetUser(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "User id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "User key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var UserDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                      .Select(x => new OUserAccount.Details
                                      {
                                          UserId = x.Id,
                                          UserKey = x.Guid,
                                          Name = x.DisplayName,
                                          FirstName = x.FirstName,
                                          LastName = x.LastName,
                                          MobileNumber = x.MobileNumber,
                                          EmailAddress = x.EmailAddress,
                                          Designation = x.Designation,

                                          UserName = x.AtAccountAuthUsers.Where(a => a.UserId == x.Id).Select(z => z.UserName).FirstOrDefault(),
                                          Password = x.AtAccountAuthUsers.Where(a => a.UserId == x.Id).Select(z => z.Password).FirstOrDefault(),

                                          RoleId = x.RoleId,
                                          RoleKey = x.Role.SystemName,
                                          RoleName = x.Role.Name,

                                          DepartmentId = x.DepartmentId,
                                          DepartmentKey = x.Department.Guid,
                                          DepartmentName = x.Department.DepartmentName,

                                          OrganizationId = x.OrganizationId,
                                          OrganizationKey = x.Organization.Guid,
                                          OrganizationName = x.Organization.DisplayName,

                                          DivisionId = x.DivisionId,
                                          DivisionKey = x.Division.Guid,
                                          DivisionName = x.Division.DivisionName,

                                          Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                          CityId = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.CityId).FirstOrDefault(),
                                          CityKey = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.City.Guid).FirstOrDefault(),
                                          CityName = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.City.CityName).FirstOrDefault(),

                                          StateId = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.StateId).FirstOrDefault(),
                                          StateKey = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.State.Guid).FirstOrDefault(),
                                          StateName = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.State.StateName).FirstOrDefault(),

                                          CreateDate = x.CreatedDate,
                                          CreatedById = x.CreatedById,
                                          CreatedByKey = x.CreatedBy.Guid,
                                          CreatedByDisplayName = x.CreatedBy.DisplayName,

                                          ModifyDate = x.ModifiedDate,
                                          ModifyById = x.ModifiedById,
                                          ModifyByKey = x.ModifiedBy.Guid,
                                          ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                          StatusId = x.StatusId,
                                          StatusCode = x.Status.SystemName,
                                          StatusName = x.Status.Name,
                                      }).FirstOrDefaultAsync();
                    if (UserDetails != null)
                    {
                        if (!string.IsNullOrEmpty(UserDetails.Password))
                        {
                            UserDetails.Password = HCoreEncrypt.DecryptHash(UserDetails.Password);
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, UserDetails, "ATCLDELETE", "User details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "User details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetUser", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetUsers(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    var _Response = await GetUsersDownload(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "UserId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "UserId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.User
                                                && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OUserAccount.List
                                                {
                                                    UserId = x.Id,
                                                    UserKey = x.Guid,
                                                    Name = x.DisplayName,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.SystemName,
                                                    RoleName = x.Role.Name,

                                                    Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifiedDate,
                                                    ModifyById = x.ModifiedById,
                                                    ModifyByKey = x.ModifiedBy.Guid,
                                                    ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OUserAccount.List> _Users = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.User
                                                     && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                     .Select(x => new OUserAccount.List
                                                     {
                                                         UserId = x.Id,
                                                         UserKey = x.Guid,
                                                         Name = x.DisplayName,
                                                         FirstName = x.FirstName,
                                                         LastName = x.LastName,
                                                         MobileNumber = x.MobileNumber,
                                                         EmailAddress = x.EmailAddress,

                                                         RoleId = x.RoleId,
                                                         RoleKey = x.Role.SystemName,
                                                         RoleName = x.Role.Name,

                                                         Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                                         CreateDate = x.CreatedDate,
                                                         CreatedById = x.CreatedById,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifiedDate,
                                                         ModifyById = x.ModifiedById,
                                                         ModifyByKey = x.ModifiedBy.Guid,
                                                         ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                     }).OrderBy(_Request.SortExpression)
                                                     .Where(_Request.SearchCondition)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Users, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "User list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetUsers", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetUsersDownload(OList.Request _Request)
        {
            try
            {
                _UserDownload = new List<OUserAccount.UserListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "UserId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.User
                                                && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new
                                                {
                                                    UserId = x.Id,
                                                    UserKey = x.Guid,
                                                    Name = x.DisplayName,
                                                    FirstName = x.FirstName,
                                                    LastName = x.LastName,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,

                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.SystemName,
                                                    RoleName = x.Role.Name,

                                                    Designation = x.Designation,

                                                    DepartmentId = x.DepartmentId,
                                                    DepartmentKey = x.Department.Guid,
                                                    DepartmentName = x.Department.DepartmentName,

                                                    DivisionId = x.DivisionId,
                                                    DivisionKey = x.Division.Guid,
                                                    DivisionName = x.Division.DivisionName,

                                                    Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifiedDate,
                                                    ModifyById = x.ModifiedById,
                                                    ModifyByKey = x.ModifiedBy.Guid,
                                                    ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _Users = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.User
                                                     && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                     .Select(x => new
                                                     {
                                                         UserId = x.Id,
                                                         UserKey = x.Guid,
                                                         Name = x.DisplayName,
                                                         FirstName = x.FirstName,
                                                         LastName = x.LastName,
                                                         MobileNumber = x.MobileNumber,
                                                         EmailAddress = x.EmailAddress,

                                                         RoleId = x.RoleId,
                                                         RoleKey = x.Role.SystemName,
                                                         RoleName = x.Role.Name,

                                                         Designation = x.Designation,

                                                         DepartmentId = x.DepartmentId,
                                                         DepartmentKey = x.Department.Guid,
                                                         DepartmentName = x.Department.DepartmentName,

                                                         DivisionId = x.DivisionId,
                                                         DivisionKey = x.Division.Guid,
                                                         DivisionName = x.Division.DivisionName,

                                                         Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                                         CreateDate = x.CreatedDate,
                                                         CreatedById = x.CreatedById,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifiedDate,
                                                         ModifyById = x.ModifiedById,
                                                         ModifyByKey = x.ModifiedBy.Guid,
                                                         ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                     }).OrderBy(_Request.SortExpression)
                                                     .Where(_Request.SearchCondition)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToListAsync();

                    foreach (var _DataItem in _Users)
                    {
                        _UserDownload.Add(new OUserAccount.UserListDownload
                        {
                            User_Name = _DataItem.Name,
                            Email = _DataItem.EmailAddress,
                            Mobile_Number = _DataItem.MobileNumber,
                            Designation = _DataItem.Designation,
                            Division = _DataItem.DivisionName,
                            Department = _DataItem.DepartmentName,
                            User_Role = _DataItem.RoleName,
                            Status = _DataItem.StatusName,
                            Registered_By = _DataItem.CreatedByDisplayName,
                            Registered_On = _DataItem.CreateDate.ToString()
                        });
                    }

                    return _UserDownload;
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetUsers", _Exception, _Request.UserReference);
            }
        }
    }
}
