﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HCore.Helper;
using HCore.Data;
using HCore.Data.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
using HCore.RKBS.Core.Object.Master;
using DocumentFormat.OpenXml.Wordprocessing;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkClient
    {
        HCoreContext? _HCoreContext;
        AtAccount? _AtAccount;
        AtAddress? _AtAddress;
        Random? _Random;

        internal async Task<OResponse> SaveClient(OClient.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATNAME", "Please enter the client name");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter the client email address");
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATMBNO", "Please enter the client mobile number");
                }
                if(_Request.AddressDetails == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATADDRESS", "Please enter the client address details");
                }
                if(_Request.AddressDetails != null)
                {
                    if (string.IsNullOrEmpty(_Request.AddressDetails.Address))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATNAME", "Please enter address");
                    }
                    if (string.IsNullOrEmpty(_Request.AddressDetails.StateKey))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please select state");
                    }
                    if (string.IsNullOrEmpty(_Request.AddressDetails.CityKey))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATMBNO", "Please select city");
                    }
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATINSTS", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string? SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool _IsExists = await _HCoreContext.AtAccounts.AnyAsync(x => x.SystemName == SystemName && x.OrganizationId == _Request.UserReference.OrganizationId && x.AccountTypeId == UserAccountType.Client);
                    if(_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXISTS", "Client details already present");
                    }

                    _Random = new Random();
                    string AccountCode = HCoreHelper.GenerateAccountCode(6);
                    string[] FullName = _Request.Name.Split(" ");
                    string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                    if (FullName.Length == 0 || FullName.Length == 1)
                    {
                        FirstName = _Request.Name;
                    }
                    else if (FullName.Length == 2)
                    {
                        FirstName = FullName[0];
                        LastName = FullName[1];
                    }
                    else if (FullName.Length == 3)
                    {
                        FirstName = FullName[0];
                        LastName = FullName[2];
                    }

                    _AtAccount = new AtAccount();
                    _AtAccount.Guid = HCoreHelper.GenerateGuid();
                    _AtAccount.AccountTypeId = UserAccountType.Client;
                    _AtAccount.OrganizationId = _Request.UserReference.OrganizationId;
                    _AtAccount.MobileNumber = _Request.MobileNumber;
                    _AtAccount.DisplayName = _Request.Name;
                    _AtAccount.FirstName = FirstName;
                    _AtAccount.LastName = LastName;
                    _AtAccount.EmailAddress = _Request.EmailAddress;
                    _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                    _AtAccount.CpName = _Request.CpName;
                    _AtAccount.CpMobileNumber = _Request.CpMobileNumber;
                    _AtAccount.CpEmailAddress = _Request.CpEmailAddress;
                    _AtAccount.AccountCode = AccountCode;
                    _AtAccount.CreatedById = _Request.UserReference.AccountId;
                    _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAccount.StatusId = StatusId;
                    _HCoreContext.AtAccounts.Add(_AtAccount);
                    await _HCoreContext.SaveChangesAsync();

                    if (_Request.AddressDetails != null)
                    {
                        long? StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                        long? CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();

                        _AtAddress = new AtAddress();
                        _AtAddress.Guid = HCoreHelper.GenerateGuid();
                        _AtAddress.AccountId = _AtAccount.Id;
                        _AtAddress.Address = _Request.AddressDetails.Address;
                        _AtAddress.CountryId = 1;
                        _AtAddress.StateId = (long)StateId;
                        _AtAddress.CityId = (long)CityId;
                        _AtAddress.StatusId = StatusId;
                        _AtAddress.CreatedById = _Request.UserReference.AccountId;
                        _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                        await _HCoreContext.SaveChangesAsync();
                    }

                    var _Response = new
                    {
                        UserId = _AtAccount.Id,
                        UserKey = _AtAccount.Guid,
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "Client registered successfully.");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveClient", _Exception, _Request.UserReference);
            }
        }
    }
}
