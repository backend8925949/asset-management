﻿using Akka.Actor;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using SixLabors.ImageSharp.Drawing.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkBulkUpload
    {
        HCoreContext? _HCoreContext;
        AtAccount? _AtAccount;
        AtAccountAuth? _AtAccountAuth;
        AtDivision? _AtDivision;
        AtDepartment? _AtDepartment;
        AtLocationAccount? _AtLocationAccount;
        AtLocation? _AtLocation;
        Random? _Random;

        internal async Task SaveEmployees(OBulkUpload.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    foreach (var Employee in _Request.Employees)
                    {
                        var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.EmailAddress == Employee.EmailAddress && x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                        if(EmployeeDetails == null)
                        {
                            _Random = new Random();
                            string[] FullName = Employee.Name.Split(" ");
                            string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                            if (FullName.Length == 0 || FullName.Length == 1)
                            {
                                FirstName = Employee.Name;
                            }
                            else if (FullName.Length == 2)
                            {
                                FirstName = FullName[0];
                                LastName = FullName[1];
                            }
                            else if (FullName.Length == 3)
                            {
                                FirstName = FullName[0];
                                LastName = FullName[2];
                            }

                            long? DepartmentId = 0;
                            long? DivisionId = await _HCoreContext.AtDivisions.Where(x => x.DivisionName == Employee.Division && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            if (DivisionId != null && DivisionId > 0)
                            {
                                DepartmentId = await _HCoreContext.AtDepartments.Where(x => x.DivisionId == DivisionId && x.DepartmentName == Employee.Department && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            }
                            else
                            {
                                _AtDivision = new AtDivision();
                                _AtDivision.Guid = HCoreHelper.GenerateGuid();
                                _AtDivision.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDivision.DivisionName = Employee.Division;
                                _AtDivision.CreatedById = _Request.UserReference.AccountId;
                                _AtDivision.CreatedDate = HCoreHelper.GetDateTime();
                                _AtDivision.StatusId = HelperStatus.Default.Active;
                                await _HCoreContext.AtDivisions.AddAsync(_AtDivision);
                                await _HCoreContext.SaveChangesAsync();

                                DivisionId = _AtDivision.Id;

                                _AtDepartment = new AtDepartment();
                                _AtDepartment.Guid = HCoreHelper.GenerateGuid();
                                _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDepartment.DivisionId = DivisionId;
                                _AtDepartment.DepartmentName = Employee.Department;
                                _AtDepartment.StatusId = HelperStatus.Default.Active;
                                _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                                _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                                await _HCoreContext.SaveChangesAsync();

                                DepartmentId = _AtDepartment.Id;
                            }

                            if (DivisionId > 0 && DepartmentId < 1)
                            {
                                _AtDepartment = new AtDepartment();
                                _AtDepartment.Guid = HCoreHelper.GenerateGuid();
                                _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDepartment.DivisionId = DivisionId;
                                _AtDepartment.DepartmentName = Employee.Department;
                                _AtDepartment.StatusId = HelperStatus.Default.Active;
                                _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                                _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                                await _HCoreContext.SaveChangesAsync();

                                DepartmentId = _AtDepartment.Id;
                            }

                            _AtAccount = new AtAccount();
                            _AtAccount.Guid = HCoreHelper.GenerateGuid();
                            _AtAccount.AccountTypeId = UserAccountType.Employee;
                            _AtAccount.MobileNumber = Employee.MobileNumber;
                            _AtAccount.OrganizationId = _Request.UserReference.OrganizationId;
                            _AtAccount.DivisionId = DivisionId;
                            _AtAccount.DepartmentId = DepartmentId;
                            _AtAccount.DisplayName = Employee.Name;
                            _AtAccount.FirstName = FirstName;
                            _AtAccount.LastName = LastName;
                            _AtAccount.EmailAddress = Employee.EmailAddress;
                            _AtAccount.Designation = Employee.Designation;
                            _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                            _AtAccount.AccountCode = Employee.AccountCode;
                            _AtAccount.CreatedById = _Request.UserReference.AccountId;
                            _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                            _AtAccount.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.AtAccounts.Add(_AtAccount);
                            await _HCoreContext.SaveChangesAsync();

                            _AtAccountAuth = new AtAccountAuth();
                            _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _AtAccountAuth.UserId = _AtAccount.Id;
                            _AtAccountAuth.UserName = Employee.EmailAddress;
                            _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                            _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                            _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                            _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                            await _HCoreContext.SaveChangesAsync();

                            long? LocationId = await _HCoreContext.AtLocations.Where(x => x.LocationName == Employee.Location && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            long? SubLocationId = 0;
                            if(LocationId > 0)
                            {
                                SubLocationId = await _HCoreContext.AtLocations.Where(x => x.LocationName == Employee.SubLocation && x.ParentId == LocationId && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                                if(SubLocationId < 1)
                                {
                                    _AtLocation = new AtLocation();
                                    _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                    _AtLocation.LocationName = Employee.SubLocation;
                                    _AtLocation.ParentId = LocationId;
                                    _AtLocation.DivisionId = DivisionId;
                                    _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtLocation.StatusId = HelperStatus.Default.Active;
                                    _AtLocation.CreatedById = _Request.UserReference.AccountId;
                                    _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                    await _HCoreContext.SaveChangesAsync();

                                    SubLocationId = _AtLocation.Id;
                                }
                            }
                            else
                            {
                                _AtLocation = new AtLocation();
                                _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                _AtLocation.LocationName = Employee.Location;
                                _AtLocation.DivisionId = DivisionId;
                                _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtLocation.StatusId = HelperStatus.Default.Active;
                                _AtLocation.CreatedById = _Request.UserReference.AccountId;
                                _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                await _HCoreContext.SaveChangesAsync();

                                LocationId = _AtLocation.Id;

                                _AtLocation = new AtLocation();
                                _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                _AtLocation.LocationName = Employee.SubLocation;
                                _AtLocation.ParentId = LocationId;
                                _AtLocation.DivisionId = DivisionId;
                                _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtLocation.StatusId = HelperStatus.Default.Active;
                                _AtLocation.CreatedById = _Request.UserReference.AccountId;
                                _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                await _HCoreContext.SaveChangesAsync();

                                SubLocationId = _AtLocation.Id;
                            }

                            _AtLocationAccount = new AtLocationAccount();
                            _AtLocationAccount.Guid = HCoreHelper.GenerateGuid();
                            _AtLocationAccount.AccountId = _AtAccount.Id;
                            _AtLocationAccount.LocationId = LocationId;
                            _AtLocationAccount.SubLocationId = SubLocationId;
                            _AtLocationAccount.CreatedById = _Request.UserReference.AccountId;
                            _AtLocationAccount.CreatedDate = HCoreHelper.GetDateTime();
                            _AtLocationAccount.StatusId = HelperStatus.Default.Active;
                            await _HCoreContext.AtLocationAccounts.AddAsync(_AtLocationAccount);
                            await _HCoreContext.SaveChangesAsync();
                        }
                        else
                        {
                            long? DepartmentId = 0;
                            long? DivisionId = await _HCoreContext.AtDivisions.Where(x => x.DivisionName == Employee.Division && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            if (DivisionId != null && DivisionId > 0)
                            {
                                DepartmentId = await _HCoreContext.AtDepartments.Where(x => x.DivisionId == DivisionId && x.DepartmentName == Employee.Department && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            }
                            else
                            {
                                _AtDivision = new AtDivision();
                                _AtDivision.Guid = HCoreHelper.GenerateGuid();
                                _AtDivision.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDivision.DivisionName = Employee.Division;
                                _AtDivision.CreatedById = _Request.UserReference.AccountId;
                                _AtDivision.CreatedDate = HCoreHelper.GetDateTime();
                                _AtDivision.StatusId = HelperStatus.Default.Active;
                                await _HCoreContext.AtDivisions.AddAsync(_AtDivision);
                                await _HCoreContext.SaveChangesAsync();

                                DivisionId = _AtDivision.Id;

                                _AtDepartment = new AtDepartment();
                                _AtDepartment.Guid = HCoreHelper.GenerateGuid();
                                _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDepartment.DivisionId = DivisionId;
                                _AtDepartment.DepartmentName = Employee.Department;
                                _AtDepartment.StatusId = HelperStatus.Default.Active;
                                _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                                _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                                await _HCoreContext.SaveChangesAsync();

                                DepartmentId = _AtDepartment.Id;
                            }

                            if (DivisionId > 0 && DepartmentId < 1)
                            {
                                _AtDepartment = new AtDepartment();
                                _AtDepartment.Guid = HCoreHelper.GenerateGuid();
                                _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDepartment.DivisionId = DivisionId;
                                _AtDepartment.DepartmentName = Employee.Department;
                                _AtDepartment.StatusId = HelperStatus.Default.Active;
                                _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                                _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                                await _HCoreContext.SaveChangesAsync();

                                DepartmentId = _AtDepartment.Id;
                            }

                            long? LocationId = await _HCoreContext.AtLocations.Where(x => x.LocationName == Employee.Location && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            long? SubLocationId = 0;
                            if (LocationId > 0)
                            {
                                SubLocationId = await _HCoreContext.AtLocations.Where(x => x.LocationName == Employee.SubLocation && x.ParentId == LocationId && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                                if (SubLocationId < 1)
                                {
                                    _AtLocation = new AtLocation();
                                    _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                    _AtLocation.LocationName = Employee.Location;
                                    _AtLocation.ParentId = LocationId;
                                    _AtLocation.DivisionId = DivisionId;
                                    _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtLocation.StatusId = HelperStatus.Default.Active;
                                    _AtLocation.CreatedById = _Request.UserReference.AccountId;
                                    _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                    await _HCoreContext.SaveChangesAsync();

                                    SubLocationId = _AtLocation.Id;
                                }
                            }
                            else
                            {
                                _AtLocation = new AtLocation();
                                _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                _AtLocation.LocationName = Employee.Location;
                                _AtLocation.DivisionId = DivisionId;
                                _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtLocation.StatusId = HelperStatus.Default.Active;
                                _AtLocation.CreatedById = _Request.UserReference.AccountId;
                                _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                await _HCoreContext.SaveChangesAsync();

                                LocationId = _AtLocation.Id;

                                _AtLocation = new AtLocation();
                                _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                _AtLocation.LocationName = Employee.SubLocation;
                                _AtLocation.ParentId = LocationId;
                                _AtLocation.DivisionId = DivisionId;
                                _AtLocation.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtLocation.StatusId = HelperStatus.Default.Active;
                                _AtLocation.CreatedById = _Request.UserReference.AccountId;
                                _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                await _HCoreContext.SaveChangesAsync();

                                SubLocationId = _AtLocation.Id;
                            }

                            EmployeeDetails.AccountCode = Employee.AccountCode;
                            EmployeeDetails.Designation = Employee.Designation;
                            EmployeeDetails.DivisionId = DivisionId;
                            EmployeeDetails.DepartmentId = DepartmentId;
                            EmployeeDetails.ModifiedById = _Request.UserReference.AccountId;
                            EmployeeDetails.ModifiedDate = HCoreHelper.GetDateTime();

                            var _AccountLocationDetails = await _HCoreContext.AtLocationAccounts.Where(x => x.AccountId == EmployeeDetails.Id).FirstOrDefaultAsync();
                            if(_AccountLocationDetails != null)
                            {
                                _AccountLocationDetails.LocationId = LocationId;
                                _AccountLocationDetails.SubLocationId = SubLocationId;
                                _AccountLocationDetails.ModifiedById = _Request.UserReference.AccountId;
                                _AccountLocationDetails.ModifiedDate = HCoreHelper.GetDateTime();
                            }
                            else
                            {
                                _AtLocationAccount = new AtLocationAccount();
                                _AtLocationAccount.Guid = HCoreHelper.GenerateGuid();
                                _AtLocationAccount.AccountId = EmployeeDetails.Id;
                                _AtLocationAccount.LocationId = LocationId;
                                _AtLocationAccount.SubLocationId = SubLocationId;
                                _AtLocationAccount.CreatedById = _Request.UserReference.AccountId;
                                _AtLocationAccount.CreatedDate = HCoreHelper.GetDateTime();
                                _AtLocationAccount.StatusId = HelperStatus.Default.Active;
                                await _HCoreContext.AtLocationAccounts.AddAsync(_AtLocationAccount);
                            }

                            await _HCoreContext.SaveChangesAsync();
                        }
                    }

                    await _HCoreContext.DisposeAsync();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveEmployees", _Exception, _Request.UserReference);
            }
        }

        internal async Task SaveUsers(OBulkUpload.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    foreach (var User in _Request.Users)
                    {
                        string? Body = "<div>";
                        Body += "<hr>";
                        Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                        Body += "<label> Please use the below credentials to log-in on asset management web application using given link(URL).</label>";
                        Body += "</div>";
                        Body += "<hr>";
                        Body += "<br>";
                        Body += "<div>Username: - {{UserName}}</div>";
                        Body += "<br>";
                        Body += "<div>Password: - {{Password}}</div>";
                        Body += "<br>";
                        Body += "<div>URL: - <a href=\"{{URL}}\">{{URL}}</a></div>";
                        Body += "<br>";
                        Body += "<hr>";
                        Body += "<div style=\"font -weight: 600; font-size: large; font-style: italic; font-family: system-ui;\">";
                        Body += "<label> Please use the below credentials to log-in on asset management mobile application.</label>";
                        Body += "</div>";
                        Body += "<hr>";
                        Body += "<br>";
                        Body += "<div>Username: - {{MobileNumber}}</div>";
                        Body += "<br>";
                        Body += "<div>Password: - {{AuthPin}}</div>";
                        Body += "</div>";
                        StringBuilder? EmailBody = new StringBuilder(Body);

                        var _UserDetails = await _HCoreContext.AtAccounts.Where(x => (x.EmailAddress == User.EmailAddress || x.MobileNumber == User.MobileNumber) && (x.RoleId == UserRole.Admin || x.RoleId == UserRole.SuperAdmin) && x.OrganizationId == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                        if (_UserDetails == null)
                        {
                            _Random = new Random();
                            long? DepartmentId = 0;
                            long? DivisionId = await _HCoreContext.AtDivisions.Where(x => x.DivisionName == User.Division && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            if (DivisionId != null && DivisionId > 0)
                            {
                                DepartmentId = await _HCoreContext.AtDepartments.Where(x => x.DivisionId == DivisionId && x.DepartmentName == User.Department && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                            }
                            else
                            {
                                _AtDivision = new AtDivision();
                                _AtDivision.Guid = HCoreHelper.GenerateGuid();
                                _AtDivision.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDivision.DivisionName = User.Division;
                                _AtDivision.CreatedById = _Request.UserReference.AccountId;
                                _AtDivision.CreatedDate = HCoreHelper.GetDateTime();
                                _AtDivision.StatusId = HelperStatus.Default.Active;
                                await _HCoreContext.AtDivisions.AddAsync(_AtDivision);
                                await _HCoreContext.SaveChangesAsync();

                                DivisionId = _AtDivision.Id;

                                _AtDepartment = new AtDepartment();
                                _AtDepartment.Guid = HCoreHelper.GenerateGuid();
                                _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDepartment.DivisionId = DivisionId;
                                _AtDepartment.DepartmentName = User.Department;
                                _AtDepartment.StatusId = HelperStatus.Default.Active;
                                _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                                _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                                await _HCoreContext.SaveChangesAsync();

                                DepartmentId = _AtDepartment.Id;
                            }

                            if (DivisionId > 0 && DepartmentId < 0)
                            {
                                _AtDepartment = new AtDepartment();
                                _AtDepartment.Guid = HCoreHelper.GenerateGuid();
                                _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtDepartment.DivisionId = DivisionId;
                                _AtDepartment.DepartmentName = User.Department;
                                _AtDepartment.StatusId = HelperStatus.Default.Active;
                                _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                                _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                                await _HCoreContext.SaveChangesAsync();

                                DepartmentId = _AtDepartment.Id;
                            }

                            _AtAccount = new AtAccount();
                            _AtAccount.Guid = HCoreHelper.GenerateGuid();
                            _AtAccount.AccountTypeId = UserAccountType.User;
                            _AtAccount.RoleId = UserRole.Admin;
                            _AtAccount.SourceId = RegistrationSource.WebApplication;
                            _AtAccount.MobileNumber = User.MobileNumber;
                            _AtAccount.OrganizationId = _Request.UserReference.OrganizationId;
                            _AtAccount.DivisionId = DivisionId;
                            _AtAccount.DepartmentId = DepartmentId;
                            _AtAccount.DisplayName = User.UserName;
                            _AtAccount.FirstName = User.FirstName;
                            _AtAccount.LastName = User.LastName;
                            _AtAccount.EmailAddress = User.EmailAddress;
                            _AtAccount.Designation = User.Designation;
                            _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                            _AtAccount.AccountCode = HCoreHelper.GenerateAccountCode();
                            _AtAccount.CreatedById = _Request.UserReference.AccountId;
                            _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                            _AtAccount.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.AtAccounts.Add(_AtAccount);
                            await _HCoreContext.SaveChangesAsync();

                            _AtAccountAuth = new AtAccountAuth();
                            _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _AtAccountAuth.UserId = _AtAccount.Id;
                            _AtAccountAuth.UserName = User.EmailAddress;
                            _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(10));
                            _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                            _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                            _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                            await _HCoreContext.SaveChangesAsync();

                            EmailBody.Replace("{{UserName}}", _AtAccountAuth.UserName);
                            EmailBody.Replace("{{Password}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));
                            EmailBody.Replace("{{URL}}", "http://asset-management.rajkamalbarscan.in/");

                            _AtAccountAuth = new AtAccountAuth();
                            _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _AtAccountAuth.UserId = _AtAccount.Id;
                            _AtAccountAuth.UserName = User.MobileNumber;
                            _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                            _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                            _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                            _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                            await _HCoreContext.SaveChangesAsync();

                            EmailBody.Replace("{{MobileNumber}}", _AtAccountAuth.UserName);
                            EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));

                            await HCoreHelper.BroadCastEmail(User.EmailAddress, "Asset Management Log-In Credentials", EmailBody.ToString(), _Request.UserReference);
                        }
                        else
                        {
                            var _User = await _HCoreContext.AtAccountAuths.Where(x => x.UserId == _UserDetails.Id).FirstOrDefaultAsync();
                            if(_User != null)
                            {
                                EmailBody.Replace("{{UserName}}", _User.UserName);
                                EmailBody.Replace("{{Password}}", HCoreEncrypt.DecryptHash(_User.Password));
                                EmailBody.Replace("{{URL}}", "http://asset-management.rajkamalbarscan.in/");

                                var _MobileUser = await _HCoreContext.AtAccountAuths.Where(x => x.UserName == _UserDetails.MobileNumber).FirstOrDefaultAsync();
                                if(_MobileUser != null)
                                {
                                    EmailBody.Replace("{{MobileNumber}}", _MobileUser.UserName);
                                    EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(_MobileUser.Password));
                                }
                                else
                                {

                                    _AtAccountAuth = new AtAccountAuth();
                                    _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                    _AtAccountAuth.UserId = _UserDetails.Id;
                                    _AtAccountAuth.UserName = _UserDetails.MobileNumber;
                                    _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                    _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                    _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                    _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                    await _HCoreContext.SaveChangesAsync();

                                    EmailBody.Replace("{{MobileNumber}}", _AtAccountAuth.UserName);
                                    EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));
                                }

                                await HCoreHelper.BroadCastEmail(User.EmailAddress, "Asset Management Log-In Credentials", EmailBody.ToString(), _Request.UserReference);
                            }
                            else
                            {
                                _AtAccountAuth = new AtAccountAuth();
                                _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _AtAccountAuth.UserId = _UserDetails.Id;
                                _AtAccountAuth.UserName = _UserDetails.EmailAddress;
                                _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(10));
                                _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                await _HCoreContext.SaveChangesAsync();

                                EmailBody.Replace("{{UserName}}", _AtAccountAuth.UserName);
                                EmailBody.Replace("{{Password}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));
                                EmailBody.Replace("{{URL}}", "http://asset-management.rajkamalbarscan.in/");

                                _AtAccountAuth = new AtAccountAuth();
                                _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _AtAccountAuth.UserId = _UserDetails.Id;
                                _AtAccountAuth.UserName = _UserDetails.MobileNumber;
                                _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                await _HCoreContext.SaveChangesAsync();

                                EmailBody.Replace("{{MobileNumber}}", _AtAccountAuth.UserName);
                                EmailBody.Replace("{{AuthPin}}", HCoreEncrypt.DecryptHash(_AtAccountAuth.Password));

                                await HCoreHelper.BroadCastEmail(User.EmailAddress, "Asset Management Log-In Credentials", EmailBody.ToString(), _Request.UserReference);
                            }
                        }
                    }

                    await _HCoreContext.DisposeAsync();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveEmployees", _Exception, _Request.UserReference);
            }
        }
    }
}
