﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkConfigurations
    {

        HCoreContext? _HCoreContext;
        AtAccountEmailConfiguration? _AccountEmailConfiguration;
        AtDepreciationConfiguration? _DepreciationConfiguration;
        AtAssetRequisitionConfig? _AssetRequisitionConfig;
        OConfigurations.EmailConfiguration.Details? _EmailConfigurationDetails;
        OConfigurations.DepreciationConfiguration.Details? _DepreciationConfigurationDetails;

        internal async Task<OResponse> SaveEmailConfiguration(OConfigurations.EmailConfiguration.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter email address");
                }
                if (string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter password");
                }
                if (string.IsNullOrEmpty(_Request.SmtpServer))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter smtp server");
                }
                if (_Request.SmtpPort == null || _Request.SmtpPort < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter smtp port");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _AccountEmailConfiguration = new AtAccountEmailConfiguration();
                    _AccountEmailConfiguration.Guid = HCoreHelper.GenerateGuid();
                    _AccountEmailConfiguration.AccountId = _Request.UserReference.OrganizationId;
                    _AccountEmailConfiguration.EmailAddress = _Request.EmailAddress;
                    _AccountEmailConfiguration.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    _AccountEmailConfiguration.SmtpServer = _Request.SmtpServer;
                    _AccountEmailConfiguration.Port = _Request.SmtpPort;
                    _AccountEmailConfiguration.StatusId = HelperStatus.Default.Active;
                    _AccountEmailConfiguration.CreatedDate = HCoreHelper.GetDateTime();
                    _AccountEmailConfiguration.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContext.AtAccountEmailConfigurations.AddAsync(_AccountEmailConfiguration);
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSUCCESS", "Email configuration details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveEmailConfiguration", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> SaveDepreciationConfiguration(OConfigurations.DepreciationConfiguration.Request _Request)
        {
            try
            {
                int? MethodId = 0;
                if (string.IsNullOrEmpty(_Request.MethodCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREQ", "Please select depreciation method");
                }
                else
                {
                    MethodId = HCoreHelper.GetSystemHelperId(_Request.MethodCode, _Request.UserReference);
                    if (MethodId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Invalid depreciation method selected");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _DepreciationConfiguration = new AtDepreciationConfiguration();
                    _DepreciationConfiguration.Guid = HCoreHelper.GenerateGuid();
                    _DepreciationConfiguration.AccountId = _Request.UserReference.OrganizationId;
                    _DepreciationConfiguration.DepreciationMethodId = MethodId;
                    if (_Request.IsFromReceivedDate == true)
                    {
                        _DepreciationConfiguration.IsFromReceivedDate = 1;
                    }
                    else
                    {
                        _DepreciationConfiguration.IsFromReceivedDate = 0;
                    }
                    if (_Request.IsFromIssuedDate == true)
                    {
                        _DepreciationConfiguration.IsFromIssuedDate = 1;
                    }
                    else
                    {
                        _DepreciationConfiguration.IsFromIssuedDate = 0;
                    }
                    if (_Request.IsWithActualValue == true)
                    {
                        _DepreciationConfiguration.IsWithActualValue = 1;
                    }
                    else
                    {
                        _DepreciationConfiguration.IsWithActualValue = 0;
                    }
                    if (_Request.IsWithSalvageValue == true)
                    {
                        _DepreciationConfiguration.IsWithSalvageValue = 1;
                    }
                    else
                    {
                        _DepreciationConfiguration.IsWithSalvageValue = 0;
                    }
                    _DepreciationConfiguration.StatusId = HelperStatus.Default.Active;
                    _DepreciationConfiguration.CreatedDate = HCoreHelper.GetDateTime();
                    _DepreciationConfiguration.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContext.AtDepreciationConfigurations.AddAsync(_DepreciationConfiguration);
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSUCCESS", "Depreciation configuration details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDepreciationConfiguration", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateEmailConfiguration(OConfigurations.EmailConfiguration.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter email address");
                }
                if (string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter password");
                }
                if (string.IsNullOrEmpty(_Request.SmtpServer))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter smtp server");
                }
                if (_Request.SmtpPort == null || _Request.SmtpPort < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEMAIL", "Please enter smtp port");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var EmailConfiguration = await _HCoreContext.AtAccountEmailConfigurations.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (EmailConfiguration == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Please add email configuration");
                    }
                    EmailConfiguration.EmailAddress = _Request.EmailAddress;
                    EmailConfiguration.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                    EmailConfiguration.SmtpServer = _Request.SmtpServer;
                    EmailConfiguration.Port = _Request.SmtpPort;
                    EmailConfiguration.ModifiedDate = HCoreHelper.GetDateTime();
                    EmailConfiguration.ModifiedById = _Request.UserReference.AccountId;
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATUPDATE", "Email configuration details updates successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateEmailConfiguration", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateDepreciationConfiguration(OConfigurations.DepreciationConfiguration.Request _Request)
        {
            try
            {
                int? MethodId = 0;
                if (string.IsNullOrEmpty(_Request.MethodCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREQ", "Please select depreciation method");
                }
                else
                {
                    MethodId = HCoreHelper.GetSystemHelperId(_Request.MethodCode, _Request.UserReference);
                    if (MethodId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Invalid depreciation method selected");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepreciationConfiguration = await _HCoreContext.AtDepreciationConfigurations.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (DepreciationConfiguration == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Please add depreciation configuration");
                    }
                    DepreciationConfiguration.DepreciationMethodId = MethodId;
                    if (_Request.IsFromReceivedDate == true)
                    {
                        DepreciationConfiguration.IsFromReceivedDate = 1;
                    }
                    else
                    {
                        DepreciationConfiguration.IsFromReceivedDate = 0;
                    }
                    if (_Request.IsFromIssuedDate == true)
                    {
                        DepreciationConfiguration.IsFromIssuedDate = 1;
                    }
                    else
                    {
                        DepreciationConfiguration.IsFromIssuedDate = 0;
                    }
                    if (_Request.IsWithActualValue == true)
                    {
                        DepreciationConfiguration.IsWithActualValue = 1;
                    }
                    else
                    {
                        DepreciationConfiguration.IsWithActualValue = 0;
                    }
                    if (_Request.IsWithSalvageValue == true)
                    {
                        DepreciationConfiguration.IsWithSalvageValue = 1;
                    }
                    else
                    {
                        DepreciationConfiguration.IsWithSalvageValue = 0;
                    }
                    DepreciationConfiguration.ModifiedDate = HCoreHelper.GetDateTime();
                    DepreciationConfiguration.ModifiedById = _Request.UserReference.AccountId;
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATUPDATE", "Depreciation configuration details updated successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateDepreciationConfiguration", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetEmailConfiguration(OReference _Request)
        {
            try
            {
                _EmailConfigurationDetails = new OConfigurations.EmailConfiguration.Details();
                using (_HCoreContext = new HCoreContext())
                {
                    var EmailConfiguration = await _HCoreContext.AtAccountEmailConfigurations.Where(x => x.AccountId == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                    if (EmailConfiguration == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Please add email configuration");
                    }

                    _EmailConfigurationDetails.ReferenceId = EmailConfiguration.Id;
                    _EmailConfigurationDetails.ReferenceKey = EmailConfiguration.Guid;
                    _EmailConfigurationDetails.EmailAddress = EmailConfiguration.EmailAddress;
                    _EmailConfigurationDetails.Password = HCoreEncrypt.DecryptHash(EmailConfiguration.Password);
                    _EmailConfigurationDetails.SmtpServer = EmailConfiguration.SmtpServer;
                    _EmailConfigurationDetails.SmtpPort = EmailConfiguration.Port;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _EmailConfigurationDetails, "AT200", "Email configuration details loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetEmailConfiguration", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDepreciationConfiguration(OReference _Request)
        {
            try
            {
                _DepreciationConfigurationDetails = new OConfigurations.DepreciationConfiguration.Details();
                using (_HCoreContext = new HCoreContext())
                {
                    var DepreciationConfiguration = await _HCoreContext.AtDepreciationConfigurations.Where(x => x.AccountId == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                    if (DepreciationConfiguration == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Please add depreciation configuration");
                    }

                    _DepreciationConfigurationDetails.ReferenceId = DepreciationConfiguration.Id;
                    _DepreciationConfigurationDetails.ReferenceKey = DepreciationConfiguration.Guid;
                    if (DepreciationConfiguration.IsFromReceivedDate == 1)
                    {
                        _DepreciationConfigurationDetails.IsFromReceivedDate = true;
                    }
                    else
                    {
                        _DepreciationConfigurationDetails.IsFromReceivedDate = false;
                    }

                    if (DepreciationConfiguration.IsFromIssuedDate == 1)
                    {
                        _DepreciationConfigurationDetails.IsFromIssuedDate = true;
                    }
                    else
                    {
                        _DepreciationConfigurationDetails.IsFromIssuedDate = false;
                    }

                    if (DepreciationConfiguration.IsWithActualValue == 1)
                    {
                        _DepreciationConfigurationDetails.IsWithActualValue = true;
                    }
                    else
                    {
                        _DepreciationConfigurationDetails.IsWithActualValue = false;
                    }

                    if (DepreciationConfiguration.IsWithSalvageValue == 1)
                    {
                        _DepreciationConfigurationDetails.IsWithSalvageValue = true;
                    }
                    else
                    {
                        _DepreciationConfigurationDetails.IsWithSalvageValue = false;
                    }

                    _DepreciationConfigurationDetails.MethodId = await _HCoreContext.AtCores.Where(x => x.Id == DepreciationConfiguration.DepreciationMethodId).Select(x => x.Id).FirstOrDefaultAsync();
                    _DepreciationConfigurationDetails.MethodCode = await _HCoreContext.AtCores.Where(x => x.Id == DepreciationConfiguration.DepreciationMethodId).Select(x => x.SystemName).FirstOrDefaultAsync();
                    _DepreciationConfigurationDetails.MethodName = await _HCoreContext.AtCores.Where(x => x.Id == DepreciationConfiguration.DepreciationMethodId).Select(x => x.Name).FirstOrDefaultAsync();

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DepreciationConfigurationDetails, "AT200", "Depreciation configuration details loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDepreciationConfiguration", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> SaveRequisitionConfig(OConfigurations.RequisitionConfig.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATROLEKEY", "Please select role");
                }
                long? RoleId = 0;
                if (!string.IsNullOrEmpty(_Request.RoleKey))
                {
                    RoleId = await HCoreHelper.GetUserRole(_Request.RoleKey, _Request.UserReference);
                    if (RoleId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINVROLE", "Invalid role selected");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool _IsExist = await _HCoreContext.AtAssetRequisitionConfigs.AnyAsync(x => x.RoleId == RoleId && x.OrganizationId == _Request.UserReference.OrganizationId);
                    if(_IsExist)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXIST", "Approval configuration details are already present for selected role");
                    }

                    _AssetRequisitionConfig = new AtAssetRequisitionConfig();
                    _AssetRequisitionConfig.Guid = HCoreHelper.GenerateGuid();
                    _AssetRequisitionConfig.RoleId = (int)RoleId;
                    _AssetRequisitionConfig.OrganizationId = _Request.UserReference.OrganizationId;

                    if (_Request.IssueRequest)
                    {
                        _AssetRequisitionConfig.IssueRequest = 1;
                    }
                    if(_Request.IssueApproval)
                    {
                        _AssetRequisitionConfig.IssueApproval = 1;
                    }
                    if(_Request.TransferRequest)
                    {
                        _AssetRequisitionConfig.TransferRequest = 1;
                    }
                    if(_Request.TransferApproval)
                    {
                        _AssetRequisitionConfig.TransferApproval = 1;
                    }
                    if(_Request.ReturnRequest)
                    {
                        _AssetRequisitionConfig.ReturnRequest = 1;
                    }
                    if(_Request.ReturnApproval)
                    {
                        _AssetRequisitionConfig.ReturnApproval = 1;
                    }
                    if(_Request.MaintenanceRequest)
                    {
                        _AssetRequisitionConfig.MaintenanceRequest = 1;
                    }
                    if(_Request.MaintenanceApproval)
                    {
                        _AssetRequisitionConfig.MaintenanceApproval = 1;
                    }
                    if(_Request.PORequest)
                    {
                        _AssetRequisitionConfig.PoRequest = 1;
                    }
                    if(_Request.POApproval)
                    {
                        _AssetRequisitionConfig.PoApproval = 1;
                    }

                    _AssetRequisitionConfig.CreatedDate = HCoreHelper.GetDateTime();
                    _AssetRequisitionConfig.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContext.AtAssetRequisitionConfigs.AddAsync(_AssetRequisitionConfig);
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "AT0200", "Requisition configuration details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRequisitionConfig", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateRequisitionConfig(OConfigurations.RequisitionConfig.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.RoleKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATROLEKEY", "Please select role");
                }
                long? RoleId = 0;
                if (!string.IsNullOrEmpty(_Request.RoleKey))
                {
                    RoleId = await HCoreHelper.GetUserRole(_Request.RoleKey, _Request.UserReference);
                    if (RoleId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINVROLE", "Invalid role selected");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = await _HCoreContext.AtAssetRequisitionConfigs.Where(x => x.RoleId == RoleId && x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                    if (Details == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Approval configuration details not found for selected role");
                    }

                    if (_Request.IssueRequest)
                    {
                        Details.IssueRequest = 1;
                    }
                    else
                    {
                        Details.IssueRequest = 0;
                    }

                    if (_Request.IssueApproval)
                    {
                        Details.IssueApproval = 1;
                    }
                    else
                    {
                        Details.IssueApproval = 0;
                    }

                    if (_Request.TransferRequest)
                    {
                        Details.TransferRequest = 1;
                    }
                    else
                    {
                        Details.TransferRequest = 0;
                    }

                    if (_Request.TransferApproval)
                    {
                        Details.TransferApproval = 1;
                    }
                    else
                    {
                        Details.TransferApproval = 0;
                    }

                    if (_Request.ReturnRequest)
                    {
                        Details.ReturnRequest = 1;
                    }
                    else
                    {
                        Details.ReturnRequest = 0;
                    }

                    if (_Request.ReturnApproval)
                    {
                        Details.ReturnApproval = 1;
                    }
                    else
                    {
                        Details.ReturnApproval = 0;
                    }

                    if (_Request.MaintenanceRequest)
                    {
                        Details.MaintenanceRequest = 1;
                    }
                    else
                    {
                        Details.MaintenanceRequest = 0;
                    }

                    if (_Request.MaintenanceApproval)
                    {
                        Details.MaintenanceApproval = 1;
                    }
                    else
                    {
                        Details.MaintenanceApproval = 0;
                    }

                    if (_Request.PORequest)
                    {
                        Details.PoRequest = 1;
                    }
                    else
                    {
                        Details.PoRequest = 0;
                    }

                    if (_Request.POApproval)
                    {
                        Details.PoApproval = 1;
                    }
                    else
                    {
                        Details.PoApproval = 0;
                    }

                    Details.ModifiedDate = HCoreHelper.GetDateTime();
                    Details.ModifiedById = _Request.UserReference.AccountId;
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "AT0200", "Approval configuration details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveRequisitionConfig", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetRequisitionConfig(OReference _Request)
        {
            try
            {
                if(string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFKEY", "Please select valid details");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREFID", "Please select valid details");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = await _HCoreContext.AtAssetRequisitionConfigs.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                  .Select(x => new OConfigurations.RequisitionConfig.Details
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,
                                      RoleId = x.RoleId,
                                      RoleKey = x.Role.Guid,
                                      RoleName = x.Role.Name,
                                      IsIssueRequest = x.IssueRequest,
                                      IsIssueApproval = x.IssueApproval,
                                      IsTransferRequest = x.TransferRequest,
                                      IsTransferApproval = x.TransferApproval,
                                      IsReturnRequest = x.ReturnRequest,
                                      IsReturnApproval = x.ReturnApproval,
                                      IsMaintenanceRequest = x.MaintenanceRequest,
                                      IsMaintenanceApproval = x.MaintenanceApproval,
                                      IsPORequest = x.PoRequest,
                                      IsPOApproval = x.PoApproval,
                                  }).FirstOrDefaultAsync();
                    if (Details == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Please select valid details");
                    }
                    else
                    {
                        if (Details.IsIssueRequest == 1)
                        {
                            Details.IssueRequest = true;
                        }
                        else
                        {
                            Details.IssueRequest = false;
                        }

                        if (Details.IsIssueApproval == 1)
                        {
                            Details.IssueApproval = true;
                        }
                        else
                        {
                            Details.IssueApproval = false;
                        }

                        if (Details.IsTransferRequest == 1)
                        {
                            Details.TransferRequest = true;
                        }
                        else
                        {
                            Details.TransferRequest = false;
                        }

                        if (Details.IsTransferApproval == 1)
                        {
                            Details.TransferApproval = true;
                        }
                        else
                        {
                            Details.TransferApproval = false;
                        }

                        if (Details.IsReturnRequest == 1)
                        {
                            Details.ReturnRequest = true;
                        }
                        else
                        {
                            Details.ReturnRequest = false;
                        }

                        if (Details.IsReturnApproval == 1)
                        {
                            Details.ReturnApproval = true;
                        }
                        else
                        {
                            Details.ReturnApproval = false;
                        }

                        if (Details.IsMaintenanceRequest == 1)
                        {
                            Details.MaintenanceRequest = true;
                        }
                        else
                        {
                            Details.MaintenanceRequest = false;
                        }

                        if (Details.IsMaintenanceApproval == 1)
                        {
                            Details.MaintenanceApproval = true;
                        }
                        else
                        {
                            Details.MaintenanceApproval = false;
                        }

                        if (Details.IsPORequest == 1)
                        {
                            Details.PORequest = true;
                        }
                        else
                        {
                            Details.PORequest = false;
                        }

                        if (Details.IsPOApproval == 1)
                        {
                            Details.POApproval = true;
                        }
                        else
                        {
                            Details.POApproval = false;
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "AT200", "Requisition configuration details loaded successfully");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRequisitionConfig", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetRequisitionConfig(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRequisitionConfigs.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OConfigurations.RequisitionConfig.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    RoleId = x.RoleId,
                                                    RoleKey = x.Role.Guid,
                                                    RoleName = x.Role.Name,
                                                    IsIssueRequest = x.IssueRequest,
                                                    IsIssueApproval = x.IssueApproval,
                                                    IsTransferRequest = x.TransferRequest,
                                                    IsTransferApproval = x.TransferApproval,
                                                    IsReturnRequest = x.ReturnRequest,
                                                    IsReturnApproval = x.ReturnApproval,
                                                    IsMaintenanceRequest = x.MaintenanceRequest,
                                                    IsMaintenanceApproval = x.MaintenanceApproval,
                                                    IsPORequest = x.PoRequest,
                                                    IsPOApproval = x.PoApproval,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OConfigurations.RequisitionConfig.List> _List = await _HCoreContext.AtAssetRequisitionConfigs.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                                         .Select(x => new OConfigurations.RequisitionConfig.List
                                                                         {
                                                                             ReferenceId = x.Id,
                                                                             ReferenceKey = x.Guid,
                                                                             RoleId = x.RoleId,
                                                                             RoleKey = x.Role.Guid,
                                                                             RoleName = x.Role.Name,
                                                                             IsIssueRequest = x.IssueRequest,
                                                                             IsIssueApproval = x.IssueApproval,
                                                                             IsTransferRequest = x.TransferRequest,
                                                                             IsTransferApproval = x.TransferApproval,
                                                                             IsReturnRequest = x.ReturnRequest,
                                                                             IsReturnApproval = x.ReturnApproval,
                                                                             IsMaintenanceRequest = x.MaintenanceRequest,
                                                                             IsMaintenanceApproval = x.MaintenanceApproval,
                                                                             IsPORequest = x.PoRequest,
                                                                             IsPOApproval = x.PoApproval,
                                                                         }).OrderBy(_Request.SortExpression)
                                                                         .Where(_Request.SearchCondition)
                                                                         .Skip(_Request.Offset)
                                                                         .Take(_Request.Limit)
                                                                         .ToListAsync();

                    if(_List.Count > 0)
                    {
                        foreach(var _Item in _List)
                        {
                            if (_Item.IsIssueRequest == 1)
                            {
                                _Item.IssueRequest = true;
                            }
                            else
                            {
                                _Item.IssueRequest = false;
                            }

                            if (_Item.IsIssueApproval == 1)
                            {
                                _Item.IssueApproval = true;
                            }
                            else
                            {
                                _Item.IssueApproval = false;
                            }

                            if (_Item.IsTransferRequest == 1)
                            {
                                _Item.TransferRequest = true;
                            }
                            else
                            {
                                _Item.TransferRequest = false;
                            }

                            if (_Item.IsTransferApproval == 1)
                            {
                                _Item.TransferApproval = true;
                            }
                            else
                            {
                                _Item.TransferApproval = false;
                            }

                            if (_Item.IsReturnRequest == 1)
                            {
                                _Item.ReturnRequest = true;
                            }
                            else
                            {
                                _Item.ReturnRequest = false;
                            }

                            if (_Item.IsReturnApproval == 1)
                            {
                                _Item.ReturnApproval = true;
                            }
                            else
                            {
                                _Item.ReturnApproval = false;
                            }

                            if (_Item.IsMaintenanceRequest == 1)
                            {
                                _Item.MaintenanceRequest = true;
                            }
                            else
                            {
                                _Item.MaintenanceRequest = false;
                            }

                            if (_Item.IsMaintenanceApproval == 1)
                            {
                                _Item.MaintenanceApproval = true;
                            }
                            else
                            {
                                _Item.MaintenanceApproval = false;
                            }

                            if (_Item.IsPORequest == 1)
                            {
                                _Item.PORequest = true;
                            }
                            else
                            {
                                _Item.PORequest = false;
                            }

                            if (_Item.IsPOApproval == 1)
                            {
                                _Item.POApproval = true;
                            }
                            else
                            {
                                _Item.POApproval = false;
                            }
                        }
                    }

                    OList.Response _OListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OListResponse, "AT0200", "Result loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRequisitionConfig", _Exception, _Request.UserReference);
                OList.Response _OListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OListResponse, "AT500", "Internal server error occured");
            }
        }
    }
}
