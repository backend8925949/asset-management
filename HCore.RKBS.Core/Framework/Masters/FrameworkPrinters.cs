﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Printing;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using System.IO;
using Akka.IO;
using HCore.Data.Logging;
using HCore.Data.Logging.Models;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkPrinters
    {
        HCoreContextLogging? _HCoreContextLogging;
        AtAssetPrintLog? _AtAssetPrintLog;

        internal async Task<OResponse> GetPrinters(OList.Request _Request)
        {
			try
			{
                string[] Printers = PrinterSettings.InstalledPrinters.Cast<string>().ToArray();
                List<OPrinter.List>? _Printers = new List<OPrinter.List>();
                foreach (string Printer in Printers)
                {
                    _Printers.Add(new OPrinter.List
                    {
                        Name = Printer
                    });
                }

                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Printers, "ATCLUPDATE", "Location details updated successfully");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPrinters", _Exception, _Request.UserReference);
            }
        }

        internal async Task PrintBarcode(string? AssetBarcode, string ExAssetId, string? PrinterName)
        {
            try
            {
                string? Data = "<xpml><page quantity='0' pitch='25.0 mm'></xpml>SIZE 24.10 mm, 25 mm\r\nDIRECTION 0,0\r\nREFERENCE 0,0\r\nOFFSET 0 mm\r\nSET PEEL OFF\r\nSET CUTTER OFF\r\nSET PARTIAL_CUTTER OFF\r\n<xpml></page></xpml><xpml><page quantity='1' pitch='25.0 mm'></xpml>SET TEAR ON\r\nCLS\r\nQRCODE 141,141,L,4,A,180,M2,S7," + "\"" + AssetBarcode + "\"" + "\r\nCODEPAGE 1252\r\nTEXT 154,173,\"ROMAN.TTF\",180,1,6," + "\"" + AssetBarcode + "\"" + "\r\nTEXT 139,41,\"ROMAN.TTF\",180,1,6," + "\"" + ExAssetId + "\"" + "\r\nPRINT 1,1\r\n<xpml></page></xpml><xpml><end/></xpml>\r\n";

                HCorePrintingHelper.SendStringToPrinter(PrinterName, Data);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPrinters", _Exception, null);
            }
        }

        internal async Task<OResponse> SavePrintData(OPrinter.Request _Request)
        {
            try
            {
                using(_HCoreContextLogging = new HCoreContextLogging())
                {
                    _AtAssetPrintLog = new AtAssetPrintLog();
                    _AtAssetPrintLog.Guid = HCoreHelper.GenerateGuid();
                    _AtAssetPrintLog.AssetId = (long)_Request.AssetId;
                    _AtAssetPrintLog.AssetBarcode = _Request.AssetBarcode;
                    _AtAssetPrintLog.ExAssetId = _Request.ExAssetId;
                    _AtAssetPrintLog.PrintDate = HCoreHelper.GetDateTime();
                    _AtAssetPrintLog.Status = "Successful";
                    _AtAssetPrintLog.CreatedDate = HCoreHelper.GetDateTime();
                    _AtAssetPrintLog.CreatedById = _Request.UserReference.AccountId;
                    await _HCoreContextLogging.AtAssetPrintLogs.AddAsync(_AtAssetPrintLog);
                    await _HCoreContextLogging.SaveChangesAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "", "");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SavePrintData", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> SavePrintsData(OPrinter.ListRequest _Request)
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if(_Request.Assets.Count > 0)
                    {
                        foreach (var Asset in _Request.Assets)
                        {
                            _AtAssetPrintLog = new AtAssetPrintLog();
                            _AtAssetPrintLog.Guid = HCoreHelper.GenerateGuid();
                            _AtAssetPrintLog.AssetId = (long)Asset.AssetId;
                            _AtAssetPrintLog.AssetBarcode = Asset.AssetBarcode;
                            _AtAssetPrintLog.ExAssetId = Asset.ExAssetId;
                            _AtAssetPrintLog.PrintDate = HCoreHelper.GetDateTime(); ;
                            _AtAssetPrintLog.Status = "Successful";
                            _AtAssetPrintLog.CreatedDate = HCoreHelper.GetDateTime();
                            _AtAssetPrintLog.CreatedById = _Request.UserReference.AccountId;
                            await _HCoreContextLogging.AtAssetPrintLogs.AddAsync(_AtAssetPrintLog);
                        }
                        await _HCoreContextLogging.SaveChangesAsync();
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATPRINT", "Print data saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SavePrintsData", _Exception, _Request.UserReference);
            }
        }
    }
}
