﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkClass
    {
        HCoreContext? _HCoreContext;
        AtClass? _AtClass;

        internal async Task<OResponse> SaveClass(OClass.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ClassName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class name required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.ClassName == _Request.ClassName && x.OrganizationId == _Request.UserReference.OrganizationId).FirstOrDefaultAsync();
                    if (ClassDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details already exists for the selected organization");
                    }
                    else
                    {
                        _AtClass = new AtClass();
                        _AtClass.Guid = HCoreHelper.GenerateGuid();
                        _AtClass.OrganizationId = _Request.UserReference.OrganizationId;
                        _AtClass.ClassName = _Request.ClassName;
                        _AtClass.StatusId = (int)StatusId;
                        _AtClass.CreatedById = _Request.UserReference.AccountId;
                        _AtClass.CreatedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.AtClasses.AddAsync(_AtClass);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        var _Response = new
                        {
                            ClassId = _AtClass.Id,
                            ClassKey = _AtClass.Guid,
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Class created successfully");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveClass", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateClass(OClass.Update.Request _Request)
        {
            try
            {
                if (_Request.ClassId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class id required");
                }
                if (string.IsNullOrEmpty(_Request.ClassKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.OrganizationKey))
                    {
                        var OrgDetails = await _HCoreContext.AtAccounts.Where(x => x.Guid == _Request.OrganizationKey).FirstOrDefaultAsync();
                        if (OrgDetails != null)
                        {
                            var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.Guid == _Request.ClassKey && x.Id == _Request.ClassId).FirstOrDefaultAsync();
                            if (ClassDetails != null)
                            {
                                bool _Exists = await _HCoreContext.AtClasses.AnyAsync(x => x.ClassName == _Request.ClassName && x.Guid != ClassDetails.Guid && x.OrganizationId == OrgDetails.Id);
                                if (!_Exists)
                                {
                                    if (!string.IsNullOrEmpty(_Request.ClassName))
                                    {
                                        ClassDetails.ClassName = _Request.ClassName;
                                    }
                                    if (OrgDetails.Id != ClassDetails.OrganizationId)
                                    {
                                        ClassDetails.OrganizationId = OrgDetails.Id;
                                    }
                                    if (StatusId > 0 && StatusId != ClassDetails.StatusId)
                                    {
                                        ClassDetails.StatusId = (int)StatusId;
                                    }
                                    ClassDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    ClassDetails.ModifiedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.SaveChangesAsync();
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Class details updated successfully");
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details already exists for the selected organization");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details not found");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Organization details not found");
                        }
                    }
                    else
                    {
                        var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.Guid == _Request.ClassKey && x.Id == _Request.ClassId).FirstOrDefaultAsync();
                        if (ClassDetails != null)
                        {
                            bool _Exists = await _HCoreContext.AtClasses.AnyAsync(x => x.ClassName == _Request.ClassName && x.Guid != ClassDetails.Guid && x.OrganizationId == null);
                            if (!_Exists)
                            {
                                if (!string.IsNullOrEmpty(_Request.ClassName))
                                {
                                    ClassDetails.ClassName = _Request.ClassName;
                                }
                                if (StatusId > 0 && StatusId != ClassDetails.StatusId)
                                {
                                    ClassDetails.StatusId = (int)StatusId;
                                }
                                ClassDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                ClassDetails.ModifiedById = _Request.UserReference.AccountId;
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Class details updated successfully");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class name already exists");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details not found");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateClass", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteClass(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (ClassDetails != null)
                    {
                        bool _IsCategory = await _HCoreContext.AtCategories.AnyAsync(x => x.ClassId == ClassDetails.Id);
                        if (_IsCategory)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this class as it is assigned to some categories");
                        }
                        bool _IsAsset = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.AssetClassId == ClassDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this class as it is assigned to some assets");
                        }
                        _HCoreContext.AtClasses.Remove(ClassDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Class removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteClass", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetClass(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Class key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ClassDetails = await _HCoreContext.AtClasses.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OClass.Details.Response
                                       {
                                           ClassId = x.Id,
                                           ClassKey = x.Guid,
                                           ClassName = x.ClassName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (ClassDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, ClassDetails, "ATCLDELETE", "Class details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Class details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetClass", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetClasses(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ClassId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtClasses.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OClass.List.Response
                                           {
                                               ClassId = x.Id,
                                               ClassKey = x.Guid,
                                               ClassName = x.ClassName,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OClass.List.Response> _Classes = await _HCoreContext.AtClasses.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OClass.List.Response
                                       {
                                           ClassId = x.Id,
                                           ClassKey = x.Guid,
                                           ClassName = x.ClassName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Classes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Class list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetClasses", _Exception, _Request.UserReference);
            }
        }
    }
}
