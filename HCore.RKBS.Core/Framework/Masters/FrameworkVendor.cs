﻿

using Akka.Actor;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkVendor
    {
        AtAccount? _AtAccount;
        AtAccountAuth? _AtAccountAuth;
        HCoreContext? _HCoreContext;
        Random? _Random;
        AtAddress? _AtAddress;
        List<OVendor.VendorListDownload>? _VendorDownload;

        internal async Task<OResponse> SaveVendor(OVendor.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Name required.");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Email required.");
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Mobile Number required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => (x.MobileNumber == _Request.MobileNumber || x.EmailAddress == _Request.EmailAddress) && x.AccountTypeId == UserAccountType.Vendor).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV036", "Vendor already exists.");
                    }
                    else
                    {
                        _Random = new Random();
                        string AccountCode = HCoreHelper.GenerateAccountCode(6);
                        string[] FullName = _Request.Name.Split(" ");
                        string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                        if (FullName.Length == 0 || FullName.Length == 1)
                        {
                            FirstName = _Request.Name;
                        }
                        else if (FullName.Length == 2)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[1];
                        }
                        else if (FullName.Length == 3)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[2];
                        }

                        _AtAccount = new AtAccount();
                        _AtAccount.Guid = HCoreHelper.GenerateGuid();
                        _AtAccount.AccountTypeId = UserAccountType.Vendor;
                        _AtAccount.OrganizationId = _Request.UserReference.OrganizationId;
                        _AtAccount.MobileNumber = _Request.MobileNumber;
                        _AtAccount.DisplayName = _Request.Name;
                        _AtAccount.FirstName = FirstName;
                        _AtAccount.LastName = LastName;
                        _AtAccount.EmailAddress = _Request.EmailAddress;
                        _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                        _AtAccount.GstNumber = _Request.GstNumber;
                        _AtAccount.PanNumber = _Request.PanNumber;
                        _AtAccount.CpName = _Request.CpName;
                        _AtAccount.CpMobileNumber = _Request.CpMobileNumber;
                        _AtAccount.CpEmailAddress = _Request.CpEmailAddress;
                        _AtAccount.Designation = _Request.CpDesignation;
                        _AtAccount.AccountCode = AccountCode;
                        _AtAccount.CreatedById = _Request.UserReference.AccountId;
                        _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                        _AtAccount.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.AtAccounts.Add(_AtAccount);
                        await _HCoreContext.SaveChangesAsync();

                        _AtAccountAuth = new AtAccountAuth();
                        _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _AtAccountAuth.UserId = _AtAccount.Id;
                        _AtAccountAuth.UserName = _Request.EmailAddress;
                        _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                        _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                        _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                        _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                        await _HCoreContext.SaveChangesAsync();

                        if (_Request.AddressDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.AddressDetails.Address))
                            {
                                long? StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                long? CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();

                                _AtAddress = new AtAddress();
                                _AtAddress.Guid = HCoreHelper.GenerateGuid();
                                _AtAddress.AccountId = _AtAccount.Id;
                                _AtAddress.Address = _Request.AddressDetails.Address;
                                _AtAddress.CountryId = 1;
                                if (StateId > 0)
                                {
                                    _AtAddress.StateId = (long)StateId;
                                }
                                if (CityId > 0)
                                {
                                    _AtAddress.CityId = (long)CityId;
                                }
                                _AtAddress.StatusId = HelperStatus.Default.Active;
                                _AtAddress.CreatedById = _Request.UserReference.AccountId;
                                _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                                await _HCoreContext.SaveChangesAsync();
                            }
                        }

                        await _HCoreContext.DisposeAsync();

                        var _Response = new
                        {
                            VendorId = _AtAccount.Id,
                            VendorKey = _AtAccount.Guid,
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "Vendor registered successfully.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveVendor", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateVendor(OVendor.Update.Request _Request)
        {
            try
            {
                if (_Request.VendorId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Vendor id required.");
                }
                if (string.IsNullOrEmpty(_Request.VendorKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Vendor key required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.VendorId && x.Guid == _Request.VendorKey).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        bool _IsExists = await _HCoreContext.AtAccounts.AnyAsync(x => x.Guid != AccountDetails.Guid && (x.MobileNumber == _Request.MobileNumber || x.EmailAddress == _Request.EmailAddress) && x.AccountTypeId == UserAccountType.Vendor);
                        if (_IsExists)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV036", "Vendor already exists.");
                        }

                        string[] FullName = _Request.Name.Split(" ");
                        string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                        if (FullName.Length == 0 || FullName.Length == 1)
                        {
                            FirstName = _Request.Name;
                        }
                        else if (FullName.Length == 2)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[1];
                        }
                        else if (FullName.Length == 3)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[2];
                        }

                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && AccountDetails.MobileNumber != _Request.MobileNumber)
                        {
                            AccountDetails.MobileNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && AccountDetails.DisplayName != _Request.Name)
                        {
                            AccountDetails.DisplayName = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(FirstName) && AccountDetails.FirstName != FirstName)
                        {
                            AccountDetails.FirstName = FirstName;
                        }
                        if (!string.IsNullOrEmpty(LastName) && AccountDetails.LastName != LastName)
                        {
                            AccountDetails.LastName = LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                        {
                            AccountDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && AccountDetails.ContactNumber != _Request.MobileNumber)
                        {
                            AccountDetails.ContactNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.GstNumber) && AccountDetails.GstNumber != _Request.GstNumber)
                        {
                            AccountDetails.GstNumber = _Request.GstNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.PanNumber) && AccountDetails.PanNumber != _Request.PanNumber)
                        {
                            AccountDetails.PanNumber = _Request.PanNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.CpName) && AccountDetails.CpName != _Request.CpName)
                        {
                            AccountDetails.CpName = _Request.CpName;
                        }
                        if (!string.IsNullOrEmpty(_Request.CpMobileNumber) && AccountDetails.CpMobileNumber != _Request.CpMobileNumber)
                        {
                            AccountDetails.CpMobileNumber = _Request.CpMobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.CpEmailAddress) && AccountDetails.CpEmailAddress != _Request.CpEmailAddress)
                        {
                            AccountDetails.CpEmailAddress = _Request.CpEmailAddress;
                        }
                        AccountDetails.ModifiedById = _Request.UserReference.AccountId;
                        AccountDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        AccountDetails.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.SaveChangesAsync();

                        if (_Request.AddressDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.AddressDetails.Address))
                            {
                                long? StateId = 0;
                                long? CityId = 0;

                                var AddressDetails = await _HCoreContext.AtAddresses.Where(x => x.AccountId == AccountDetails.Id).FirstOrDefaultAsync();
                                if (AddressDetails != null)
                                {
                                    if (!string.IsNullOrEmpty(_Request.AddressDetails.StateKey))
                                    {
                                        StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    }
                                    if (!string.IsNullOrEmpty(_Request.AddressDetails.CityKey))
                                    {
                                        CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    }

                                    if (!string.IsNullOrEmpty(_Request.AddressDetails.Address) && AddressDetails.Address != _Request.AddressDetails.Address)
                                    {
                                        AddressDetails.Address = _Request.AddressDetails.Address;
                                    }
                                    if (StateId > 0 && AddressDetails.StateId != (long)StateId)
                                    {
                                        AddressDetails.StateId = (long)StateId;
                                    }
                                    if (CityId > 0 && AddressDetails.CityId != (long)CityId)
                                    {
                                        AddressDetails.CityId = (long)CityId;
                                    }
                                    AddressDetails.ModifiedById = _Request.UserReference.AccountId;
                                    AddressDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                else
                                {
                                    StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    _AtAddress = new AtAddress();
                                    _AtAddress.Guid = HCoreHelper.GenerateGuid();
                                    _AtAddress.AccountId = AccountDetails.Id;
                                    _AtAddress.Address = _Request.AddressDetails.Address;
                                    _AtAddress.CountryId = 1;
                                    _AtAddress.StateId = (long)StateId;
                                    _AtAddress.CityId = (long)CityId;
                                    _AtAddress.StatusId = HelperStatus.Default.Active;
                                    _AtAddress.CreatedById = _Request.UserReference.AccountId;
                                    _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                            }
                        }

                        await _HCoreContext.DisposeAsync();

                        var _Response = new
                        {
                            VendorId = AccountDetails.Id,
                            VendorKey = AccountDetails.Guid,
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "Vendor details updated successfully.");
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV036", "Vendor details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateVendor", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteVendor(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Vendor id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Vendor key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        _HCoreContext.AtAccounts.Remove(AccountDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Vendor removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Vendor details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteClass", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetVendor(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Vednor id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Vednor key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var VendorDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OVendor.Details.Response
                                       {
                                           VendorId = x.Id,
                                           VendorKey = x.Guid,
                                           Name = x.DisplayName,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           VendorCode = x.AccountCode,
                                           GstNumber = x.GstNumber,
                                           PanNumber = x.PanNumber,

                                           CpName = x.CpName,
                                           CpMobileNumber = x.CpMobileNumber,
                                           CpEmailAddress = x.CpEmailAddress,
                                           CpDesignation = x.Designation,

                                           Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                           CityId = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.CityId).FirstOrDefault(),
                                           CityKey = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.City.Guid).FirstOrDefault(),
                                           CityName = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.City.CityName).FirstOrDefault(),

                                           StateId = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.StateId).FirstOrDefault(),
                                           StateKey = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.State.Guid).FirstOrDefault(),
                                           StateName = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.State.StateName).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (VendorDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, VendorDetails, "ATCLDELETE", "Vednor details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Vednor details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVendor", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetVendors(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    var _Response = await GetVendorsDownload(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "VendorId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Vendor && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OVendor.List.Response
                                           {
                                               VendorId = x.Id,
                                               VendorKey = x.Guid,
                                               Name = x.DisplayName,
                                               MobileNumber = x.MobileNumber,
                                               EmailAddress = x.EmailAddress,
                                               VendorCode = x.AccountCode,
                                               GstNumber = x.GstNumber,
                                               PanNumber = x.PanNumber,

                                               CpName = x.CpName,
                                               CpMobileNumber = x.CpMobileNumber,
                                               CpEmailAddress = x.CpEmailAddress,
                                               CpDesignation = x.Designation,

                                               Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OVendor.List.Response> _Vendors = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Vendor && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OVendor.List.Response
                                       {
                                           VendorId = x.Id,
                                           VendorKey = x.Guid,
                                           Name = x.DisplayName,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           VendorCode = x.AccountCode,
                                           GstNumber = x.GstNumber,
                                           PanNumber = x.PanNumber,

                                           CpName = x.CpName,
                                           CpMobileNumber = x.CpMobileNumber,
                                           CpEmailAddress = x.CpEmailAddress,
                                           CpDesignation = x.Designation,

                                           Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Vendors, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Vednor list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVendors", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetVendorsDownload(OList.Request _Request)
        {
            try
            {
                _VendorDownload = new List<OVendor.VendorListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "VendorId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Vendor && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new
                                           {
                                               VendorId = x.Id,
                                               VendorKey = x.Guid,
                                               Name = x.DisplayName,
                                               MobileNumber = x.MobileNumber,
                                               EmailAddress = x.EmailAddress,
                                               VendorCode = x.AccountCode,
                                               GstNumber = x.GstNumber,
                                               PanNumber = x.PanNumber,

                                               CpName = x.CpName,
                                               CpMobileNumber = x.CpMobileNumber,
                                               CpEmailAddress = x.CpEmailAddress,
                                               CpDesignation = x.Designation,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Vendors = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Vendor && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new
                                       {
                                           VendorId = x.Id,
                                           VendorKey = x.Guid,
                                           Name = x.DisplayName,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           VendorCode = x.AccountCode,
                                           GstNumber = x.GstNumber,
                                           PanNumber = x.PanNumber,

                                           CpName = x.CpName,
                                           CpMobileNumber = x.CpMobileNumber,
                                           CpEmailAddress = x.CpEmailAddress,
                                           CpDesignation = x.Designation,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Vendors)
                    {
                        _VendorDownload.Add(new OVendor.VendorListDownload
                        {
                            Company_Name = _DataItem.Name,
                            Company_Code = _DataItem.VendorCode,
                            Email = _DataItem.EmailAddress,
                            Mobile_Number = _DataItem.MobileNumber,
                            GST_Number = _DataItem.GstNumber,
                            PAN_Number = _DataItem.PanNumber,
                            Contact_Person_Name = _DataItem.CpName,
                            Contact_Person_Email = _DataItem.CpEmailAddress,
                            Contact_Person_Mobile_Number = _DataItem.CpMobileNumber,
                            Registered_By = _DataItem.CreatedByDisplayName,
                            Registered_On = _DataItem.CreateDate.ToString()
                        });
                    }

                    return _VendorDownload;
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVendors", _Exception, _Request.UserReference);
            }
        }
    }
}
