﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace HCore.RKBS.Core.Framework.Masters
{
    internal class FrameworkAddress
    {
        HCoreContext? _HCoreContext;
        AtAddress? _AtAddress;

        internal async Task<OResponse> SaveLocation(OAddress.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Address))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Address required");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? ParentId = 0;
                if (!string.IsNullOrEmpty(_Request.ParentKey))
                {
                    ParentId = HCoreHelper.GetLocationId(_Request.ParentKey, _Request.UserReference);
                    if (ParentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? SubParentId = 0;
                if (!string.IsNullOrEmpty(_Request.SubParentKey))
                {
                    SubParentId = HCoreHelper.GetSubLocationId(_Request.SubParentKey, (long)ParentId, _Request.UserReference);
                    if (SubParentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? OrganizationId = 0;
                if (!string.IsNullOrEmpty(_Request.OrganizationKey))
                {
                    OrganizationId = HCoreHelper.GetOrganizationId(_Request.OrganizationKey, _Request.UserReference);
                    if (OrganizationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? AccountId = 0;
                if (!string.IsNullOrEmpty(_Request.AccountKey))
                {
                    AccountId = HCoreHelper.GetAccountId(_Request.AccountKey, _Request.UserReference);
                    if (AccountId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, (long)OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? SubDivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.SubDivisionKey))
                {
                    SubDivisionId = HCoreHelper.GetSubDivisionId(_Request.SubDivisionKey, (long)DivisionId, _Request.UserReference);
                    if (SubDivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                int? CountryId = 0;
                if (!string.IsNullOrEmpty(_Request.CountryKey))
                {
                    CountryId = HCoreHelper.GetCountryId(_Request.CountryKey, _Request.UserReference);
                    if (CountryId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? StateId = 0;
                if (!string.IsNullOrEmpty(_Request.StateKey))
                {
                    StateId = HCoreHelper.GetStateId(_Request.StateKey, (int)CountryId, _Request.UserReference);
                    if (StateId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                long? CityId = 0;
                if (!string.IsNullOrEmpty(_Request.CityKey))
                {
                    CityId = HCoreHelper.GetCityId(_Request.CityKey, (long)StateId, _Request.UserReference);
                    if (CityId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _AtAddress = new AtAddress();
                    _AtAddress.Guid = HCoreHelper.GenerateGuid();
                    if (ParentId > 0)
                    {
                        _AtAddress.ParentId = ParentId;
                    }
                    if (SubParentId > 0)
                    {
                        _AtAddress.SubParentId = SubParentId;
                    }
                    if (AccountId > 0)
                    {
                        _AtAddress.AccountId = (long)AccountId;
                    }
                    else
                    {
                        _AtAddress.AccountId = _Request.UserReference.AccountId;
                    }
                    if (OrganizationId > 0)
                    {
                        _AtAddress.OrganizationId = OrganizationId;
                    }
                    if (DivisionId > 0)
                    {
                        _AtAddress.DivisionId = DivisionId;
                    }
                    if (DepartmentId > 0)
                    {
                        _AtAddress.DepartmentId = DepartmentId;
                    }
                    if (CountryId > 0)
                    {
                        _AtAddress.CountryId = (int)CountryId;
                    }
                    if (StateId > 0)
                    {
                        _AtAddress.StateId = (long)StateId;
                    }
                    if (CityId > 0)
                    {
                        _AtAddress.CityId = (long)CityId;
                    }
                    if (StatusId > 0)
                    {
                        _AtAddress.StatusId = StatusId;
                    }
                    //_AtAddress.MobileNumber = _Request.LocationName;
                    //_AtAddress.EmailAddress = CategoryDetails.Id;
                    _AtAddress.Address = _Request.Address;
                    _AtAddress.CreatedById = _Request.UserReference.AccountId;
                    _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();

                    var _Response = new
                    {
                        LocationId = _AtAddress.Id,
                        LocationKey = _AtAddress.Guid,
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Location created successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateLocation(OAddress.Update.Request _Request)
        {
            try
            {
                if (_Request.AddressId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location id required");
                }
                if (string.IsNullOrEmpty(_Request.AddressKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location key required");
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var LocationDetails = await _HCoreContext.AtAddresses.Where(x => x.Guid == _Request.AddressKey && x.Id == _Request.AddressId).FirstOrDefaultAsync();
                    if (LocationDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Address))
                        {
                            LocationDetails.Address = _Request.Address;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            LocationDetails.MobileNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            LocationDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (StatusId > 0 && StatusId != LocationDetails.StatusId)
                        {
                            LocationDetails.StatusId = StatusId;
                        }
                        LocationDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        LocationDetails.ModifiedById = _Request.UserReference.AccountId;
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Location details updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Location details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteLocation(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var LocationDetails = await _HCoreContext.AtAddresses.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (LocationDetails != null)
                    {
                        bool _IsAsset = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.LocationId == LocationDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Location as it is assigned to some assets");
                        }
                        _HCoreContext.AtAddresses.Remove(LocationDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Location removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Location details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetLocation(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Location key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var LocationDetails = await _HCoreContext.AtAddresses.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OAddress.Details.Response
                                       {
                                           AddressId = x.Id,
                                           AddressKey = x.Guid,
                                           Address = x.Address,

                                           AccountId = x.AccountId,
                                           AccountKey = x.Account.Guid,
                                           AccountName = x.Account.DisplayName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           ParentId = x.ParentId,
                                           ParentKey = x.Parent.Guid,
                                           ParentName = x.Parent.Address,

                                           SubParentId = x.SubParentId,
                                           SubParentKey = x.SubParent.Guid,
                                           SubParentName = x.SubParent.Address,

                                           CountryId = x.CountryId,
                                           CountryKey = x.Country.Guid,
                                           CountryName = x.Country.CountryName,

                                           StateId = x.StateId,
                                           StateKey = x.State.Guid,
                                           StateName = x.State.StateName,

                                           CityId = x.CityId,
                                           CityKey = x.City.Guid,
                                           CityName = x.City.CityName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (LocationDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, LocationDetails, "ATCLDELETE", "Location details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Location details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetLocation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetLocations(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AddressId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAddresses
                                           .Select(x => new OAddress.List.Response
                                           {
                                               AddressId = x.Id,
                                               AddressKey = x.Guid,
                                               Address = x.Address,

                                               AccountId = x.AccountId,
                                               AccountKey = x.Account.Guid,
                                               AccountName = x.Account.DisplayName,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               ParentId = x.ParentId,
                                               ParentKey = x.Parent.Guid,
                                               ParentName = x.Parent.Address,

                                               SubParentId = x.SubParentId,
                                               SubParentKey = x.SubParent.Guid,
                                               SubParentName = x.SubParent.Address,

                                               CountryId = x.CountryId,
                                               CountryKey = x.Country.Guid,
                                               CountryName = x.Country.CountryName,

                                               StateId = x.StateId,
                                               StateKey = x.State.Guid,
                                               StateName = x.State.StateName,

                                               CityId = x.CityId,
                                               CityKey = x.City.Guid,
                                               CityName = x.City.CityName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).CountAsync();
                    }

                    List<OAddress.List.Response> _Categoryes = await _HCoreContext.AtAddresses
                                       .Select(x => new OAddress.List.Response
                                       {
                                           AddressId = x.Id,
                                           AddressKey = x.Guid,
                                           Address = x.Address,

                                           AccountId = x.AccountId,
                                           AccountKey = x.Account.Guid,
                                           AccountName = x.Account.DisplayName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           ParentId = x.ParentId,
                                           ParentKey = x.Parent.Guid,
                                           ParentName = x.Parent.Address,

                                           SubParentId = x.SubParentId,
                                           SubParentKey = x.SubParent.Guid,
                                           SubParentName = x.SubParent.Address,

                                           CountryId = x.CountryId,
                                           CountryKey = x.Country.Guid,
                                           CountryName = x.Country.CountryName,

                                           StateId = x.StateId,
                                           StateKey = x.State.Guid,
                                           StateName = x.State.StateName,

                                           CityId = x.CityId,
                                           CityKey = x.City.Guid,
                                           CityName = x.City.CityName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Location list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLocations", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetSubLocations(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AddressId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAddresses.Where(x => x.ParentId > 0)
                                           .Select(x => new OAddress.List.Response
                                           {
                                               AddressId = x.Id,
                                               AddressKey = x.Guid,
                                               Address = x.Address,

                                               AccountId = x.AccountId,
                                               AccountKey = x.Account.Guid,
                                               AccountName = x.Account.DisplayName,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               ParentId = x.ParentId,
                                               ParentKey = x.Parent.Guid,
                                               ParentName = x.Parent.Address,

                                               SubParentId = x.SubParentId,
                                               SubParentKey = x.SubParent.Guid,
                                               SubParentName = x.SubParent.Address,

                                               CountryId = x.CountryId,
                                               CountryKey = x.Country.Guid,
                                               CountryName = x.Country.CountryName,

                                               StateId = x.StateId,
                                               StateKey = x.State.Guid,
                                               StateName = x.State.StateName,

                                               CityId = x.CityId,
                                               CityKey = x.City.Guid,
                                               CityName = x.City.CityName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).CountAsync();
                    }

                    List<OAddress.List.Response> _Categoryes = await _HCoreContext.AtAddresses.Where(x => x.ParentId > 0)
                                       .Select(x => new OAddress.List.Response
                                       {
                                           AddressId = x.Id,
                                           AddressKey = x.Guid,
                                           Address = x.Address,

                                           AccountId = x.AccountId,
                                           AccountKey = x.Account.Guid,
                                           AccountName = x.Account.DisplayName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           ParentId = x.ParentId,
                                           ParentKey = x.Parent.Guid,
                                           ParentName = x.Parent.Address,

                                           SubParentId = x.SubParentId,
                                           SubParentKey = x.SubParent.Guid,
                                           SubParentName = x.SubParent.Address,

                                           CountryId = x.CountryId,
                                           CountryKey = x.Country.Guid,
                                           CountryName = x.Country.CountryName,

                                           StateId = x.StateId,
                                           StateKey = x.State.Guid,
                                           StateName = x.State.StateName,

                                           CityId = x.CityId,
                                           CityKey = x.City.Guid,
                                           CityName = x.City.CityName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Location list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSubLocations", _Exception, _Request.UserReference);
            }
        }
    }
}
