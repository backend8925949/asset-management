﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using static HCore.RKBS.Core.Object.Master.OClient;

namespace HCore.RKBS.Core.Framework.Masters
{
    internal class FrameworkEmployee
    {
        HCoreContext? _HCoreContext;
        Random? _Random;
        AtAccount? _AtAccount;
        AtAccountAuth? _AtAccountAuth;
        AtAddress? _AtAddress;
        AtLocationAccount? _AtLocationAccount;
        List<OEmployee.EmployeeListDownload>? _EmployeeDownload;

        internal async Task<OResponse> SaveEmployee(OEmployee.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Name required.");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Email required.");
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select division");
                }

                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid department");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select department");
                }

                long? LocationId = 0;
                if (!string.IsNullOrEmpty(_Request.LocationKey))
                {
                    LocationId = HCoreHelper.GetLocationId(_Request.LocationKey, _Request.UserReference);
                    if (LocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select location");
                }

                long? SubLocationId = 0;
                if (!string.IsNullOrEmpty(_Request.SubLocationKey))
                {
                    SubLocationId = HCoreHelper.GetSubLocationId(_Request.SubLocationKey, (long)LocationId, _Request.UserReference);
                    if (SubLocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid sub location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select sub location");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => x.EmailAddress == _Request.EmailAddress && x.AccountTypeId == UserAccountType.Employee).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV036", "Employee already exists.");
                    }
                    else
                    {
                        _Random = new Random();
                        string[] FullName = _Request.Name.Split(" ");
                        string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                        if (FullName.Length == 0 || FullName.Length == 1)
                        {
                            FirstName = _Request.Name;
                        }
                        else if (FullName.Length == 2)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[1];
                        }
                        else if (FullName.Length == 3)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[2];
                        }

                        long? SubDivisionId = await _HCoreContext.AtDivisions.Where(x => x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefaultAsync();

                        _AtAccount = new AtAccount();
                        _AtAccount.Guid = HCoreHelper.GenerateGuid();
                        _AtAccount.AccountTypeId = UserAccountType.Employee;
                        _AtAccount.MobileNumber = _Request.MobileNumber;
                        _AtAccount.OrganizationId = _Request.UserReference.OrganizationId;
                        _AtAccount.DivisionId = DivisionId;
                        _AtAccount.DepartmentId = DepartmentId;
                        _AtAccount.DisplayName = _Request.Name;
                        _AtAccount.FirstName = FirstName;
                        _AtAccount.LastName = LastName;
                        _AtAccount.EmailAddress = _Request.EmailAddress;
                        _AtAccount.Designation = _Request.Designation;
                        _AtAccount.ContactNumber = _AtAccount.MobileNumber;
                        _AtAccount.AccountCode = _Request.AccountCode;
                        _AtAccount.CreatedById = _Request.UserReference.AccountId;
                        _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                        _AtAccount.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.AtAccounts.Add(_AtAccount);
                        await _HCoreContext.SaveChangesAsync();

                        _AtAccountAuth = new AtAccountAuth();
                        _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _AtAccountAuth.UserId = _AtAccount.Id;
                        _AtAccountAuth.UserName = _Request.EmailAddress;
                        _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                        _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                        _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                        _AtAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                        await _HCoreContext.SaveChangesAsync();

                        _AtLocationAccount = new AtLocationAccount();
                        _AtLocationAccount.Guid = HCoreHelper.GenerateGuid();
                        _AtLocationAccount.AccountId = _AtAccount.Id;
                        _AtLocationAccount.LocationId = LocationId;
                        _AtLocationAccount.SubLocationId = SubLocationId;
                        _AtLocationAccount.CreatedById = _Request.UserReference.AccountId;
                        _AtLocationAccount.CreatedDate = HCoreHelper.GetDateTime();
                        _AtLocationAccount.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.AtLocationAccounts.AddAsync(_AtLocationAccount);
                        await _HCoreContext.SaveChangesAsync();

                        if(_Request.AddressDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.AddressDetails.Address))
                            {
                                long? StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                long? CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();

                                _AtAddress = new AtAddress();
                                _AtAddress.Guid = HCoreHelper.GenerateGuid();
                                _AtAddress.AccountId = _AtAccount.Id;
                                _AtAddress.Address = _Request.AddressDetails.Address;
                                _AtAddress.CountryId = 1;
                                _AtAddress.StateId = (long)StateId;
                                _AtAddress.CityId = (long)CityId;
                                _AtAddress.StatusId = HelperStatus.Default.Active;
                                _AtAddress.CreatedById = _Request.UserReference.AccountId;
                                _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                                await _HCoreContext.SaveChangesAsync();
                            }
                        }

                        await _HCoreContext.DisposeAsync();

                        var _Response = new
                        {
                            EmployeeId = _AtAccount.Id,
                            EmployeeKey = _AtAccount.Guid,
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "Employee registered successfully.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveEmployee", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateEmployee(OEmployee.Update.Request _Request)
        {
            try
            {
                if (_Request.EmployeeId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Employee id required.");
                }
                if (string.IsNullOrEmpty(_Request.EmployeeKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "USR007", "Employee key required.");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = 0;
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select division");
                }

                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid department");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select department");
                }

                long? LocationId = 0;
                if (!string.IsNullOrEmpty(_Request.LocationKey))
                {
                    LocationId = HCoreHelper.GetLocationId(_Request.LocationKey, _Request.UserReference);
                    if (LocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select location");
                }

                long? SubLocationId = 0;
                if (!string.IsNullOrEmpty(_Request.SubLocationKey))
                {
                    SubLocationId = HCoreHelper.GetSubLocationId(_Request.SubLocationKey, (long)LocationId, _Request.UserReference);
                    if (SubLocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid sub location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select sub location");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.EmployeeId && x.Guid == _Request.EmployeeKey).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        bool _IsExists = await _HCoreContext.AtAccounts.AnyAsync(x => x.Guid != AccountDetails.Guid && x.EmailAddress == _Request.EmailAddress && x.AccountTypeId == UserAccountType.Employee);
                        if (_IsExists)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV036", "Employee already exists.");
                        }

                        string[] FullName = _Request.Name.Split(" ");
                        string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                        if (FullName.Length == 0 || FullName.Length == 1)
                        {
                            FirstName = _Request.Name;
                        }
                        else if (FullName.Length == 2)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[1];
                        }
                        else if (FullName.Length == 3)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[2];
                        }

                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && AccountDetails.MobileNumber != _Request.MobileNumber)
                        {
                            AccountDetails.MobileNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name) && AccountDetails.DisplayName != _Request.Name)
                        {
                            AccountDetails.DisplayName = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(FirstName) && AccountDetails.FirstName != FirstName)
                        {
                            AccountDetails.FirstName = FirstName;
                        }
                        if (!string.IsNullOrEmpty(LastName) && AccountDetails.LastName != LastName)
                        {
                            AccountDetails.LastName = LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                        {
                            AccountDetails.EmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber) && AccountDetails.ContactNumber != _Request.MobileNumber)
                        {
                            AccountDetails.ContactNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Designation) && AccountDetails.Designation != _Request.Designation)
                        {
                            AccountDetails.Designation = _Request.Designation;
                        }
                        if (!string.IsNullOrEmpty(_Request.AccountCode) && AccountDetails.AccountCode != _Request.AccountCode)
                        {
                            AccountDetails.AccountCode = _Request.AccountCode;
                        }
                        if (DivisionId > 0 && AccountDetails.DivisionId != DivisionId)
                        {
                            AccountDetails.DivisionId = DivisionId;
                        }
                        if (DepartmentId > 0 && AccountDetails.DepartmentId != DepartmentId)
                        {
                            AccountDetails.DepartmentId = DepartmentId;
                        }
                        if (StatusId > 0 && AccountDetails.StatusId != StatusId)
                        {
                            AccountDetails.StatusId = StatusId;
                        }
                        AccountDetails.ModifiedById = _Request.UserReference.AccountId;
                        AccountDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.SaveChangesAsync();

                        if(_Request.AddressDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.AddressDetails.Address))
                            {
                                long? StateId = 0;
                                long? CityId = 0;

                                var AddressDetails = await _HCoreContext.AtAddresses.Where(x => x.AccountId == AccountDetails.Id).FirstOrDefaultAsync();
                                if (AddressDetails != null)
                                {
                                    if (!string.IsNullOrEmpty(_Request.AddressDetails.StateKey))
                                    {
                                        StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    }
                                    if (!string.IsNullOrEmpty(_Request.AddressDetails.CityKey))
                                    {
                                        CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    }

                                    if (!string.IsNullOrEmpty(_Request.AddressDetails.Address) && AddressDetails.Address != _Request.AddressDetails.Address)
                                    {
                                        AddressDetails.Address = _Request.AddressDetails.Address;
                                    }
                                    if (StateId > 0 && AddressDetails.StateId != (long)StateId)
                                    {
                                        AddressDetails.StateId = (long)StateId;
                                    }
                                    if (CityId > 0 && AddressDetails.CityId != (long)CityId)
                                    {
                                        AddressDetails.CityId = (long)CityId;
                                    }
                                    if (StatusId > 0 && AddressDetails.StatusId != StatusId)
                                    {
                                        AddressDetails.StatusId = StatusId;
                                    }
                                    AddressDetails.ModifiedById = _Request.UserReference.AccountId;
                                    AddressDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                else
                                {
                                    StateId = await _HCoreContext.AtStates.Where(x => x.Guid == _Request.AddressDetails.StateKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    CityId = await _HCoreContext.AtCities.Where(x => x.Guid == _Request.AddressDetails.CityKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    _AtAddress = new AtAddress();
                                    _AtAddress.Guid = HCoreHelper.GenerateGuid();
                                    _AtAddress.AccountId = AccountDetails.Id;
                                    _AtAddress.Address = _Request.AddressDetails.Address;
                                    _AtAddress.CountryId = 1;
                                    _AtAddress.StateId = (long)StateId;
                                    _AtAddress.CityId = (long)CityId;
                                    _AtAddress.StatusId = StatusId;
                                    _AtAddress.CreatedById = _Request.UserReference.AccountId;
                                    _AtAddress.CreatedDate = HCoreHelper.GetDateTime();
                                    await _HCoreContext.AtAddresses.AddAsync(_AtAddress);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                            }
                        }

                        if(LocationId > 0 && SubLocationId > 0)
                        {
                            var LocationDetails = await _HCoreContext.AtLocationAccounts.Where(x => x.AccountId == AccountDetails.Id).FirstOrDefaultAsync();
                            if (LocationDetails != null)
                            {
                                if (LocationId > 0 && LocationId != LocationDetails.LocationId)
                                {
                                    LocationDetails.LocationId = LocationId;
                                }
                                if (SubLocationId > 0 && SubLocationId != LocationDetails.SubLocationId)
                                {
                                    LocationDetails.SubLocationId = SubLocationId;
                                }
                                if (StatusId > 0 && LocationDetails.StatusId != StatusId)
                                {
                                    LocationDetails.StatusId = StatusId;
                                }
                                LocationDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                LocationDetails.ModifiedById = _Request.UserReference.AccountId;
                                await _HCoreContext.SaveChangesAsync();
                            }
                            else
                            {
                                _AtLocationAccount = new AtLocationAccount();
                                _AtLocationAccount.Guid = HCoreHelper.GenerateGuid();
                                _AtLocationAccount.AccountId = AccountDetails.Id;
                                _AtLocationAccount.LocationId = LocationId;
                                _AtLocationAccount.SubLocationId = SubLocationId;
                                _AtLocationAccount.CreatedById = _Request.UserReference.AccountId;
                                _AtLocationAccount.CreatedDate = HCoreHelper.GetDateTime();
                                _AtLocationAccount.StatusId = StatusId;
                                await _HCoreContext.AtLocationAccounts.AddAsync(_AtLocationAccount);
                                await _HCoreContext.SaveChangesAsync();
                            }
                        }

                        await _HCoreContext.DisposeAsync();

                        var _Response = new
                        {
                            EmployeeId = AccountDetails.Id,
                            EmployeeKey = AccountDetails.Guid,
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCV036", "Employee details updated successfully.");
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV036", "Employee details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateEmployee", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteEmployee(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Employee id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Employee key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        _HCoreContext.AtAccounts.Remove(AccountDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Employee removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Employee details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteClass", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetEmployee(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Employee id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Employee key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OEmployee.Details.Response
                                       {
                                           EmployeeId = x.Id,
                                           EmployeeKey = x.Guid,
                                           Name = x.DisplayName,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           AccountCode = x.AccountCode,
                                           Designation = x.Designation,

                                           Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                           CityId = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.CityId).FirstOrDefault(),
                                           CityKey = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.City.Guid).FirstOrDefault(),
                                           CityName = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.City.CityName).FirstOrDefault(),

                                           StateId = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.StateId).FirstOrDefault(),
                                           StateKey = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.State.Guid).FirstOrDefault(),
                                           StateName = x.AtAddressAccounts.Where(y => y.AccountId == x.Id).Select(a => a.State.StateName).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (EmployeeDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, EmployeeDetails, "ATCLDELETE", "Employee details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Employee details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetEmployee", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetEmployees(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    var _Response = await GetEmployeesDownload(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "EmployeeId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OEmployee.List.Response
                                           {
                                               EmployeeId = x.Id,
                                               EmployeeKey = x.Guid,
                                               Name = x.DisplayName,
                                               MobileNumber = x.MobileNumber,
                                               EmailAddress = x.EmailAddress,
                                               AccountCode = x.AccountCode,
                                               Designation = x.Designation,

                                               Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OEmployee.List.Response> _Employees = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OEmployee.List.Response
                                       {
                                           EmployeeId = x.Id,
                                           EmployeeKey = x.Guid,
                                           Name = x.DisplayName,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           AccountCode = x.AccountCode,
                                           Designation = x.Designation,

                                           Address = x.AtAddressAccounts.Where(a => a.AccountId == x.Id).Select(m => m.Address).FirstOrDefault(),

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Employees, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Employee list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetEmployees", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetEmployeesDownload(OList.Request _Request)
        {
            try
            {
                _EmployeeDownload = new List<OEmployee.EmployeeListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "EmployeeId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new
                                           {
                                               EmployeeId = x.Id,
                                               EmployeeKey = x.Guid,
                                               Name = x.DisplayName,
                                               MobileNumber = x.MobileNumber,
                                               EmailAddress = x.EmailAddress,
                                               AccountCode = x.AccountCode,
                                               Designation = x.Designation,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Employees = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new
                                       {
                                           EmployeeId = x.Id,
                                           EmployeeKey = x.Guid,
                                           Name = x.DisplayName,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           AccountCode = x.AccountCode,
                                           Designation = x.Designation,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                           
                                           CreateDate = x.CreatedDate,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Employees)
                    {
                        int? _IssuedQuantity = await _HCoreContext.AtAssetIssueDetails.Where(x => x.EmployeeId == _DataItem.EmployeeId).SumAsync(x => x.Quantity);

                        _EmployeeDownload.Add(new OEmployee.EmployeeListDownload
                        {
                            Employee_Name = _DataItem.Name,
                            Employee_Code = _DataItem.AccountCode,
                            Email = _DataItem.EmailAddress,
                            Mobile_Number = _DataItem.MobileNumber,
                            Issued_Assets = _IssuedQuantity,
                            Designation = _DataItem.Designation,
                            Division = _DataItem.DivisionName,
                            Department = _DataItem.DepartmentName,
                            Location = _DataItem.LocationName,
                            Sub_Location = _DataItem.SubLocationName,
                            Status = _DataItem.StatusName,
                            Registered_By = _DataItem.CreatedByDisplayName,
                            Registered_On = _DataItem.CreateDate.ToString()
                        });
                    }

                    return _EmployeeDownload;
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetEmployees", _Exception, _Request.UserReference);
            }
        }
    }
}
