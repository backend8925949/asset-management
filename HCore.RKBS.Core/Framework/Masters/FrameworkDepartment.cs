﻿

using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System.Security.Principal;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Masters
{
    public class FrameworkDepartment
    {
        HCoreContext? _HCoreContext;
        AtDepartment? _AtDepartment;
        AtLocationAccount? _AtLocationAccount;

        internal async Task<OResponse> SaveDepartment(ODepartment.Save.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.DepartmentName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department name required");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select division");
                }

                long? LocationId = 0;
                if (!string.IsNullOrEmpty(_Request.LocationKey))
                {
                    LocationId = HCoreHelper.GetLocationId(_Request.LocationKey, _Request.UserReference);
                    if (LocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select location");
                }

                long? SubLocationId = 0;
                if (!string.IsNullOrEmpty(_Request.SubLocationKey))
                {
                    SubLocationId = HCoreHelper.GetSubLocationId(_Request.SubLocationKey, (long)LocationId, _Request.UserReference);
                    if (SubLocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid sub location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select sub location");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    long? SubDivisionId = await _HCoreContext.AtDivisions.Where(x => x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefaultAsync();
                    bool _IsExists = await _HCoreContext.AtDepartments.AnyAsync(x => x.DivisionId == DivisionId && x.DepartmentName == _Request.DepartmentName);
                    if(_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATEXSTS", "Department details already present in the selected division");
                    }

                    _AtDepartment = new AtDepartment();
                    _AtDepartment.Guid = "DEPT-" + HCoreHelper.GenerateGuid();
                    _AtDepartment.OrganizationId = _Request.UserReference.OrganizationId;
                    _AtDepartment.DivisionId = DivisionId;
                    if (StatusId > 0)
                    {
                        _AtDepartment.StatusId = StatusId;
                    }
                    else
                    {
                        _AtDepartment.StatusId = HelperStatus.Default.Active;
                    }
                    _AtDepartment.DepartmentName = _Request.DepartmentName;
                    _AtDepartment.CreatedById = _Request.UserReference.AccountId;
                    _AtDepartment.CreatedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.AtDepartments.AddAsync(_AtDepartment);
                    await _HCoreContext.SaveChangesAsync();

                    _AtLocationAccount = new AtLocationAccount();
                    _AtLocationAccount.Guid = HCoreHelper.GenerateGuid();
                    _AtLocationAccount.DepartmentId = _AtDepartment.Id;
                    _AtLocationAccount.LocationId = LocationId;
                    _AtLocationAccount.SubLocationId = SubLocationId;
                    _AtLocationAccount.CreatedById = _Request.UserReference.AccountId;
                    _AtLocationAccount.CreatedDate = HCoreHelper.GetDateTime();
                    _AtLocationAccount.StatusId = HelperStatus.Default.Active;
                    await _HCoreContext.AtLocationAccounts.AddAsync(_AtLocationAccount);
                    await _HCoreContext.SaveChangesAsync();

                    await _HCoreContext.DisposeAsync();

                    var _Response = new
                    {
                        DepartmentId = _AtDepartment.Id,
                        DepartmentKey = _AtDepartment.Guid,
                    };

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATCLSAVE", "Department created successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDepartment", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateDepartment(ODepartment.Update.Request _Request)
        {
            try
            {
                if (_Request.DepartmentId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department id required");
                }
                if (string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid status");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATSTS", "Please select status");
                }

                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid Division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select division");
                }

                long? LocationId = 0;
                if (!string.IsNullOrEmpty(_Request.LocationKey))
                {
                    LocationId = HCoreHelper.GetLocationId(_Request.LocationKey, _Request.UserReference);
                    if (LocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select location");
                }

                long? SubLocationId = 0;
                if (!string.IsNullOrEmpty(_Request.SubLocationKey))
                {
                    SubLocationId = HCoreHelper.GetSubLocationId(_Request.SubLocationKey, (long)LocationId, _Request.UserReference);
                    if (SubLocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL002", "Invalid sub location");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATL002", "Please select sub location");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepartmentDetails = await _HCoreContext.AtDepartments.Where(x => x.Guid == _Request.DepartmentKey && x.Id == _Request.DepartmentId).FirstOrDefaultAsync();
                    if (DepartmentDetails != null)
                    {
                        long? SubDivisionId = await _HCoreContext.AtDivisions.Where(x => x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefaultAsync();
                        bool _IsExists = await _HCoreContext.AtDepartments.AnyAsync(x => x.DepartmentName == _Request.DepartmentName && x.Guid != DepartmentDetails.Guid && x.DivisionId == DivisionId);
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATEXSTS", "Department details already present in the selected division");
                        }

                        if (!string.IsNullOrEmpty(_Request.DepartmentName) && DepartmentDetails.DepartmentName != _Request.DepartmentName)
                        {
                            DepartmentDetails.DepartmentName = _Request.DepartmentName;
                        }
                        if (DivisionId > 0 && DivisionId != DepartmentDetails.DivisionId)
                        {
                            DepartmentDetails.DivisionId = DivisionId;
                        }
                        if (StatusId > 0 && StatusId != DepartmentDetails.StatusId)
                        {
                            DepartmentDetails.StatusId = StatusId;
                        }
                        DepartmentDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        DepartmentDetails.ModifiedById = _Request.UserReference.AccountId;

                        if (LocationId > 0 && SubLocationId > 0)
                        {
                            var LocationDetails = await _HCoreContext.AtLocationAccounts.Where(x => x.DepartmentId == DepartmentDetails.Id).FirstOrDefaultAsync();
                            if (LocationDetails != null)
                            {
                                if (LocationId > 0 && LocationId != LocationDetails.LocationId)
                                {
                                    LocationDetails.LocationId = LocationId;
                                }
                                if (SubLocationId > 0 && SubLocationId != LocationDetails.SubLocationId)
                                {
                                    LocationDetails.SubLocationId = SubLocationId;
                                }
                                LocationDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                LocationDetails.ModifiedById = _Request.UserReference.AccountId;
                            }
                            else
                            {
                                _AtLocationAccount = new AtLocationAccount();
                                _AtLocationAccount.Guid = HCoreHelper.GenerateGuid();
                                _AtLocationAccount.DepartmentId = DepartmentDetails.Id;
                                _AtLocationAccount.LocationId = LocationId;
                                _AtLocationAccount.SubLocationId = SubLocationId;
                                _AtLocationAccount.CreatedById = _Request.UserReference.AccountId;
                                _AtLocationAccount.CreatedDate = HCoreHelper.GetDateTime();
                                _AtLocationAccount.StatusId = HelperStatus.Default.Active;
                                await _HCoreContext.AtLocationAccounts.AddAsync(_AtLocationAccount);
                            }
                        }

                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLUPDATE", "Department details updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Department details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateDepartment", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DeleteDepartment(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepartmentDetails = await _HCoreContext.AtDepartments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (DepartmentDetails != null)
                    {
                        bool _IsAccount = await _HCoreContext.AtAccounts.AnyAsync(x => x.DepartmentId == DepartmentDetails.Id);
                        if (_IsAccount)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Department as it has some accounts assigned");
                        }
                        bool _IsAddress = await _HCoreContext.AtAddresses.AnyAsync(x => x.DepartmentId == DepartmentDetails.Id);
                        if (_IsAddress)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Department as it has some addresses present on the system");
                        }
                        bool _IsAsset = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.DepartmentId == DepartmentDetails.Id);
                        if (_IsAsset)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "You cannot delete this Department as it has some assets assigned");
                        }
                        _HCoreContext.AtDepartments.Remove(DepartmentDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATCLDELETE", "Department removed successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Department details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteDepartment", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDepartment(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Department key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepartmentDetails = await _HCoreContext.AtDepartments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new ODepartment.Details.Response
                                       {
                                           DepartmentId = x.Id,
                                           DepartmentKey = x.Guid,
                                           DepartmentName = x.DepartmentName,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (DepartmentDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, DepartmentDetails, "ATCLDELETE", "Department details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Department details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDepartment", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDepartments(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "DepartmentId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtDepartments.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new ODepartment.List.Response
                                           {
                                               DepartmentId = x.Id,
                                               DepartmentKey = x.Guid,
                                               DepartmentName = x.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<ODepartment.List.Response> _Categoryes = await _HCoreContext.AtDepartments.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new ODepartment.List.Response
                                       {
                                           DepartmentId = x.Id,
                                           DepartmentKey = x.Guid,
                                           DepartmentName = x.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "ATCLDELETE", "Department list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDepartments", _Exception, _Request.UserReference);
            }
        }
    }
}
