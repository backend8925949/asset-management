﻿using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.App;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using HCore.RKBS.Core.Object.Assets;

namespace HCore.RKBS.Core.Framework.App
{
    public class FrameworkAssets
    {
        HCoreContext? _HCoreContext;
        List<OAssets.Department>? _Departments;
        List<OAssets.Location>? _Locations;
        List<OAssets.SubLocation>? _SubLocations;
        List<OAssets.UnIssuedAssets>? _UnIssuedAssets;

        internal async Task<OResponse> GetDivisions(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "DivisionId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "DivisionId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtDivisions.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == HelperStatus.Default.Active)
                                           .Select(x => new OAssets.Division
                                           {
                                               DivisionId = x.Id,
                                               DivisionKey = x.Guid,
                                               DivisionName = x.DivisionName,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAssets.Division> _Divisions = await _HCoreContext.AtDivisions.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == HelperStatus.Default.Active)
                                       .Select(x => new OAssets.Division
                                       {
                                           DivisionId = x.Id,
                                           DivisionKey = x.Guid,
                                           DivisionName = x.DivisionName
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .ToListAsync();
                    if (_Divisions.Count > 1)
                    {
                        foreach (var Division in _Divisions)
                        {
                            _Departments = new List<OAssets.Department>();
                            _Locations = new List<OAssets.Location>();
                            _SubLocations = new List<OAssets.SubLocation>();

                            _Departments = await _HCoreContext.AtDepartments.Where(x => x.DivisionId == Division.DivisionId && x.StatusId == HelperStatus.Default.Active)
                                           .Select(x => new OAssets.Department
                                           {
                                               DivisionId = Division.DivisionId,
                                               DivisionKey = Division.DivisionKey,
                                               DepartmentId = x.Id,
                                               DepartmentKey = x.Guid,
                                               DepartmentName = x.DepartmentName,
                                               LocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                               SubLocationId = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtLocationAccounts.Where(a => a.DepartmentId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                           }).ToListAsync();

                            Division.Departments = _Departments;

                            _Locations = await _HCoreContext.AtLocations.Where(x => x.DivisionId == Division.DivisionId && (x.ParentId == null || x.ParentId < 1) && x.StatusId == HelperStatus.Default.Active)
                                           .Select(x => new OAssets.Location
                                           {
                                               DivisionId = Division.DivisionId,
                                               DivisionKey = Division.DivisionKey,
                                               LocationId = x.Id,
                                               LocationKey = x.Guid,
                                               LocationName = x.LocationName,
                                           }).ToListAsync();
                            if (_Locations.Count > 0)
                            {
                                foreach (var Location in _Locations)
                                {
                                    _SubLocations = await _HCoreContext.AtLocations.Where(x => x.ParentId == Location.LocationId && x.StatusId == HelperStatus.Default.Active)
                                                   .Select(x => new OAssets.SubLocation
                                                   {
                                                       DivisionId = Division.DivisionId,
                                                       DivisionKey = Division.DivisionKey,
                                                       LocationId = Location.LocationId,
                                                       LocationKey = Location.LocationKey,
                                                       SubLocationId = x.Id,
                                                       SubLocationKey = x.Guid,
                                                       SubLocationName = x.LocationName
                                                   }).ToListAsync();

                                    Location.SubLocations = _SubLocations;
                                }
                            }

                            Division.Locations = _Locations;
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Divisions, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDivisions", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetIssuedAssets(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetIssueDetails.Where(x => x.IsTransfered == 0 && (x.Asset.StatusId != AssetStatus.Disposed || x.Asset.StatusId != AssetStatus.Expired) && x.OrganizationId == _Request.UserReference.OrganizationId && x.Asset.AssetTypeId == AssetType.WithBarcode)
                                           .Select(x => new OAssets.IssuedAssets
                                           {
                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNumber = x.Asset.AssetSerialNumber,
                                               Remark = x.Remark,

                                               AssetTypeId = x.Asset.AssetTypeId,
                                               AssetTypeCode = x.Asset.AssetType.SystemName,
                                               AssetTypeName = x.Asset.AssetType.Name,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.LocationId,
                                               LocationKey = x.Location.Guid,
                                               LocationName = x.Location.LocationName,

                                               SubLocationId = x.SubLocationId,
                                               SubLocationKey = x.SubLocation.Guid,
                                               SubLocationName = x.SubLocation.LocationName,

                                               EmployeeId = x.EmployeeId,
                                               EmployeeKey = x.Employee.Guid,
                                               EmployeeName = x.Employee.DisplayName,

                                               AssetClassId = x.Asset.AssetClassId,
                                               AssetClassKey = x.Asset.AssetClass.Guid,
                                               AssetClassName = x.Asset.AssetClass.ClassName,

                                               AssetCategoryId = x.Asset.AssetCategoryId,
                                               AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                               AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.Asset.AssetSubCategoryId,
                                               AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,
                                           }).CountAsync();
                    }

                    List<OAssets.IssuedAssets> _IssuedAssets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.IsTransfered == 0 && (x.Asset.StatusId != AssetStatus.Disposed || x.Asset.StatusId != AssetStatus.Expired) && x.OrganizationId == _Request.UserReference.OrganizationId && x.Asset.AssetTypeId == AssetType.WithBarcode)
                                       .Select(x => new OAssets.IssuedAssets
                                       {
                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNumber = x.Asset.AssetSerialNumber,
                                           Remark = x.Remark,

                                           AssetTypeId = x.Asset.AssetTypeId,
                                           AssetTypeCode = x.Asset.AssetType.SystemName,
                                           AssetTypeName = x.Asset.AssetType.Name,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.LocationId,
                                           LocationKey = x.Location.Guid,
                                           LocationName = x.Location.LocationName,

                                           SubLocationId = x.SubLocationId,
                                           SubLocationKey = x.SubLocation.Guid,
                                           SubLocationName = x.SubLocation.LocationName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,

                                           AssetClassId = x.Asset.AssetClassId,
                                           AssetClassKey = x.Asset.AssetClass.Guid,
                                           AssetClassName = x.Asset.AssetClass.ClassName,

                                           AssetCategoryId = x.Asset.AssetCategoryId,
                                           AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                           AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.Asset.AssetSubCategoryId,
                                           AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _IssuedAssets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetIssuedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetUnIssuedAssets(OList.Request _Request)
        {
            try
            {
                _UnIssuedAssets = new List<OAssets.UnIssuedAssets>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && (x.StatusId != AssetStatus.Disposed || x.StatusId != AssetStatus.Expired))
                                  .Select(x => new
                                  {
                                      AssetId = x.Id,
                                      AssetKey = x.Guid,
                                      AssetName = x.AssetName,
                                      AssetBarcode = x.AssetBarcode,
                                      TotalQuantity = x.TotalQuantity,
                                      AssetSerialNumber = x.AssetSerialNumber,

                                      AssetTypeId = x.AssetTypeId,
                                      AssetTypeCode = x.AssetType.SystemName,
                                      AssetTypeName = x.AssetType.Name,

                                      AssetClassId = x.AssetClassId,
                                      AssetClassKey = x.AssetClass.Guid,
                                      AssetClassName = x.AssetClass.ClassName,

                                      AssetCategoryId = x.AssetCategoryId,
                                      AssetCategoryKey = x.AssetCategory.Guid,
                                      AssetCategoryName = x.AssetCategory.CategoryName,

                                      AssetSubCategoryId = x.AssetSubCategoryId,
                                      AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                      AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,
                                  }).OrderBy(_Request.SortExpression)
                                  .Where(_Request.SearchCondition)
                                  .ToListAsync();

                    if (_Assets.Count > 0)
                    {
                        foreach(var Asset in _Assets)
                        {
                            if(Asset.AssetTypeId == AssetType.WithBarcode)
                            {
                                bool _IsIssued = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.AssetId == Asset.AssetId);
                                if(!_IsIssued)
                                {
                                    _UnIssuedAssets.Add(new OAssets.UnIssuedAssets
                                    {
                                        AssetId = Asset.AssetId,
                                        AssetKey = Asset.AssetKey,
                                        AssetName = Asset.AssetName,
                                        AssetBarcode = Asset.AssetBarcode,
                                        AssetSerialNumber = Asset.AssetSerialNumber,

                                        AssetTypeId = Asset.AssetTypeId,
                                        AssetTypeCode = Asset.AssetTypeCode,
                                        AssetTypeName = Asset.AssetTypeName,

                                        AssetClassId = Asset.AssetClassId,
                                        AssetClassKey = Asset.AssetClassKey,
                                        AssetClassName = Asset.AssetClassName,

                                        AssetCategoryId = Asset.AssetCategoryId,
                                        AssetCategoryKey = Asset.AssetCategoryKey,
                                        AssetCategoryName = Asset.AssetCategoryName,

                                        AssetSubCategoryId = Asset.AssetSubCategoryId,
                                        AssetSubCategoryKey = Asset.AssetSubCategoryKey,
                                        AssetSubCategoryName = Asset.AssetSubCategoryName,
                                    });
                                }
                            }
                            if (Asset.AssetTypeId == AssetType.WithoutBarcode)
                            {
                                int? IssuedQuantity = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId).Select(x => x.Quantity).FirstOrDefaultAsync();
                                int? RemainingQuantity = Asset.TotalQuantity - IssuedQuantity;
                                if(RemainingQuantity > 0)
                                {
                                    _UnIssuedAssets.Add(new OAssets.UnIssuedAssets
                                    {
                                        AssetId = Asset.AssetId,
                                        AssetKey = Asset.AssetKey,
                                        AssetName = Asset.AssetName,
                                        AssetBarcode = Asset.AssetBarcode,
                                        AssetSerialNumber = Asset.AssetSerialNumber,
                                        Quantity = RemainingQuantity,

                                        AssetTypeId = Asset.AssetTypeId,
                                        AssetTypeCode = Asset.AssetTypeCode,
                                        AssetTypeName = Asset.AssetTypeName,

                                        AssetClassId = Asset.AssetClassId,
                                        AssetClassKey = Asset.AssetClassKey,
                                        AssetClassName = Asset.AssetClassName,

                                        AssetCategoryId = Asset.AssetCategoryId,
                                        AssetCategoryKey = Asset.AssetCategoryKey,
                                        AssetCategoryName = Asset.AssetCategoryName,

                                        AssetSubCategoryId = Asset.AssetSubCategoryId,
                                        AssetSubCategoryKey = Asset.AssetSubCategoryKey,
                                        AssetSubCategoryName = Asset.AssetSubCategoryName,
                                    });
                                }
                            }
                        }

                        if(_UnIssuedAssets.Count > 0)
                        {
                            if(_Request.RefreshCount)
                            {
                                _Request.TotalRecords = _UnIssuedAssets.Count;
                            }
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _UnIssuedAssets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetUnIssuedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssets(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && (x.StatusId != AssetStatus.Disposed || x.StatusId != AssetStatus.Expired))
                                           .Select(x => new OAssets.Assets
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNumber = x.AssetSerialNumber,
                                               EmployeeId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.EmployeeId).FirstOrDefault(),
                                               EmployeeKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.Guid).FirstOrDefault(),
                                               EmployeeName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.DisplayName).FirstOrDefault(),
                                               EmployeeCode = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.AccountCode).FirstOrDefault(),
                                               EmployeeEmail = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.EmailAddress).FirstOrDefault(),
                                               DepartmentId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.DepartmentId).FirstOrDefault(),
                                               DepartmentKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.Guid).FirstOrDefault(),
                                               DepartmentName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.DepartmentName).FirstOrDefault(),
                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),
                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),
                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),
                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).CountAsync();
                    }

                    List<OAssets.Assets> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && (x.StatusId != AssetStatus.Disposed || x.StatusId != AssetStatus.Expired))
                                       .Select(x => new OAssets.Assets
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNumber = x.AssetSerialNumber,
                                           EmployeeId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.EmployeeId).FirstOrDefault(),
                                           EmployeeKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.Guid).FirstOrDefault(),
                                           EmployeeName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.DisplayName).FirstOrDefault(),
                                           EmployeeCode = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.AccountCode).FirstOrDefault(),
                                           EmployeeEmail = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Employee.EmailAddress).FirstOrDefault(),
                                           DepartmentId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.DepartmentId).FirstOrDefault(),
                                           DepartmentKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.Guid).FirstOrDefault(),
                                           DepartmentName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.DepartmentName).FirstOrDefault(),
                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),
                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),
                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),
                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetEmployees(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "EmployeeId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "EmployeeId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == HelperStatus.Default.Active)
                                           .Select(x => new OAssets.Employee
                                           {
                                               EmployeeId = x.Id,
                                               EmployeeKey = x.Guid,
                                               EmployeeName = x.DisplayName,
                                               EmployeeCode = x.AccountCode,
                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,
                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,
                                               LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                               SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                           }).CountAsync();
                    }

                    List<OAssets.Employee> _Employees = await _HCoreContext.AtAccounts.Where(x => x.AccountTypeId == UserAccountType.Employee && x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == HelperStatus.Default.Active)
                                       .Select(x => new OAssets.Employee
                                       {
                                           EmployeeId = x.Id,
                                           EmployeeKey = x.Guid,
                                           EmployeeName = x.DisplayName,
                                           EmployeeCode = x.AccountCode,
                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,
                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,
                                           LocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),
                                           SubLocationId = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtLocationAccountAccounts.Where(a => a.AccountId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Employees, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetEmployees", _Exception, _Request.UserReference);
            }
        }
    }
}
