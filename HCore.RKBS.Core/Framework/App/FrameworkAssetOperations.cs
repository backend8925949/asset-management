﻿using HCore.Data.Logging;
using HCore.Data.Logging.Models;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.App;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.App
{
    public class FrameworkAssetOperations
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        AtAssetIssueDetail? _AtAssetIssueDetail;
        AtAssetIssue? _AtAssetIssues;
        AtAssetTransfer? _AtAssetTransfer;
        AtAssetMapping? _AtAssetMapping;
        AtAssetMappingLog? _AtAssetMappingLog;
        AtAssetAuditLog? _AtAssetAuditLog;
        AtAssetRequisition? _AssetRequisition;
        AtAssetTransferDetail? _AssetTransferDetail;
        AtAssetHistory? _AtAssetHistory;

        internal async Task<OResponse> MapAssetBarcode(OAssetOperations.MapAssetBarcode _Request)
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    foreach (var AssetBarcode in _Request.MapAssetBarcodes)
                    {
                        _AtAssetMapping = new AtAssetMapping();
                        _AtAssetMapping.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetMapping.Asset = AssetBarcode.AssetBarcode;
                        _AtAssetMapping.Organization = _Request.UserReference.OrganizationKey;
                        _AtAssetMapping.Asset = AssetBarcode.AssetBarcode;
                        _AtAssetMapping.RefId = AssetBarcode.RfId;
                        _AtAssetMapping.EpcId = AssetBarcode.EpcId;
                        _AtAssetMapping.Status = AssetStatus.InProcess;
                        _AtAssetMapping.CreatedBy = _Request.UserReference.AccountId;
                        _AtAssetMapping.CreatedDate = (DateTime)AssetBarcode.CreateDate;
                        await _HCoreContextLogging.AtAssetMappings.AddAsync(_AtAssetMapping);
                    }
                    await _HCoreContextLogging.SaveChangesAsync();
                    await _HCoreContextLogging.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Aseet mapping details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("MapAssetBarcode", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> IssueAsset(OAssetOperations.IssueAsset _Request)
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    foreach (var AssetBarcode in _Request.IssueAssets)
                    {
                        string? ReceiptNo = HCoreHelper.GenerateAccountCode(10);
                        _AtAssetIssues = new AtAssetIssue();
                        _AtAssetIssues.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetIssues.Asset = AssetBarcode.AssetBarcode;
                        _AtAssetIssues.ReceiptNo = ReceiptNo;
                        _AtAssetIssues.Organization = _Request.UserReference.OrganizationKey;
                        _AtAssetIssues.Division = AssetBarcode.DivisionKey;
                        _AtAssetIssues.Department = AssetBarcode.DepartmentKey;
                        _AtAssetIssues.Location = AssetBarcode.LocationKey;
                        _AtAssetIssues.SubLocation = AssetBarcode.SubLocationKey;
                        _AtAssetIssues.Employee = AssetBarcode.EmployeeKey;
                        _AtAssetIssues.Remark = AssetBarcode.Remark;
                        _AtAssetIssues.Quantity = AssetBarcode.Quantity;
                        _AtAssetIssues.Status = AssetStatus.InProcess;
                        _AtAssetIssues.CreatedBy = _Request.UserReference.AccountId;
                        _AtAssetIssues.CreatedDate = (DateTime)AssetBarcode.CreateDate;

                        var _Assets = _Request.IssueAssets.Where(x => x.EmployeeKey == AssetBarcode.EmployeeKey && x.ReceiptNo == null).ToList();
                        foreach (var _Asset in _Assets)
                        {
                            _Asset.ReceiptNo = ReceiptNo;
                        }

                        await _HCoreContextLogging.AtAssetIssues.AddAsync(_AtAssetIssues);
                    }
                    await _HCoreContextLogging.SaveChangesAsync();
                    await _HCoreContextLogging.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Aseet issue details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("IssueAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> TransferAsset(OAssetOperations.TransferAsset _Request)
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    foreach (var AssetBarcode in _Request.TransferAssets)
                    {
                        string? ReceiptNo = HCoreHelper.GenerateAccountCode(10);
                        _AtAssetTransfer = new AtAssetTransfer();
                        _AtAssetTransfer.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetTransfer.Asset = AssetBarcode.AssetBarcode;
                        _AtAssetTransfer.Organization = _Request.UserReference.OrganizationKey;
                        _AtAssetTransfer.Division = AssetBarcode.DivisionKey;
                        _AtAssetTransfer.Department = AssetBarcode.DepartmentKey;
                        _AtAssetTransfer.Location = AssetBarcode.LocationKey;
                        _AtAssetTransfer.SubLocation = AssetBarcode.SubLocationKey;
                        _AtAssetTransfer.Employee = AssetBarcode.EmployeeKey;
                        _AtAssetTransfer.Remark = AssetBarcode.Remark;
                        _AtAssetTransfer.Status = AssetStatus.InProcess;
                        _AtAssetTransfer.CreatedBy = _Request.UserReference.AccountId;
                        _AtAssetTransfer.CreatedDate = (DateTime)AssetBarcode.CreateDate;

                        var _Assets = _Request.TransferAssets.Where(x => x.EmployeeKey == AssetBarcode.EmployeeKey && x.ReceiptNo == null).ToList();
                        foreach (var _Asset in _Assets)
                        {
                            _Asset.ReceiptNo = ReceiptNo;
                        }

                        await _HCoreContextLogging.AtAssetTransfers.AddAsync(_AtAssetTransfer);
                    }
                    await _HCoreContextLogging.SaveChangesAsync();
                    await _HCoreContextLogging.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Aseet issue details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("TrasnsferAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> Asset_Audit(OAssetOperations.Audit.AuditRequest _Request)
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    foreach (var AssetBarcode in _Request.AuditRequests)
                    {
                        _AtAssetAuditLog = new AtAssetAuditLog();
                        _AtAssetAuditLog.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetAuditLog.AssetBarcode = AssetBarcode.AssetBarcode;
                        _AtAssetAuditLog.OrganizationId = _Request.UserReference.OrganizationId;
                        _AtAssetAuditLog.Division = AssetBarcode.Division;
                        _AtAssetAuditLog.Location = AssetBarcode.Location;
                        _AtAssetAuditLog.SubLocation = AssetBarcode.SubLocation;
                        _AtAssetAuditLog.Department = AssetBarcode.Department;
                        _AtAssetAuditLog.Employee = AssetBarcode.Employee;
                        _AtAssetAuditLog.AssetStatus = AssetBarcode.AssetStatus;
                        _AtAssetAuditLog.AuditDivision = AssetBarcode.AuditDivision;
                        _AtAssetAuditLog.AuditLocation = AssetBarcode.AuditLocation;
                        _AtAssetAuditLog.AuditSubLocation = AssetBarcode.AuditSubLocation;
                        _AtAssetAuditLog.AuditStatus = AssetBarcode.AuditStatus;
                        _AtAssetAuditLog.UserId = _Request.UserReference.AccountId;
                        _AtAssetAuditLog.AuditDate = HCoreHelper.GetDateTime();
                        await _HCoreContextLogging.AtAssetAuditLogs.AddAsync(_AtAssetAuditLog);
                    }
                    await _HCoreContextLogging.SaveChangesAsync();
                    await _HCoreContextLogging.DisposeAsync();
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Asset audit details saved successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("Asset_Audit", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ValidateBarcode(OReference _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var BarcodeDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                         .Select(x => new
                                         {
                                             AssetId = x.Id,
                                             AssetKey = x.Guid,
                                             AssetName = x.AssetName,
                                             AssetBarcode = x.AssetBarcode,
                                             AssetSerialNo = x.AssetSerialNumber,
                                             AssetType = x.AssetType.SystemName,
                                             AssetTypeName = x.AssetType.Name,
                                             AssetClassName = x.AssetClass.ClassName,
                                             AssetCategoryName = x.AssetCategory.CategoryName,
                                             AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,
                                             ExAssetId = x.ExAssetId,
                                             AssetMake = x.AssetMake,
                                             AssetModel = x.AssetModel,
                                             TotalQuantity = x.TotalQuantity,
                                             StatusId = x.StatusId,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                             CompanyId = x.OrganizationId,
                                             CompanyCode = x.Organization.AccountCode,
                                             CompanyName = x.Organization.DisplayName,
                                             RemainingQuantity = x.RemainingQuantity,
                                         }).FirstOrDefaultAsync();
                    if (BarcodeDetails != null)
                    {
                        OAssetOperations.BarcodeDetails _BarcodeDetails = new OAssetOperations.BarcodeDetails();
                        _BarcodeDetails.AssetId = BarcodeDetails.AssetId;
                        _BarcodeDetails.AssetKey = BarcodeDetails.AssetKey;
                        _BarcodeDetails.AssetName = BarcodeDetails.AssetName;
                        _BarcodeDetails.AssetBarcode = BarcodeDetails.AssetBarcode;
                        _BarcodeDetails.AssetSerialNo = BarcodeDetails.AssetSerialNo;
                        _BarcodeDetails.AssetTypeCode = BarcodeDetails.AssetType;
                        _BarcodeDetails.AssetTypeName = BarcodeDetails.AssetTypeName;
                        _BarcodeDetails.AssetClassName = BarcodeDetails.AssetClassName;
                        _BarcodeDetails.AssetCategoryName = BarcodeDetails.AssetCategoryName;
                        _BarcodeDetails.AssetSubCategoryName = BarcodeDetails.AssetSubCategoryName;
                        _BarcodeDetails.ExAssetId = BarcodeDetails.ExAssetId;
                        _BarcodeDetails.AssetMake = BarcodeDetails.AssetMake;
                        _BarcodeDetails.AssetModel = BarcodeDetails.AssetModel;
                        _BarcodeDetails.StatusId = BarcodeDetails.StatusId;
                        _BarcodeDetails.StatusCode = BarcodeDetails.StatusCode;
                        _BarcodeDetails.StatusName = BarcodeDetails.StatusName;
                        _BarcodeDetails.CompanyId = BarcodeDetails.CompanyId;
                        _BarcodeDetails.CompanyCode = BarcodeDetails.CompanyCode;
                        _BarcodeDetails.CompanyName = BarcodeDetails.CompanyName;
                        _BarcodeDetails.ReprintCount = 0;
                        _BarcodeDetails.RemainingQuantity = BarcodeDetails.RemainingQuantity;

                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            _BarcodeDetails.IsPrinted = await _HCoreContextLogging.AtAssetPrintLogs.AnyAsync(x => x.AssetId == _BarcodeDetails.AssetId && x.OrganizationId == _Request.UserReference.OrganizationId);
                            _BarcodeDetails.ReprintCount = await _HCoreContextLogging.AtAssetPrintLogs.Where(x => x.AssetId == _BarcodeDetails.AssetId && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();
                        }

                        var Details = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == BarcodeDetails.AssetId && x.StatusId == AssetStatus.Issued)
                                      .Select(x => new
                                      {
                                          DepartmentId = x.DepartmentId,
                                          DepartmentKey = x.Department.Guid,
                                          DepartmentName = x.Department.DepartmentName,

                                          DivisionId = x.DivisionId,
                                          DivisionKey = x.Division.Guid,
                                          DivisionName = x.Division.DivisionName,

                                          LocationId = x.LocationId,
                                          LocationKey = x.Location.Guid,
                                          LocationName = x.Location.LocationName,

                                          SubLocationId = x.SubLocationId,
                                          SubLocationKey = x.SubLocation.Guid,
                                          SubLocationName = x.SubLocation.LocationName,

                                          EmployeeId = x.EmployeeId,
                                          EmployeeKey = x.Employee.Guid,
                                          EmployeeName = x.Employee.DisplayName,

                                          CreateDate = x.CreatedDate,
                                      }).FirstOrDefaultAsync();

                        if (Details != null)
                        {
                            _BarcodeDetails.IsIssued = true;
                            _BarcodeDetails.DivisionId = Details.DivisionId;
                            _BarcodeDetails.DivisionKey = Details.DivisionKey;
                            _BarcodeDetails.DivisionName = Details.DivisionName;
                            _BarcodeDetails.DepartmentId = Details.DepartmentId;
                            _BarcodeDetails.DepartmentKey = Details.DepartmentKey;
                            _BarcodeDetails.DepartmentName = Details.DepartmentName;
                            _BarcodeDetails.LocationId = Details.LocationId;
                            _BarcodeDetails.LocationKey = Details.LocationKey;
                            _BarcodeDetails.LocationName = Details.LocationName;
                            _BarcodeDetails.SubLocationId = Details.SubLocationId;
                            _BarcodeDetails.SubLocationKey = Details.SubLocationKey;
                            _BarcodeDetails.SubLocationName = Details.SubLocationName;
                            _BarcodeDetails.EmployeeId = Details.EmployeeId;
                            _BarcodeDetails.EmployeeKey = Details.EmployeeKey;
                            _BarcodeDetails.EmployeeName = Details.EmployeeName;
                            _BarcodeDetails.IssuedDate = Details.CreateDate;
                        }
                        else
                        {
                            _BarcodeDetails.IsIssued = false;
                        }

                        var _PurchaseDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == BarcodeDetails.AssetId)
                                               .Select(x => new
                                               {
                                                   VendorName = x.Vendor.DisplayName,
                                                   PoNumber = x.PoNumber,
                                                   InvoiceNumber = x.InvoiceNumber,
                                                   RecievedDate = x.RecievedDate,
                                                   WarrentyStartDate = x.WarrantyStartDate,
                                                   WarrentyEndDate = x.WarrantyEndDate,
                                                   ExpiresIn = x.ExpiresIn,
                                                   ExpiryDate = x.ExpiryDate,
                                                   PurchasePrice = x.PurchasePrice,
                                                   AdditionalPrice = x.AdditionalPrice,
                                                   IsAmcApplicable = x.IsAmcApplicable,
                                                   IsInsurances = x.IsInsurance,
                                               }).FirstOrDefaultAsync();

                        if (_PurchaseDetails != null)
                        {
                            _BarcodeDetails.VendorName = _PurchaseDetails.VendorName;
                            _BarcodeDetails.PoNumber = _PurchaseDetails.PoNumber;
                            _BarcodeDetails.InvoiceNumber = _PurchaseDetails.InvoiceNumber;
                            _BarcodeDetails.RecievedDate = _PurchaseDetails.RecievedDate;
                            _BarcodeDetails.WarrentyStartDate = _PurchaseDetails.WarrentyStartDate;
                            _BarcodeDetails.WarrentyEndDate = _PurchaseDetails.WarrentyEndDate;
                            _BarcodeDetails.ExpiresIn = _PurchaseDetails.ExpiresIn;
                            _BarcodeDetails.ExpiryDate = _PurchaseDetails.ExpiryDate;
                            _BarcodeDetails.PurchasePrice = _PurchaseDetails.PurchasePrice;
                            _BarcodeDetails.AdditionalPrice = _PurchaseDetails.AdditionalPrice;
                            if (_PurchaseDetails.IsAmcApplicable == 1)
                            {
                                _BarcodeDetails.IsAmc = true;
                            }
                            else
                            {
                                _BarcodeDetails.IsAmc = false;
                            }
                            if (_PurchaseDetails.IsInsurances == 1)
                            {
                                _BarcodeDetails.IsInsurance = true;
                            }
                            else
                            {
                                _BarcodeDetails.IsInsurance = false;
                            }
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BarcodeDetails, "ATSUCCESS", "Barcode validated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Invalid barcode");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ValidateBarcode", _Exception, _Request.UserReference);
            }
        }

        internal async Task IssueAssets()
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var Assets = await _HCoreContextLogging.AtAssetIssues.Where(x => x.Status == AssetStatus.InProcess).OrderByDescending(x => x.CreatedDate).ToListAsync();
                    if (Assets.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var _Assets = await _HCoreContextLogging.AtAssetIssues.Where(x => x.Status == AssetStatus.Processed).ToListAsync();
                            if (_Assets.Count > 0)
                            {
                                _HCoreContextLogging.AtAssetIssues.RemoveRange(_Assets);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                            foreach (var Asset in Assets)
                            {
                                var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.Asset || x.RfId == Asset.Asset).FirstOrDefaultAsync();
                                if (AssetDetails != null)
                                {
                                    var _IsExists = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && x.Asset.AssetTypeId == AssetType.WithBarcode && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                                    if (_IsExists)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, there's already a request present for this asset of issue.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    else if (AssetDetails.StatusId == AssetStatus.Disposed)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, This asset is disposed.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    else if (AssetDetails.AssetTypeId == AssetType.WithBarcode && AssetDetails.StatusId == AssetStatus.Issued)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, this asset is already issued.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    else if (AssetDetails.AssetTypeId == AssetType.WithoutBarcode && AssetDetails.RemainingQuantity == 0)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, the remaining quantity is '0' to issue.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    else if (AssetDetails.AssetTypeId == AssetType.WithoutBarcode && AssetDetails.RemainingQuantity < Asset.Quantity)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, the requested quantity is greater than remaining quantity.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    else
                                    {
                                        long? OrganizationId = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.Organization).Select(x => x.Id).FirstOrDefaultAsync();
                                        long? DivisionId = await _HCoreContext.AtDivisions.Where(x => x.Guid == Asset.Division).Select(x => x.Id).FirstOrDefaultAsync();
                                        long? DepartmentId = await _HCoreContext.AtDepartments.Where(x => x.Guid == Asset.Department).Select(x => x.Id).FirstOrDefaultAsync();
                                        long? LocationId = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.Location).Select(x => x.Id).FirstOrDefaultAsync();
                                        long? SubLocationId = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.SubLocation).Select(x => x.Id).FirstOrDefaultAsync();
                                        long? AccountId = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.Employee).Select(x => x.Id).FirstOrDefaultAsync();

                                        _AssetRequisition = new AtAssetRequisition();
                                        _AssetRequisition.Guid = HCoreHelper.GenerateGuid();
                                        _AssetRequisition.AssetId = AssetDetails.Id;
                                        _AssetRequisition.ApprovalTypeId = AssetApproval.IssueApproval;

                                        long? UserRoleId = await _HCoreContext.AtAccounts.Where(x => x.Id == Asset.CreatedBy).Select(x => x.RoleId).FirstOrDefaultAsync();

                                        if (UserRoleId == UserRole.HeadOfDiv)
                                        {
                                            _AssetRequisition.IsDeptHeadApproved = 1;
                                            _AssetRequisition.IsDivHeadApproved = 1;
                                            _AssetRequisition.IsHrApproved = 0;
                                            _AssetRequisition.IsAdminApproved = 0;
                                            _AssetRequisition.IsSuperAdminApproved = 0;
                                        }
                                        else if (UserRoleId == UserRole.Admin)
                                        {
                                            _AssetRequisition.IsDeptHeadApproved = 1;
                                            _AssetRequisition.IsDivHeadApproved = 1;
                                            _AssetRequisition.IsHrApproved = 1;
                                            _AssetRequisition.IsAdminApproved = 1;
                                            _AssetRequisition.IsSuperAdminApproved = 0;
                                        }
                                        else if (UserRoleId == UserRole.SuperAdmin)
                                        {
                                            _AssetRequisition.IsDeptHeadApproved = 1;
                                            _AssetRequisition.IsDivHeadApproved = 1;
                                            _AssetRequisition.IsHrApproved = 1;
                                            _AssetRequisition.IsAdminApproved = 1;
                                            _AssetRequisition.IsSuperAdminApproved = 1;
                                        }
                                        else
                                        {
                                            _AssetRequisition.IsDeptHeadApproved = 0;
                                            _AssetRequisition.IsDivHeadApproved = 0;
                                            _AssetRequisition.IsHrApproved = 0;
                                            _AssetRequisition.IsAdminApproved = 0;
                                            _AssetRequisition.IsSuperAdminApproved = 0;
                                        }

                                        _AssetRequisition.CreatedDate = HCoreHelper.GetDateTime();
                                        _AssetRequisition.CreatedById = Asset.CreatedBy;
                                        await _HCoreContext.AtAssetRequisitions.AddAsync(_AssetRequisition);
                                        await _HCoreContext.SaveChangesAsync();

                                        _AtAssetIssueDetail = new AtAssetIssueDetail();
                                        _AtAssetIssueDetail.Guid = HCoreHelper.GenerateGuid();
                                        _AtAssetIssueDetail.AssetId = AssetDetails.Id;
                                        _AtAssetIssueDetail.SourceId = RegistrationSource.MobileApp;
                                        _AtAssetIssueDetail.OrganizationId = (long)OrganizationId;
                                        _AtAssetIssueDetail.DivisionId = (long)DivisionId;
                                        if (DepartmentId > 0)
                                        {
                                            _AtAssetIssueDetail.DepartmentId = (long)DepartmentId;
                                        }
                                        if (AccountId > 0)
                                        {
                                            _AtAssetIssueDetail.EmployeeId = (long)AccountId;
                                        }
                                        _AtAssetIssueDetail.LocationId = (long)LocationId;
                                        _AtAssetIssueDetail.SubLocationId = (long)SubLocationId;
                                        _AtAssetIssueDetail.Remark = Asset.Remark;
                                        _AtAssetIssueDetail.Quantity = Asset.Quantity;
                                        _AtAssetIssueDetail.IsTransfered = 0;

                                        if (UserRoleId == UserRole.HeadOfDiv)
                                        {
                                            _AtAssetIssueDetail.StatusId = AssetStatus.AssetApproval.PendingHRApproval;

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "The issue request for this asset has been approved by the <b>Division Head</b> and sent for approval to <b>HR</b> to <b>";

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";
                                            string? EmployeeEmail = "";

                                            if (AccountId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                            Email = x.EmailAddress
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                    EmployeeEmail = Employee.Email;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Issue Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = Asset.CreatedBy;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else if (UserRoleId == UserRole.Admin)
                                        {
                                            _AtAssetIssueDetail.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "The issue request fot this asset has been approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b> to <b>";

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";
                                            string? EmployeeEmail = "";

                                            if (AccountId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                            Email = x.EmailAddress
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                    EmployeeEmail = Employee.Email;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Issue Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = Asset.CreatedBy;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else if (UserRoleId == UserRole.SuperAdmin)
                                        {
                                            _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                                            _AtAssetIssueDetail.ApprovedDate = HCoreHelper.GetDateTime();
                                            _AtAssetIssueDetail.ApprovedById = Asset.CreatedBy;
                                            AssetDetails.StatusId = AssetStatus.Issued;

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "This asset has been issued to <b>";

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";
                                            string? EmployeeEmail = "";

                                            if (AccountId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                            Email = x.EmailAddress
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                    EmployeeEmail = Employee.Email;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b> by the approval of <b>Super Admin</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Issued";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = Asset.CreatedBy;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else
                                        {
                                            _AtAssetIssueDetail.StatusId = AssetStatus.AssetApproval.PendingDepartmentHeadApproval;

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "An issue request has been raised for this asset to <b>";

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";

                                            if (AccountId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b>. The request has been sent for approval to the <b>Department Head</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Issue Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = Asset.CreatedBy;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }

                                        _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                                        _AtAssetIssueDetail.CreatedById = Asset.CreatedBy;
                                        _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                                        _HCoreContext.AtAssetIssueDetails.Add(_AtAssetIssueDetail);
                                        await _HCoreContext.SaveChangesAsync();

                                        AssetDetails.RemainingQuantity = AssetDetails.TotalQuantity - _AtAssetIssueDetail.Quantity;
                                        AssetDetails.ModifiedById = Asset.CreatedBy;
                                        AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.SaveChangesAsync();

                                        Asset.Comment = "Processed Successfully.";
                                        Asset.Status = AssetStatus.Processed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                }
                                else
                                {
                                    Asset.Comment = "Failed to issue. Beacuse, no asset found with barcode or rf id.";
                                    Asset.Status = AssetStatus.Failed;
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }
                        }

                        await _HCoreContext.DisposeAsync();
                    }
                }

                await _HCoreContextLogging.DisposeAsync();
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("IssueAssets-Mobile", _Exception, null);
            }
        }

        internal async Task TransferAssets()
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var Assets = await _HCoreContextLogging.AtAssetTransfers.Where(x => x.Status == AssetStatus.InProcess).OrderByDescending(x => x.CreatedDate).ToListAsync();
                    if (Assets.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var _Assets = await _HCoreContextLogging.AtAssetTransfers.Where(x => x.Status == AssetStatus.Processed).ToListAsync();
                            if (_Assets.Count > 0)
                            {
                                _HCoreContextLogging.AtAssetTransfers.RemoveRange(_Assets);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                            foreach (var Asset in Assets)
                            {
                                var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.Asset || x.RfId == Asset.Asset).FirstOrDefaultAsync();
                                if (AssetDetails != null)
                                {
                                    long? UserRoleId = await _HCoreContext.AtAccounts.Where(x => x.Id == Asset.CreatedBy).Select(x => x.RoleId).FirstOrDefaultAsync();

                                    var _IsExists = await _HCoreContext.AtAssetTransferDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                                    if (_IsExists)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, there's already a request present for this asset of transfer.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    if (AssetDetails.StatusId == AssetStatus.Disposed)
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, this asset is already disposed.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    var AssetIssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == AssetDetails.Id && x.StatusId == AssetStatus.Issued).FirstOrDefaultAsync();
                                    if (AssetIssueDetails != null)
                                    {
                                        bool _IsReturnPending = await _HCoreContext.AtAssetReturnDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                                        if (_IsReturnPending)
                                        {
                                            Asset.Comment = "Failed to issue. Beacuse, there's a request for this asset for return.";
                                            Asset.Status = AssetStatus.Failed;
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                        else
                                        {
                                            long? OrganizationId = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.Organization).Select(x => x.Id).FirstOrDefaultAsync();
                                            long? DivisionId = await _HCoreContext.AtDivisions.Where(x => x.Guid == Asset.Division).Select(x => x.Id).FirstOrDefaultAsync();
                                            long? DepartmentId = await _HCoreContext.AtDepartments.Where(x => x.Guid == Asset.Department).Select(x => x.Id).FirstOrDefaultAsync();
                                            long? LocationId = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.Location).Select(x => x.Id).FirstOrDefaultAsync();
                                            long? SubLocationId = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.SubLocation).Select(x => x.Id).FirstOrDefaultAsync();
                                            long? AccountId = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.Employee).Select(x => x.Id).FirstOrDefaultAsync();

                                            if (AccountId > 0 && AccountId != null && AssetIssueDetails.EmployeeId == AccountId)
                                            {
                                                Asset.Comment = "Failed to issue. Beacuse, tried to transfer the asset to same employee.";
                                                Asset.Status = AssetStatus.Failed;
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                            else
                                            {
                                                _AssetRequisition = new AtAssetRequisition();
                                                _AssetRequisition.Guid = HCoreHelper.GenerateGuid();
                                                _AssetRequisition.AssetId = AssetDetails.Id;
                                                _AssetRequisition.ApprovalTypeId = AssetApproval.TransferApproval;

                                                if (UserRoleId == UserRole.HeadOfDiv)
                                                {
                                                    _AssetRequisition.IsDeptHeadApproved = 1;
                                                    _AssetRequisition.IsDivHeadApproved = 1;
                                                    _AssetRequisition.IsHrApproved = 0;
                                                    _AssetRequisition.IsAdminApproved = 0;
                                                    _AssetRequisition.IsSuperAdminApproved = 0;
                                                }
                                                else if (UserRoleId == UserRole.Admin)
                                                {
                                                    _AssetRequisition.IsDeptHeadApproved = 1;
                                                    _AssetRequisition.IsDivHeadApproved = 1;
                                                    _AssetRequisition.IsHrApproved = 1;
                                                    _AssetRequisition.IsAdminApproved = 1;
                                                    _AssetRequisition.IsSuperAdminApproved = 0;
                                                }
                                                else if (UserRoleId == UserRole.SuperAdmin)
                                                {
                                                    _AssetRequisition.IsDeptHeadApproved = 1;
                                                    _AssetRequisition.IsDivHeadApproved = 1;
                                                    _AssetRequisition.IsHrApproved = 1;
                                                    _AssetRequisition.IsAdminApproved = 1;
                                                    _AssetRequisition.IsSuperAdminApproved = 1;
                                                }
                                                else
                                                {
                                                    _AssetRequisition.IsDeptHeadApproved = 0;
                                                    _AssetRequisition.IsDivHeadApproved = 0;
                                                    _AssetRequisition.IsHrApproved = 0;
                                                    _AssetRequisition.IsAdminApproved = 0;
                                                    _AssetRequisition.IsSuperAdminApproved = 0;
                                                }

                                                _AssetRequisition.CreatedDate = HCoreHelper.GetDateTime();
                                                _AssetRequisition.CreatedById = Asset.CreatedBy;
                                                await _HCoreContext.AtAssetRequisitions.AddAsync(_AssetRequisition);
                                                await _HCoreContext.SaveChangesAsync();

                                                string Reference = HCoreHelper.GenerateGuid();

                                                AssetIssueDetails.IsTransfered = 1;
                                                AssetIssueDetails.TransferedTo = AccountId;
                                                AssetIssueDetails.TransferedBy = Asset.CreatedBy;
                                                AssetIssueDetails.TransferedDate = HCoreHelper.GetDateTime();
                                                AssetIssueDetails.TransferReference = Reference;
                                                AssetIssueDetails.StatusId = AssetStatus.ApprovalPending;
                                                AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                                AssetIssueDetails.ModifiedById = Asset.CreatedBy;
                                                await _HCoreContext.SaveChangesAsync();

                                                _AssetTransferDetail = new AtAssetTransferDetail();
                                                _AssetTransferDetail.Guid = AssetIssueDetails.TransferReference;
                                                _AssetTransferDetail.AssetId = AssetDetails.Id;
                                                _AssetTransferDetail.AssetIssueId = AssetIssueDetails.Id;
                                                _AssetTransferDetail.SourceId = RegistrationSource.WebApplication;
                                                _AssetTransferDetail.OrganizationId = AssetIssueDetails.OrganizationId;
                                                _AssetTransferDetail.DivisionId = (long)DivisionId;
                                                if (DepartmentId > 0)
                                                {
                                                    _AssetTransferDetail.DepartmentId = DepartmentId;
                                                }
                                                if (AccountId > 0)
                                                {
                                                    _AssetTransferDetail.EmployeeId = AccountId;
                                                }
                                                _AssetTransferDetail.LocationId = (long)LocationId;
                                                _AssetTransferDetail.SubLocationId = (long)SubLocationId;
                                                _AssetTransferDetail.Remark = Asset.Remark;
                                                _AssetTransferDetail.Quantity = AssetIssueDetails.Quantity;

                                                if (UserRoleId == UserRole.HeadOfDiv)
                                                {
                                                    _AssetTransferDetail.StatusId = AssetStatus.AssetApproval.PendingHRApproval;

                                                    string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                                    string? Body = "The transfer request for this asset has been raised and approved by the <b>Division Head</b> and sent for approval to <b>HR</b> to <b>";

                                                    string? EmployeeName = "";
                                                    string? EmployeeCode = "";
                                                    string? EmployeeEmail = "";

                                                    if (AccountId > 0)
                                                    {
                                                        var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                                .Select(x => new
                                                                                {
                                                                                    Name = x.DisplayName,
                                                                                    AccountCode = x.AccountCode,
                                                                                    Email = x.EmailAddress
                                                                                }).FirstOrDefaultAsync();
                                                        if (Employee != null)
                                                        {
                                                            EmployeeName = Employee.Name;
                                                            EmployeeCode = Employee.AccountCode;
                                                            EmployeeEmail = Employee.Email;
                                                        }

                                                        Body += EmployeeName + "(" + EmployeeCode + "), ";
                                                    }

                                                    string? Department = "";

                                                    if (DepartmentId > 0)
                                                    {
                                                        Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        if (!string.IsNullOrEmpty(Department))
                                                        {
                                                            Body += Department + ", ";
                                                        }
                                                    }

                                                    Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                                    if (AssetIssueDetails.EmployeeId > 0)
                                                    {
                                                        var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                              .Select(x => new
                                                                              {
                                                                                  DisplayName = x.DisplayName,
                                                                                  AccountCode = x.AccountCode,
                                                                              }).FirstOrDefaultAsync();
                                                        if (EmployeeDetails != null)
                                                        {
                                                            Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                        }
                                                    }
                                                    if (AssetIssueDetails.DepartmentId > 0)
                                                    {
                                                        string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        Body += DepartmentName + ", ";
                                                    }

                                                    string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                                    using (_HCoreContextLogging = new HCoreContextLogging())
                                                    {
                                                        _AtAssetHistory = new AtAssetHistory();
                                                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                        _AtAssetHistory.OrganizationId = OrganizationId;
                                                        _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                        _AtAssetHistory.Status = "Transfer Request";
                                                        _AtAssetHistory.Activity = Body;
                                                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                        _AtAssetHistory.UserId = Asset.CreatedBy;
                                                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                        await _HCoreContextLogging.SaveChangesAsync();
                                                    }
                                                }
                                                else if (UserRoleId == UserRole.Admin)
                                                {
                                                    _AssetTransferDetail.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;

                                                    string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                                    string? Body = "The transfer request for this asset has been raised and approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b> to <b>";

                                                    string? EmployeeName = "";
                                                    string? EmployeeCode = "";
                                                    string? EmployeeEmail = "";

                                                    if (AccountId > 0)
                                                    {
                                                        var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                                .Select(x => new
                                                                                {
                                                                                    Name = x.DisplayName,
                                                                                    AccountCode = x.AccountCode,
                                                                                    Email = x.EmailAddress
                                                                                }).FirstOrDefaultAsync();
                                                        if (Employee != null)
                                                        {
                                                            EmployeeName = Employee.Name;
                                                            EmployeeCode = Employee.AccountCode;
                                                            EmployeeEmail = Employee.Email;
                                                        }

                                                        Body += EmployeeName + "(" + EmployeeCode + "), ";
                                                    }

                                                    string? Department = "";

                                                    if (DepartmentId > 0)
                                                    {
                                                        Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        if (!string.IsNullOrEmpty(Department))
                                                        {
                                                            Body += Department + ", ";
                                                        }
                                                    }

                                                    Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                                    if (AssetIssueDetails.EmployeeId > 0)
                                                    {
                                                        var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                              .Select(x => new
                                                                              {
                                                                                  DisplayName = x.DisplayName,
                                                                                  AccountCode = x.AccountCode,
                                                                              }).FirstOrDefaultAsync();
                                                        if (EmployeeDetails != null)
                                                        {
                                                            Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                        }
                                                    }
                                                    if (AssetIssueDetails.DepartmentId > 0)
                                                    {
                                                        string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        Body += DepartmentName + ", ";
                                                    }

                                                    string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                                    using (_HCoreContextLogging = new HCoreContextLogging())
                                                    {
                                                        _AtAssetHistory = new AtAssetHistory();
                                                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                        _AtAssetHistory.OrganizationId = OrganizationId;
                                                        _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                        _AtAssetHistory.Status = "Transfer Request";
                                                        _AtAssetHistory.Activity = Body;
                                                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                        _AtAssetHistory.UserId = Asset.CreatedBy;
                                                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                        await _HCoreContextLogging.SaveChangesAsync();
                                                    }
                                                }
                                                else if (UserRoleId == UserRole.SuperAdmin)
                                                {
                                                    _AssetTransferDetail.StatusId = AssetStatus.Transfered;
                                                    _AssetTransferDetail.ApprovedDate = HCoreHelper.GetDateTime();
                                                    _AssetTransferDetail.ApprovedById = Asset.CreatedBy;

                                                    AssetDetails.StatusId = AssetStatus.Issued;
                                                    AssetIssueDetails.StatusId = AssetStatus.Transfered;

                                                    _AtAssetIssueDetail = new AtAssetIssueDetail();
                                                    _AtAssetIssueDetail.Guid = AssetIssueDetails.TransferReference;
                                                    _AtAssetIssueDetail.AssetId = AssetDetails.Id;
                                                    _AtAssetIssueDetail.SourceId = RegistrationSource.WebApplication;
                                                    _AtAssetIssueDetail.OrganizationId = _AssetTransferDetail.OrganizationId;
                                                    _AtAssetIssueDetail.DivisionId = _AssetTransferDetail.DivisionId;
                                                    if (_AssetTransferDetail.DepartmentId > 0)
                                                    {
                                                        _AtAssetIssueDetail.DepartmentId = _AssetTransferDetail.DepartmentId;
                                                    }
                                                    if (_AssetTransferDetail.EmployeeId > 0)
                                                    {
                                                        _AtAssetIssueDetail.EmployeeId = _AssetTransferDetail.EmployeeId;
                                                    }
                                                    _AtAssetIssueDetail.LocationId = _AssetTransferDetail.LocationId;
                                                    _AtAssetIssueDetail.SubLocationId = _AssetTransferDetail.SubLocationId;
                                                    _AtAssetIssueDetail.Remark = _AssetTransferDetail.Remark;
                                                    _AtAssetIssueDetail.OsName = _AssetTransferDetail.OsName;
                                                    _AtAssetIssueDetail.HostName = _AssetTransferDetail.HostName;
                                                    _AtAssetIssueDetail.IpAddress = _AssetTransferDetail.IpAddress;

                                                    _AtAssetIssueDetail.Quantity = _AssetTransferDetail.Quantity;
                                                    _AtAssetIssueDetail.IsTransfered = 0;
                                                    _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                                                    _AtAssetIssueDetail.CreatedById = Asset.CreatedBy;
                                                    _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetIssueDetails.AddAsync(_AtAssetIssueDetail);

                                                    string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                                    string? Body = "This asset has been transferred to <b>";

                                                    string? EmployeeName = "";
                                                    string? EmployeeCode = "";

                                                    if (AccountId > 0)
                                                    {
                                                        var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                                .Select(x => new
                                                                                {
                                                                                    Name = x.DisplayName,
                                                                                    AccountCode = x.AccountCode,
                                                                                }).FirstOrDefaultAsync();
                                                        if (Employee != null)
                                                        {
                                                            EmployeeName = Employee.Name;
                                                            EmployeeCode = Employee.AccountCode;
                                                        }

                                                        Body += EmployeeName + "(" + EmployeeCode + "), ";
                                                    }

                                                    string? Department = "";

                                                    if (DepartmentId > 0)
                                                    {
                                                        Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        if (!string.IsNullOrEmpty(Department))
                                                        {
                                                            Body += Department + ", ";
                                                        }
                                                    }

                                                    Body += Division + ", " + Location + ", " + SubLocation + "</b> by the approval of <b>Super Admin</b> from <b>";

                                                    if (AssetIssueDetails.EmployeeId > 0)
                                                    {
                                                        var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                              .Select(x => new
                                                                              {
                                                                                  DisplayName = x.DisplayName,
                                                                                  AccountCode = x.AccountCode,
                                                                              }).FirstOrDefaultAsync();
                                                        if (EmployeeDetails != null)
                                                        {
                                                            Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                        }
                                                    }
                                                    if (AssetIssueDetails.DepartmentId > 0)
                                                    {
                                                        string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        Body += DepartmentName + ", ";
                                                    }

                                                    string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                                    using (_HCoreContextLogging = new HCoreContextLogging())
                                                    {
                                                        _AtAssetHistory = new AtAssetHistory();
                                                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                        _AtAssetHistory.OrganizationId = OrganizationId;
                                                        _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                        _AtAssetHistory.Status = "Issued";
                                                        _AtAssetHistory.Activity = Body;
                                                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                        _AtAssetHistory.UserId = Asset.CreatedBy;
                                                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                        await _HCoreContextLogging.SaveChangesAsync();
                                                    }
                                                }
                                                else
                                                {
                                                    _AssetTransferDetail.StatusId = AssetStatus.AssetApproval.PendingDepartmentHeadApproval;

                                                    string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                                    string? Body = "An transfer request has been raised for this asset to <b>";

                                                    string? EmployeeName = "";
                                                    string? EmployeeCode = "";
                                                    string? EmployeeEmail = "";

                                                    if (AccountId > 0)
                                                    {
                                                        var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                                                .Select(x => new
                                                                                {
                                                                                    Name = x.DisplayName,
                                                                                    AccountCode = x.AccountCode,
                                                                                    Email = x.EmailAddress
                                                                                }).FirstOrDefaultAsync();
                                                        if (Employee != null)
                                                        {
                                                            EmployeeName = Employee.Name;
                                                            EmployeeCode = Employee.AccountCode;
                                                            EmployeeEmail = Employee.Email;
                                                        }

                                                        Body += EmployeeName + "(" + EmployeeCode + "), ";
                                                    }

                                                    string? Department = "";

                                                    if (DepartmentId > 0)
                                                    {
                                                        Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        if (!string.IsNullOrEmpty(Department))
                                                        {
                                                            Body += Department + ", ";
                                                        }
                                                    }

                                                    Body += Division + ", " + Location + ", " + SubLocation + "</b>. The request has been sent for approval to the <b>Department Head</b> from <b>";

                                                    if (AssetIssueDetails.EmployeeId > 0)
                                                    {
                                                        var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                              .Select(x => new
                                                                              {
                                                                                  DisplayName = x.DisplayName,
                                                                                  AccountCode = x.AccountCode,
                                                                              }).FirstOrDefaultAsync();
                                                        if (EmployeeDetails != null)
                                                        {
                                                            Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                        }
                                                    }
                                                    if (AssetIssueDetails.DepartmentId > 0)
                                                    {
                                                        string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                        Body += DepartmentName + ", ";
                                                    }

                                                    string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                    string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                    Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                                    using (_HCoreContextLogging = new HCoreContextLogging())
                                                    {
                                                        _AtAssetHistory = new AtAssetHistory();
                                                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                        _AtAssetHistory.OrganizationId = OrganizationId;
                                                        _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                        _AtAssetHistory.Status = "Transfer Request";
                                                        _AtAssetHistory.Activity = Body;
                                                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                        _AtAssetHistory.UserId = Asset.CreatedBy;
                                                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                        await _HCoreContextLogging.SaveChangesAsync();
                                                    }
                                                }

                                                _AssetTransferDetail.CreatedById = Asset.CreatedBy;
                                                _AssetTransferDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetTransferDetails.AddAsync(_AssetTransferDetail);
                                                await _HCoreContext.SaveChangesAsync();

                                                Asset.Comment = "Processed Successfully.";
                                                Asset.Status = AssetStatus.Processed;
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Asset.Comment = "Failed to issue. Beacuse, this asset is not issued to anyone.";
                                        Asset.Status = AssetStatus.Failed;
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                }
                                else
                                {
                                    Asset.Comment = "Failed to issue. Beacuse, no asset found with barcode or rf id.";
                                    Asset.Status = AssetStatus.Failed;
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }
                        }

                        await _HCoreContext.DisposeAsync();
                    }
                }

                await _HCoreContextLogging.DisposeAsync();
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TrasnsferAsset-Mobile", _Exception, null);
            }
        }

        internal async Task MapAssetBarcodes()
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var Assets = await _HCoreContextLogging.AtAssetMappings.Where(x => x.Status == AssetStatus.InProcess).OrderByDescending(x => x.CreatedDate).ToListAsync();
                    if (Assets.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var _Assets = await _HCoreContextLogging.AtAssetMappings.Where(x => x.Status == AssetStatus.Processed).ToListAsync();
                            if (_Assets.Count > 0)
                            {
                                _HCoreContextLogging.AtAssetMappings.RemoveRange(_Assets);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                            foreach (var Asset in Assets)
                            {
                                var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.Asset).FirstOrDefaultAsync();
                                if (AssetDetails != null)
                                {
                                    if (!string.IsNullOrEmpty(AssetDetails.RfId) && !string.IsNullOrEmpty(AssetDetails.EpcId))
                                    {
                                        AssetDetails.RfId = Asset.RefId;
                                        AssetDetails.EpcId = Asset.EpcId;
                                        AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                        AssetDetails.ModifiedById = Asset.CreatedBy;
                                        await _HCoreContext.SaveChangesAsync();

                                        _AtAssetMappingLog = new AtAssetMappingLog();
                                        _AtAssetMappingLog.Guid = AssetDetails.Guid;
                                        _AtAssetMappingLog.Organization = Asset.Organization;
                                        _AtAssetMappingLog.Asset = AssetDetails.AssetBarcode;
                                        _AtAssetMappingLog.RefId = AssetDetails.RfId;
                                        _AtAssetMappingLog.EpcId = AssetDetails.EpcId;
                                        _AtAssetMappingLog.CreatedBy = Asset.CreatedBy;
                                        _AtAssetMappingLog.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContextLogging.AddAsync(_AtAssetMappingLog);
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                    else
                                    {
                                        AssetDetails.RfId = Asset.RefId;
                                        AssetDetails.EpcId = Asset.EpcId;
                                        AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                        AssetDetails.ModifiedById = Asset.CreatedBy;
                                        await _HCoreContext.SaveChangesAsync();

                                        _AtAssetMappingLog = new AtAssetMappingLog();
                                        _AtAssetMappingLog.Guid = AssetDetails.Guid;
                                        _AtAssetMappingLog.Organization = Asset.Organization;
                                        _AtAssetMappingLog.Asset = AssetDetails.AssetBarcode;
                                        _AtAssetMappingLog.RefId = AssetDetails.RfId;
                                        _AtAssetMappingLog.EpcId = AssetDetails.EpcId;
                                        _AtAssetMappingLog.CreatedBy = Asset.CreatedBy;
                                        _AtAssetMappingLog.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContextLogging.AddAsync(_AtAssetMappingLog);
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }
                                }
                                else
                                {
                                    Asset.Status = AssetStatus.Failed;
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }

                            await _HCoreContextLogging.DisposeAsync();
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("MapAssetBarcodes", _Exception, null);
            }
        }
    }
}
