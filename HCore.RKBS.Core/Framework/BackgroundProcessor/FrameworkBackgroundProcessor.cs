﻿using HCore.Data.Logging;
using HCore.Data.Logging.Models;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.BackgroundProcessor;
using HCore.RKBS.Core.Operations.BackgroundProcessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.RKBS.Core.Object.Assets.OAsset.Maintenance;

namespace HCore.RKBS.Core.Framework.BackgroundProcessor
{
    public class FrameworkBackgroundProcessor
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        AtClass? _AtClass;
        AtCategory? _AtCategory;
        AtSubCategory? _AtSubCategory;
        AtAccount? _AtAccount;
        AtAccountAuth? _AtAccountAuth;
        AtAssetRegister? _AtAssetRegister;
        AtAssetVendorDetail? _AtAssetVendorDetail;
        AtDivision? _AtDivision;
        AtLocation? _AtLocation;
        AtAssetHistory? _AtAssetHistory;
        AtAssetLocation? _AtAssetLocation;
        AtAssetDepreciationConfiguration? _AtAssetDepreciationConfiguration;
        AtAssetDepreciation? _AtAssetDepreciation;
        OBackgroundProcessor.Request? _AssetAuditRequest;
        AtAssetAmcDetail? _AtAssetAmcDetail;
        ManageBackgroundProcessor? _ManageBackgroundProcessor;

        internal async Task RegisterAssets()
        {
            try
            {
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    var Assets = await _HCoreContextLogging.AtAssetUploads.Where(x => x.StatusId == AssetStatus.InProcess).OrderByDescending(x => x.CreatedDate).ToListAsync();
                    if (Assets.Count > 0)
                    {
                        foreach (var Asset in Assets)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                bool _IsExist = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.AssetSerialNumber == Asset.AssetSerialNo);
                                if (!_IsExist)
                                {
                                    int? AssetTypeId = await _HCoreContext.AtCores.Where(x => x.Name == Asset.AssetType).Select(x => x.Id).FirstOrDefaultAsync();
                                    int? ClassId = await _HCoreContext.AtClasses.Where(x => x.ClassName == Asset.AssetClass && x.OrganizationId == Asset.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();

                                    int? CategoryId = 0;
                                    if (ClassId > 0)
                                    {
                                        CategoryId = await _HCoreContext.AtCategories.Where(x => x.CategoryName == Asset.AssetCategory && x.ClassId == ClassId).Select(x => x.Id).FirstOrDefaultAsync();
                                    }

                                    int? SubCategoryId = 0;
                                    if (CategoryId > 0)
                                    {
                                        SubCategoryId = await _HCoreContext.AtSubCategories.Where(x => x.SubCategoryName == Asset.AssetSubCategory && x.CategoryId == CategoryId).Select(x => x.Id).FirstOrDefaultAsync();
                                    }

                                    long? VendorId = await _HCoreContext.AtAccounts.Where(x => x.DisplayName == Asset.AssetVendor && x.OrganizationId == Asset.OrganizationId && (x.AccountTypeId == VendorType.InsuranceVendor || x.AccountTypeId == VendorType.AssetVendor)).Select(x => x.Id).FirstOrDefaultAsync();

                                    if (CategoryId > 0 && SubCategoryId < 1)
                                    {
                                        _AtSubCategory = new AtSubCategory();
                                        _AtSubCategory.Guid = HCoreHelper.GenerateGuid();
                                        _AtSubCategory.CategoryId = (int)CategoryId;
                                        _AtSubCategory.SubCategoryName = Asset.AssetSubCategory;
                                        _AtSubCategory.StatusId = HelperStatus.Default.Active;
                                        _AtSubCategory.CreatedById = (long)Asset.CreatedById;
                                        _AtSubCategory.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtSubCategories.AddAsync(_AtSubCategory);
                                        await _HCoreContext.SaveChangesAsync();

                                        SubCategoryId = _AtSubCategory.Id;
                                    }
                                    if (ClassId > 0 && CategoryId < 1)
                                    {
                                        _AtCategory = new AtCategory();
                                        _AtCategory.Guid = HCoreHelper.GenerateGuid();
                                        _AtCategory.ClassId = (int)ClassId;
                                        _AtCategory.OrganizationId = Asset.OrganizationId;
                                        _AtCategory.CategoryName = Asset.AssetCategory;
                                        _AtCategory.StatusId = HelperStatus.Default.Active;
                                        _AtCategory.CreatedById = (long)Asset.CreatedById;
                                        _AtCategory.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtCategories.AddAsync(_AtCategory);
                                        await _HCoreContext.SaveChangesAsync();
                                        CategoryId = _AtCategory.Id;

                                        _AtSubCategory = new AtSubCategory();
                                        _AtSubCategory.Guid = HCoreHelper.GenerateGuid();
                                        _AtSubCategory.CategoryId = (int)CategoryId;
                                        _AtSubCategory.SubCategoryName = Asset.AssetSubCategory;
                                        _AtSubCategory.StatusId = HelperStatus.Default.Active;
                                        _AtSubCategory.CreatedById = (long)Asset.CreatedById;
                                        _AtSubCategory.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtSubCategories.AddAsync(_AtSubCategory);
                                        await _HCoreContext.SaveChangesAsync();

                                        SubCategoryId = _AtSubCategory.Id;
                                    }
                                    if (ClassId < 1)
                                    {
                                        _AtClass = new AtClass();
                                        _AtClass.Guid = HCoreHelper.GenerateGuid();
                                        _AtClass.OrganizationId = Asset.OrganizationId;
                                        _AtClass.ClassName = Asset.AssetClass;
                                        _AtClass.StatusId = HelperStatus.Default.Active;
                                        _AtClass.CreatedById = (long)Asset.CreatedById;
                                        _AtClass.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtClasses.AddAsync(_AtClass);
                                        await _HCoreContext.SaveChangesAsync();
                                        ClassId = _AtClass.Id;

                                        _AtCategory = new AtCategory();
                                        _AtCategory.Guid = HCoreHelper.GenerateGuid();
                                        _AtCategory.ClassId = (int)ClassId;
                                        _AtCategory.OrganizationId = Asset.OrganizationId;
                                        _AtCategory.CategoryName = Asset.AssetCategory;
                                        _AtCategory.StatusId = HelperStatus.Default.Active;
                                        _AtCategory.CreatedById = (long)Asset.CreatedById;
                                        _AtCategory.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtCategories.AddAsync(_AtCategory);
                                        await _HCoreContext.SaveChangesAsync();
                                        CategoryId = _AtCategory.Id;

                                        _AtSubCategory = new AtSubCategory();
                                        _AtSubCategory.Guid = HCoreHelper.GenerateGuid();
                                        _AtSubCategory.CategoryId = (int)CategoryId;
                                        _AtSubCategory.SubCategoryName = Asset.AssetSubCategory;
                                        _AtSubCategory.StatusId = HelperStatus.Default.Active;
                                        _AtSubCategory.CreatedById = (long)Asset.CreatedById;
                                        _AtSubCategory.CreatedDate = HCoreHelper.GetDateTime();
                                        await _HCoreContext.AtSubCategories.AddAsync(_AtSubCategory);
                                        await _HCoreContext.SaveChangesAsync();

                                        SubCategoryId = _AtSubCategory.Id;
                                    }

                                    long? DivisionId = 0;
                                    long? LocationId = 0;
                                    long? SubLocationId = 0;
                                    if (!string.IsNullOrEmpty(Asset.Division))
                                    {
                                        DivisionId = await _HCoreContext.AtDivisions.Where(x => x.DivisionName == Asset.Division && x.OrganizationId == Asset.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                                        if (DivisionId == null || DivisionId < 1)
                                        {
                                            _AtDivision = new AtDivision();
                                            _AtDivision.Guid = HCoreHelper.GenerateGuid();
                                            _AtDivision.OrganizationId = (long)Asset.OrganizationId;
                                            _AtDivision.DivisionName = Asset.Division;
                                            _AtDivision.CreatedById = (long)Asset.CreatedById;
                                            _AtDivision.CreatedDate = HCoreHelper.GetDateTime();
                                            _AtDivision.StatusId = HelperStatus.Default.Active;
                                            await _HCoreContext.AtDivisions.AddAsync(_AtDivision);
                                            await _HCoreContext.SaveChangesAsync();

                                            DivisionId = _AtDivision.Id;
                                        }

                                        LocationId = await _HCoreContext.AtLocations.Where(x => x.LocationName == Asset.Location && x.OrganizationId == Asset.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                                        if (LocationId > 0)
                                        {
                                            SubLocationId = await _HCoreContext.AtLocations.Where(x => x.LocationName == Asset.SubLocation && x.ParentId == LocationId && x.OrganizationId == Asset.OrganizationId).Select(x => x.Id).FirstOrDefaultAsync();
                                            if (SubLocationId < 1)
                                            {
                                                _AtLocation = new AtLocation();
                                                _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                                _AtLocation.LocationName = Asset.Location;
                                                _AtLocation.ParentId = LocationId;
                                                _AtLocation.DivisionId = DivisionId;
                                                _AtLocation.OrganizationId = Asset.OrganizationId;
                                                _AtLocation.StatusId = HelperStatus.Default.Active;
                                                _AtLocation.CreatedById = (long)Asset.CreatedById;
                                                _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                                await _HCoreContext.SaveChangesAsync();

                                                SubLocationId = _AtLocation.Id;
                                            }
                                        }
                                        else
                                        {
                                            _AtLocation = new AtLocation();
                                            _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                            _AtLocation.LocationName = Asset.Location;
                                            _AtLocation.DivisionId = DivisionId;
                                            _AtLocation.OrganizationId = Asset.OrganizationId;
                                            _AtLocation.StatusId = HelperStatus.Default.Active;
                                            _AtLocation.CreatedById = (long)Asset.CreatedById;
                                            _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                            await _HCoreContext.SaveChangesAsync();

                                            LocationId = _AtLocation.Id;

                                            _AtLocation = new AtLocation();
                                            _AtLocation.Guid = HCoreHelper.GenerateGuid();
                                            _AtLocation.LocationName = Asset.SubLocation;
                                            _AtLocation.ParentId = LocationId;
                                            _AtLocation.DivisionId = DivisionId;
                                            _AtLocation.OrganizationId = Asset.OrganizationId;
                                            _AtLocation.StatusId = HelperStatus.Default.Active;
                                            _AtLocation.CreatedById = (long)Asset.CreatedById;
                                            _AtLocation.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtLocations.AddAsync(_AtLocation);
                                            await _HCoreContext.SaveChangesAsync();

                                            SubLocationId = _AtLocation.Id;
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(Asset.ExAssetId))
                                    {
                                        bool _IsPresent = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.ExAssetId == Asset.ExAssetId);
                                        if (_IsPresent)
                                        {
                                            Asset.StatusId = AssetStatus.Failed;
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                        else
                                        {
                                            string? AssetBarcode = "";
                                            string? SerialNo = "";
                                            SerialNo = await HCoreHelper.GenerateSerialNumber(Asset.OrganizationId);
                                            AssetBarcode = HCoreHelper.GenerateBarcode(SerialNo);

                                            bool _IsExists = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.AssetBarcode == AssetBarcode && !string.IsNullOrEmpty(x.AssetBarcode) && x.OrganizationId == Asset.OrganizationId);
                                            if (_IsExists)
                                            {
                                                SerialNo = await HCoreHelper.GenerateSerialNumber(Asset.OrganizationId);
                                                AssetBarcode = HCoreHelper.GenerateBarcode(SerialNo);

                                                _AtAssetRegister = new AtAssetRegister();
                                                _AtAssetRegister.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetRegister.OrganizationId = Asset.OrganizationId;
                                                _AtAssetRegister.AssetTypeId = (int)AssetTypeId;
                                                _AtAssetRegister.AssetClassId = (int)ClassId;
                                                _AtAssetRegister.AssetCategoryId = (int)CategoryId;
                                                _AtAssetRegister.AssetSubCategoryId = (int)SubCategoryId;
                                                _AtAssetRegister.AssetBarcode = AssetBarcode;
                                                _AtAssetRegister.SerialNumber = Convert.ToInt32(SerialNo);
                                                _AtAssetRegister.AssetSerialNumber = Asset.AssetSerialNo;
                                                _AtAssetRegister.AssetName = Asset.AssetName;
                                                _AtAssetRegister.ExAssetId = Asset.ExAssetId;
                                                _AtAssetRegister.AssetMake = Asset.AssetMake;
                                                _AtAssetRegister.AssetModel = Asset.AssetModel;
                                                _AtAssetRegister.TotalQuantity = Asset.TotalQuantity;
                                                _AtAssetRegister.RemainingQuantity = Asset.TotalQuantity;
                                                _AtAssetRegister.SalvageValue = Asset.SalvageValue;
                                                _AtAssetRegister.StatusId = AssetStatus.Created;
                                                _AtAssetRegister.CreatedById = (long)Asset.CreatedById;
                                                _AtAssetRegister.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetRegisters.AddAsync(_AtAssetRegister);
                                                await _HCoreContext.SaveChangesAsync();

                                                if (VendorId < 1 || VendorId == null)
                                                {
                                                    string AccountCode = HCoreHelper.GenerateAccountCode(6);
                                                    string[] FullName = Asset.AssetVendor.Split(" ");
                                                    string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                                                    if (FullName.Length == 0 || FullName.Length == 1)
                                                    {
                                                        FirstName = Asset.AssetVendor;
                                                    }
                                                    else if (FullName.Length == 2)
                                                    {
                                                        FirstName = FullName[0];
                                                        LastName = FullName[1];
                                                    }
                                                    else if (FullName.Length == 3)
                                                    {
                                                        FirstName = FullName[0];
                                                        LastName = FullName[2];
                                                    }

                                                    _AtAccount = new AtAccount();
                                                    _AtAccount.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAccount.AccountTypeId = VendorType.AssetVendor;
                                                    _AtAccount.OrganizationId = Asset.OrganizationId;
                                                    _AtAccount.DisplayName = Asset.AssetVendor;
                                                    _AtAccount.FirstName = FirstName;
                                                    _AtAccount.LastName = LastName;
                                                    _AtAccount.EmailAddress = Asset.AssetVendor.ToLower() + "@gmail.com";
                                                    _AtAccount.ContactNumber = "0000000000";
                                                    _AtAccount.AccountCode = AccountCode;
                                                    _AtAccount.CreatedById = (long)Asset.CreatedById;
                                                    _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                                                    _AtAccount.StatusId = HelperStatus.Default.Active;
                                                    _HCoreContext.AtAccounts.Add(_AtAccount);
                                                    await _HCoreContext.SaveChangesAsync();

                                                    _AtAccountAuth = new AtAccountAuth();
                                                    _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAccountAuth.UserId = _AtAccount.Id;
                                                    _AtAccountAuth.UserName = HCoreHelper.GenerateRandomString(24);
                                                    _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                                                    _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                                    _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                                    _AtAccountAuth.CreatedById = (long)Asset.CreatedById;
                                                    await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                                    await _HCoreContext.SaveChangesAsync();
                                                    VendorId = _AtAccount.Id;

                                                    _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                    _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                    _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                    _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                    _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                    _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                    _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                    _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                    _AtAssetVendorDetail.AdditionalPrice = Asset.AdditionalPrice;
                                                    _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                    _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                    _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                    _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                    _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                    await _HCoreContext.SaveChangesAsync();
                                                }
                                                else
                                                {
                                                    _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                    _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                    _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                    _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                    _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                    _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                    _AtAssetVendorDetail.ExpiresIn = Asset.ExpiresIn;
                                                    _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                    _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                    _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                    _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                    _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                    _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                    _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                    _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                    await _HCoreContext.SaveChangesAsync();
                                                }

                                                _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                                                _AssetAuditRequest = new OBackgroundProcessor.Request();
                                                _AssetAuditRequest.AssetId = _AtAssetRegister.Id;
                                                _AssetAuditRequest.AssetKey = _AtAssetRegister.Guid;
                                                await _ManageBackgroundProcessor.AssetAudit(_AssetAuditRequest);

                                                Asset.Comment = "Processed Successfully.";
                                                Asset.StatusId = AssetStatus.Processed;
                                                await _HCoreContextLogging.SaveChangesAsync();

                                                if (DivisionId > 0 && LocationId > 0 && SubLocationId > 0)
                                                {
                                                    _AtAssetLocation = new AtAssetLocation();
                                                    _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAssetLocation.AssetId = _AtAssetRegister.Id;
                                                    _AtAssetLocation.DivisionId = (long)DivisionId;
                                                    _AtAssetLocation.LocationId = (long)LocationId;
                                                    _AtAssetLocation.SubLocationId = (long)SubLocationId;
                                                    _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                                                    _AtAssetLocation.CreatedById = Asset.CreatedById;
                                                    _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                                                    await _HCoreContext.SaveChangesAsync();
                                                }

                                                string? Division = "";
                                                string? Location = "";
                                                string? SubLocation = "";
                                                Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                                string? Body = "";
                                                Body = "<div>";
                                                Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                                Body += "Location Details: -";
                                                Body += "</label>";
                                                Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                                Body += "<div>";
                                                Body += "Division: - {{Division}}";
                                                Body += "</div>";
                                                Body += "<div>";
                                                Body += "Location: - {{Location}}";
                                                Body += "</div>";
                                                Body += "<div>";
                                                Body += "Sub Location: - {{SubLocation}}";
                                                Body += "</div>";
                                                Body += "</div>";
                                                Body += "</div>";
                                                StringBuilder? Activty = new StringBuilder(Body);
                                                Activty.Replace("{{Division}}", Division);
                                                Activty.Replace("{{Location}}", Location);
                                                Activty.Replace("{{SubLocation}}", SubLocation);

                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.AssetBarcode = _AtAssetRegister.AssetBarcode;
                                                _AtAssetHistory.OrganizationId = _AtAssetRegister.OrganizationId;
                                                _AtAssetHistory.Status = "Registered";
                                                _AtAssetHistory.Activity = Activty.ToString();
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = Asset.CreatedById;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();

                                                await _HCoreContext.DisposeAsync();
                                            }
                                            else if (!_IsExists)
                                            {
                                                _AtAssetRegister = new AtAssetRegister();
                                                _AtAssetRegister.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetRegister.OrganizationId = Asset.OrganizationId;
                                                _AtAssetRegister.AssetTypeId = (int)AssetTypeId;
                                                _AtAssetRegister.AssetClassId = (int)ClassId;
                                                _AtAssetRegister.AssetCategoryId = (int)CategoryId;
                                                _AtAssetRegister.AssetSubCategoryId = (int)SubCategoryId;
                                                _AtAssetRegister.AssetBarcode = AssetBarcode;
                                                _AtAssetRegister.SerialNumber = Convert.ToInt32(SerialNo);
                                                _AtAssetRegister.AssetSerialNumber = Asset.AssetSerialNo;
                                                _AtAssetRegister.AssetName = Asset.AssetName;
                                                _AtAssetRegister.ExAssetId = Asset.ExAssetId;
                                                _AtAssetRegister.AssetMake = Asset.AssetMake;
                                                _AtAssetRegister.AssetModel = Asset.AssetModel;
                                                _AtAssetRegister.TotalQuantity = Asset.TotalQuantity;
                                                _AtAssetRegister.RemainingQuantity = Asset.TotalQuantity;
                                                _AtAssetRegister.SalvageValue = Asset.SalvageValue;
                                                _AtAssetRegister.StatusId = AssetStatus.Created;
                                                _AtAssetRegister.CreatedById = (long)Asset.CreatedById;
                                                _AtAssetRegister.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetRegisters.AddAsync(_AtAssetRegister);
                                                await _HCoreContext.SaveChangesAsync();

                                                if (VendorId < 1 || VendorId == null)
                                                {
                                                    string AccountCode = HCoreHelper.GenerateAccountCode(6);
                                                    string[] FullName = Asset.AssetVendor.Split(" ");
                                                    string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                                                    if (FullName.Length == 0 || FullName.Length == 1)
                                                    {
                                                        FirstName = Asset.AssetVendor;
                                                    }
                                                    else if (FullName.Length == 2)
                                                    {
                                                        FirstName = FullName[0];
                                                        LastName = FullName[1];
                                                    }
                                                    else if (FullName.Length == 3)
                                                    {
                                                        FirstName = FullName[0];
                                                        LastName = FullName[2];
                                                    }

                                                    _AtAccount = new AtAccount();
                                                    _AtAccount.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAccount.AccountTypeId = VendorType.AssetVendor;
                                                    _AtAccount.OrganizationId = Asset.OrganizationId;
                                                    _AtAccount.DisplayName = Asset.AssetVendor;
                                                    _AtAccount.FirstName = FirstName;
                                                    _AtAccount.LastName = LastName;
                                                    _AtAccount.EmailAddress = Asset.AssetVendor.ToLower() + "@gmail.com";
                                                    _AtAccount.ContactNumber = "0000000000";
                                                    _AtAccount.AccountCode = AccountCode;
                                                    _AtAccount.CreatedById = (long)Asset.CreatedById;
                                                    _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                                                    _AtAccount.StatusId = HelperStatus.Default.Active;
                                                    _HCoreContext.AtAccounts.Add(_AtAccount);
                                                    await _HCoreContext.SaveChangesAsync();

                                                    _AtAccountAuth = new AtAccountAuth();
                                                    _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAccountAuth.UserId = _AtAccount.Id;
                                                    _AtAccountAuth.UserName = HCoreHelper.GenerateRandomString(24);
                                                    _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                                                    _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                                    _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                                    _AtAccountAuth.CreatedById = (long)Asset.CreatedById;
                                                    await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                                    await _HCoreContext.SaveChangesAsync();
                                                    VendorId = _AtAccount.Id;

                                                    _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                    _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                    _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                    _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                    _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                    _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                    _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                    _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                    _AtAssetVendorDetail.AdditionalPrice = Asset.AdditionalPrice;
                                                    _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                    _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                    _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                    _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                    _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                    await _HCoreContext.SaveChangesAsync();
                                                }
                                                else
                                                {
                                                    _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                    _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                    _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                    _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                    _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                    _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                    _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                    _AtAssetVendorDetail.ExpiresIn = Asset.ExpiresIn;
                                                    _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                    _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                    _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                    _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                    _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                    _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                    _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                    _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                    await _HCoreContext.SaveChangesAsync();
                                                }

                                                _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                                                _AssetAuditRequest = new OBackgroundProcessor.Request();
                                                _AssetAuditRequest.AssetId = _AtAssetRegister.Id;
                                                _AssetAuditRequest.AssetKey = _AtAssetRegister.Guid;
                                                await _ManageBackgroundProcessor.AssetAudit(_AssetAuditRequest);

                                                Asset.Comment = "Processed Successfully.";
                                                Asset.StatusId = AssetStatus.Processed;
                                                await _HCoreContextLogging.SaveChangesAsync();

                                                if (DivisionId > 0 && LocationId > 0 && SubLocationId > 0)
                                                {
                                                    _AtAssetLocation = new AtAssetLocation();
                                                    _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                                                    _AtAssetLocation.AssetId = _AtAssetRegister.Id;
                                                    _AtAssetLocation.DivisionId = (long)DivisionId;
                                                    _AtAssetLocation.LocationId = (long)LocationId;
                                                    _AtAssetLocation.SubLocationId = (long)SubLocationId;
                                                    _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                                                    _AtAssetLocation.CreatedById = Asset.CreatedById;
                                                    _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                                                    await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                                                    await _HCoreContext.SaveChangesAsync();
                                                }

                                                string? Division = "";
                                                string? Location = "";
                                                string? SubLocation = "";
                                                Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                                Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                                SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                                string? Body = "";
                                                Body = "<div>";
                                                Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                                Body += "Location Details: -";
                                                Body += "</label>";
                                                Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                                Body += "<div>";
                                                Body += "Division: - {{Division}}";
                                                Body += "</div>";
                                                Body += "<div>";
                                                Body += "Location: - {{Location}}";
                                                Body += "</div>";
                                                Body += "<div>";
                                                Body += "Sub Location: - {{SubLocation}}";
                                                Body += "</div>";
                                                Body += "</div>";
                                                Body += "</div>";
                                                StringBuilder? Activty = new StringBuilder(Body);
                                                Activty.Replace("{{Division}}", Division);
                                                Activty.Replace("{{Location}}", Location);
                                                Activty.Replace("{{SubLocation}}", SubLocation);

                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.AssetBarcode = _AtAssetRegister.AssetBarcode;
                                                _AtAssetHistory.OrganizationId = _AtAssetRegister.OrganizationId;
                                                _AtAssetHistory.Status = "Registered";
                                                _AtAssetHistory.Activity = Activty.ToString();
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = Asset.CreatedById;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();

                                                await _HCoreContext.DisposeAsync();
                                            }
                                            else
                                            {
                                                Asset.StatusId = AssetStatus.Failed;
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string? AssetBarcode = "";
                                        string? SerialNo = "";
                                        SerialNo = await HCoreHelper.GenerateSerialNumber(Asset.OrganizationId);
                                        AssetBarcode = HCoreHelper.GenerateBarcode(SerialNo);

                                        bool _IsExists = await _HCoreContext.AtAssetRegisters.AnyAsync(x => x.AssetBarcode == AssetBarcode && !string.IsNullOrEmpty(x.AssetBarcode) && x.OrganizationId == Asset.OrganizationId);
                                        if (_IsExists)
                                        {
                                            SerialNo = await HCoreHelper.GenerateSerialNumber(Asset.OrganizationId);
                                            AssetBarcode = HCoreHelper.GenerateBarcode(SerialNo);

                                            _AtAssetRegister = new AtAssetRegister();
                                            _AtAssetRegister.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetRegister.OrganizationId = Asset.OrganizationId;
                                            _AtAssetRegister.AssetTypeId = (int)AssetTypeId;
                                            _AtAssetRegister.AssetClassId = (int)ClassId;
                                            _AtAssetRegister.AssetCategoryId = (int)CategoryId;
                                            _AtAssetRegister.AssetSubCategoryId = (int)SubCategoryId;
                                            _AtAssetRegister.AssetBarcode = AssetBarcode;
                                            _AtAssetRegister.SerialNumber = Convert.ToInt32(SerialNo);
                                            _AtAssetRegister.AssetSerialNumber = Asset.AssetSerialNo;
                                            _AtAssetRegister.AssetName = Asset.AssetName;
                                            _AtAssetRegister.ExAssetId = Asset.ExAssetId;
                                            _AtAssetRegister.AssetMake = Asset.AssetMake;
                                            _AtAssetRegister.AssetModel = Asset.AssetModel;
                                            _AtAssetRegister.TotalQuantity = Asset.TotalQuantity;
                                            _AtAssetRegister.RemainingQuantity = Asset.TotalQuantity;
                                            _AtAssetRegister.SalvageValue = Asset.SalvageValue;
                                            _AtAssetRegister.StatusId = AssetStatus.Created;
                                            _AtAssetRegister.CreatedById = (long)Asset.CreatedById;
                                            _AtAssetRegister.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtAssetRegisters.AddAsync(_AtAssetRegister);
                                            await _HCoreContext.SaveChangesAsync();

                                            if (VendorId < 1 || VendorId == null)
                                            {
                                                string AccountCode = HCoreHelper.GenerateAccountCode(6);
                                                string[] FullName = Asset.AssetVendor.Split(" ");
                                                string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                                                if (FullName.Length == 0 || FullName.Length == 1)
                                                {
                                                    FirstName = Asset.AssetVendor;
                                                }
                                                else if (FullName.Length == 2)
                                                {
                                                    FirstName = FullName[0];
                                                    LastName = FullName[1];
                                                }
                                                else if (FullName.Length == 3)
                                                {
                                                    FirstName = FullName[0];
                                                    LastName = FullName[2];
                                                }

                                                _AtAccount = new AtAccount();
                                                _AtAccount.Guid = HCoreHelper.GenerateGuid();
                                                _AtAccount.AccountTypeId = VendorType.AssetVendor;
                                                _AtAccount.OrganizationId = Asset.OrganizationId;
                                                _AtAccount.DisplayName = Asset.AssetVendor;
                                                _AtAccount.FirstName = FirstName;
                                                _AtAccount.LastName = LastName;
                                                _AtAccount.EmailAddress = Asset.AssetVendor.ToLower() + "@gmail.com";
                                                _AtAccount.ContactNumber = "0000000000";
                                                _AtAccount.AccountCode = AccountCode;
                                                _AtAccount.CreatedById = (long)Asset.CreatedById;
                                                _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                                                _AtAccount.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.AtAccounts.Add(_AtAccount);
                                                await _HCoreContext.SaveChangesAsync();

                                                _AtAccountAuth = new AtAccountAuth();
                                                _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                _AtAccountAuth.UserId = _AtAccount.Id;
                                                _AtAccountAuth.UserName = HCoreHelper.GenerateRandomString(24);
                                                _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                                                _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                                _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                                _AtAccountAuth.CreatedById = (long)Asset.CreatedById;
                                                await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                                await _HCoreContext.SaveChangesAsync();
                                                VendorId = _AtAccount.Id;

                                                _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                _AtAssetVendorDetail.AdditionalPrice = Asset.AdditionalPrice;
                                                _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                await _HCoreContext.SaveChangesAsync();
                                            }
                                            else
                                            {
                                                _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                _AtAssetVendorDetail.ExpiresIn = Asset.ExpiresIn;
                                                _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                await _HCoreContext.SaveChangesAsync();
                                            }

                                            _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                                            _AssetAuditRequest = new OBackgroundProcessor.Request();
                                            _AssetAuditRequest.AssetId = _AtAssetRegister.Id;
                                            _AssetAuditRequest.AssetKey = _AtAssetRegister.Guid;
                                            await _ManageBackgroundProcessor.AssetAudit(_AssetAuditRequest);

                                            Asset.StatusId = AssetStatus.Processed;
                                            await _HCoreContextLogging.SaveChangesAsync();

                                            if (DivisionId > 0 && LocationId > 0 && SubLocationId > 0)
                                            {
                                                _AtAssetLocation = new AtAssetLocation();
                                                _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetLocation.AssetId = _AtAssetRegister.Id;
                                                _AtAssetLocation.DivisionId = (long)DivisionId;
                                                _AtAssetLocation.LocationId = (long)LocationId;
                                                _AtAssetLocation.SubLocationId = (long)SubLocationId;
                                                _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                                                _AtAssetLocation.CreatedById = Asset.CreatedById;
                                                _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                                                await _HCoreContext.SaveChangesAsync();
                                            }

                                            string? Division = "";
                                            string? Location = "";
                                            string? SubLocation = "";
                                            Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "";
                                            Body = "<div>";
                                            Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                            Body += "Location Details: -";
                                            Body += "</label>";
                                            Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                            Body += "<div>";
                                            Body += "Division: - {{Division}}";
                                            Body += "</div>";
                                            Body += "<div>";
                                            Body += "Location: - {{Location}}";
                                            Body += "</div>";
                                            Body += "<div>";
                                            Body += "Sub Location: - {{SubLocation}}";
                                            Body += "</div>";
                                            Body += "</div>";
                                            Body += "</div>";
                                            StringBuilder? Activty = new StringBuilder(Body);
                                            Activty.Replace("{{Division}}", Division);
                                            Activty.Replace("{{Location}}", Location);
                                            Activty.Replace("{{SubLocation}}", SubLocation);

                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.AssetBarcode = _AtAssetRegister.AssetBarcode;
                                            _AtAssetHistory.OrganizationId = _AtAssetRegister.OrganizationId;
                                            _AtAssetHistory.Status = "Registered";
                                            _AtAssetHistory.Activity = Activty.ToString();
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = Asset.CreatedById;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();

                                            await _HCoreContext.DisposeAsync();
                                        }
                                        else if (!_IsExists)
                                        {
                                            SerialNo = await HCoreHelper.GenerateSerialNumber(Asset.OrganizationId);
                                            AssetBarcode = HCoreHelper.GenerateBarcode(SerialNo);

                                            _AtAssetRegister = new AtAssetRegister();
                                            _AtAssetRegister.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetRegister.OrganizationId = Asset.OrganizationId;
                                            _AtAssetRegister.AssetTypeId = (int)AssetTypeId;
                                            _AtAssetRegister.AssetClassId = (int)ClassId;
                                            _AtAssetRegister.AssetCategoryId = (int)CategoryId;
                                            _AtAssetRegister.AssetSubCategoryId = (int)SubCategoryId;
                                            _AtAssetRegister.AssetBarcode = AssetBarcode;
                                            _AtAssetRegister.SerialNumber = Convert.ToInt32(SerialNo);
                                            _AtAssetRegister.AssetSerialNumber = Asset.AssetSerialNo;
                                            _AtAssetRegister.AssetName = Asset.AssetName;
                                            _AtAssetRegister.ExAssetId = Asset.ExAssetId;
                                            _AtAssetRegister.AssetMake = Asset.AssetMake;
                                            _AtAssetRegister.AssetModel = Asset.AssetModel;
                                            _AtAssetRegister.TotalQuantity = Asset.TotalQuantity;
                                            _AtAssetRegister.RemainingQuantity = Asset.TotalQuantity;
                                            _AtAssetRegister.SalvageValue = Asset.SalvageValue;
                                            _AtAssetRegister.StatusId = AssetStatus.Created;
                                            _AtAssetRegister.CreatedById = (long)Asset.CreatedById;
                                            _AtAssetRegister.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtAssetRegisters.AddAsync(_AtAssetRegister);
                                            await _HCoreContext.SaveChangesAsync();

                                            if (VendorId < 1 || VendorId == null)
                                            {
                                                string AccountCode = HCoreHelper.GenerateAccountCode(6);
                                                string[] FullName = Asset.AssetVendor.Split(" ");
                                                string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                                                if (FullName.Length == 0 || FullName.Length == 1)
                                                {
                                                    FirstName = Asset.AssetVendor;
                                                }
                                                else if (FullName.Length == 2)
                                                {
                                                    FirstName = FullName[0];
                                                    LastName = FullName[1];
                                                }
                                                else if (FullName.Length == 3)
                                                {
                                                    FirstName = FullName[0];
                                                    LastName = FullName[2];
                                                }

                                                _AtAccount = new AtAccount();
                                                _AtAccount.Guid = HCoreHelper.GenerateGuid();
                                                _AtAccount.AccountTypeId = VendorType.AssetVendor;
                                                _AtAccount.OrganizationId = Asset.OrganizationId;
                                                _AtAccount.DisplayName = Asset.AssetVendor;
                                                _AtAccount.FirstName = FirstName;
                                                _AtAccount.LastName = LastName;
                                                _AtAccount.EmailAddress = Asset.AssetVendor.ToLower() + "@gmail.com";
                                                _AtAccount.ContactNumber = "0000000000";
                                                _AtAccount.AccountCode = AccountCode;
                                                _AtAccount.CreatedById = (long)Asset.CreatedById;
                                                _AtAccount.CreatedDate = HCoreHelper.GetDateTime();
                                                _AtAccount.StatusId = HelperStatus.Default.Active;
                                                _HCoreContext.AtAccounts.Add(_AtAccount);
                                                await _HCoreContext.SaveChangesAsync();

                                                _AtAccountAuth = new AtAccountAuth();
                                                _AtAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                                _AtAccountAuth.UserId = _AtAccount.Id;
                                                _AtAccountAuth.UserName = HCoreHelper.GenerateRandomString(24);
                                                _AtAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                                                _AtAccountAuth.CreatedDate = HCoreHelper.GetDateTime();
                                                _AtAccountAuth.StatusId = HelperStatus.Default.Active;
                                                _AtAccountAuth.CreatedById = (long)Asset.CreatedById;
                                                await _HCoreContext.AtAccountAuths.AddAsync(_AtAccountAuth);
                                                await _HCoreContext.SaveChangesAsync();
                                                VendorId = _AtAccount.Id;

                                                _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                _AtAssetVendorDetail.AdditionalPrice = Asset.AdditionalPrice;
                                                _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                await _HCoreContext.SaveChangesAsync();
                                            }
                                            else
                                            {
                                                _AtAssetVendorDetail = new AtAssetVendorDetail();
                                                _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                                                _AtAssetVendorDetail.VendorId = (long)VendorId;
                                                _AtAssetVendorDetail.PoNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.InvoiceNumber = Asset.PoNumber;
                                                _AtAssetVendorDetail.RecievedDate = Asset.ReceivedDate;
                                                _AtAssetVendorDetail.WarrantyStartDate = Asset.StartDate;
                                                _AtAssetVendorDetail.WarrantyEndDate = Asset.EndDate;
                                                _AtAssetVendorDetail.ExpiresIn = Asset.ExpiresIn;
                                                _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                _AtAssetVendorDetail.ExpiryDate = Asset.ExpiryDate;
                                                _AtAssetVendorDetail.PurchasePrice = Asset.PurchasePrice;
                                                _AtAssetVendorDetail.IsAmcApplicable = Asset.IsAmc;
                                                _AtAssetVendorDetail.IsInsurance = Asset.IsInsurance;
                                                _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                                                _AtAssetVendorDetail.CreatedById = (long)Asset.CreatedById;
                                                _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                                                await _HCoreContext.SaveChangesAsync();
                                            }

                                            _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                                            _AssetAuditRequest = new OBackgroundProcessor.Request();
                                            _AssetAuditRequest.AssetId = _AtAssetRegister.Id;
                                            _AssetAuditRequest.AssetKey = _AtAssetRegister.Guid;
                                            await _ManageBackgroundProcessor.AssetAudit(_AssetAuditRequest);

                                            Asset.StatusId = AssetStatus.Processed;
                                            await _HCoreContextLogging.SaveChangesAsync();

                                            if (DivisionId > 0 && LocationId > 0 && SubLocationId > 0)
                                            {
                                                _AtAssetLocation = new AtAssetLocation();
                                                _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetLocation.AssetId = _AtAssetRegister.Id;
                                                _AtAssetLocation.DivisionId = (long)DivisionId;
                                                _AtAssetLocation.LocationId = (long)LocationId;
                                                _AtAssetLocation.SubLocationId = (long)SubLocationId;
                                                _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                                                _AtAssetLocation.CreatedById = Asset.CreatedById;
                                                _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                                                await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                                                await _HCoreContext.SaveChangesAsync();
                                            }

                                            string? Division = "";
                                            string? Location = "";
                                            string? SubLocation = "";
                                            Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "";
                                            Body = "<div>";
                                            Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                            Body += "Location Details: -";
                                            Body += "</label>";
                                            Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                                            Body += "<div>";
                                            Body += "Division: - {{Division}}";
                                            Body += "</div>";
                                            Body += "<div>";
                                            Body += "Location: - {{Location}}";
                                            Body += "</div>";
                                            Body += "<div>";
                                            Body += "Sub Location: - {{SubLocation}}";
                                            Body += "</div>";
                                            Body += "</div>";
                                            Body += "</div>";
                                            StringBuilder? Activty = new StringBuilder(Body);
                                            Activty.Replace("{{Division}}", Division);
                                            Activty.Replace("{{Location}}", Location);
                                            Activty.Replace("{{SubLocation}}", SubLocation);

                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.AssetBarcode = _AtAssetRegister.AssetBarcode;
                                            _AtAssetHistory.OrganizationId = _AtAssetRegister.OrganizationId;
                                            _AtAssetHistory.Status = "Registered";
                                            _AtAssetHistory.Activity = Activty.ToString();
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = Asset.CreatedById;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();

                                            await _HCoreContext.DisposeAsync();
                                        }
                                        else
                                        {
                                            Asset.Comment = "Failed to upload this asset because an asset is already present in the system with the same SAP code";
                                            Asset.StatusId = AssetStatus.Failed;
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                }
                                else
                                {
                                    Asset.Comment = "Failed to upload this asset because an asset is already present in the system with the same serial number";
                                    Asset.StatusId = AssetStatus.Failed;
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }
                        }

                        await _HCoreContextLogging.DisposeAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RegisterAssets", _Exception, null);
            }
        }

        internal async Task UpdateSession()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Sessions = await _HCoreContext.AtAccountSessions.Where(x => x.StatusId == HelperStatus.Default.Active).ToListAsync();
                    if (Sessions.Count > 0)
                    {
                        foreach (var Session in Sessions)
                        {
                            Session.StatusId = HelperStatus.Default.Inactive;
                            Session.ModifyDate = HCoreHelper.GetDateTime();
                        }

                        await _HCoreContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSession", _Exception, null);
            }
        }

        internal async Task ExpireAsset()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime? ActiveDate = HCoreHelper.GetDateTime();
                    var Assets = await _HCoreContext.AtAssetVendorDetails.Where(x => x.StatusId != AssetStatus.Expired && x.ExpiryDate.Value.Date.Date <= ActiveDate.Value.Date.Date).ToListAsync();
                    if (Assets.Count > 0)
                    {
                        foreach (var Asset in Assets)
                        {
                            var _Asset = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == Asset.AssetId).FirstOrDefaultAsync();
                            Asset.StatusId = AssetStatus.Expired;
                            _Asset.StatusId = AssetStatus.Expired;
                        }

                        await _HCoreContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ExpireAsset", _Exception);
            }
        }

        internal async Task ExpireAssetWarrenty()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime? ActiveDate = HCoreHelper.GetDateTime();
                    var Insurances = await _HCoreContext.AtAssetInsuranceDetails.Where(x => x.StatusId != AssetStatus.WarrentyExpired && x.EndDate.Date.Date <= ActiveDate.Value.Date.Date).ToListAsync();
                    if (Insurances.Count > 0)
                    {
                        foreach (var Insurance in Insurances)
                        {
                            var AssetVendor = await _HCoreContext.AtAssetVendorDetails.Where(x => x.Id == Insurance.VendorDetailsId).FirstOrDefaultAsync();
                            var _Asset = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == AssetVendor.AssetId).FirstOrDefaultAsync();
                            Insurance.StatusId = AssetStatus.WarrentyExpired;
                            _Asset.StatusId = AssetStatus.WarrentyExpired;
                        }

                        await _HCoreContext.SaveChangesAsync();
                    }
                    var Amcs = await _HCoreContext.AtAssetAmcDetails.Where(x => x.StatusId != AssetStatus.WarrentyExpired && x.EndDate.Date.Date <= ActiveDate.Value.Date.Date).ToListAsync();
                    if (Amcs.Count > 0)
                    {
                        foreach (var Amc in Amcs)
                        {
                            var AssetVendor = await _HCoreContext.AtAssetVendorDetails.Where(x => x.Id == Amc.VendorDetailsId).FirstOrDefaultAsync();
                            var _Asset = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == AssetVendor.AssetId).FirstOrDefaultAsync();
                            Amc.StatusId = AssetStatus.WarrentyExpired;
                            _Asset.StatusId = AssetStatus.WarrentyExpired;
                        }

                        await _HCoreContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ExpireAsset", _Exception);
            }
        }

        internal async Task AssetAudit(OBackgroundProcessor.Request Asset)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == Asset.AssetId && x.Guid == Asset.AssetKey).FirstOrDefaultAsync();
                    if (AssetDetails != null)
                    {
                        var AssetDepreciationsConfigs = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == AssetDetails.Id).ToListAsync();
                        if (AssetDepreciationsConfigs.Count > 0)
                        {
                            _HCoreContext.AtAssetDepreciationConfigurations.RemoveRange(AssetDepreciationsConfigs);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var AssetDepreciations = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.Id).ToListAsync();
                        if (AssetDepreciations.Count > 0)
                        {
                            _HCoreContext.AtAssetDepreciations.RemoveRange(AssetDepreciations);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var AssetVendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == AssetDetails.Id)
                                                 .Select(x => new
                                                 {
                                                     PurchasePrice = x.PurchasePrice,
                                                     AdditionalPrice = x.AdditionalPrice,
                                                     ExpiresIn = x.ExpiresIn,
                                                     RecievedDate = x.RecievedDate,
                                                     ExpiryDate = x.ExpiryDate,
                                                 }).FirstOrDefaultAsync();

                        if (AssetVendorDetails != null)
                        {
                            if(AssetVendorDetails.RecievedDate != null)
                            {
                                int? DepreciationMethodId = await _HCoreContext.AtDepreciationConfigurations.Where(x => x.AccountId == AssetDetails.OrganizationId).Select(x => x.DepreciationMethodId).FirstOrDefaultAsync();

                                if (DepreciationMethodId > 0 && DepreciationMethodId != null)
                                {
                                    decimal? CostOfAcquisition = 0;
                                    decimal? DepreciationExpense = 0;
                                    decimal? DepreciationRate = 0;
                                    decimal? AccumulatedDepreciation = 0;
                                    decimal? BookValue = 0;
                                    decimal? NetBookValue = 0;
                                    decimal? AssetValue = 0;
                                    decimal? LastAssetValue = 0;
                                    decimal? LastDepreciationExpense = 0;
                                    decimal? LastAccumulatedDepreciation = 0;
                                    decimal? LastBookValue = 0;

                                    CostOfAcquisition = AssetVendorDetails.PurchasePrice - AssetVendorDetails.AdditionalPrice;

                                    if (DepreciationMethodId == HCoreConstant.Helpers.Depreciation_Methods.Straight_line)
                                    {
                                        decimal? ResaleValue = ((AssetDetails.SalvageValue / AssetVendorDetails.PurchasePrice) * 100);
                                        DepreciationRate = ((100 - ResaleValue)) / AssetVendorDetails.ExpiresIn;
                                        DepreciationExpense = ((AssetVendorDetails.PurchasePrice - AssetDetails.SalvageValue) / AssetVendorDetails.ExpiresIn); // (AssetVendorDetails.PurchasePrice * DepreciationRate) / AssetVendorDetails.ExpiresIn
                                        AccumulatedDepreciation = DepreciationExpense;
                                        BookValue = (AssetVendorDetails.PurchasePrice - DepreciationExpense);
                                        NetBookValue = (AssetVendorDetails.PurchasePrice - AccumulatedDepreciation);
                                    }

                                    if (DepreciationMethodId == HCoreConstant.Helpers.Depreciation_Methods.Written_Down_Value)
                                    {
                                        double Ratio = (double)(AssetDetails.SalvageValue / AssetVendorDetails.PurchasePrice);
                                        double Power = (double)(1.0 / AssetVendorDetails.ExpiresIn);
                                        DepreciationRate = (decimal?)((1 - Math.Pow(Ratio, Power)) * 100.0);
                                        DepreciationExpense = ((AssetVendorDetails.PurchasePrice / AssetVendorDetails.ExpiresIn) * DepreciationRate);
                                        AccumulatedDepreciation = DepreciationExpense;
                                        BookValue = (AssetVendorDetails.PurchasePrice - DepreciationExpense);
                                        NetBookValue = (AssetVendorDetails.PurchasePrice - AccumulatedDepreciation);
                                    }

                                    _AtAssetDepreciationConfiguration = new AtAssetDepreciationConfiguration();
                                    _AtAssetDepreciationConfiguration.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetDepreciationConfiguration.OrganizationId = (long)AssetDetails.OrganizationId;
                                    _AtAssetDepreciationConfiguration.DepreciationMethodId = (int)DepreciationMethodId;
                                    _AtAssetDepreciationConfiguration.AssetId = AssetDetails.Id;
                                    _AtAssetDepreciationConfiguration.AssetPurchaseDate = AssetVendorDetails.RecievedDate;
                                    _AtAssetDepreciationConfiguration.CostOfAcquisition = CostOfAcquisition;
                                    _AtAssetDepreciationConfiguration.UsefulLife = AssetVendorDetails.ExpiresIn;
                                    _AtAssetDepreciationConfiguration.SalvageValue = AssetDetails.SalvageValue;
                                    _AtAssetDepreciationConfiguration.DepreciationRate = DepreciationRate;
                                    _AtAssetDepreciationConfiguration.AccumulatedDepreciation = AccumulatedDepreciation;
                                    _AtAssetDepreciationConfiguration.BookValue = BookValue;
                                    _AtAssetDepreciationConfiguration.DepreciationExpense = DepreciationExpense;
                                    _AtAssetDepreciationConfiguration.NetBookValue = NetBookValue;
                                    _AtAssetDepreciationConfiguration.DepreciationStartDate = AssetVendorDetails.RecievedDate;
                                    _AtAssetDepreciationConfiguration.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                    _AtAssetDepreciationConfiguration.CreatedDate = HCoreHelper.GetDateTime();
                                    _AtAssetDepreciationConfiguration.CreatedById = AssetDetails.CreatedById;
                                    await _HCoreContext.AtAssetDepreciationConfigurations.AddAsync(_AtAssetDepreciationConfiguration);
                                    await _HCoreContext.SaveChangesAsync();

                                    DateTime StartDate = (DateTime)AssetVendorDetails.RecievedDate;
                                    DateTime EndDate = (DateTime)AssetVendorDetails.ExpiryDate;
                                    TimeSpan Difference = EndDate - StartDate;

                                    int UsefulLife = Difference.Days + 1;

                                    if (DepreciationMethodId == HCoreConstant.Helpers.Depreciation_Methods.Straight_line)
                                    {
                                        DepreciationExpense = ((AssetVendorDetails.PurchasePrice - AssetDetails.SalvageValue) / UsefulLife);
                                        AccumulatedDepreciation = DepreciationExpense;
                                        BookValue = (AssetVendorDetails.PurchasePrice - DepreciationExpense);
                                        AssetValue = BookValue;

                                        _AtAssetDepreciation = new AtAssetDepreciation();
                                        _AtAssetDepreciation.Guid = HCoreHelper.GenerateGuid();
                                        _AtAssetDepreciation.DepreciationTypeId = HCoreConstant.Helpers.DepreciationTypes.Daily;
                                        _AtAssetDepreciation.OrganizationId = (long)AssetDetails.OrganizationId;
                                        _AtAssetDepreciation.AssetId = AssetDetails.Id;
                                        _AtAssetDepreciation.Year = StartDate.Year;
                                        _AtAssetDepreciation.Month = StartDate.Month;
                                        _AtAssetDepreciation.Date = StartDate;
                                        _AtAssetDepreciation.AssetValue = AssetValue;
                                        _AtAssetDepreciation.DepreciationExpense = DepreciationExpense;
                                        _AtAssetDepreciation.AccumulatedDepreciation = AccumulatedDepreciation;
                                        _AtAssetDepreciation.OpeningBookValue = AssetVendorDetails.PurchasePrice;
                                        _AtAssetDepreciation.BookValue = BookValue;
                                        _AtAssetDepreciation.StatusId = HelperStatus.Default.Active;
                                        _AtAssetDepreciation.CreatedDate = HCoreHelper.GetDateTime();
                                        _AtAssetDepreciation.CreatedById = AssetDetails.CreatedById;
                                        await _HCoreContext.AtAssetDepreciations.AddAsync(_AtAssetDepreciation);
                                        await _HCoreContext.SaveChangesAsync();

                                        LastAssetValue = DepreciationExpense;
                                        LastDepreciationExpense = _AtAssetDepreciation.DepreciationExpense;
                                        LastAccumulatedDepreciation = _AtAssetDepreciation.AccumulatedDepreciation;
                                        LastBookValue = _AtAssetDepreciation.BookValue;

                                        for (int i = 1; i <= UsefulLife - 1; i++)
                                        {
                                            AccumulatedDepreciation = (LastDepreciationExpense + LastAccumulatedDepreciation);
                                            BookValue = (LastBookValue - LastDepreciationExpense);
                                            AssetValue = BookValue;

                                            _AtAssetDepreciation = new AtAssetDepreciation();
                                            _AtAssetDepreciation.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetDepreciation.DepreciationTypeId = HCoreConstant.Helpers.DepreciationTypes.Daily;
                                            _AtAssetDepreciation.OrganizationId = (long)AssetDetails.OrganizationId;
                                            _AtAssetDepreciation.AssetId = AssetDetails.Id;
                                            _AtAssetDepreciation.Year = StartDate.AddDays(i).Year;
                                            _AtAssetDepreciation.Month = StartDate.AddDays(i).Month;
                                            _AtAssetDepreciation.Date = StartDate.AddDays(i);
                                            _AtAssetDepreciation.AssetValue = AssetValue;
                                            _AtAssetDepreciation.DepreciationExpense = DepreciationExpense;
                                            _AtAssetDepreciation.AccumulatedDepreciation = AccumulatedDepreciation;
                                            _AtAssetDepreciation.OpeningBookValue = LastBookValue;
                                            _AtAssetDepreciation.BookValue = BookValue;
                                            _AtAssetDepreciation.StatusId = HelperStatus.Default.Active;
                                            _AtAssetDepreciation.CreatedDate = HCoreHelper.GetDateTime();
                                            _AtAssetDepreciation.CreatedById = AssetDetails.CreatedById;
                                            await _HCoreContext.AtAssetDepreciations.AddAsync(_AtAssetDepreciation);

                                            LastAssetValue = AssetValue;
                                            LastDepreciationExpense = _AtAssetDepreciation.DepreciationExpense;
                                            LastAccumulatedDepreciation = _AtAssetDepreciation.AccumulatedDepreciation;
                                            LastBookValue = _AtAssetDepreciation.BookValue;
                                        }

                                        await _HCoreContext.SaveChangesAsync();
                                    }

                                    if (DepreciationMethodId == HCoreConstant.Helpers.Depreciation_Methods.Written_Down_Value)
                                    {
                                        double Ratio = (double)(AssetDetails.SalvageValue / AssetVendorDetails.PurchasePrice);
                                        double Power = (double)(1.0 / UsefulLife);
                                        DepreciationRate = (decimal?)((1 - Math.Pow(Ratio, Power)) * 100.0);
                                        DepreciationExpense = ((AssetVendorDetails.PurchasePrice / UsefulLife) * DepreciationRate);
                                        AccumulatedDepreciation = DepreciationExpense;
                                        BookValue = (AssetVendorDetails.PurchasePrice - DepreciationExpense);

                                        _AtAssetDepreciation = new AtAssetDepreciation();
                                        _AtAssetDepreciation.Guid = HCoreHelper.GenerateGuid();
                                        _AtAssetDepreciation.DepreciationTypeId = HCoreConstant.Helpers.DepreciationTypes.Daily;
                                        _AtAssetDepreciation.OrganizationId = (long)AssetDetails.OrganizationId;
                                        _AtAssetDepreciation.AssetId = AssetDetails.Id;
                                        _AtAssetDepreciation.Year = StartDate.Year;
                                        _AtAssetDepreciation.Month = StartDate.Month;
                                        _AtAssetDepreciation.Date = StartDate;
                                        _AtAssetDepreciation.AssetValue = AssetValue;
                                        _AtAssetDepreciation.DepreciationExpense = DepreciationExpense;
                                        _AtAssetDepreciation.AccumulatedDepreciation = AccumulatedDepreciation;
                                        _AtAssetDepreciation.OpeningBookValue = AssetVendorDetails.PurchasePrice;
                                        _AtAssetDepreciation.BookValue = BookValue;
                                        _AtAssetDepreciation.StatusId = HelperStatus.Default.Active;
                                        _AtAssetDepreciation.CreatedDate = HCoreHelper.GetDateTime();
                                        _AtAssetDepreciation.CreatedById = AssetDetails.CreatedById;
                                        await _HCoreContext.AtAssetDepreciations.AddAsync(_AtAssetDepreciation);
                                        await _HCoreContext.SaveChangesAsync();

                                        LastAssetValue = AssetValue;
                                        LastDepreciationExpense = _AtAssetDepreciation.DepreciationExpense;
                                        LastAccumulatedDepreciation = _AtAssetDepreciation.AccumulatedDepreciation;
                                        LastBookValue = _AtAssetDepreciation.BookValue;

                                        for (int i = 1; i <= UsefulLife - 1; i++)
                                        {
                                            DepreciationExpense = ((LastAssetValue / UsefulLife) * DepreciationRate);
                                            AccumulatedDepreciation = (LastAssetValue + LastAccumulatedDepreciation);
                                            BookValue = (LastAssetValue - DepreciationExpense);

                                            _AtAssetDepreciation = new AtAssetDepreciation();
                                            _AtAssetDepreciation.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetDepreciation.DepreciationTypeId = HCoreConstant.Helpers.DepreciationTypes.Daily;
                                            _AtAssetDepreciation.OrganizationId = (long)AssetDetails.OrganizationId;
                                            _AtAssetDepreciation.AssetId = AssetDetails.Id;
                                            _AtAssetDepreciation.Year = StartDate.AddDays(i).Year;
                                            _AtAssetDepreciation.Month = StartDate.AddDays(i).Month;
                                            _AtAssetDepreciation.Date = StartDate.AddDays(i);
                                            _AtAssetDepreciation.AssetValue = AssetValue;
                                            _AtAssetDepreciation.DepreciationExpense = DepreciationExpense;
                                            _AtAssetDepreciation.AccumulatedDepreciation = AccumulatedDepreciation;
                                            _AtAssetDepreciation.OpeningBookValue = LastBookValue;
                                            _AtAssetDepreciation.BookValue = BookValue;
                                            _AtAssetDepreciation.StatusId = HelperStatus.Default.Active;
                                            _AtAssetDepreciation.CreatedDate = HCoreHelper.GetDateTime();
                                            _AtAssetDepreciation.CreatedById = AssetDetails.CreatedById;
                                            await _HCoreContext.AtAssetDepreciations.AddAsync(_AtAssetDepreciation);

                                            LastAssetValue = AssetValue;
                                            LastDepreciationExpense = _AtAssetDepreciation.DepreciationExpense;
                                            LastAccumulatedDepreciation = _AtAssetDepreciation.AccumulatedDepreciation;
                                            LastBookValue = _AtAssetDepreciation.BookValue;
                                        }

                                        await _HCoreContext.SaveChangesAsync();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AssetAudit", _Exception, null);
            }
        }
    }
}
