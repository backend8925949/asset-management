﻿using HCore.Data.Logging;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Assets;
using HCore.RKBS.Core.Operations.Assets;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Assets
{
    public class FrameworkAssetReport
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        OAssetReport.AssetsOverview? _AssetsOverview;
        OAssetReport.AssetsOverview? _Overview;
        ManageDownload? _ManageDownload;

        internal async Task<OResponse> GetAssetsOverview(OReference _Request)
        {
            try
            {
                _AssetsOverview = new OAssetReport.AssetsOverview();
                _Overview = new OAssetReport.AssetsOverview();
                OAssetReport.AssetsReport? _AssetReport = new OAssetReport.AssetsReport();
                OAssetReport.AMCInsuranceReport? _AMCInsuranceReport = new OAssetReport.AMCInsuranceReport();
                OAssetReport.WarrentyReport? _WarrentyReport = new OAssetReport.WarrentyReport();
                using (_HCoreContext = new HCoreContext())
                {
                    _AssetsOverview.TotalAssets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed).SumAsync(x => x.TotalQuantity);
                    _AssetsOverview.IssuedAssets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Issued).SumAsync(x => x.Quantity);
                    _AssetsOverview.ReturnedAssets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Returned).SumAsync(x => x.Quantity);
                    _AssetsOverview.TransferedAssets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Transfered).SumAsync(x => x.Quantity);
                    _AssetsOverview.DisposedAssets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Disposed).SumAsync(x => x.TotalQuantity);
                    _AssetsOverview.ExpiredAssets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Expired).SumAsync(x => x.TotalQuantity);
                    _AssetsOverview.UnIssuedAssets = _AssetsOverview.TotalAssets - _AssetsOverview.IssuedAssets;

                    _Overview.IssuedAssets = _AssetsOverview.IssuedAssets;
                    _Overview.UnIssuedAssets = _AssetsOverview.UnIssuedAssets;
                    _Overview.ReturnedAssets = _AssetsOverview.ReturnedAssets;
                    _Overview.TransferedAssets = _AssetsOverview.TransferedAssets;
                    _Overview.DisposedAssets = _AssetsOverview.DisposedAssets;
                    _Overview.ExpiredAssets = _AssetsOverview.ExpiredAssets;

                    _AssetsOverview.Overview = _Overview;

                    _AssetReport.WithBarcode = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetTypeId == AssetType.WithBarcode && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).SumAsync(x => x.TotalQuantity);
                    _AssetReport.WithoutBarcode = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetTypeId == AssetType.WithoutBarcode && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).SumAsync(x => x.TotalQuantity);

                    _AssetsOverview.AssetsReport = _AssetReport;

                    _AMCInsuranceReport.WithAMC = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetVendorDetails.Any(a => a.IsAmcApplicable == 1 && a.IsInsurance == 0) && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();
                    _AMCInsuranceReport.WithInsurance = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetVendorDetails.Any(a => a.IsAmcApplicable == 0 && a.IsInsurance == 1) && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();
                    _AMCInsuranceReport.WithAMC_and_Insurance = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetVendorDetails.Any(a => a.IsAmcApplicable == 1 && a.IsInsurance == 1) && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();

                    _AssetsOverview.AMCInsuranceReport = _AMCInsuranceReport;

                    _WarrentyReport.InWarrenty = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetVendorDetails.Any(a => a.StatusId != AssetStatus.WarrentyExpired) && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();
                    _WarrentyReport.WarrentyExpired = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetVendorDetails.Any(a => a.StatusId == AssetStatus.WarrentyExpired) && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();

                    _AssetsOverview.WarrentyReport = _WarrentyReport;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AssetsOverview, "AT0200", "Overview loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetsOverview", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetIssueReport(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetIssueId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetIssueId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _Request.ReferenceId && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OAssetReport.IssueAsset.List
                                           {
                                               AssetIssueId = x.Id,
                                               AssetIssueKey = x.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               DepartmentName = x.Department.DepartmentName,
                                               DivisionName = x.Division.DivisionName,
                                               LocationName = x.Location.LocationName,
                                               SubLocationName = x.SubLocation.LocationName,
                                               EmployeeName = x.Employee.DisplayName,
                                               CreateDate = x.CreatedDate,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,
                                               ModifyDate = x.ModifiedDate,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAssetReport.IssueAsset.List> _Assets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _Request.ReferenceId && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OAssetReport.IssueAsset.List
                                       {
                                           AssetIssueId = x.Id,
                                           AssetIssueKey = x.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           DepartmentName = x.Department.DepartmentName,
                                           DivisionName = x.Division.DivisionName,
                                           LocationName = x.Location.LocationName,
                                           SubLocationName = x.SubLocation.LocationName,
                                           EmployeeName = x.Employee.DisplayName,
                                           CreateDate = x.CreatedDate,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                           ModifyDate = x.ModifiedDate,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetReport", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetMaintenanceReport(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetMaintenances.Where(x => x.AssetId == _Request.ReferenceId && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OAssetReport.Maintenance.List
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,

                                               Description = x.Description,
                                               Remark = x.Remark,
                                               RequestDate = x.RequestDate,
                                               ExpectedDate = x.ExpectedDate,
                                               ReceivedDate = x.ReceivedDate,
                                               Amount = (double?)x.Amount,

                                               MaintenanceTypeId = x.MaintenanceType,
                                               MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                               MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                               AssetVendorId = x.VendorId,
                                               AssetVendorKey = x.Vendor.Guid,
                                               AssetVendorName = x.Vendor.DisplayName,
                                               MobileNumber = x.Vendor.MobileNumber,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAssetReport.Maintenance.List> _Assets = await _HCoreContext.AtAssetMaintenances.Where(x => x.AssetId == _Request.ReferenceId && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OAssetReport.Maintenance.List
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,

                                           Description = x.Description,
                                           Remark = x.Remark,
                                           RequestDate = x.RequestDate,
                                           ExpectedDate = x.ExpectedDate,
                                           ReceivedDate = x.ReceivedDate,
                                           Amount = (double?)x.Amount,

                                           MaintenanceTypeId = x.MaintenanceType,
                                           MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                           MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                           AssetVendorId = x.VendorId,
                                           AssetVendorKey = x.Vendor.Guid,
                                           AssetVendorName = x.Vendor.DisplayName,
                                           MobileNumber = x.Vendor.MobileNumber,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetMaintenanceReport", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetReturnReport(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetReturnDetails.Where(x => x.AssetId == _Request.ReferenceId && x.Asset.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OAssetReport.RetrurnAsset.List
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,

                                               ReturnById = x.ReturnBy,
                                               ReturnByKey = x.ReturnByNavigation.Guid,
                                               ReturnByName = x.ReturnByNavigation.DisplayName,

                                               ReturnToId = x.ReturnTo,
                                               ReturnToKey = x.ReturnToNavigation.Guid,
                                               ReturnToName = x.ReturnToNavigation.DisplayName,

                                               ReturnCondition = x.ReturnCondition,
                                               ReturnNotes = x.ReturnNotes,
                                               ReturnQuantity = x.ReturnQuantity,
                                               ReturnReference = x.ReturnReference,
                                               ReturnValue = x.ReturnValue,

                                               ApprovedById = x.ReturnApprovedBy,
                                               ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                               ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                               ApprovedDate = x.ReturnApprovedDate,

                                               CreateDate = x.CreatedDate,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAssetReport.RetrurnAsset.List> _Assets = await _HCoreContext.AtAssetReturnDetails.Where(x => x.AssetId == _Request.ReferenceId && x.Asset.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OAssetReport.RetrurnAsset.List
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,

                                           ReturnById = x.ReturnBy,
                                           ReturnByKey = x.ReturnByNavigation.Guid,
                                           ReturnByName = x.ReturnByNavigation.DisplayName,

                                           ReturnToId = x.ReturnTo,
                                           ReturnToKey = x.ReturnToNavigation.Guid,
                                           ReturnToName = x.ReturnToNavigation.DisplayName,

                                           ReturnCondition = x.ReturnCondition,
                                           ReturnNotes = x.ReturnNotes,
                                           ReturnQuantity = x.ReturnQuantity,
                                           ReturnReference = x.ReturnReference,
                                           ReturnValue = x.ReturnValue,

                                           ApprovedById = x.ReturnApprovedBy,
                                           ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                           ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                           ApprovedDate = x.ReturnApprovedDate,

                                           CreateDate = x.CreatedDate,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetMaintenanceReport", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetWithBarcodeAssetsStock(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetWithBarcodeAssetsStock(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithBarcode)
                                                .Select(x => new OAssetReport.StockReport
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetName = x.AssetName,
                                                    AssetSerialNo = x.AssetSerialNumber,

                                                    DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                    EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                    EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                    EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                    EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAssetReport.StockReport> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithBarcode)
                                                .Select(x => new OAssetReport.StockReport
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetName = x.AssetName,
                                                    AssetSerialNo = x.AssetSerialNumber,

                                                    DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                    EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                    EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                    EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                    EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).OrderBy(_Request.SortExpression)
                                                .Where(_Request.SearchCondition)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetWithBarcodeAssetsStock", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetWithoutBarcodeAssetsStock(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetWithBarcodeAssetsStock(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithoutBarcode)
                                                .Select(x => new OAssetReport.StockReport
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetName = x.AssetName,
                                                    AssetSerialNo = x.AssetSerialNumber,
                                                    TotalQuantity = x.TotalQuantity,
                                                    RemainingQuantity = x.RemainingQuantity,
                                                    IssuedQuantity = x.TotalQuantity - x.RemainingQuantity,

                                                    DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                    EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                    EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                    EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                    EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAssetReport.StockReport> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithoutBarcode)
                                                             .Select(x => new OAssetReport.StockReport
                                                             {
                                                                 AssetId = x.Id,
                                                                 AssetKey = x.Guid,
                                                                 AssetBarcode = x.AssetBarcode,
                                                                 AssetName = x.AssetName,
                                                                 AssetSerialNo = x.AssetSerialNumber,
                                                                 TotalQuantity = x.TotalQuantity,
                                                                 RemainingQuantity = x.RemainingQuantity,
                                                                 IssuedQuantity = x.TotalQuantity - x.RemainingQuantity,

                                                                 DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                                 DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                                 DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                                 DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                                 DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                                 DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                                 LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                                 LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                                 LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                                 SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                                 SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                                 SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                                 EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                                 EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                                 EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                                 EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),

                                                                 CreateDate = x.CreatedDate,

                                                                 StatusId = x.StatusId,
                                                                 StatusCode = x.Status.SystemName,
                                                                 StatusName = x.Status.Name,
                                                             }).OrderBy(_Request.SortExpression)
                                                             .Where(_Request.SearchCondition)
                                                             .Skip(_Request.Offset)
                                                             .Take(_Request.Limit)
                                                             .ToListAsync();

                    if(_Assets.Count > 0)
                    {
                        foreach(var Asset in _Assets)
                        {
                            Asset.IssuedQuantity = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId).SumAsync(x => x.Quantity);
                            Asset.RemainingQuantity = Asset.TotalQuantity - Asset.IssuedQuantity;
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetWithoutBarcodeAssetsStock", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetIssuedAssets(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using(_HCoreContextLogging = new HCoreContextLogging())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetIssues.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                .Select(x => new OAssetReport.IssuedAssets
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.Asset,
                                                    DivisionKey = x.Division,
                                                    DepartmentKey = x.Department,
                                                    LocationKey = x.Location,
                                                    SubLocationKey = x.SubLocation,
                                                    EmployeeKey = x.Employee,
                                                    Quantity = x.Quantity,
                                                    StatusId = x.Status,
                                                    CreateDate = x.CreatedDate,
                                                    Comment = x.Comment,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAssetReport.IssuedAssets> _Assets = await _HCoreContextLogging.AtAssetIssues.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                              .Select(x => new OAssetReport.IssuedAssets
                                                              {
                                                                  AssetId = x.Id,
                                                                  AssetKey = x.Guid,
                                                                  AssetBarcode = x.Asset,
                                                                  DivisionKey = x.Division,
                                                                  DepartmentKey = x.Department,
                                                                  LocationKey = x.Location,
                                                                  SubLocationKey = x.SubLocation,
                                                                  EmployeeKey = x.Employee,
                                                                  Quantity = x.Quantity,
                                                                  StatusId = x.Status,
                                                                  CreateDate = x.CreatedDate,
                                                                  Comment = x.Comment,
                                                              }).Where(_Request.SearchCondition)
                                                              .OrderBy(_Request.SortExpression)
                                                              .Skip(_Request.Offset)
                                                              .Take(_Request.Limit)
                                                              .ToListAsync();

                    if(_Assets.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Asset in _Assets)
                            {
                                Asset.DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Guid == Asset.DivisionKey).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                Asset.DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Guid == Asset.DepartmentKey).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                Asset.LocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.LocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Asset.SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.SubLocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Asset.EmployeeName = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                Asset.StatusCode = await _HCoreContext.AtCores.Where(x => x.Id == Asset.StatusId).Select(x => x.Name).FirstOrDefaultAsync();
                                Asset.StatusName = await _HCoreContext.AtCores.Where(x => x.Id == Asset.StatusId).Select(x => x.Name).FirstOrDefaultAsync();
                            }
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetIssuedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetTransferredAssets(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetTransfers.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                .Select(x => new OAssetReport.TransferredAssets
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.Asset,
                                                    DivisionKey = x.Division,
                                                    DepartmentKey = x.Department,
                                                    LocationKey = x.Location,
                                                    SubLocationKey = x.SubLocation,
                                                    EmployeeKey = x.Employee,
                                                    StatusId = x.Status,
                                                    CreateDate = x.CreatedDate,
                                                    Comment = x.Comment,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAssetReport.TransferredAssets> _Assets = await _HCoreContextLogging.AtAssetTransfers.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                                   .Select(x => new OAssetReport.TransferredAssets
                                                                   {
                                                                       AssetId = x.Id,
                                                                       AssetKey = x.Guid,
                                                                       AssetBarcode = x.Asset,
                                                                       DivisionKey = x.Division,
                                                                       DepartmentKey = x.Department,
                                                                       LocationKey = x.Location,
                                                                       SubLocationKey = x.SubLocation,
                                                                       EmployeeKey = x.Employee,
                                                                       StatusId = x.Status,
                                                                       CreateDate = x.CreatedDate,
                                                                       Comment = x.Comment,
                                                                   }).Where(_Request.SearchCondition)
                                                                   .OrderBy(_Request.SortExpression)
                                                                   .Skip(_Request.Offset)
                                                                   .Take(_Request.Limit)
                                                                   .ToListAsync();

                    if (_Assets.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Asset in _Assets)
                            {
                                Asset.DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Guid == Asset.DivisionKey).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                Asset.DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Guid == Asset.DepartmentKey).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                Asset.LocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.LocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Asset.SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.SubLocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Asset.EmployeeName = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                Asset.StatusCode = await _HCoreContext.AtCores.Where(x => x.Id == Asset.StatusId).Select(x => x.Name).FirstOrDefaultAsync();
                                Asset.StatusName = await _HCoreContext.AtCores.Where(x => x.Id == Asset.StatusId).Select(x => x.Name).FirstOrDefaultAsync();
                            }
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetTransferredAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetMappedAssets(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetMappings.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                .Select(x => new OAssetReport.MappedAssets
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.Asset,
                                                    RfId = x.RefId,
                                                    EpcId = x.EpcId,
                                                    StatusId = x.Status,
                                                    CreateDate = x.CreatedDate,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAssetReport.MappedAssets> _Assets = await _HCoreContextLogging.AtAssetMappings.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                              .Select(x => new OAssetReport.MappedAssets
                                                              {
                                                                  AssetId = x.Id,
                                                                  AssetKey = x.Guid,
                                                                  AssetBarcode = x.Asset,
                                                                  RfId = x.RefId,
                                                                  EpcId = x.EpcId,
                                                                  StatusId = x.Status,
                                                                  CreateDate = x.CreatedDate,
                                                              }).Where(_Request.SearchCondition)
                                                              .OrderBy(_Request.SortExpression)
                                                              .Skip(_Request.Offset)
                                                              .Take(_Request.Limit)
                                                              .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMappedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssets(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if(_Request.Type == "issued")
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = await _HCoreContextLogging.AtAssetIssues.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                    .Select(x => new OAssetReport.Assets
                                                    {
                                                        AssetId = x.Id,
                                                        AssetKey = x.Guid,
                                                        AssetBarcode = x.Asset,
                                                        DivisionKey = x.Division,
                                                        DepartmentKey = x.Department,
                                                        LocationKey = x.Location,
                                                        SubLocationKey = x.SubLocation,
                                                        EmployeeKey = x.Employee,
                                                        Quantity = x.Quantity,
                                                        ReceiptNo = x.ReceiptNo,
                                                        ReceiptDate = x.ReceiptDate.ToString(),
                                                        UserId = x.CreatedBy,
                                                        CreateDate = x.CreatedDate,
                                                        OrganizationKey = x.Organization,
                                                    }).Where(_Request.SearchCondition)
                                                    .CountAsync();
                        }

                        List<OAssetReport.Assets> _Assets = await _HCoreContextLogging.AtAssetIssues.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                                  .Select(x => new OAssetReport.Assets
                                                                  {
                                                                      AssetId = x.Id,
                                                                      AssetKey = x.Guid,
                                                                      AssetBarcode = x.Asset,
                                                                      DivisionKey = x.Division,
                                                                      DepartmentKey = x.Department,
                                                                      LocationKey = x.Location,
                                                                      SubLocationKey = x.SubLocation,
                                                                      EmployeeKey = x.Employee,
                                                                      Quantity = x.Quantity,
                                                                      ReceiptNo = x.ReceiptNo,
                                                                      ReceiptDate = x.ReceiptDate.ToString(),
                                                                      UserId = x.CreatedBy,
                                                                      CreateDate = x.CreatedDate,
                                                                      OrganizationKey = x.Organization,
                                                                  }).Where(_Request.SearchCondition)
                                                                  .OrderBy(_Request.SortExpression)
                                                                  .Skip(_Request.Offset)
                                                                  .Take(_Request.Limit)
                                                                  .ToListAsync();

                        if (_Assets.Count > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var Asset in _Assets)
                                {
                                    Asset.AssetName = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.AssetBarcode).Select(x => x.AssetName).FirstOrDefaultAsync();
                                    Asset.AssetSerialNo = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.AssetBarcode).Select(x => x.AssetSerialNumber).FirstOrDefaultAsync();
                                    Asset.DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Guid == Asset.DivisionKey).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                    Asset.DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Guid == Asset.DepartmentKey).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    Asset.LocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.LocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    Asset.SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.SubLocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    Asset.EmployeeName = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                    Asset.EmployeeCode = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.AccountCode).FirstOrDefaultAsync();
                                    Asset.EmployeeEmail = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.EmailAddress).FirstOrDefaultAsync();
                                    Asset.UserName = await _HCoreContext.AtAccounts.Where(x => x.Id == Asset.UserId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                    Asset.CompanyName = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.OrganizationKey).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                }
                            }
                        }

                        OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = await _HCoreContextLogging.AtAssetTransfers.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                    .Select(x => new OAssetReport.Assets
                                                    {
                                                        AssetId = x.Id,
                                                        AssetKey = x.Guid,
                                                        AssetBarcode = x.Asset,
                                                        DivisionKey = x.Division,
                                                        DepartmentKey = x.Department,
                                                        LocationKey = x.Location,
                                                        SubLocationKey = x.SubLocation,
                                                        EmployeeKey = x.Employee,
                                                        ReceiptNo = x.ReceiptNo,
                                                        ReceiptDate = x.ReceiptDate.ToString(),
                                                        UserId = x.CreatedBy,
                                                        CreateDate = x.CreatedDate,
                                                        OrganizationKey = x.Organization,
                                                    }).Where(_Request.SearchCondition)
                                                    .CountAsync();
                        }

                        List<OAssetReport.Assets> _Assets = await _HCoreContextLogging.AtAssetTransfers.Where(x => x.Organization == _Request.UserReference.OrganizationKey)
                                                                       .Select(x => new OAssetReport.Assets
                                                                       {
                                                                           AssetId = x.Id,
                                                                           AssetKey = x.Guid,
                                                                           AssetBarcode = x.Asset,
                                                                           DivisionKey = x.Division,
                                                                           DepartmentKey = x.Department,
                                                                           LocationKey = x.Location,
                                                                           SubLocationKey = x.SubLocation,
                                                                           EmployeeKey = x.Employee,
                                                                           ReceiptNo = x.ReceiptNo,
                                                                           ReceiptDate = x.ReceiptDate.ToString(),
                                                                           UserId = x.CreatedBy,
                                                                           CreateDate = x.CreatedDate,
                                                                           OrganizationKey = x.Organization,
                                                                       }).Where(_Request.SearchCondition)
                                                                       .OrderBy(_Request.SortExpression)
                                                                       .Skip(_Request.Offset)
                                                                       .Take(_Request.Limit)
                                                                       .ToListAsync();

                        if (_Assets.Count > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var Asset in _Assets)
                                {
                                    Asset.AssetName = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.AssetBarcode).Select(x => x.AssetName).FirstOrDefaultAsync();
                                    Asset.AssetSerialNo = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Asset.AssetBarcode).Select(x => x.AssetSerialNumber).FirstOrDefaultAsync();
                                    Asset.DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Guid == Asset.DivisionKey).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                    Asset.DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Guid == Asset.DepartmentKey).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    Asset.LocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.LocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    Asset.SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Guid == Asset.SubLocationKey).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    Asset.EmployeeId = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.Id).FirstOrDefaultAsync();
                                    Asset.EmployeeName = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                    Asset.EmployeeCode = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.AccountCode).FirstOrDefaultAsync();
                                    Asset.EmployeeEmail = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.EmployeeKey).Select(x => x.EmailAddress).FirstOrDefaultAsync();
                                    Asset.TransferredFromDivisionName = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.Division.DivisionName).FirstOrDefaultAsync();
                                    Asset.TransferredFromDepartmentName = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.Department.DepartmentName).FirstOrDefaultAsync();
                                    Asset.TransferredFromLocationName = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.Location.LocationName).FirstOrDefaultAsync();
                                    Asset.TransferredFromSubLocationName = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.SubLocation.LocationName).FirstOrDefaultAsync();
                                    Asset.TransferredFromEmployeeId = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.EmployeeId).FirstOrDefaultAsync();
                                    Asset.TransferredFromEmployeeName = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.Employee.DisplayName).FirstOrDefaultAsync();
                                    Asset.TransferredFromEmployeeCode = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.Employee.AccountCode).FirstOrDefaultAsync();
                                    Asset.TransferredFromEmployeeEmail = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == Asset.AssetId && x.TransferedTo == Asset.EmployeeId && x.IsTransfered == 1).Select(x => x.Employee.EmailAddress).FirstOrDefaultAsync();
                                    Asset.UserName = await _HCoreContext.AtAccounts.Where(x => x.Id == Asset.UserId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                    Asset.CompanyName = await _HCoreContext.AtAccounts.Where(x => x.Guid == Asset.OrganizationKey).Select(x => x.DisplayName).FirstOrDefaultAsync();
                                }
                            }
                        }

                        OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssets", _Exception, _Request.UserReference);
            }
        }
    }
}
