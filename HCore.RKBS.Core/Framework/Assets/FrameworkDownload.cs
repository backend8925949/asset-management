﻿using HCore.Data.Logging;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Assets;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Assets
{
    public class FrameworkDownload
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        List<ODownload.AssetListDownload>? _AssetDownload;
        List<ODownload.IssuedAssetListDownload>? _IssuedAssets;
        List<ODownload.UnIssuedAssetListDownload>? _UnIssuedAssets;
        List<ODownload.DisposedAssetListDownload>? _DisposedAssets;
        List<ODownload.AssetUploadListDownload>? _UploadedAssets;
        List<ODownload.TransferredAssetListDownload>? _TransferrdAssets;
        List<ODownload.ReturnedAssetListDownload>? _ReturnedAssets;
        List<ODownload.AssetMaintenanceListDownload>? _AssetMaintenance;
        List<ODownload.StockReport>? _StockReportDownload;
        List<ODownload.AssetHistoryDownload>? _HistoryDownload;
        ODownload.AssetLocationDetails? _LocationDetails;
        List<ODownload.AssetAuditReportDownload>? _AuditReportDownload;
        List<ODownload.AssetDepreciation>? _AssetDepreciation;
        List<ODownload.AssetDepreciationDownload.Response>? _AssetDepreciationDownload;
        ODownload.AssetDepreciationDownload.Response? _Depreciation;
        List<ODownload.ExpiredAssetListDownload>? _ExpiredAssets;

        internal async Task<object> GetAssets(OList.Request _Request)
        {
            try
            {
                _AssetDownload = new List<ODownload.AssetListDownload>();
                _LocationDetails = new ODownload.AssetLocationDetails();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                           .Select(x => new
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,
                                               ExAssetId = x.ExAssetId,
                                               TotalQuantity = x.TotalQuantity,
                                               RemainingQuantity = x.RemainingQuantity,
                                               AssetMake = x.AssetMake,
                                               AssetModel = x.AssetModel,

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               AssetClassId = x.AssetClass.Id,
                                               AssetClassKey = x.AssetClass.Guid,
                                               AssetClassName = x.AssetClass.ClassName,

                                               AssetCategoryId = x.AssetCategory.Id,
                                               AssetCategoryKey = x.AssetCategory.Guid,
                                               AssetCategoryName = x.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),

                                               CompanyId = x.OrganizationId,
                                               CompanyCode = x.Organization.AccountCode,
                                               CompanyName = x.Organization.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                       .Select(x => new
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,
                                           ExAssetId = x.ExAssetId,
                                           TotalQuantity = x.TotalQuantity,
                                           RemainingQuantity = x.RemainingQuantity,
                                           AssetMake = x.AssetMake,
                                           AssetModel = x.AssetModel,

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),

                                           CompanyId = x.OrganizationId,
                                           CompanyCode = x.Organization.AccountCode,
                                           CompanyName = x.Organization.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        string? Status = _DataItem.Status;
                        var _IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _DataItem.AssetId && x.EmployeeId == _DataItem.EmployeeId)
                                        .Select(x => new ODownload.IssueAsset.List
                                        {
                                            DepartmentName = x.Department.DepartmentName,
                                            EmployeeName = x.Employee.DisplayName,
                                            EmployeeCode = x.Employee.AccountCode,
                                            EmployeeEmailAddress = x.Employee.EmailAddress,
                                            Remark = x.Remark,
                                            Quantity = x.Quantity,
                                            CreateDate = x.CreatedDate,
                                            StatusName = x.Status.Name,
                                        }).FirstOrDefaultAsync();

                        if (_IssueDetails == null)
                        {
                            _IssueDetails = new ODownload.IssueAsset.List();
                            _IssueDetails.DepartmentName = "";
                            _IssueDetails.EmployeeName = "";
                            _IssueDetails.EmployeeCode = "";
                            _IssueDetails.EmployeeEmailAddress = "";
                            _IssueDetails.Remark = "";
                            _IssueDetails.Quantity = 0;
                            _IssueDetails.CreateDate = null;
                        }
                        else
                        {
                            Status = _IssueDetails.StatusName;
                        }

                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _AssetDownload.Add(new ODownload.AssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Employee = _IssueDetails.EmployeeName,
                            Employee_Code = _IssueDetails.EmployeeCode,
                            Email = _IssueDetails.EmployeeEmailAddress,
                            Issued_Date = _IssueDetails.CreateDate.ToString(),
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Total_Quantity = _DataItem.TotalQuantity,
                            Remaining_Quantity = _DataItem.RemainingQuantity,
                            Issued_Quantity = _IssueDetails.Quantity,
                            Division = _DataItem.DivisionName,
                            Department = _IssueDetails.DepartmentName,
                            Location = _DataItem.LocationName,
                            SubLocation = _DataItem.SubLocationName,
                            Remark = _IssueDetails.Remark,
                            Status = Status,
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }
                }

                return _AssetDownload;
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetDisposedAssets(OList.Request _Request)
        {
            try
            {
                _DisposedAssets = new List<ODownload.DisposedAssetListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Disposed)
                                           .Select(x => new
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,
                                               AssetMake = x.AssetMake,
                                               AssetModel = x.AssetModel,
                                               ExAssetId = x.ExAssetId,
                                               TotalQuantity = x.TotalQuantity,
                                               RemainingQuantity = x.RemainingQuantity,

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               AssetClassId = x.AssetClass.Id,
                                               AssetClassKey = x.AssetClass.Guid,
                                               AssetClassName = x.AssetClass.ClassName,

                                               AssetCategoryId = x.AssetCategory.Id,
                                               AssetCategoryKey = x.AssetCategory.Guid,
                                               AssetCategoryName = x.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Disposed)
                                       .Select(x => new
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,
                                           AssetMake = x.AssetMake,
                                           AssetModel = x.AssetModel,
                                           ExAssetId = x.ExAssetId,
                                           TotalQuantity = x.TotalQuantity,
                                           RemainingQuantity = x.RemainingQuantity,

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        var _IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _DataItem.AssetId && x.EmployeeId == _DataItem.EmployeeId)
                                        .Select(x => new ODownload.IssueAsset.List
                                        {
                                            DivisionName = x.Division.DivisionName,
                                            LocationName = x.Location.LocationName,
                                            SubLocationName = x.SubLocation.LocationName,
                                            DepartmentName = x.Department.DepartmentName,
                                            EmployeeName = x.Employee.DisplayName,
                                            EmployeeCode = x.Employee.AccountCode,
                                            EmployeeEmailAddress = x.Employee.EmailAddress,
                                            Remark = x.Remark,
                                            Quantity = x.Quantity,
                                            CreateDate = x.CreatedDate,
                                        }).FirstOrDefaultAsync();

                        if (_IssueDetails == null)
                        {
                            _IssueDetails = new ODownload.IssueAsset.List();
                            _IssueDetails.DivisionName = "";
                            _IssueDetails.LocationName = "";
                            _IssueDetails.SubLocationName = "";
                            _IssueDetails.DepartmentName = "";
                            _IssueDetails.EmployeeName = "";
                            _IssueDetails.EmployeeCode = "";
                            _IssueDetails.EmployeeEmailAddress = "";
                            _IssueDetails.Remark = "";
                            _IssueDetails.Quantity = 0;
                            _IssueDetails.CreateDate = null;
                        }

                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _DisposedAssets.Add(new ODownload.DisposedAssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Employee = _IssueDetails.EmployeeName,
                            Employee_Code = _IssueDetails.EmployeeCode,
                            Email = _IssueDetails.EmployeeEmailAddress,
                            Issued_Date = _IssueDetails.CreateDate.ToString(),
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Total_Quantity = _DataItem.TotalQuantity,
                            Issued_Quantity = _IssueDetails.Quantity,
                            Remaining_Quantity = _DataItem.RemainingQuantity,
                            Division = _IssueDetails.DivisionName,
                            Department = _IssueDetails.DepartmentName,
                            Location = _IssueDetails.LocationName,
                            SubLocation = _IssueDetails.SubLocationName,
                            Remark = _IssueDetails.Remark,
                            Disposed_By = _DataItem.ModifyByDisplayName,
                            Disposed_On = _DataItem.ModifyDate.ToString(),
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString(),
                        });
                    }
                }

                return _DisposedAssets;
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDispossedAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetExpiredAssets(OList.Request _Request)
        {
            try
            {
                _ExpiredAssets = new List<ODownload.ExpiredAssetListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Expired)
                                           .Select(x => new
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,
                                               AssetMake = x.AssetMake,
                                               AssetModel = x.AssetModel,
                                               ExAssetId = x.ExAssetId,
                                               TotalQuantity = x.TotalQuantity,
                                               RemainingQuantity = x.RemainingQuantity,

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               AssetClassId = x.AssetClass.Id,
                                               AssetClassKey = x.AssetClass.Guid,
                                               AssetClassName = x.AssetClass.ClassName,

                                               AssetCategoryId = x.AssetCategory.Id,
                                               AssetCategoryKey = x.AssetCategory.Guid,
                                               AssetCategoryName = x.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Expired)
                                       .Select(x => new
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,
                                           AssetMake = x.AssetMake,
                                           AssetModel = x.AssetModel,
                                           ExAssetId = x.ExAssetId,
                                           TotalQuantity = x.TotalQuantity,
                                           RemainingQuantity = x.RemainingQuantity,

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        var _IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _DataItem.AssetId && x.EmployeeId == _DataItem.EmployeeId)
                                        .Select(x => new ODownload.IssueAsset.List
                                        {
                                            DivisionName = x.Division.DivisionName,
                                            LocationName = x.Location.LocationName,
                                            SubLocationName = x.SubLocation.LocationName,
                                            DepartmentName = x.Department.DepartmentName,
                                            EmployeeName = x.Employee.DisplayName,
                                            EmployeeCode = x.Employee.AccountCode,
                                            EmployeeEmailAddress = x.Employee.EmailAddress,
                                            Remark = x.Remark,
                                            Quantity = x.Quantity,
                                            CreateDate = x.CreatedDate,
                                        }).FirstOrDefaultAsync();

                        if (_IssueDetails == null)
                        {
                            _IssueDetails = new ODownload.IssueAsset.List();
                            _IssueDetails.DivisionName = "";
                            _IssueDetails.LocationName = "";
                            _IssueDetails.SubLocationName = "";
                            _IssueDetails.DepartmentName = "";
                            _IssueDetails.EmployeeName = "";
                            _IssueDetails.EmployeeCode = "";
                            _IssueDetails.EmployeeEmailAddress = "";
                            _IssueDetails.Remark = "";
                            _IssueDetails.Quantity = 0;
                            _IssueDetails.CreateDate = null;
                        }

                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _ExpiredAssets.Add(new ODownload.ExpiredAssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Employee = _IssueDetails.EmployeeName,
                            Employee_Code = _IssueDetails.EmployeeCode,
                            Email = _IssueDetails.EmployeeEmailAddress,
                            Issued_Date = _IssueDetails.CreateDate.ToString(),
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Total_Quantity = _DataItem.TotalQuantity,
                            Issued_Quantity = _IssueDetails.Quantity,
                            Remaining_Quantity = _DataItem.RemainingQuantity,
                            Division = _IssueDetails.DivisionName,
                            Department = _IssueDetails.DepartmentName,
                            Location = _IssueDetails.LocationName,
                            SubLocation = _IssueDetails.SubLocationName,
                            Remark = _IssueDetails.Remark,
                            Expired_On = _DataItem.ModifyDate.ToString(),
                            Create_Date = _DataItem.CreateDate.ToString(),
                        });
                    }
                }

                return _ExpiredAssets;
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetExpiredAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetBulkAssets(OList.Request _Request)
        {
            try
            {
                _UploadedAssets = new List<ODownload.AssetUploadListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetUploads.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new
                                           {
                                               AssetId = x.Id,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNo,
                                               AssetMake = x.AssetMake,
                                               AssetModel = x.AssetModel,
                                               Quantity = x.TotalQuantity,
                                               ExAssetId = x.ExAssetId,
                                               AssetCategoryName = x.AssetCategory,
                                               AssetSubCategoryName = x.AssetSubCategory,
                                               AssetClassName = x.AssetClass,
                                               AssetTypeName = x.AssetType,
                                               AssetVendor = x.AssetVendor,
                                               PoNumber = x.PoNumber,
                                               InvoiceNumber = x.InvoiceNumber,
                                               RecievedDate = x.ReceivedDate,
                                               StartDate = x.StartDate,
                                               EndDate = x.EndDate,
                                               ExpiryDate = x.ExpiryDate,
                                               Amount = x.PurchasePrice,
                                               Currency = x.Currency,
                                               Amc = x.IsAmc,
                                               Insurance = x.IsInsurance,
                                               TotalQuantity = x.TotalQuantity,
                                               CreateDate = x.CreatedDate,
                                               StatusId = x.StatusId,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContextLogging.AtAssetUploads.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new
                                       {
                                           AssetId = x.Id,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNo,
                                           AssetMake = x.AssetMake,
                                           AssetModel = x.AssetModel,
                                           Quantity = x.TotalQuantity,
                                           ExAssetId = x.ExAssetId,
                                           AssetCategoryName = x.AssetCategory,
                                           AssetSubCategoryName = x.AssetSubCategory,
                                           AssetClassName = x.AssetClass,
                                           AssetTypeName = x.AssetType,
                                           AssetVendor = x.AssetVendor,
                                           PoNumber = x.PoNumber,
                                           InvoiceNumber = x.InvoiceNumber,
                                           RecievedDate = x.ReceivedDate,
                                           StartDate = x.StartDate,
                                           EndDate = x.EndDate,
                                           ExpiryDate = x.ExpiryDate,
                                           Amount = x.PurchasePrice,
                                           Currency = x.Currency,
                                           Amc = x.IsAmc,
                                           Insurance = x.IsInsurance,
                                           TotalQuantity = x.TotalQuantity,
                                           CreateDate = x.CreatedDate,
                                           StatusId = x.StatusId,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        bool AMC = false;
                        bool Insurance = false;
                        if (_DataItem.Amc == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_DataItem.Insurance == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }

                        _UploadedAssets.Add(new ODownload.AssetUploadListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Asset_Vendor = _DataItem.AssetVendor,
                            PO_Number = _DataItem.PoNumber,
                            Invoice_Number = _DataItem.InvoiceNumber,
                            Asset_Recieved_Date = _DataItem.RecievedDate.ToString(),
                            Warrenty_Start_Date = _DataItem.StartDate.ToString(),
                            Warrenty_End_Date = _DataItem.EndDate.ToString(),
                            Expiry_Date = _DataItem.ExpiryDate.ToString(),
                            Amount = _DataItem.Amount,
                            Currency = _DataItem.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Total_Quantity = _DataItem.TotalQuantity,
                        });
                    }
                }

                return _UploadedAssets;
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBulkAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetIssuedAssets(OList.Request _Request)
        {
            try
            {
                _IssuedAssets = new List<ODownload.IssuedAssetListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Issued && x.StatusId != AssetStatus.Disposed)
                                           .Select(x => new
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,
                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNo = x.Asset.AssetSerialNumber,
                                               TotalQuantity = x.Asset.TotalQuantity,
                                               IssuedQuantity = x.Quantity,
                                               RemainingQuantity = x.Asset.RemainingQuantity,
                                               AssetMake = x.Asset.AssetMake,
                                               AssetModel = x.Asset.AssetModel,
                                               ExAssetId = x.Asset.ExAssetId,
                                               Remark = x.Remark,

                                               AssetTypeId = x.Asset.AssetTypeId,
                                               AssetTypeCode = x.Asset.AssetType.SystemName,
                                               AssetTypeName = x.Asset.AssetType.Name,

                                               AssetClassId = x.Asset.AssetClass.Id,
                                               AssetClassKey = x.Asset.AssetClass.Guid,
                                               AssetClassName = x.Asset.AssetClass.ClassName,

                                               AssetCategoryId = x.Asset.AssetCategory.Id,
                                               AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                               AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.Asset.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.LocationId,
                                               LocationKey = x.Location.Guid,
                                               LocationName = x.Location.LocationName,

                                               SubLocationId = x.SubLocationId,
                                               SubLocationKey = x.SubLocation.Guid,
                                               SubLocationName = x.SubLocation.LocationName,

                                               EmployeeId = x.EmployeeId,
                                               EmployeeKey = x.Employee.Guid,
                                               EmployeeName = x.Employee.DisplayName,
                                               EmployeeCode = x.Employee.AccountCode,
                                               EmployeeEmail = x.Employee.EmailAddress,

                                               CreateDate = x.CreatedDate,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Issued && x.StatusId != AssetStatus.Disposed)
                                       .Select(x => new
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,
                                           TotalQuantity = x.Asset.TotalQuantity,
                                           IssuedQuantity = x.Quantity,
                                           RemainingQuantity = x.Asset.RemainingQuantity,
                                           AssetMake = x.Asset.AssetMake,
                                           AssetModel = x.Asset.AssetModel,
                                           ExAssetId = x.Asset.ExAssetId,
                                           Remark = x.Remark,

                                           AssetTypeId = x.Asset.AssetTypeId,
                                           AssetTypeCode = x.Asset.AssetType.SystemName,
                                           AssetTypeName = x.Asset.AssetType.Name,

                                           AssetClassId = x.Asset.AssetClass.Id,
                                           AssetClassKey = x.Asset.AssetClass.Guid,
                                           AssetClassName = x.Asset.AssetClass.ClassName,

                                           AssetCategoryId = x.Asset.AssetCategory.Id,
                                           AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                           AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.Asset.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.LocationId,
                                           LocationKey = x.Location.Guid,
                                           LocationName = x.Location.LocationName,

                                           SubLocationId = x.SubLocationId,
                                           SubLocationKey = x.SubLocation.Guid,
                                           SubLocationName = x.SubLocation.LocationName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,
                                           EmployeeCode = x.Employee.AccountCode,
                                           EmployeeEmail = x.Employee.EmailAddress,

                                           CreateDate = x.CreatedDate,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _IssuedAssets.Add(new ODownload.IssuedAssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Employee = _DataItem.EmployeeName,
                            Employee_Code = _DataItem.EmployeeCode,
                            Email = _DataItem.EmployeeEmail,
                            Issued_Date = _DataItem.CreateDate.ToString(),
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Total_Quantity = _DataItem.TotalQuantity,
                            Issued_Quantity = _DataItem.IssuedQuantity,
                            Remaining_Quantity = _DataItem.RemainingQuantity,
                            Division = _DataItem.DivisionName,
                            Department = _DataItem.DepartmentName,
                            Location = _DataItem.LocationName,
                            SubLocation = _DataItem.SubLocationName,
                            Remark = _DataItem.Remark,
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }
                }

                return _IssuedAssets;
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetIssuedAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetUnIssuedAssets(OList.Request _Request)
        {
            try
            {
                _UnIssuedAssets = new List<ODownload.UnIssuedAssetListDownload>();
                _LocationDetails = new ODownload.AssetLocationDetails();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.RemainingQuantity > 0 && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,
                                               AssetMake = x.AssetMake,
                                               AssetModel = x.AssetModel,
                                               ExAssetId = x.ExAssetId,
                                               TotalQuantity = x.TotalQuantity,

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               AssetClassId = x.AssetClass.Id,
                                               AssetClassKey = x.AssetClass.Guid,
                                               AssetClassName = x.AssetClass.ClassName,

                                               AssetCategoryId = x.AssetCategory.Id,
                                               AssetCategoryKey = x.AssetCategory.Guid,
                                               AssetCategoryName = x.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.RemainingQuantity > 0 && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,
                                           AssetMake = x.AssetMake,
                                           AssetModel = x.AssetModel,
                                           ExAssetId = x.ExAssetId,
                                           TotalQuantity = x.TotalQuantity,

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }

                        _UnIssuedAssets.Add(new ODownload.UnIssuedAssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Division = _DataItem.DivisionName,
                            Location = _DataItem.LocationName,
                            SubLocation = _DataItem.SubLocationName,
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Total_Quantity = _DataItem.TotalQuantity,
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }
                }

                return _UnIssuedAssets;
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetUnIssuedAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetTransferedAssets(OList.Request _Request)
        {
            try
            {
                _TransferrdAssets = new List<ODownload.TransferredAssetListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetIssueDetails.Where(x => x.StatusId == AssetStatus.Transfered && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new
                                           {
                                               AssetIssueId = x.Id,
                                               AssetIssueKey = x.Guid,
                                               Remark = x.Remark,
                                               Quantity = x.Quantity,
                                               AssetMake = x.Asset.AssetMake,
                                               AssetModel = x.Asset.AssetModel,
                                               ExAssetId = x.Asset.ExAssetId,

                                               AssetTypeId = x.Asset.AssetTypeId,
                                               AssetTypeCode = x.Asset.AssetType.SystemName,
                                               AssetTypeName = x.Asset.AssetType.Name,

                                               AssetClassId = x.Asset.AssetClass.Id,
                                               AssetClassKey = x.Asset.AssetClass.Guid,
                                               AssetClassName = x.Asset.AssetClass.ClassName,

                                               AssetCategoryId = x.Asset.AssetCategory.Id,
                                               AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                               AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.Asset.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,

                                               TransferedToId = x.TransferedTo,
                                               TransferedToKey = x.TransferedToNavigation.Guid,
                                               TransferedToName = x.TransferedToNavigation.DisplayName,
                                               TransferedToCode = x.TransferedToNavigation.AccountCode,
                                               TransferedToEmail = x.TransferedToNavigation.EmailAddress,

                                               TransferedById = x.TransferedBy,
                                               TransferedByKey = x.TransferedByNavigation.Guid,
                                               TransferedByName = x.TransferedByNavigation.DisplayName,

                                               IsTransfered = x.IsTransfered,
                                               TransferReference = x.TransferReference,
                                               TransferedDate = x.TransferedDate,

                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNo = x.Asset.AssetSerialNumber,

                                               OrganizationId = x.OrganizationId,
                                               OrganizationKey = x.Organization.Guid,
                                               OrganizationName = x.Organization.DisplayName,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.LocationId,
                                               LocationKey = x.Location.Guid,
                                               LocationName = x.Location.LocationName,

                                               SubLocationId = x.SubLocationId,
                                               SubLocationKey = x.SubLocation.Guid,
                                               SubLocationName = x.SubLocation.LocationName,

                                               EmployeeId = x.EmployeeId,
                                               EmployeeKey = x.Employee.Guid,
                                               EmployeeName = x.Employee.DisplayName,
                                               EmployeeCode = x.Employee.AccountCode,

                                               OsName = x.OsName,
                                               HostName = x.HostName,
                                               IpAddress = x.IpAddress,

                                               ApprovedDate = x.ApprovedDate,
                                               ApprovedById = x.ApprovedById,
                                               ApprovedByKey = x.ApprovedBy.Guid,
                                               ApprovedByName = x.ApprovedBy.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetIssueDetails.Where(x => x.StatusId == AssetStatus.Transfered && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new
                                       {
                                           AssetIssueId = x.Id,
                                           AssetIssueKey = x.Guid,
                                           Remark = x.Remark,
                                           Quantity = x.Quantity,
                                           AssetMake = x.Asset.AssetMake,
                                           AssetModel = x.Asset.AssetModel,
                                           ExAssetId = x.Asset.ExAssetId,

                                           AssetTypeId = x.Asset.AssetTypeId,
                                           AssetTypeCode = x.Asset.AssetType.SystemName,
                                           AssetTypeName = x.Asset.AssetType.Name,

                                           AssetClassId = x.Asset.AssetClass.Id,
                                           AssetClassKey = x.Asset.AssetClass.Guid,
                                           AssetClassName = x.Asset.AssetClass.ClassName,

                                           AssetCategoryId = x.Asset.AssetCategory.Id,
                                           AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                           AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.Asset.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,

                                           TransferedToId = x.TransferedTo,
                                           TransferedToKey = x.TransferedToNavigation.Guid,
                                           TransferedToName = x.TransferedToNavigation.DisplayName,
                                           TransferedToCode = x.TransferedToNavigation.AccountCode,
                                           TransferedToEmail = x.TransferedToNavigation.EmailAddress,

                                           TransferedById = x.TransferedBy,
                                           TransferedByKey = x.TransferedByNavigation.Guid,
                                           TransferedByName = x.TransferedByNavigation.DisplayName,

                                           IsTransfered = x.IsTransfered,
                                           TransferReference = x.TransferReference,
                                           TransferedDate = x.TransferedDate,

                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.LocationId,
                                           LocationKey = x.Location.Guid,
                                           LocationName = x.Location.LocationName,

                                           SubLocationId = x.SubLocationId,
                                           SubLocationKey = x.SubLocation.Guid,
                                           SubLocationName = x.SubLocation.LocationName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,
                                           EmployeeCode = x.Employee.AccountCode,

                                           OsName = x.OsName,
                                           HostName = x.HostName,
                                           IpAddress = x.IpAddress,

                                           ApprovedDate = x.ApprovedDate,
                                           ApprovedById = x.ApprovedById,
                                           ApprovedByKey = x.ApprovedBy.Guid,
                                           ApprovedByName = x.ApprovedBy.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();

                        var TransferDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _DataItem.AssetId && x.Guid == _DataItem.TransferReference)
                                              .Select(x => new
                                              {
                                                  TransferredToDivision = x.Division.DivisionName,
                                                  TransferredToDepartment = x.Department.DepartmentName,
                                                  TransferredToLocation = x.Location.LocationName,
                                                  TransferredToSubLocation = x.SubLocation.LocationName,
                                              }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _TransferrdAssets.Add(new ODownload.TransferredAssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Transfered_Date = _DataItem.TransferedDate.ToString(),
                            Division = _DataItem.DivisionName,
                            Department = _DataItem.DepartmentName,
                            Location = _DataItem.LocationName,
                            SubLocation = _DataItem.SubLocationName,
                            Employee = _DataItem.EmployeeName,
                            Employee_Code = _DataItem.EmployeeCode,
                            Transferred_To_Division = TransferDetails.TransferredToDivision,
                            Transferred_To_Department = TransferDetails.TransferredToDepartment,
                            Transferred_To_Location = TransferDetails.TransferredToLocation,
                            Transferred_To_SubLocation = TransferDetails.TransferredToSubLocation,
                            Transferred_To_Employee = _DataItem.TransferedToName,
                            Transferred_To_Employee_Code = _DataItem.TransferedToCode,
                            Transferred_To_Employee_Email = _DataItem.TransferedToEmail,
                            Approved_By = _DataItem.ApprovedByName,
                            Approved_Date = _DataItem.ApprovedDate.ToString(),
                            Remark = _DataItem.Remark,
                            Status = _DataItem.Status,
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }
                }

                return _TransferrdAssets;
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTransferedAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetReturnedAssets(OList.Request _Request)
        {
            try
            {
                _ReturnedAssets = new List<ODownload.ReturnedAssetListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetReturnDetails.Where(x => x.Asset.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                           .Select(x => new
                                           {
                                               AssetReturnId = x.Id,
                                               AssetReturnKey = x.Guid,

                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNo = x.Asset.AssetSerialNumber,
                                               AssetMake = x.Asset.AssetMake,
                                               AssetModel = x.Asset.AssetModel,
                                               ExAssetId = x.Asset.ExAssetId,
                                               TotalQuantity = x.Asset.TotalQuantity,
                                               RemainingQuantity = x.Asset.RemainingQuantity,

                                               AssetTypeId = x.Asset.AssetTypeId,
                                               AssetTypeCode = x.Asset.AssetType.SystemName,
                                               AssetTypeName = x.Asset.AssetType.Name,

                                               AssetClassId = x.Asset.AssetClass.Id,
                                               AssetClassKey = x.Asset.AssetClass.Guid,
                                               AssetClassName = x.Asset.AssetClass.ClassName,

                                               AssetCategoryId = x.Asset.AssetCategory.Id,
                                               AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                               AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.Asset.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,

                                               ReturnById = x.ReturnBy,
                                               ReturnByKey = x.ReturnByNavigation.Guid,
                                               ReturnByName = x.ReturnByNavigation.DisplayName,

                                               ReturnToId = x.ReturnTo,
                                               ReturnToKey = x.ReturnToNavigation.Guid,
                                               ReturnToName = x.ReturnToNavigation.DisplayName,

                                               ReturnCondition = x.ReturnCondition,
                                               ReturnNotes = x.ReturnNotes,
                                               ReturnQuantity = x.ReturnQuantity,
                                               ReturnReference = x.ReturnReference,
                                               ReturnValue = x.ReturnValue,

                                               ApprovedById = x.ReturnApprovedBy,
                                               ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                               ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                               ApprovedDate = x.ReturnApprovedDate,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               Status = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetReturnDetails.Where(x => x.Asset.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                       .Select(x => new
                                       {
                                           AssetReturnId = x.Id,
                                           AssetReturnKey = x.Guid,

                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,
                                           AssetMake = x.Asset.AssetMake,
                                           AssetModel = x.Asset.AssetModel,
                                           ExAssetId = x.Asset.ExAssetId,
                                           TotalQuantity = x.Asset.TotalQuantity,
                                           RemainingQuantity = x.Asset.RemainingQuantity,

                                           AssetTypeId = x.Asset.AssetTypeId,
                                           AssetTypeCode = x.Asset.AssetType.SystemName,
                                           AssetTypeName = x.Asset.AssetType.Name,

                                           AssetClassId = x.Asset.AssetClass.Id,
                                           AssetClassKey = x.Asset.AssetClass.Guid,
                                           AssetClassName = x.Asset.AssetClass.ClassName,

                                           AssetCategoryId = x.Asset.AssetCategory.Id,
                                           AssetCategoryKey = x.Asset.AssetCategory.Guid,
                                           AssetCategoryName = x.Asset.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.Asset.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.Asset.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.Asset.AssetSubCategory.SubCategoryName,

                                           ReturnById = x.ReturnBy,
                                           ReturnByKey = x.ReturnByNavigation.Guid,
                                           ReturnByName = x.ReturnByNavigation.DisplayName,

                                           ReturnToId = x.ReturnTo,
                                           ReturnToKey = x.ReturnToNavigation.Guid,
                                           ReturnToName = x.ReturnToNavigation.DisplayName,

                                           ReturnCondition = x.ReturnCondition,
                                           ReturnNotes = x.ReturnNotes,
                                           ReturnQuantity = x.ReturnQuantity,
                                           ReturnReference = x.ReturnReference,
                                           ReturnValue = x.ReturnValue,

                                           ApprovedById = x.ReturnApprovedBy,
                                           ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                           ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                           ApprovedDate = x.ReturnApprovedDate,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           Status = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        var _IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.EmployeeId == _DataItem.ReturnById && x.AssetId == _DataItem.AssetId)
                                        .Select(x => new
                                        {
                                            DivisionName = x.Division.DivisionName,
                                            LocationName = x.Location.LocationName,
                                            SubLocationName = x.SubLocation.LocationName,
                                            DepartmentName = x.Department.DepartmentName,
                                            EmployeeName = x.Employee.DisplayName,
                                            EmployeeCode = x.Employee.AccountCode,
                                            EmailAddress = x.Employee.EmailAddress,
                                            Remark = x.Remark,
                                            Quantity = x.Quantity,
                                            IssuedOn = x.CreatedDate,
                                        }).FirstOrDefaultAsync();

                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _ReturnedAssets.Add(new ODownload.ReturnedAssetListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Type = _DataItem.AssetTypeName,
                            Asset_Class = _DataItem.AssetClassName,
                            Asset_Category = _DataItem.AssetCategoryName,
                            Asset_Sub_Category = _DataItem.AssetSubCategoryName,
                            Employee = _IssueDetails.EmployeeName,
                            Employee_Code = _IssueDetails.EmployeeCode,
                            Email = _IssueDetails.EmailAddress,
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Issued_Date = _IssueDetails.IssuedOn.ToString(),
                            Total_Quantity = _DataItem.TotalQuantity,
                            Issued_Quantity = _IssueDetails.Quantity,
                            Remaining_Quantity = _DataItem.RemainingQuantity,
                            Division = _IssueDetails.DivisionName,
                            Department = _IssueDetails.DepartmentName,
                            Location = _IssueDetails.LocationName,
                            SubLocation = _IssueDetails.SubLocationName,
                            Remark = _IssueDetails.Remark,
                            Status = _DataItem.Status,
                            Approved_Date = _DataItem.ApprovedDate.ToString(),
                            Approved_By = _DataItem.ApprovedByName,
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }
                }

                return _ReturnedAssets;
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetReturnedAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetMaintenanceAssets(OList.Request _Request)
        {
            try
            {
                _AssetMaintenance = new List<ODownload.AssetMaintenanceListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetMaintenances
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AssetId = x.AssetId,
                                                    AssetKey = x.Asset.Guid,
                                                    AssetName = x.Asset.AssetName,
                                                    AssetBarcode = x.Asset.AssetBarcode,
                                                    AssetSerialNo = x.Asset.AssetSerialNumber,
                                                    AssetMake = x.Asset.AssetMake,
                                                    AssetModel = x.Asset.AssetModel,
                                                    ExAssetId = x.Asset.ExAssetId,

                                                    Description = x.Description,
                                                    Remark = x.Remark,
                                                    RequestDate = x.RequestDate,
                                                    ExpectedDate = x.ExpectedDate,
                                                    ReceivedDate = x.ReceivedDate,
                                                    Amount = (double?)x.Amount,

                                                    MaintenanceTypeId = x.MaintenanceType,
                                                    MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                                    MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                                    AssetVendorId = x.VendorId,
                                                    AssetVendorKey = x.Vendor.Guid,
                                                    AssetVendorName = x.Vendor.DisplayName,
                                                    MobileNumber = x.Vendor.MobileNumber,

                                                    CreateDate = x.CreatedDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifiedDate,
                                                    ModifyById = x.ModifiedById,
                                                    ModifyByKey = x.ModifiedBy.Guid,
                                                    ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    Status = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetMaintenances
                                  .Select(x => new
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,

                                      AssetId = x.AssetId,
                                      AssetKey = x.Asset.Guid,
                                      AssetName = x.Asset.AssetName,
                                      AssetBarcode = x.Asset.AssetBarcode,
                                      AssetSerialNo = x.Asset.AssetSerialNumber,
                                      AssetMake = x.Asset.AssetMake,
                                      AssetModel = x.Asset.AssetModel,
                                      ExAssetId = x.Asset.ExAssetId,

                                      Description = x.Description,
                                      Remark = x.Remark,
                                      RequestDate = x.RequestDate,
                                      ExpectedDate = x.ExpectedDate,
                                      ReceivedDate = x.ReceivedDate,
                                      Amount = (double?)x.Amount,

                                      MaintenanceTypeId = x.MaintenanceType,
                                      MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                      MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                      AssetVendorId = x.VendorId,
                                      AssetVendorKey = x.Vendor.Guid,
                                      AssetVendorName = x.Vendor.DisplayName,
                                      MobileNumber = x.Vendor.MobileNumber,

                                      CreateDate = x.CreatedDate,
                                      CreatedById = x.CreatedById,
                                      CreatedByKey = x.CreatedBy.Guid,
                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                      ModifyDate = x.ModifiedDate,
                                      ModifyById = x.ModifiedById,
                                      ModifyByKey = x.ModifiedBy.Guid,
                                      ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                      StatusId = x.StatusId,
                                      StatusCode = x.Status.SystemName,
                                      Status = x.Status.Name,
                                  }).OrderBy(_Request.SortExpression)
                                  .Where(_Request.SearchCondition)
                                  .Skip(_Request.Offset)
                                  .Take(_Request.Limit)
                                  .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        _AssetMaintenance.Add(new ODownload.AssetMaintenanceListDownload
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Asset_Make = _DataItem.AssetMake ?? "--",
                            Asset_Model = _DataItem.AssetModel ?? "--",
                            SAP_Asset_Code = _DataItem.ExAssetId ?? "--",
                            Asset_Vendor = _DataItem.AssetVendorName,
                            Vednor_Mobile_Number = _DataItem.MobileNumber,
                            Maintenance_Type = _DataItem.MaintenanceTypeName,
                            Description = _DataItem.Description,
                            Remark = _DataItem.Remark,
                            Request_Date = _DataItem.RequestDate.ToString(),
                            Expected_Date = _DataItem.ExpectedDate.ToString(),
                            Received_Date = _DataItem.ReceivedDate.ToString(),
                            Amount = (decimal?)_DataItem.Amount,
                            Status = _DataItem.Status,
                            User = _DataItem.CreatedByDisplayName,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }
                }

                return _AssetMaintenance;
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMaintenanceAssets-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetWithBarcodeAssetsStock(OList.Request _Request)
        {
            try
            {
                _StockReportDownload = new List<ODownload.StockReport>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithBarcode)
                                                .Select(x => new
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetName = x.AssetName,
                                                    AssetSerialNo = x.AssetSerialNumber,

                                                    DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                    EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                    EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                    EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                    EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),
                                                    EmployeeEmail = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.EmailAddress).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,
                                                    CreatedBy = x.CreatedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    Status = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithBarcode)
                                                .Select(x => new
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetName = x.AssetName,
                                                    AssetSerialNo = x.AssetSerialNumber,

                                                    DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                    EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                    EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                    EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                    EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),
                                                    EmployeeEmail = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.EmailAddress).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,
                                                    CreatedBy = x.CreatedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    Status = x.Status.Name,
                                                }).OrderBy(_Request.SortExpression)
                                                .Where(_Request.SearchCondition)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        string? Status = _DataItem.Status;
                        string? _IssueStatus = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _DataItem.AssetId && x.EmployeeId == _DataItem.EmployeeId).Select(x => x.Status.Name).FirstOrDefaultAsync();

                        if (!string.IsNullOrEmpty(_IssueStatus))
                        {
                            Status = _IssueStatus;
                        }

                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _StockReportDownload.Add(new ODownload.StockReport
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Employee = _DataItem.EmployeeName,
                            Employee_Code = _DataItem.EmployeeCode,
                            Email = _DataItem.EmployeeEmail,
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Division = _DataItem.DivisionName,
                            Department = _DataItem.DepartmentName,
                            Location = _DataItem.LocationName,
                            SubLocation = _DataItem.SubLocationName,
                            Status = Status,
                            User = _DataItem.CreatedBy,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }

                    return _StockReportDownload;
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetWithBarcodeAssetsStock-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetWithoutBarcodeAssetsStock(OList.Request _Request)
        {
            try
            {
                _StockReportDownload = new List<ODownload.StockReport>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithoutBarcode)
                                                .Select(x => new
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetName = x.AssetName,
                                                    AssetSerialNo = x.AssetSerialNumber,
                                                    TotalQuantity = x.TotalQuantity,
                                                    RemainingQuantity = x.RemainingQuantity,
                                                    IssuedQuantity = x.TotalQuantity - x.RemainingQuantity,

                                                    DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                    EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                    EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                    EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                    EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),
                                                    EmployeeEmail = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.EmailAddress).FirstOrDefault(),

                                                    CreateDate = x.CreatedDate,
                                                    CreatedBy = x.CreatedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    Status = x.Status.Name,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId && x.AssetTypeId == AssetType.WithoutBarcode)
                                                             .Select(x => new
                                                             {
                                                                 AssetId = x.Id,
                                                                 AssetKey = x.Guid,
                                                                 AssetBarcode = x.AssetBarcode,
                                                                 AssetName = x.AssetName,
                                                                 AssetSerialNo = x.AssetSerialNumber,
                                                                 TotalQuantity = x.TotalQuantity,
                                                                 RemainingQuantity = x.RemainingQuantity,
                                                                 IssuedQuantity = x.TotalQuantity - x.RemainingQuantity,

                                                                 DivisionId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.DivisionId).FirstOrDefault(),
                                                                 DivisionKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.Guid).FirstOrDefault(),
                                                                 DivisionName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Division.DivisionName).FirstOrDefault(),

                                                                 DepartmentId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.DepartmentId).FirstOrDefault(),
                                                                 DepartmentKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.Guid).FirstOrDefault(),
                                                                 DepartmentName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id).Select(a => a.Department.DepartmentName).FirstOrDefault(),

                                                                 LocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.LocationId).FirstOrDefault(),
                                                                 LocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.Guid).FirstOrDefault(),
                                                                 LocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.Location.LocationName).FirstOrDefault(),

                                                                 SubLocationId = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocationId).FirstOrDefault(),
                                                                 SubLocationKey = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.Guid).FirstOrDefault(),
                                                                 SubLocationName = x.AtAssetLocations.Where(y => y.AssetId == x.Id).Select(a => a.SubLocation.LocationName).FirstOrDefault(),

                                                                 EmployeeId = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.EmployeeId).FirstOrDefault(),
                                                                 EmployeeKey = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.Guid).FirstOrDefault(),
                                                                 EmployeeName = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.DisplayName).FirstOrDefault(),
                                                                 EmployeeCode = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.AccountCode).FirstOrDefault(),
                                                                 EmployeeEmail = x.AtAssetIssueDetails.Where(y => y.AssetId == x.Id && y.StatusId == AssetStatus.Issued).Select(a => a.Employee.EmailAddress).FirstOrDefault(),

                                                                 CreateDate = x.CreatedDate,
                                                                 CreatedBy = x.CreatedBy.DisplayName,

                                                                 StatusId = x.StatusId,
                                                                 StatusCode = x.Status.SystemName,
                                                                 Status = x.Status.Name,
                                                             }).OrderBy(_Request.SortExpression)
                                                             .Where(_Request.SearchCondition)
                                                             .Skip(_Request.Offset)
                                                             .Take(_Request.Limit)
                                                             .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        string? Status = _DataItem.Status;
                        string? _IssueStatus = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == _DataItem.AssetId && x.EmployeeId == _DataItem.EmployeeId).Select(x => x.Status.Name).FirstOrDefaultAsync();

                        if (!string.IsNullOrEmpty(_IssueStatus))
                        {
                            Status = _IssueStatus;
                        }

                        var _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == _DataItem.AssetId)
                                         .Select(x => new
                                         {
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();
                        bool AMC = false;
                        bool Insurance = false;
                        if (_VendorDetails.IsAmcApplicable == 1)
                        {
                            AMC = true;
                        }
                        else
                        {
                            AMC = false;
                        }
                        if (_VendorDetails.IsInsurances == 1)
                        {
                            Insurance = true;
                        }
                        else
                        {
                            Insurance = false;
                        }
                        _StockReportDownload.Add(new ODownload.StockReport
                        {
                            Asset_Name = _DataItem.AssetName,
                            Asset_Barcode = _DataItem.AssetBarcode,
                            Asset_SerialNo = _DataItem.AssetSerialNo ?? "--",
                            Employee = _DataItem.EmployeeName,
                            Employee_Code = _DataItem.EmployeeCode,
                            Email = _DataItem.EmployeeEmail,
                            Asset_Vendor = _VendorDetails.VendorName,
                            PO_Number = _VendorDetails.PoNumber,
                            Invoice_Number = _VendorDetails.InvoiceNumber,
                            Asset_Recieved_Date = _VendorDetails.RecievedDate.ToString(),
                            Warrenty_Start_Date = _VendorDetails.StartDate.ToString(),
                            Warrenty_End_Date = _VendorDetails.EndDate.ToString(),
                            Expiry_Date = _VendorDetails.ExpiryDate.ToString(),
                            Amount = _VendorDetails.Price,
                            Currency = _VendorDetails.Currency,
                            AMC = AMC,
                            Insurance = Insurance,
                            Division = _DataItem.DivisionName,
                            Department = _DataItem.DepartmentName,
                            Location = _DataItem.LocationName,
                            SubLocation = _DataItem.SubLocationName,
                            Status = Status,
                            User = _DataItem.CreatedBy,
                            Create_Date = _DataItem.CreateDate.ToString()
                        });
                    }

                    return _StockReportDownload;
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetWithoutBarcodeAssetsStock-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetAssetHistory(OList.Request _Request)
        {
            try
            {
                _HistoryDownload = new List<ODownload.AssetHistoryDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetHistories.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    Status = x.Guid,
                                                    Activity = x.Activity,
                                                    UserId = x.UserId,
                                                    ActivityDate = x.ActivityDate,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _History = await _HCoreContextLogging.AtAssetHistories.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                   .Select(x => new
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       AssetBarcode = x.AssetBarcode,
                                                       Status = x.Guid,
                                                       Activity = x.Activity,
                                                       UserId = x.UserId,
                                                       ActivityDate = x.ActivityDate,
                                                   }).OrderBy(_Request.SortExpression)
                                                   .Where(_Request.SearchCondition)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToListAsync();

                    foreach (var _DataItem in _History)
                    {
                        string? User = "";
                        string? ExAssetId = "";
                        string? AssetName = "";
                        string? SerialNumber = "";
                        using (_HCoreContext = new HCoreContext())
                        {
                            User = await _HCoreContext.AtAccounts.Where(x => x.Id == _DataItem.UserId).Select(x => x.DisplayName).FirstOrDefaultAsync();

                            var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _DataItem.AssetBarcode && x.OrganizationId == _Request.UserReference.OrganizationId)
                                               .Select(x => new
                                               {
                                                   ExAssetId = x.ExAssetId,
                                                   AssetName = x.AssetName,
                                                   SerialNumber = x.AssetSerialNumber,
                                               }).FirstOrDefaultAsync();

                            ExAssetId = AssetDetails.ExAssetId;
                            AssetName = AssetDetails.AssetName;
                            SerialNumber = AssetDetails.SerialNumber;
                        }

                        _HistoryDownload.Add(new ODownload.AssetHistoryDownload
                        {
                            Asset_Bracode = _DataItem.AssetBarcode,
                            SAP_Asset_Code = ExAssetId,
                            Asset_Name = AssetName,
                            Asset_Serial_No = SerialNumber,
                            Activity = _DataItem.Activity,
                            Status = _DataItem.Status,
                            User = User,
                            Activity_Date = _DataItem.ActivityDate.ToString()
                        });
                    }

                    return _HistoryDownload;
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetHistory-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetAssetAuditReport(OList.Request _Request)
        {
            try
            {
                _AuditReportDownload = new List<ODownload.AssetAuditReportDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetAuditLogs.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    Division = x.Division,
                                                    Location = x.Location,
                                                    SubLocation = x.SubLocation,
                                                    Department = x.Department,
                                                    Employee = x.Employee,
                                                    AssetStatus = x.AssetStatus,
                                                    AuditDivision = x.AuditDivision,
                                                    AuditLocation = x.AuditLocation,
                                                    AuditSubLocation = x.AuditSubLocation,
                                                    AuditStatus = x.AuditStatus,
                                                    AuditDate = x.AuditDate,
                                                    UserId = x.UserId,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _Assets = await _HCoreContextLogging.AtAssetAuditLogs.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                   .Select(x => new
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       AssetBarcode = x.AssetBarcode,
                                                       Division = x.Division,
                                                       Location = x.Location,
                                                       SubLocation = x.SubLocation,
                                                       Department = x.Department,
                                                       Employee = x.Employee,
                                                       AssetStatus = x.AssetStatus,
                                                       AuditDivision = x.AuditDivision,
                                                       AuditLocation = x.AuditLocation,
                                                       AuditSubLocation = x.AuditSubLocation,
                                                       AuditStatus = x.AuditStatus,
                                                       AuditDate = x.AuditDate,
                                                       UserId = x.UserId,
                                                   }).OrderBy(_Request.SortExpression)
                                                   .Where(_Request.SearchCondition)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        string? User = "";
                        string? ExAssetId = "";
                        using (_HCoreContext = new HCoreContext())
                        {
                            User = await _HCoreContext.AtAccounts.Where(x => x.Id == _DataItem.UserId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                            ExAssetId = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _DataItem.AssetBarcode && x.OrganizationId == _Request.UserReference.OrganizationId).Select(x => x.ExAssetId).FirstOrDefaultAsync();
                        }
                        _AuditReportDownload.Add(new ODownload.AssetAuditReportDownload
                        {
                            Asset_Barcode = _DataItem.AssetBarcode,
                            SAP_Asset_Code = ExAssetId,
                            Division = _DataItem.Division,
                            Location = _DataItem.Location,
                            Sub_Location = _DataItem.SubLocation,
                            Department = _DataItem.Department,
                            Employee = _DataItem.Employee,
                            Asset_Status = _DataItem.AssetStatus,
                            Audit_Division = _DataItem.AuditDivision,
                            Audit_Location = _DataItem.AuditLocation,
                            Audit_SubLocation = _DataItem.AuditSubLocation,
                            Audit_Status = _DataItem.AuditStatus,
                            Audit_Date = _DataItem.AuditDate.ToString(),
                            User = User,
                        });
                    }

                    return _AuditReportDownload;
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetAuditReport-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetAssetDepreciation(OList.Request _Request)
        {
            try
            {
                _AssetDepreciation = new List<ODownload.AssetDepreciation>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetDepreciations.Any() && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new ODownload.AssetDepreciationList
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetName = x.AssetName,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetSerialNo = x.AssetSerialNumber,
                                                    ExAssetId = x.ExAssetId,

                                                    AssetPrice = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.PurchasePrice).FirstOrDefault(),
                                                    ExpiresIn = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiresIn).FirstOrDefault(),
                                                    ReceivedDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.RecievedDate).FirstOrDefault(),
                                                    ExpiryDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiryDate).FirstOrDefault(),

                                                    AssetClassId = x.AssetClass.Id,
                                                    AssetClassKey = x.AssetClass.Guid,
                                                    AssetClassName = x.AssetClass.ClassName,

                                                    AssetCategoryId = x.AssetCategory.Id,
                                                    AssetCategoryKey = x.AssetCategory.Guid,
                                                    AssetCategoryName = x.AssetCategory.CategoryName,

                                                    AssetSubCategoryId = x.AssetSubCategory.Id,
                                                    AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                                    AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                                    DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.DepartmentName).FirstOrDefault(),

                                                    OpeningBookValueOnStartDate = 0,
                                                    ClosingBookValueOnStartDate = 0,
                                                    OpeningBookValueOnEndDate = 0,
                                                    ClosingBookValueOnEndDate = 0,
                                                    DepreciationExpense = 0,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    var _AssetDepreciations = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetDepreciations.Any() && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                   .Select(x => new ODownload.AssetDepreciationList
                                                   {
                                                       AssetId = x.Id,
                                                       AssetKey = x.Guid,
                                                       AssetName = x.AssetName,
                                                       AssetBarcode = x.AssetBarcode,
                                                       AssetSerialNo = x.AssetSerialNumber,
                                                       ExAssetId = x.ExAssetId,

                                                       AssetPrice = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.PurchasePrice).FirstOrDefault(),
                                                       ExpiresIn = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiresIn).FirstOrDefault(),
                                                       ReceivedDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.RecievedDate).FirstOrDefault(),
                                                       ExpiryDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiryDate).FirstOrDefault(),

                                                       AssetClassId = x.AssetClass.Id,
                                                       AssetClassKey = x.AssetClass.Guid,
                                                       AssetClassName = x.AssetClass.ClassName,

                                                       AssetCategoryId = x.AssetCategory.Id,
                                                       AssetCategoryKey = x.AssetCategory.Guid,
                                                       AssetCategoryName = x.AssetCategory.CategoryName,

                                                       AssetSubCategoryId = x.AssetSubCategory.Id,
                                                       AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                                       AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                                       DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                                       DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                                       DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                                       LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                                       LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                                       LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                                       SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                                       SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                                       SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                                       DepartmentId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.DepartmentId).FirstOrDefault(),
                                                       DepartmentKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.Guid).FirstOrDefault(),
                                                       DepartmentName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.DepartmentName).FirstOrDefault(),

                                                       OpeningBookValueOnStartDate = 0,
                                                       ClosingBookValueOnStartDate = 0,
                                                       OpeningBookValueOnEndDate = 0,
                                                       ClosingBookValueOnEndDate = 0,
                                                       DepreciationExpense = 0,
                                                   }).OrderBy(_Request.SortExpression)
                                                   .Where(_Request.SearchCondition)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToListAsync();

                    if (_AssetDepreciations.Count > 0)
                    {
                        foreach (var _Asset in _AssetDepreciations)
                        {
                            if (_Request.StartDate == null)
                            {
                                _Request.StartDate = _Asset.ReceivedDate;
                            }
                            if (_Request.EndDate == null)
                            {
                                _Request.EndDate = _Asset.ExpiryDate;
                            }

                            _Asset.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ReceivedDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                            _Asset.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ReceivedDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                            _Asset.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ExpiryDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                            _Asset.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ExpiryDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();

                            if (_Request.StartDate >= _Asset.ReceivedDate && _Request.EndDate <= _Asset.ExpiryDate)
                            {
                                _Asset.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                            else if (_Request.StartDate >= _Asset.ReceivedDate && _Request.EndDate > _Asset.ExpiryDate)
                            {
                                _Asset.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.OpeningBookValueOnEndDate = _Asset.OpeningBookValueOnEndDate;
                                _Asset.ClosingBookValueOnEndDate = _Asset.ClosingBookValueOnEndDate;
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                            else if (_Request.StartDate < _Asset.ReceivedDate && _Request.EndDate <= _Asset.ExpiryDate)
                            {
                                _Asset.OpeningBookValueOnStartDate = _Asset.OpeningBookValueOnStartDate;
                                _Asset.ClosingBookValueOnStartDate = _Asset.ClosingBookValueOnStartDate;
                                _Asset.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                            else
                            {
                                _Asset.OpeningBookValueOnStartDate = _Asset.OpeningBookValueOnStartDate;
                                _Asset.ClosingBookValueOnStartDate = _Asset.ClosingBookValueOnStartDate;
                                _Asset.OpeningBookValueOnEndDate = _Asset.OpeningBookValueOnEndDate;
                                _Asset.ClosingBookValueOnEndDate = _Asset.ClosingBookValueOnEndDate;
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }

                            _AssetDepreciation.Add(new ODownload.AssetDepreciation
                            {
                                Asset_Name = _Asset.AssetName,
                                Asset_Barcode = _Asset.AssetBarcode,
                                Asset_Serial_No = _Asset.AssetSerialNo,
                                SAP_Asset_Code = _Asset.ExAssetId,
                                Asset_Class = _Asset.AssetClassName,
                                Asset_Category = _Asset.AssetCategoryName,
                                Asset_Sub_Category = _Asset.AssetSubCategoryName,
                                Division = _Asset.DivisionName,
                                Department = _Asset.DepartmentName ?? "--",
                                Location = _Asset.LocationName,
                                SubLocation = _Asset.SubLocationName,
                                Opening_Book_Value_On_Start_Date = _Asset.OpeningBookValueOnStartDate,
                                Closing_Book_Value_On_Start_Date = _Asset.ClosingBookValueOnStartDate,
                                Opening_Book_Value_On_End_Date = _Asset.OpeningBookValueOnEndDate,
                                Closing_Book_Value_On_End_Date = _Asset.ClosingBookValueOnEndDate,
                                Depreciation_Expense = _Asset.DepreciationExpense,
                                Received_Date = _Asset.ReceivedDate.ToString(),
                                Expiry_Date = _Asset.ExpiryDate.ToString(),
                                Useful_Life = _Asset.ExpiresIn,
                                Purchase_Price = _Asset.AssetPrice
                            });
                        }
                    }

                    return _AssetDepreciation;
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetDepreciation-Download", _Exception, _Request.UserReference);
            }
        }

        internal async Task<object> GetAssetDepreciation(OReference _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _Depreciation = new ODownload.AssetDepreciationDownload.Response();
                    _AssetDepreciationDownload = new List<ODownload.AssetDepreciationDownload.Response>();
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                      .Select(x => new
                                                      {
                                                          AssetId = x.Id,
                                                          AssetKey = x.Guid,
                                                          AssetName = x.AssetName,
                                                          AssetBarcode = x.AssetBarcode,
                                                          AssetSerialNumber = x.AssetSerialNumber,
                                                          ExAssetId = x.ExAssetId,
                                                          AssetType = x.AssetType.Name,
                                                          AssetMake = x.AssetMake,
                                                          AssetModel = x.AssetModel,
                                                          AssetClass = x.AssetClass.ClassName,
                                                          AssetCategory = x.AssetCategory.CategoryName,
                                                          AssetSubCategory = x.AssetSubCategory.SubCategoryName,
                                                          AssetPrice = x.AtAssetVendorDetails.Select(a => a.PurchasePrice).FirstOrDefault(),
                                                          SalvageValue = x.SalvageValue,
                                                          ReceivedDate = x.AtAssetVendorDetails.Select(a => a.RecievedDate).FirstOrDefault(),
                                                          ExpiryDate = x.AtAssetVendorDetails.Select(a => a.ExpiryDate).FirstOrDefault(),
                                                          UsefulLife = x.AtAssetVendorDetails.Select(a => a.ExpiresIn).FirstOrDefault(),
                                                      }).FirstOrDefaultAsync();

                    if (AssetDetails != null)
                    {
                        _Depreciation.Asset_Name = AssetDetails.AssetName;
                        _Depreciation.Asset_Barcode = AssetDetails.AssetBarcode;
                        _Depreciation.Asset_Serial_No = AssetDetails.AssetSerialNumber;
                        _Depreciation.SAP_Asset_Code = AssetDetails.ExAssetId;
                        _Depreciation.Asset_Type = AssetDetails.AssetType;
                        _Depreciation.Asset_Make = AssetDetails.AssetMake;
                        _Depreciation.Asset_Model = AssetDetails.AssetModel;
                        _Depreciation.Asset_Class = AssetDetails.AssetClass;
                        _Depreciation.Asset_Category = AssetDetails.AssetCategory;
                        _Depreciation.Asset_Sub_Category = AssetDetails.AssetSubCategory;
                        _Depreciation.Asset_Price = AssetDetails.AssetPrice;
                        _Depreciation.Salvage_Value = AssetDetails.SalvageValue;
                        _Depreciation.Received_Date = AssetDetails.ReceivedDate.ToString();
                        _Depreciation.Expiry_Date = AssetDetails.ExpiryDate.ToString();
                        _Depreciation.Useful_Life = AssetDetails.UsefulLife;

                        DateTime StartDate = (DateTime)AssetDetails.ReceivedDate;
                        DateTime EndDate = (DateTime)AssetDetails.ExpiryDate;

                        int? DepreciationMethodId = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == AssetDetails.AssetId).Select(x => x.DepreciationMethodId).FirstOrDefaultAsync();
                        _Depreciation.Depreciation_Method = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == AssetDetails.AssetId).Select(x => x.DepreciationMethod.Name).FirstOrDefaultAsync();
                        _Depreciation.Depreciation_Rate = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == AssetDetails.AssetId).Select(x => x.DepreciationRate).FirstOrDefaultAsync();

                        _Depreciation.Opening_Book_Value_On_Received_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == StartDate.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                        _Depreciation.Closing_Book_Value_On_Received_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == StartDate.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                        _Depreciation.Opening_Book_Value_On_Expiry_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == EndDate.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                        _Depreciation.Closing_Book_Value_On_Expiry_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == EndDate.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                        _Depreciation.Depreciation_Cost_On_Expiry_Date = _Depreciation.Opening_Book_Value_On_Received_Date - _Depreciation.Closing_Book_Value_On_Expiry_Date;

                        if (_Request.StartDate == null)
                        {
                            _Request.StartDate = StartDate;
                        }
                        if (_Request.EndDate == null)
                        {
                            _Request.EndDate = EndDate;
                        }

                        _Depreciation.Opening_Book_Value_On_Start_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == _Request.StartDate.Value.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                        _Depreciation.Closing_Book_Value_On_Start_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == _Request.StartDate.Value.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                        _Depreciation.Opening_Book_Value_On_End_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == _Request.EndDate.Value.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                        _Depreciation.Closing_Book_Value_On_End_Date = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == AssetDetails.AssetId && x.Date.Value.Date == _Request.EndDate.Value.Date).Select(x => x.BookValue).FirstOrDefaultAsync();

                        if (_Request.StartDate >= StartDate && _Request.EndDate <= EndDate)
                        {
                            _Depreciation.Opening_Book_Value_On_Start_Date = _Depreciation.Opening_Book_Value_On_Start_Date;
                            _Depreciation.Closing_Book_Value_On_Start_Date = _Depreciation.Closing_Book_Value_On_Start_Date;
                            _Depreciation.Opening_Book_Value_On_End_Date = _Depreciation.Opening_Book_Value_On_End_Date;
                            _Depreciation.Closing_Book_Value_On_End_Date = _Depreciation.Closing_Book_Value_On_End_Date;
                            _Depreciation.Depreciation_Cost_On_End_Date = _Depreciation.Opening_Book_Value_On_Start_Date - _Depreciation.Closing_Book_Value_On_End_Date;
                        }
                        else if (_Request.StartDate >= StartDate && _Request.EndDate > EndDate)
                        {
                            _Depreciation.Opening_Book_Value_On_Start_Date = _Depreciation.Opening_Book_Value_On_Start_Date;
                            _Depreciation.Closing_Book_Value_On_Start_Date = _Depreciation.Closing_Book_Value_On_Start_Date;
                            _Depreciation.Opening_Book_Value_On_End_Date = _Depreciation.Opening_Book_Value_On_Expiry_Date;
                            _Depreciation.Closing_Book_Value_On_End_Date = _Depreciation.Closing_Book_Value_On_Expiry_Date;
                            _Depreciation.Depreciation_Cost_On_End_Date = _Depreciation.Opening_Book_Value_On_Start_Date - _Depreciation.Closing_Book_Value_On_End_Date;
                        }
                        else if (_Request.StartDate < StartDate && _Request.EndDate <= EndDate)
                        {
                            _Depreciation.Opening_Book_Value_On_Start_Date = _Depreciation.Opening_Book_Value_On_Received_Date;
                            _Depreciation.Closing_Book_Value_On_Start_Date = _Depreciation.Closing_Book_Value_On_Received_Date;
                            _Depreciation.Opening_Book_Value_On_End_Date = _Depreciation.Opening_Book_Value_On_End_Date;
                            _Depreciation.Closing_Book_Value_On_End_Date = _Depreciation.Closing_Book_Value_On_End_Date;
                            _Depreciation.Depreciation_Cost_On_End_Date = _Depreciation.Opening_Book_Value_On_Start_Date - _Depreciation.Closing_Book_Value_On_End_Date;
                        }
                        else
                        {
                            _Depreciation.Opening_Book_Value_On_Start_Date = _Depreciation.Opening_Book_Value_On_Received_Date;
                            _Depreciation.Closing_Book_Value_On_Start_Date = _Depreciation.Closing_Book_Value_On_Received_Date;
                            _Depreciation.Opening_Book_Value_On_End_Date = _Depreciation.Opening_Book_Value_On_Expiry_Date;
                            _Depreciation.Closing_Book_Value_On_End_Date = _Depreciation.Closing_Book_Value_On_Expiry_Date;
                            _Depreciation.Depreciation_Cost_On_End_Date = _Depreciation.Opening_Book_Value_On_Start_Date - _Depreciation.Closing_Book_Value_On_End_Date;
                        }

                        if (_Depreciation != null)
                        {
                            _AssetDepreciationDownload.Add(new ODownload.AssetDepreciationDownload.Response
                            {
                                Asset_Name = _Depreciation.Asset_Name,
                                Asset_Barcode = _Depreciation.Asset_Barcode,
                                Asset_Serial_No = _Depreciation.Asset_Serial_No,
                                SAP_Asset_Code = _Depreciation.SAP_Asset_Code,
                                Asset_Type = _Depreciation.Asset_Type,
                                Asset_Make = _Depreciation.Asset_Make,
                                Asset_Model = _Depreciation.Asset_Model,
                                Asset_Class = _Depreciation.Asset_Class,
                                Asset_Category = _Depreciation.Asset_Category,
                                Asset_Sub_Category = _Depreciation.Asset_Sub_Category,
                                Asset_Price = _Depreciation.Asset_Price,
                                Salvage_Value = _Depreciation.Salvage_Value,
                                Received_Date = _Depreciation.Received_Date,
                                Expiry_Date = _Depreciation.Expiry_Date,
                                Useful_Life = _Depreciation.Useful_Life,
                                Opening_Book_Value_On_Received_Date = _Depreciation.Opening_Book_Value_On_Received_Date,
                                Closing_Book_Value_On_Received_Date = _Depreciation.Closing_Book_Value_On_Received_Date,
                                Opening_Book_Value_On_Expiry_Date = _Depreciation.Opening_Book_Value_On_Expiry_Date,
                                Closing_Book_Value_On_Expiry_Date = _Depreciation.Closing_Book_Value_On_Expiry_Date,
                                Depreciation_Cost_On_Expiry_Date = _Depreciation.Depreciation_Cost_On_Expiry_Date,
                                Opening_Book_Value_On_Start_Date = _Depreciation.Opening_Book_Value_On_Start_Date,
                                Closing_Book_Value_On_Start_Date = _Depreciation.Closing_Book_Value_On_Start_Date,
                                Opening_Book_Value_On_End_Date = _Depreciation.Opening_Book_Value_On_End_Date,
                                Closing_Book_Value_On_End_Date = _Depreciation.Closing_Book_Value_On_End_Date,
                                Depreciation_Cost_On_End_Date = _Depreciation.Depreciation_Cost_On_End_Date,
                                Depreciation_Rate = _Depreciation.Depreciation_Rate,
                                Depreciation_Method = _Depreciation.Depreciation_Method,
                            });
                        }

                        return _AssetDepreciationDownload;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetDepreciation-Download", _Exception, _Request.UserReference);
            }
        }
    }
}
