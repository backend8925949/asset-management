﻿using HCore.Data.Logging.Models;
using HCore.Data.Logging;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Assets;
using HCore.RKBS.Core.Operations.Master;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using System.Text;
using HCore.RKBS.Core.Object.BackgroundProcessor;
using HCore.RKBS.Core.Operations.BackgroundProcessor;

namespace HCore.RKBS.Core.Framework.Assets
{
    public class FrameworkAssetOperations
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        AtAssetReturnDetail? _AtAssetReturnDetail;
        AtAssetIssueDetail? _AtAssetIssueDetail;
        AtAssetMaintenance? _AtAssetMaintenance;
        AtAssetRegister? _AtAssetRegister;
        AtAssetVendorDetail? _AtAssetVendorDetail;
        AtAssetInsuranceDetail? _AtAssetInsuranceDetail;
        AtAssetAmcDetail? _AtAssetAmcDetail;
        AtAssetUpload? _AtAssetUpload;
        AtAssetLocation? _AtAssetLocation;
        AtAssetDisposeLog? _AtAssetDisposeLog;
        AtAssetHistory? _AtAssetHistory;
        ManageBackgroundProcessor? _ManageBackgroundProcessor;
        OBackgroundProcessor.Request? _AssetAuditRequest;
        AtAssetRequisition? _AssetRequisition;
        AtAssetTransferDetail? _AssetTransferDetail;

        internal async Task<OResponse> RegisterAsset(OAssetOperations.Register _Request)
        {
            try
            {
                long? VendorId = 0;
                if (_Request.VendorDetails != null)
                {
                    if (!string.IsNullOrEmpty(_Request.VendorDetails.VendorKey))
                    {
                        VendorId = HCoreHelper.GetAccountId(_Request.VendorDetails.VendorKey, _Request.UserReference);
                        if (VendorId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid vendor");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT004", "Please select vendor");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Vendor details required");
                }

                long? AssetDivisionId = 0;
                long? AssetLocationId = 0;
                long? AssetSubLocationId = 0;
                if (_Request.LocationDetails != null)
                {
                    if (!string.IsNullOrEmpty(_Request.LocationDetails.DivisionKey))
                    {
                        AssetDivisionId = HCoreHelper.GetDivisionId(_Request.LocationDetails.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                        if (AssetDivisionId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid division");
                        }
                        if (!string.IsNullOrEmpty(_Request.LocationDetails.LocationKey))
                        {
                            AssetLocationId = HCoreHelper.GetLocationId(_Request.LocationDetails.LocationKey, _Request.UserReference);
                            if (AssetLocationId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid location");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select location");
                        }
                        if (!string.IsNullOrEmpty(_Request.LocationDetails.SubLocationKey))
                        {
                            AssetSubLocationId = HCoreHelper.GetSubLocationId(_Request.LocationDetails.SubLocationKey, (long)AssetLocationId, _Request.UserReference);
                            if (AssetSubLocationId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid sub location");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select sub location");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Location details required");
                }

                long? AccountId = 0;
                long? DivisionId = 0;
                long? DepartmentId = 0;
                long? LocationId = 0;
                long? SubLocationId = 0;
                if (_Request.IssueDetails != null)
                {
                    if (!string.IsNullOrEmpty(_Request.IssueDetails.DivisionKey))
                    {
                        DivisionId = HCoreHelper.GetDivisionId(_Request.IssueDetails.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                        if (DivisionId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid division");
                        }
                        if (!string.IsNullOrEmpty(_Request.IssueDetails.DepartmentKey))
                        {
                            DepartmentId = HCoreHelper.GetDepartmentId(_Request.IssueDetails.DepartmentKey, _Request.UserReference);
                            if (DepartmentId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid department");
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.IssueDetails.LocationKey))
                        {
                            LocationId = HCoreHelper.GetLocationId(_Request.IssueDetails.LocationKey, _Request.UserReference);
                            if (LocationId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid location");
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.IssueDetails.SubLocationKey))
                        {
                            SubLocationId = HCoreHelper.GetSubLocationId(_Request.IssueDetails.SubLocationKey, (long)LocationId, _Request.UserReference);
                            if (SubLocationId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid sublocation");
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.IssueDetails.EmployeeKey))
                        {
                            AccountId = HCoreHelper.GetAccountId(_Request.IssueDetails.EmployeeKey, _Request.UserReference);
                            if (AccountId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid employee");
                            }
                        }
                    }
                }

                int? AssetTypeId = HCoreHelper.GetSystemHelperId(_Request.AssetType, _Request.UserReference);
                long? ClassId = HCoreHelper.GetClassId(_Request.AssetClass, _Request.UserReference);
                long? CategoryId = HCoreHelper.GetCategoryId(_Request.AssetCategory, _Request.UserReference);
                long? SubCategoryId = HCoreHelper.GetSubCategoryId(_Request.AssetSubCategory, (long)CategoryId, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    bool _IsExists = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetSerialNumber == _Request.AssetSerialNo && x.OrganizationId == _Request.UserReference.OrganizationId).AnyAsync();
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATEXISTS", "Asset details already exist with the serial number entered");
                    }

                    _AtAssetRegister = new AtAssetRegister();
                    _AtAssetRegister.Guid = HCoreHelper.GenerateGuid();
                    _AtAssetRegister.AssetTypeId = (int)AssetTypeId;
                    _AtAssetRegister.OrganizationId = _Request.UserReference.OrganizationId;
                    _AtAssetRegister.AssetClassId = (int)ClassId;
                    _AtAssetRegister.AssetCategoryId = (int)CategoryId;
                    _AtAssetRegister.AssetSubCategoryId = (int)SubCategoryId;
                    _AtAssetRegister.AssetBarcode = _Request.Barcode;
                    _AtAssetRegister.AssetSerialNumber = _Request.AssetSerialNo;
                    _AtAssetRegister.SerialNumber = _Request.SerialNo;
                    _AtAssetRegister.AssetName = _Request.AssetName;
                    _AtAssetRegister.ExAssetId = _Request.ExAssetId;
                    _AtAssetRegister.AssetMake = _Request.AssetMake;
                    _AtAssetRegister.AssetModel = _Request.AssetModel;
                    _AtAssetRegister.TotalQuantity = _Request.TotalQuantity;
                    _AtAssetRegister.RemainingQuantity = _Request.TotalQuantity;
                    _AtAssetRegister.SalvageValue = _Request.SalvageValue;
                    _AtAssetRegister.StatusId = AssetStatus.Created;
                    _AtAssetRegister.CreatedById = _Request.UserReference.AccountId;
                    _AtAssetRegister.CreatedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.AtAssetRegisters.AddAsync(_AtAssetRegister);
                    await _HCoreContext.SaveChangesAsync();

                    _AtAssetLocation = new AtAssetLocation();
                    _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                    _AtAssetLocation.AssetId = _AtAssetRegister.Id;
                    _AtAssetLocation.DivisionId = (long)AssetDivisionId;
                    _AtAssetLocation.LocationId = (long)AssetLocationId;
                    _AtAssetLocation.SubLocationId = (long)AssetSubLocationId;
                    _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                    _AtAssetLocation.CreatedById = _Request.UserReference.AccountId;
                    _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                    await _HCoreContext.SaveChangesAsync();

                    string? Division = "";
                    string? Location = "";
                    string? SubLocation = "";
                    Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetDivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                    Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetLocationId && x.DivisionId == AssetDivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                    SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetSubLocationId && x.ParentId == AssetLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                    string? Body = "This asset has been registered at <b>" + Division + ", " + Location + ", " + SubLocation + "</b>";

                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {
                        _AtAssetHistory = new AtAssetHistory();
                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetHistory.OrganizationId = _AtAssetRegister.OrganizationId;
                        _AtAssetHistory.AssetBarcode = _AtAssetRegister.AssetBarcode;
                        _AtAssetHistory.Status = "Registered";
                        _AtAssetHistory.Activity = Body;
                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                        _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                        await _HCoreContextLogging.SaveChangesAsync();
                    }

                    if (_Request.VendorDetails != null)
                    {
                        _AtAssetVendorDetail = new AtAssetVendorDetail();
                        _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetVendorDetail.AssetId = _AtAssetRegister.Id;
                        _AtAssetVendorDetail.VendorId = (long)VendorId;
                        _AtAssetVendorDetail.PoNumber = _Request.VendorDetails.PoNumber;
                        _AtAssetVendorDetail.InvoiceNumber = _Request.VendorDetails.InvoiceNumber;
                        _AtAssetVendorDetail.RecievedDate = _Request.VendorDetails.RecievedDate;
                        _AtAssetVendorDetail.WarrantyStartDate = _Request.VendorDetails.StartDate;
                        _AtAssetVendorDetail.WarrantyEndDate = _Request.VendorDetails.EndDate;
                        _AtAssetVendorDetail.ExpiresIn = _Request.VendorDetails.ExpireInYears;

                        if (_Request.VendorDetails.RecievedDate != null)
                        {
                            _AtAssetVendorDetail.ExpiryDate = _Request.VendorDetails.RecievedDate.Value.AddYears((int)_Request.VendorDetails.ExpireInYears);
                        }
                        else if (_Request.VendorDetails.RecievedDate == null && _Request.VendorDetails.StartDate != null)
                        {
                            _AtAssetVendorDetail.ExpiryDate = _Request.VendorDetails.StartDate.Value.AddYears((int)_Request.VendorDetails.ExpireInYears);
                        }
                        else
                        {
                            _AtAssetVendorDetail.ExpiryDate = HCoreHelper.GetDateTime().AddYears((int)_Request.VendorDetails.ExpireInYears);
                        }

                        _AtAssetVendorDetail.Currency = _Request.VendorDetails.Currency;
                        _AtAssetVendorDetail.PurchasePrice = _Request.VendorDetails.PurchasePrice;
                        _AtAssetVendorDetail.AdditionalPrice = _Request.VendorDetails.AdditionalPrice;

                        if (_Request.VendorDetails.IsAmc == true)
                        {
                            _AtAssetVendorDetail.IsAmcApplicable = 1;
                        }
                        else
                        {
                            _AtAssetVendorDetail.IsAmcApplicable = 0;
                        }

                        if (_Request.VendorDetails.IsInsurance == true)
                        {
                            _AtAssetVendorDetail.IsInsurance = 1;
                        }
                        else
                        {
                            _AtAssetVendorDetail.IsInsurance = 0;
                        }

                        _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                        _AtAssetVendorDetail.CreatedById = _Request.UserReference.AccountId;
                        _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                        await _HCoreContext.SaveChangesAsync();

                        if (_Request.VendorDetails.IsAmc == true)
                        {
                            foreach (var Amc in _Request.VendorDetails.AmcDetails)
                            {
                                long? AmcVendorId = HCoreHelper.GetAccountId(Amc.VendorKey, _Request.UserReference);
                                if (AmcVendorId < 1)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select vednor for amc");
                                }

                                _AtAssetAmcDetail = new AtAssetAmcDetail();
                                _AtAssetAmcDetail.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetAmcDetail.VendorId = (long)AmcVendorId;
                                _AtAssetAmcDetail.VendorDetailsId = _AtAssetVendorDetail.Id;
                                _AtAssetAmcDetail.StartDate = (DateTime)Amc.StartDate;
                                _AtAssetAmcDetail.EndDate = (DateTime)Amc.EndDate;
                                _AtAssetAmcDetail.Price = Amc.Amount;
                                _AtAssetAmcDetail.StatusId = HelperStatus.Default.Active;
                                _AtAssetAmcDetail.CreatedDate = HCoreHelper.GetDateTime();
                                _AtAssetAmcDetail.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.AtAssetAmcDetails.AddAsync(_AtAssetAmcDetail);
                            }
                            await _HCoreContext.SaveChangesAsync();
                        }

                        if (_Request.VendorDetails.IsInsurance == true)
                        {
                            foreach (var Insurance in _Request.VendorDetails.InsuranceDetails)
                            {
                                long? InsuranceVendorId = HCoreHelper.GetAccountId(Insurance.VendorKey, _Request.UserReference);
                                if (InsuranceVendorId < 1)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select vednor for amc");
                                }

                                _AtAssetInsuranceDetail = new AtAssetInsuranceDetail();
                                _AtAssetInsuranceDetail.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetInsuranceDetail.VendorId = (long)InsuranceVendorId;
                                _AtAssetInsuranceDetail.VendorDetailsId = _AtAssetVendorDetail.Id;
                                _AtAssetInsuranceDetail.StartDate = (DateTime)Insurance.StartDate;
                                _AtAssetInsuranceDetail.EndDate = (DateTime)Insurance.EndDate;
                                _AtAssetInsuranceDetail.Price = Insurance.Amount;
                                _AtAssetInsuranceDetail.StatusId = HelperStatus.Default.Active;
                                _AtAssetInsuranceDetail.CreatedDate = HCoreHelper.GetDateTime();
                                _AtAssetInsuranceDetail.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.AtAssetInsuranceDetails.AddAsync(_AtAssetInsuranceDetail);
                            }
                            await _HCoreContext.SaveChangesAsync();
                        }
                    }

                    if (_Request.IssueDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.IssueDetails.DivisionKey))
                        {
                            _AtAssetIssueDetail = new AtAssetIssueDetail();
                            _AtAssetIssueDetail.Guid = HCoreHelper.GenerateGuid();
                            _AtAssetIssueDetail.AssetId = _AtAssetRegister.Id;
                            _AtAssetIssueDetail.OrganizationId = _Request.UserReference.OrganizationId;
                            _AtAssetIssueDetail.DivisionId = (long)DivisionId;
                            if (DepartmentId > 0)
                            {
                                _AtAssetIssueDetail.DepartmentId = (long)DepartmentId;
                            }
                            if (AccountId > 0)
                            {
                                _AtAssetIssueDetail.EmployeeId = (long)AccountId;
                            }
                            _AtAssetIssueDetail.LocationId = (long)LocationId;
                            _AtAssetIssueDetail.SubLocationId = (long)SubLocationId;
                            _AtAssetIssueDetail.Remark = _Request.IssueDetails.Remark;
                            _AtAssetIssueDetail.OsName = _Request.IssueDetails.OsName;
                            _AtAssetIssueDetail.HostName = _Request.IssueDetails.HostName;
                            _AtAssetIssueDetail.IpAddress = _Request.IssueDetails.IpAddress;
                            _AtAssetIssueDetail.Quantity = _Request.IssueDetails.Quantity;
                            _AtAssetIssueDetail.IsTransfered = 0;
                            _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                            _AtAssetIssueDetail.CreatedById = _Request.UserReference.AccountId;
                            _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                            _HCoreContext.AtAssetIssueDetails.Add(_AtAssetIssueDetail);
                            await _HCoreContext.SaveChangesAsync();

                            var AssetLocationDetails = await _HCoreContext.AtAssetLocations.Where(x => x.AssetId == _AtAssetRegister.Id).FirstOrDefaultAsync();
                            if (AssetLocationDetails != null)
                            {
                                AssetLocationDetails.SubLocationId = (long)SubLocationId;
                                AssetLocationDetails.LocationId = (long)LocationId;
                                AssetLocationDetails.DivisionId = (long)DivisionId;
                            }

                            var Asset = await _HCoreContext.AtAssetRegisters.Where(x => x.Guid == _AtAssetRegister.Guid).FirstOrDefaultAsync();
                            if (Asset != null)
                            {
                                Asset.RemainingQuantity = _AtAssetRegister.TotalQuantity - _AtAssetIssueDetail.Quantity;
                                Asset.StatusId = AssetStatus.Issued;
                                await _HCoreContext.SaveChangesAsync();
                            }

                            Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                            Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                            SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                            Body = "This asset has been assigned/issued to <b>";

                            string? EmployeeName = "";
                            string? EmployeeCode = "";
                            string? EmployeeEmail = "";

                            if (AccountId > 0)
                            {
                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                        .Select(x => new
                                                        {
                                                            Name = x.DisplayName,
                                                            AccountCode = x.AccountCode,
                                                            Email = x.EmailAddress
                                                        }).FirstOrDefaultAsync();
                                if (Employee != null)
                                {
                                    EmployeeName = Employee.Name;
                                    EmployeeCode = Employee.AccountCode;
                                    EmployeeEmail = Employee.Email;
                                }

                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                            }

                            string? Department = "";

                            if (DepartmentId > 0)
                            {
                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                if (!string.IsNullOrEmpty(Department))
                                {
                                    Body += Department + ", ";
                                }
                            }

                            Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _AtAssetRegister.OrganizationId;
                                _AtAssetHistory.AssetBarcode = _AtAssetRegister.AssetBarcode;
                                _AtAssetHistory.Status = "Issued";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                    }

                    var _Response = new
                    {
                        AssetId = _AtAssetRegister.Id,
                        AssetKey = _AtAssetRegister.Guid,
                        AssetBarcode = _AtAssetRegister.AssetBarcode,
                        ExAssetId = _AtAssetRegister.ExAssetId,
                        CompanyId = _AtAssetRegister.OrganizationId,
                        CompanyCode = _Request.UserReference.OrganizationCode,
                        CompanyName = _Request.UserReference.OrganizationDisplayName,
                    };

                    _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                    _AssetAuditRequest = new OBackgroundProcessor.Request();
                    _AssetAuditRequest.AssetId = _AtAssetRegister.Id;
                    _AssetAuditRequest.AssetKey = _AtAssetRegister.Guid;
                    _AssetAuditRequest.UserReference = _Request.UserReference;
                    await _ManageBackgroundProcessor.AssetAudit(_AssetAuditRequest);

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "ATSAVE", "Asset registered successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("RegisterAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> UpdateAsset(OAssetOperations.Register _Request)
        {
            try
            {
                long? VendorId = 0;
                if (_Request.VendorDetails != null)
                {
                    if (!string.IsNullOrEmpty(_Request.VendorDetails.VendorKey))
                    {
                        VendorId = HCoreHelper.GetAccountId(_Request.VendorDetails.VendorKey, _Request.UserReference);
                        if (VendorId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid vendor");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT004", "Please select vendor");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Vendor details required");
                }

                long? AssetDivisionId = 0;
                long? AssetLocationId = 0;
                long? AssetSubLocationId = 0;
                if (_Request.LocationDetails != null)
                {
                    if (!string.IsNullOrEmpty(_Request.LocationDetails.DivisionKey))
                    {
                        AssetDivisionId = HCoreHelper.GetDivisionId(_Request.LocationDetails.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                        if (AssetDivisionId < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid division");
                        }
                        if (!string.IsNullOrEmpty(_Request.LocationDetails.LocationKey))
                        {
                            AssetLocationId = HCoreHelper.GetLocationId(_Request.LocationDetails.LocationKey, _Request.UserReference);
                            if (AssetLocationId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid location");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select location");
                        }
                        if (!string.IsNullOrEmpty(_Request.LocationDetails.SubLocationKey))
                        {
                            AssetSubLocationId = HCoreHelper.GetSubLocationId(_Request.LocationDetails.SubLocationKey, (long)AssetLocationId, _Request.UserReference);
                            if (AssetSubLocationId < 1)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid sub location");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select sub location");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select division");
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Location details required");
                }

                int? AssetTypeId = HCoreHelper.GetSystemHelperId(_Request.AssetType, _Request.UserReference);
                long? ClassId = HCoreHelper.GetClassId(_Request.AssetClass, _Request.UserReference);
                long? CategoryId = HCoreHelper.GetCategoryId(_Request.AssetCategory, _Request.UserReference);
                long? SubCategoryId = HCoreHelper.GetSubCategoryId(_Request.AssetSubCategory, (long)CategoryId, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == _Request.AssetId && x.Guid == _Request.AssetKey).FirstOrDefaultAsync();
                    if (AssetDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Asset details not found");
                    }

                    if (AssetTypeId > 0 && AssetDetails.AssetTypeId != (int)AssetTypeId)
                    {
                        AssetDetails.AssetTypeId = (int)AssetTypeId;
                    }
                    if (ClassId > 0 && AssetDetails.AssetClassId != (int)ClassId)
                    {
                        AssetDetails.AssetClassId = (int)ClassId;
                    }
                    if (CategoryId > 0 && AssetDetails.AssetCategoryId != (int)CategoryId)
                    {
                        AssetDetails.AssetCategoryId = (int)CategoryId;
                    }
                    if (SubCategoryId > 0 && AssetDetails.AssetSubCategoryId != (int)SubCategoryId)
                    {
                        AssetDetails.AssetSubCategoryId = (int)SubCategoryId;
                    }
                    if (!string.IsNullOrEmpty(_Request.AssetSerialNo) && AssetDetails.AssetSerialNumber != _Request.AssetSerialNo)
                    {
                        AssetDetails.AssetSerialNumber = _Request.AssetSerialNo;
                    }
                    if (!string.IsNullOrEmpty(_Request.AssetName) && AssetDetails.AssetName != _Request.AssetName)
                    {
                        AssetDetails.AssetName = _Request.AssetName;
                    }
                    if (!string.IsNullOrEmpty(_Request.ExAssetId) && AssetDetails.ExAssetId != _Request.ExAssetId)
                    {
                        AssetDetails.ExAssetId = _Request.ExAssetId;
                    }
                    if (!string.IsNullOrEmpty(_Request.AssetMake) && AssetDetails.AssetMake != _Request.AssetMake)
                    {
                        AssetDetails.AssetMake = _Request.AssetMake;
                    }
                    if (!string.IsNullOrEmpty(_Request.AssetModel) && AssetDetails.AssetModel != _Request.AssetModel)
                    {
                        AssetDetails.AssetModel = _Request.AssetModel;
                    }
                    if (AssetDetails.AssetTypeId == AssetType.WithoutBarcode && AssetDetails.TotalQuantity != _Request.TotalQuantity)
                    {
                        if (AssetDetails.TotalQuantity < _Request.TotalQuantity)
                        {
                            AssetDetails.RemainingQuantity += _Request.TotalQuantity - AssetDetails.TotalQuantity;
                        }
                        else
                        {
                            AssetDetails.RemainingQuantity -= AssetDetails.TotalQuantity - _Request.TotalQuantity;
                        }
                    }
                    if (_Request.TotalQuantity > 0 && AssetDetails.TotalQuantity != _Request.TotalQuantity)
                    {
                        AssetDetails.TotalQuantity = _Request.TotalQuantity;
                    }
                    if (_Request.SalvageValue > 0 && AssetDetails.SalvageValue != _Request.SalvageValue)
                    {
                        AssetDetails.SalvageValue = _Request.SalvageValue;
                    }

                    AssetDetails.ModifiedById = _Request.UserReference.AccountId;
                    AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.SaveChangesAsync();

                    var AssetLocationDetails = await _HCoreContext.AtAssetLocations.Where(x => x.AssetId == AssetDetails.Id).FirstOrDefaultAsync();
                    if (AssetLocationDetails != null)
                    {
                        if (AssetDivisionId > 0 && AssetLocationDetails.DivisionId != AssetDivisionId)
                        {
                            AssetLocationDetails.DivisionId = (long)AssetDivisionId;
                        }
                        if (AssetLocationId > 0 && AssetLocationDetails.LocationId != AssetLocationId)
                        {
                            AssetLocationDetails.LocationId = (long)AssetLocationId;
                        }
                        if (AssetSubLocationId > 0 && AssetLocationDetails.SubLocationId != AssetSubLocationId)
                        {
                            AssetLocationDetails.SubLocationId = (long)AssetSubLocationId;
                        }
                        AssetLocationDetails.ModifiedById = _Request.UserReference.AccountId;
                        AssetLocationDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.SaveChangesAsync();
                    }
                    else
                    {
                        _AtAssetLocation = new AtAssetLocation();
                        _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetLocation.AssetId = AssetDetails.Id;
                        _AtAssetLocation.DivisionId = (long)AssetDivisionId;
                        _AtAssetLocation.LocationId = (long)AssetLocationId;
                        _AtAssetLocation.SubLocationId = (long)AssetSubLocationId;
                        _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                        _AtAssetLocation.CreatedById = _Request.UserReference.AccountId;
                        _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                        await _HCoreContext.SaveChangesAsync();
                    }

                    string? Division = "";
                    string? Location = "";
                    string? SubLocation = "";
                    Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetDivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                    Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetLocationId && x.DivisionId == AssetDivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                    SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetSubLocationId && x.ParentId == AssetLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                    string? Body = "Details of this asset has been updated at <b>" + Division + ", " + Location + ", " + SubLocation + "</b>";

                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {
                        _AtAssetHistory = new AtAssetHistory();
                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetHistory.OrganizationId = AssetDetails.OrganizationId;
                        _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                        _AtAssetHistory.Status = "Updated";
                        _AtAssetHistory.Activity = Body;
                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                        _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                        await _HCoreContextLogging.SaveChangesAsync();
                    }

                    if (_Request.VendorDetails != null)
                    {
                        var VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == AssetDetails.Id).FirstOrDefaultAsync();
                        if (VendorDetails != null)
                        {
                            if (VendorId > 0 && VendorId != VendorDetails.VendorId)
                            {
                                VendorDetails.VendorId = (long)VendorId;
                            }
                            if (!string.IsNullOrEmpty(_Request.VendorDetails.PoNumber) && VendorDetails.PoNumber != _Request.VendorDetails.PoNumber)
                            {
                                VendorDetails.PoNumber = _Request.VendorDetails.PoNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.VendorDetails.InvoiceNumber) && VendorDetails.InvoiceNumber != _Request.VendorDetails.InvoiceNumber)
                            {
                                VendorDetails.InvoiceNumber = _Request.VendorDetails.InvoiceNumber;
                            }
                            if (_Request.VendorDetails.RecievedDate != null && VendorDetails.RecievedDate != _Request.VendorDetails.RecievedDate)
                            {
                                VendorDetails.RecievedDate = _Request.VendorDetails.RecievedDate;
                            }
                            if (_Request.VendorDetails.StartDate != null && VendorDetails.WarrantyStartDate != _Request.VendorDetails.StartDate)
                            {
                                VendorDetails.WarrantyStartDate = _Request.VendorDetails.StartDate;
                            }
                            if (_Request.VendorDetails.EndDate != null && VendorDetails.WarrantyEndDate != _Request.VendorDetails.EndDate)
                            {
                                VendorDetails.WarrantyEndDate = _Request.VendorDetails.EndDate;
                            }
                            if (_Request.VendorDetails.ExpireInYears > 0 && VendorDetails.ExpiresIn != _Request.VendorDetails.ExpireInYears)
                            {
                                VendorDetails.ExpiresIn = _Request.VendorDetails.ExpireInYears;
                            }
                            if (_Request.VendorDetails.ExpiryDate != null && VendorDetails.ExpiryDate != _Request.VendorDetails.ExpiryDate)
                            {
                                VendorDetails.ExpiryDate = _Request.VendorDetails.ExpiryDate;
                            }
                            if (!string.IsNullOrEmpty(_Request.VendorDetails.Currency) && VendorDetails.Currency != _Request.VendorDetails.Currency)
                            {
                                VendorDetails.Currency = _Request.VendorDetails.Currency;
                            }
                            if (_Request.VendorDetails.PurchasePrice > 0 && VendorDetails.PurchasePrice != _Request.VendorDetails.PurchasePrice)
                            {
                                VendorDetails.PurchasePrice = _Request.VendorDetails.PurchasePrice;
                            }
                            if (_Request.VendorDetails.AdditionalPrice > 0 && VendorDetails.AdditionalPrice != _Request.VendorDetails.AdditionalPrice)
                            {
                                VendorDetails.AdditionalPrice = _Request.VendorDetails.AdditionalPrice;
                            }

                            if (_Request.VendorDetails.IsAmc == true)
                            {
                                VendorDetails.IsAmcApplicable = 1;

                                var AmcDetails = await _HCoreContext.AtAssetAmcDetails.Where(x => x.VendorDetailsId == VendorDetails.Id).ToListAsync();
                                if (AmcDetails.Count > 0)
                                {
                                    _HCoreContext.AtAssetAmcDetails.RemoveRange(AmcDetails);
                                    await _HCoreContext.SaveChangesAsync();
                                }

                                foreach (var Amc in _Request.VendorDetails.AmcDetails)
                                {
                                    long? AmcVendorId = HCoreHelper.GetAccountId(Amc.VendorKey, _Request.UserReference);
                                    if (AmcVendorId < 1)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select vednor for amc");
                                    }

                                    _AtAssetAmcDetail = new AtAssetAmcDetail();
                                    _AtAssetAmcDetail.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetAmcDetail.VendorId = (long)AmcVendorId;
                                    _AtAssetAmcDetail.VendorDetailsId = VendorDetails.Id;
                                    _AtAssetAmcDetail.StartDate = (DateTime)Amc.StartDate;
                                    _AtAssetAmcDetail.EndDate = (DateTime)Amc.EndDate;
                                    _AtAssetAmcDetail.Price = Amc.Amount;
                                    _AtAssetAmcDetail.StatusId = HelperStatus.Default.Active;
                                    _AtAssetAmcDetail.CreatedDate = HCoreHelper.GetDateTime();
                                    _AtAssetAmcDetail.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.AtAssetAmcDetails.AddAsync(_AtAssetAmcDetail);
                                }

                                await _HCoreContext.SaveChangesAsync();
                            }
                            else
                            {
                                VendorDetails.IsAmcApplicable = 0;

                                var AmcDetails = await _HCoreContext.AtAssetAmcDetails.Where(x => x.VendorDetailsId == VendorDetails.Id).ToListAsync();
                                if (AmcDetails.Count > 0)
                                {
                                    _HCoreContext.AtAssetAmcDetails.RemoveRange(AmcDetails);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                            }

                            if (_Request.VendorDetails.IsInsurance == true)
                            {
                                VendorDetails.IsInsurance = 1;

                                var InsuranceDetails = await _HCoreContext.AtAssetInsuranceDetails.Where(x => x.VendorDetailsId == VendorDetails.Id).ToListAsync();
                                if (InsuranceDetails.Count > 0)
                                {
                                    _HCoreContext.AtAssetInsuranceDetails.RemoveRange(InsuranceDetails);
                                    await _HCoreContext.SaveChangesAsync();
                                }

                                foreach (var Insurance in _Request.VendorDetails.InsuranceDetails)
                                {
                                    long? InsuranceVendorId = HCoreHelper.GetAccountId(Insurance.VendorKey, _Request.UserReference);
                                    if (InsuranceVendorId < 1)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select vednor for amc");
                                    }

                                    _AtAssetInsuranceDetail = new AtAssetInsuranceDetail();
                                    _AtAssetInsuranceDetail.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetInsuranceDetail.VendorId = (long)InsuranceVendorId;
                                    _AtAssetInsuranceDetail.VendorDetailsId = VendorDetails.Id;
                                    _AtAssetInsuranceDetail.StartDate = (DateTime)Insurance.StartDate;
                                    _AtAssetInsuranceDetail.EndDate = (DateTime)Insurance.EndDate;
                                    _AtAssetInsuranceDetail.Price = Insurance.Amount;
                                    _AtAssetInsuranceDetail.StatusId = HelperStatus.Default.Active;
                                    _AtAssetInsuranceDetail.CreatedDate = HCoreHelper.GetDateTime();
                                    _AtAssetInsuranceDetail.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.AtAssetInsuranceDetails.AddAsync(_AtAssetInsuranceDetail);
                                }

                                await _HCoreContext.SaveChangesAsync();
                            }
                            else
                            {
                                VendorDetails.IsInsurance = 0;

                                var InsuranceDetails = await _HCoreContext.AtAssetInsuranceDetails.Where(x => x.VendorDetailsId == VendorDetails.Id).ToListAsync();
                                if (InsuranceDetails.Count > 0)
                                {
                                    _HCoreContext.AtAssetInsuranceDetails.RemoveRange(InsuranceDetails);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                            }

                            VendorDetails.ModifiedById = _Request.UserReference.AccountId;
                            VendorDetails.ModifiedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.SaveChangesAsync();
                        }
                        else
                        {
                            _AtAssetVendorDetail = new AtAssetVendorDetail();
                            _AtAssetVendorDetail.Guid = HCoreHelper.GenerateGuid();
                            _AtAssetVendorDetail.AssetId = AssetDetails.Id;
                            _AtAssetVendorDetail.VendorId = (long)VendorId;
                            _AtAssetVendorDetail.PoNumber = _Request.VendorDetails.PoNumber;
                            _AtAssetVendorDetail.InvoiceNumber = _Request.VendorDetails.PoNumber;
                            _AtAssetVendorDetail.RecievedDate = _Request.VendorDetails.RecievedDate;
                            _AtAssetVendorDetail.WarrantyStartDate = _Request.VendorDetails.StartDate;
                            _AtAssetVendorDetail.WarrantyEndDate = _Request.VendorDetails.EndDate;
                            _AtAssetVendorDetail.ExpiresIn = _Request.VendorDetails.ExpireInYears;

                            if (_Request.VendorDetails.RecievedDate != null)
                            {
                                _AtAssetVendorDetail.ExpiryDate = _Request.VendorDetails.RecievedDate.Value.AddYears((int)_Request.VendorDetails.ExpireInYears);
                            }
                            else if (_Request.VendorDetails.RecievedDate == null && _Request.VendorDetails.StartDate != null)
                            {
                                _AtAssetVendorDetail.ExpiryDate = _Request.VendorDetails.StartDate.Value.AddYears((int)_Request.VendorDetails.ExpireInYears);
                            }
                            else
                            {
                                _AtAssetVendorDetail.ExpiryDate = HCoreHelper.GetDateTime().AddYears((int)_Request.VendorDetails.ExpireInYears);
                            }

                            _AtAssetVendorDetail.Currency = _Request.VendorDetails.Currency;
                            _AtAssetVendorDetail.PurchasePrice = _Request.VendorDetails.PurchasePrice;
                            _AtAssetVendorDetail.AdditionalPrice = _Request.VendorDetails.AdditionalPrice;

                            if (_Request.VendorDetails.IsAmc == true)
                            {
                                _AtAssetVendorDetail.IsAmcApplicable = 1;
                            }
                            else
                            {
                                _AtAssetVendorDetail.IsAmcApplicable = 0;
                            }

                            if (_Request.VendorDetails.IsInsurance == true)
                            {
                                _AtAssetVendorDetail.IsInsurance = 1;
                            }
                            else
                            {
                                _AtAssetVendorDetail.IsInsurance = 0;
                            }

                            _AtAssetVendorDetail.StatusId = HelperStatus.Default.Active;
                            _AtAssetVendorDetail.CreatedById = _Request.UserReference.AccountId;
                            _AtAssetVendorDetail.CreatedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.AtAssetVendorDetails.AddAsync(_AtAssetVendorDetail);
                            await _HCoreContext.SaveChangesAsync();

                            if (_Request.VendorDetails.IsAmc == true)
                            {
                                _AtAssetAmcDetail = new AtAssetAmcDetail();
                                foreach (var Amc in _Request.VendorDetails.AmcDetails)
                                {
                                    long? AmcVendorId = HCoreHelper.GetAccountId(Amc.VendorKey, _Request.UserReference);
                                    if (AmcVendorId < 1)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select vednor for amc");
                                    }

                                    _AtAssetAmcDetail.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetAmcDetail.VendorId = (long)AmcVendorId;
                                    _AtAssetAmcDetail.VendorDetailsId = _AtAssetVendorDetail.Id;
                                    _AtAssetAmcDetail.StartDate = (DateTime)Amc.StartDate;
                                    _AtAssetAmcDetail.EndDate = (DateTime)Amc.EndDate;
                                    _AtAssetAmcDetail.Price = Amc.Amount;
                                    _AtAssetAmcDetail.StatusId = HelperStatus.Default.Active;
                                    _AtAssetAmcDetail.CreatedDate = HCoreHelper.GetDateTime();
                                    _AtAssetAmcDetail.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.AtAssetAmcDetails.AddAsync(_AtAssetAmcDetail);
                                }
                                await _HCoreContext.SaveChangesAsync();
                            }

                            if (_Request.VendorDetails.IsInsurance == true)
                            {
                                _AtAssetInsuranceDetail = new AtAssetInsuranceDetail();
                                foreach (var Insurance in _Request.VendorDetails.InsuranceDetails)
                                {
                                    long? InsuranceVendorId = HCoreHelper.GetAccountId(Insurance.VendorKey, _Request.UserReference);
                                    if (InsuranceVendorId < 1)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Please select vednor for amc");
                                    }

                                    _AtAssetInsuranceDetail.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetInsuranceDetail.VendorId = (long)InsuranceVendorId;
                                    _AtAssetInsuranceDetail.VendorDetailsId = _AtAssetVendorDetail.Id;
                                    _AtAssetInsuranceDetail.StartDate = (DateTime)Insurance.StartDate;
                                    _AtAssetInsuranceDetail.EndDate = (DateTime)Insurance.EndDate;
                                    _AtAssetInsuranceDetail.Price = Insurance.Amount;
                                    _AtAssetInsuranceDetail.StatusId = HelperStatus.Default.Active;
                                    _AtAssetInsuranceDetail.CreatedDate = HCoreHelper.GetDateTime();
                                    _AtAssetInsuranceDetail.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.AtAssetInsuranceDetails.AddAsync(_AtAssetInsuranceDetail);
                                }
                                await _HCoreContext.SaveChangesAsync();
                            }
                        }
                    }

                    _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                    _AssetAuditRequest = new OBackgroundProcessor.Request();
                    _AssetAuditRequest.AssetId = AssetDetails.Id;
                    _AssetAuditRequest.AssetKey = AssetDetails.Guid;
                    _AssetAuditRequest.UserReference = _Request.UserReference;
                    await _ManageBackgroundProcessor.AssetAudit(_AssetAuditRequest);

                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATSAVE", "Asset details updated successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ReturnAsset(OAssetOperations.RetrurnAsset.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.AssetKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset key required");
                }
                long? ReturnById = 0;
                if (string.IsNullOrEmpty(_Request.ReturnBy))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Return by required");
                }
                else
                {
                    ReturnById = HCoreHelper.GetAccountId(_Request.ReturnBy, _Request.UserReference);
                    if (ReturnById < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Invalid details of the person who is returning the asset");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Guid == _Request.AssetKey).FirstOrDefaultAsync();
                    if (AssetDetails != null)
                    {
                        var _IsExists = await _HCoreContext.AtAssetReturnDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXISTS", "A request for return is already exists for this asset. Please approve it instead of creating new request");
                        }

                        int? Quantity = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == AssetDetails.Id && x.StatusId == AssetStatus.Issued).Select(x => x.Quantity).FirstOrDefaultAsync();

                        _AssetRequisition = new AtAssetRequisition();
                        _AssetRequisition.Guid = HCoreHelper.GenerateGuid();
                        _AssetRequisition.AssetId = AssetDetails.Id;
                        _AssetRequisition.ApprovalTypeId = AssetApproval.ReturnApproval;

                        if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                        {
                            _AssetRequisition.IsDeptHeadApproved = 1;
                            _AssetRequisition.IsDivHeadApproved = 1;
                            _AssetRequisition.IsHrApproved = 0;
                            _AssetRequisition.IsAdminApproved = 0;
                            _AssetRequisition.IsSuperAdminApproved = 0;
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                        {
                            _AssetRequisition.IsDeptHeadApproved = 1;
                            _AssetRequisition.IsDivHeadApproved = 1;
                            _AssetRequisition.IsHrApproved = 1;
                            _AssetRequisition.IsAdminApproved = 1;
                            _AssetRequisition.IsSuperAdminApproved = 0;
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                        {
                            _AssetRequisition.IsDeptHeadApproved = 1;
                            _AssetRequisition.IsDivHeadApproved = 1;
                            _AssetRequisition.IsHrApproved = 1;
                            _AssetRequisition.IsAdminApproved = 1;
                            _AssetRequisition.IsSuperAdminApproved = 1;
                        }
                        else
                        {
                            _AssetRequisition.IsDeptHeadApproved = 0;
                            _AssetRequisition.IsDivHeadApproved = 0;
                            _AssetRequisition.IsHrApproved = 0;
                            _AssetRequisition.IsAdminApproved = 0;
                            _AssetRequisition.IsSuperAdminApproved = 0;
                        }

                        _AssetRequisition.CreatedDate = HCoreHelper.GetDateTime();
                        _AssetRequisition.CreatedById = _Request.UserReference.AccountId;
                        await _HCoreContext.AtAssetRequisitions.AddAsync(_AssetRequisition);
                        await _HCoreContext.SaveChangesAsync();

                        _AtAssetReturnDetail = new AtAssetReturnDetail();
                        _AtAssetReturnDetail.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetReturnDetail.AssetId = AssetDetails.Id;
                        _AtAssetReturnDetail.ReturnDate = HCoreHelper.GetDateTime();
                        _AtAssetReturnDetail.ReturnReference = HCoreHelper.GenerateGuid();
                        _AtAssetReturnDetail.ReturnBy = (long)ReturnById;
                        _AtAssetReturnDetail.ReturnQuantity = (int)Quantity;
                        _AtAssetReturnDetail.ReturnNotes = _Request.ReturnNotes;
                        _AtAssetReturnDetail.ReturnValue = _Request.ReturnValue;
                        _AtAssetReturnDetail.ReturnCondition = _Request.ReturnCondition;
                        _AtAssetReturnDetail.ReturnTo = _Request.UserReference.AccountId;

                        if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                        {
                            _AtAssetReturnDetail.StatusId = AssetStatus.AssetApproval.PendingHRApproval;

                            string? Body = "The return request for this asset has been raised and approved by the <b>Division Head</b> and sent for approval to <b>HR</b>.";

                            var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnTo)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnBy)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Return Request";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                        {
                            _AtAssetReturnDetail.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;

                            string? Body = "The return request for this asset has been raised and approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b>.";

                            var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnTo)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnBy)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Return Request";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                        {
                            _AtAssetReturnDetail.StatusId = AssetStatus.Returned;

                            string? Body = "The return request for this asset has been raised and approved by the <b>Super Admin</b>.";

                            var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnTo)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnBy)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Returned";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                        else
                        {
                            _AtAssetReturnDetail.StatusId = AssetStatus.AssetApproval.PendingDepartmentHeadApproval;

                            string? Body = "The return request for this asset has been raised and sent for approval to <b>Department Head</b>.";

                            var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnTo)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnBy)
                                                     .Select(x => new
                                                     {
                                                         DisplayName = x.DisplayName,
                                                         AccountCode = x.AccountCode
                                                     }).FirstOrDefaultAsync();

                            Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Return Request";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }

                        _AtAssetReturnDetail.CreatedById = _Request.UserReference.AccountId;
                        _AtAssetReturnDetail.CreatedDate = HCoreHelper.GetDateTime();
                        _HCoreContext.AtAssetReturnDetails.Add(_AtAssetReturnDetail);
                        await _HCoreContext.SaveChangesAsync();

                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Aseet return request saved successfully. Once it will get approved your return will successful.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Invalid asset details");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ReturnAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ApproveAssetReturn(OReference _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset return key required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset return id required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetReturnDetails = await _HCoreContext.AtAssetReturnDetails.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (AssetReturnDetails != null)
                    {
                        var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == AssetReturnDetails.AssetId).FirstOrDefaultAsync();
                        if (AssetDetails != null)
                        {
                            if (AssetDetails.StatusId == AssetStatus.Expired)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXPIRED", "This asset has expired.");
                            }
                            if (AssetDetails.StatusId == AssetStatus.Disposed)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATDISPOSED", "This asset has disposed.");
                            }

                            int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                            if (StatusId == AssetStatus.AssetApproval.Rejected)
                            {
                                AssetReturnDetails.StatusId = AssetStatus.AssetApproval.Rejected;
                                AssetReturnDetails.Comment = _Request.Comment;
                                AssetReturnDetails.ReturnApprovedDate = HCoreHelper.GetDateTime();
                                AssetReturnDetails.ReturnApprovedBy = _Request.UserReference.AccountId;
                                AssetReturnDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                AssetReturnDetails.ModifiedById = _Request.UserReference.AccountId;

                                string? Body = "The return request for this asset has been rejected by the <b>" + _Request.UserReference.UserRoleName + "</b>.";

                                var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnTo)
                                                         .Select(x => new
                                                         {
                                                             DisplayName = x.DisplayName,
                                                             AccountCode = x.AccountCode
                                                         }).FirstOrDefaultAsync();

                                var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnBy)
                                                         .Select(x => new
                                                         {
                                                             DisplayName = x.DisplayName,
                                                             AccountCode = x.AccountCode
                                                         }).FirstOrDefaultAsync();

                                Body += "The asset was returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    _AtAssetHistory = new AtAssetHistory();
                                    _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                    _AtAssetHistory.Status = "Return Request Rejected";
                                    _AtAssetHistory.Activity = Body;
                                    _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                    _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                    await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }

                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATUPDATE", "Asset return rejected");
                            }
                            else
                            {
                                var _AssetConfig = await _HCoreContext.AtAssetRequisitions.Where(x => x.AssetId == AssetReturnDetails.AssetId && x.ApprovalTypeId == AssetApproval.ReturnApproval).FirstOrDefaultAsync();
                                if (_AssetConfig != null)
                                {
                                    if (_Request.UserReference.UserRoleId == UserRole.HeadOfDept)
                                    {
                                        AssetReturnDetails.StatusId = AssetStatus.AssetApproval.PendingDivisionHeadApproval;
                                        _AssetConfig.IsDeptHeadApproved = 1;

                                        string? Body = "The return request for this asset has been approved by the <b>Department Head</b> and sent for approval to <b>Division Head</b>.";

                                        var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnTo)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnBy)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Return Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                                    {
                                        AssetReturnDetails.StatusId = AssetStatus.AssetApproval.PendingHRApproval;
                                        _AssetConfig.IsDivHeadApproved = 1;

                                        string? Body = "The return request for this asset has been approved by the <b>Division Head</b> and sent for approval to <b>HR</b>.";

                                        var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnTo)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnBy)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Return Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.HR)
                                    {
                                        AssetReturnDetails.StatusId = AssetStatus.AssetApproval.PendingAdminApproval;
                                        _AssetConfig.IsHrApproved = 1;

                                        string? Body = "The return request for this asset has been approved by the <b>HR</b> and sent for approval to <b>Admin</b>.";

                                        var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnTo)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnBy)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Return Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                                    {
                                        AssetReturnDetails.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;
                                        _AssetConfig.IsAdminApproved = 1;

                                        string? Body = "The return request for this asset has been approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b>.";

                                        var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnTo)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == _AtAssetReturnDetail.ReturnBy)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Return Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                                    {
                                        AssetReturnDetails.StatusId = AssetStatus.Returned;
                                        _AssetConfig.IsSuperAdminApproved = 1;

                                        var AssetIssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == AssetReturnDetails.AssetId).FirstOrDefaultAsync();
                                        if (AssetIssueDetails != null)
                                        {
                                            AssetIssueDetails.StatusId = AssetStatus.Returned;
                                            AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                            AssetIssueDetails.ModifiedById = _Request.UserReference.AccountId;
                                        }

                                        AssetDetails.StatusId = AssetStatus.Returned;
                                        AssetDetails.ModifiedById = _Request.UserReference.AccountId;
                                        AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();

                                        string? Body = "The return request for this asset has been approved by the <b>Super Admin</b>.";

                                        var ReturnedToDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnTo)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        var RetrurnedByDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetReturnDetails.ReturnBy)
                                                                 .Select(x => new
                                                                 {
                                                                     DisplayName = x.DisplayName,
                                                                     AccountCode = x.AccountCode
                                                                 }).FirstOrDefaultAsync();

                                        Body += "The asset hass been returned to <b>" + ReturnedToDetails.DisplayName + "</b> by the <b>" + RetrurnedByDetails.DisplayName + "(" + RetrurnedByDetails.AccountCode + ")" + "</b>.";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Returned";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }

                                    _AssetConfig.ModifiedDate = HCoreHelper.GetDateTime();
                                    _AssetConfig.ModifiedById = _Request.UserReference.AccountId;

                                    AssetReturnDetails.ReturnApprovedDate = HCoreHelper.GetDateTime();
                                    AssetReturnDetails.Comment = _Request.Comment;
                                    AssetReturnDetails.ReturnApprovedBy = _Request.UserReference.AccountId;
                                    AssetReturnDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    AssetReturnDetails.ModifiedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.SaveChangesAsync();

                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATUPDATE", "Asset return approved");
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset requisition config details not found for this asset");
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Invalid asset");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset return details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ApproveAssetReturn", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> IssueAsset(OAssetOperations.IssueAsset.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.AssetKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset key required");
                }
                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid division");
                    }
                }
                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid department");
                    }
                }
                long? LocationId = 0;
                if (!string.IsNullOrEmpty(_Request.LocationKey))
                {
                    LocationId = HCoreHelper.GetLocationId(_Request.LocationKey, _Request.UserReference);
                    if (LocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid location");
                    }
                }
                long? SubLocationId = 0;
                if (!string.IsNullOrEmpty(_Request.SubLocationKey))
                {
                    SubLocationId = HCoreHelper.GetSubLocationId(_Request.SubLocationKey, (long)LocationId, _Request.UserReference);
                    if (SubLocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid sublocation");
                    }
                }
                long? AccountId = 0;
                if (!string.IsNullOrEmpty(_Request.EmployeeKey))
                {
                    AccountId = HCoreHelper.GetAccountId(_Request.EmployeeKey, _Request.UserReference);
                    if (AccountId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid status");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Guid == _Request.AssetKey || x.AssetBarcode == _Request.AssetKey).FirstOrDefaultAsync();
                    if (AssetDetails != null)
                    {
                        var _IsExist = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && x.Asset.AssetTypeId == AssetType.WithBarcode && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                        if (_IsExist)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXISTS", "A request for issue is already exists for this asset.");
                        }

                        if (AssetDetails.StatusId == AssetStatus.Disposed)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATDISPOSED", "This asset is disposed you cannot issue to anyone");
                        }

                        if (AssetDetails.AssetTypeId == AssetType.WithBarcode && AssetDetails.StatusId == AssetStatus.Issued)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATISSUED", "This asset is already issued.");
                        }

                        if (AssetDetails.AssetTypeId == AssetType.WithoutBarcode && AssetDetails.RemainingQuantity == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "This asset is already assigned. Please try to transfer it to others");
                        }

                        if (AssetDetails.AssetTypeId == AssetType.WithoutBarcode && AssetDetails.RemainingQuantity < _Request.Quantity)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Quantity must be less than or equal to " + AssetDetails.RemainingQuantity);
                        }

                        _AssetRequisition = new AtAssetRequisition();
                        _AssetRequisition.Guid = HCoreHelper.GenerateGuid();
                        _AssetRequisition.AssetId = AssetDetails.Id;
                        _AssetRequisition.ApprovalTypeId = AssetApproval.IssueApproval;

                        if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                        {
                            _AssetRequisition.IsDeptHeadApproved = 1;
                            _AssetRequisition.IsDivHeadApproved = 1;
                            _AssetRequisition.IsHrApproved = 0;
                            _AssetRequisition.IsAdminApproved = 0;
                            _AssetRequisition.IsSuperAdminApproved = 0;
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                        {
                            _AssetRequisition.IsDeptHeadApproved = 1;
                            _AssetRequisition.IsDivHeadApproved = 1;
                            _AssetRequisition.IsHrApproved = 1;
                            _AssetRequisition.IsAdminApproved = 1;
                            _AssetRequisition.IsSuperAdminApproved = 0;
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                        {
                            _AssetRequisition.IsDeptHeadApproved = 1;
                            _AssetRequisition.IsDivHeadApproved = 1;
                            _AssetRequisition.IsHrApproved = 1;
                            _AssetRequisition.IsAdminApproved = 1;
                            _AssetRequisition.IsSuperAdminApproved = 1;
                        }
                        else
                        {
                            _AssetRequisition.IsDeptHeadApproved = 0;
                            _AssetRequisition.IsDivHeadApproved = 0;
                            _AssetRequisition.IsHrApproved = 0;
                            _AssetRequisition.IsAdminApproved = 0;
                            _AssetRequisition.IsSuperAdminApproved = 0;
                        }

                        _AssetRequisition.CreatedDate = HCoreHelper.GetDateTime();
                        _AssetRequisition.CreatedById = _Request.UserReference.AccountId;
                        await _HCoreContext.AtAssetRequisitions.AddAsync(_AssetRequisition);
                        await _HCoreContext.SaveChangesAsync();

                        _AtAssetIssueDetail = new AtAssetIssueDetail();
                        _AtAssetIssueDetail.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetIssueDetail.AssetId = AssetDetails.Id;
                        _AtAssetIssueDetail.SourceId = RegistrationSource.WebApplication;
                        _AtAssetIssueDetail.OrganizationId = _Request.UserReference.OrganizationId;
                        _AtAssetIssueDetail.DivisionId = (long)DivisionId;
                        if (DepartmentId > 0)
                        {
                            _AtAssetIssueDetail.DepartmentId = (long)DepartmentId;
                        }
                        if (AccountId > 0)
                        {
                            _AtAssetIssueDetail.EmployeeId = (long)AccountId;
                        }
                        _AtAssetIssueDetail.LocationId = (long)LocationId;
                        _AtAssetIssueDetail.SubLocationId = (long)SubLocationId;
                        _AtAssetIssueDetail.Remark = _Request.Remark;
                        _AtAssetIssueDetail.OsName = _Request.OsName;
                        _AtAssetIssueDetail.HostName = _Request.HostName;
                        _AtAssetIssueDetail.IpAddress = _Request.IpAddress;
                        _AtAssetIssueDetail.Quantity = _Request.Quantity;
                        _AtAssetIssueDetail.IsTransfered = 0;

                        if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                        {
                            _AtAssetIssueDetail.StatusId = AssetStatus.AssetApproval.PendingHRApproval;

                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                            string? Body = "The issue request for this asset has been approved by the <b>Division Head</b> and sent for approval to <b>HR</b> to <b>";

                            string? EmployeeName = "";
                            string? EmployeeCode = "";
                            string? EmployeeEmail = "";

                            if (AccountId > 0)
                            {
                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                        .Select(x => new
                                                        {
                                                            Name = x.DisplayName,
                                                            AccountCode = x.AccountCode,
                                                            Email = x.EmailAddress
                                                        }).FirstOrDefaultAsync();
                                if (Employee != null)
                                {
                                    EmployeeName = Employee.Name;
                                    EmployeeCode = Employee.AccountCode;
                                    EmployeeEmail = Employee.Email;
                                }

                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                            }

                            string? Department = "";

                            if (DepartmentId > 0)
                            {
                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                if (!string.IsNullOrEmpty(Department))
                                {
                                    Body += Department + ", ";
                                }
                            }

                            Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Issue Request";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                        {
                            _AtAssetIssueDetail.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;

                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                            string? Body = "The issue request fot this asset has been approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b> to <b>";

                            string? EmployeeName = "";
                            string? EmployeeCode = "";
                            string? EmployeeEmail = "";

                            if (AccountId > 0)
                            {
                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                        .Select(x => new
                                                        {
                                                            Name = x.DisplayName,
                                                            AccountCode = x.AccountCode,
                                                            Email = x.EmailAddress
                                                        }).FirstOrDefaultAsync();
                                if (Employee != null)
                                {
                                    EmployeeName = Employee.Name;
                                    EmployeeCode = Employee.AccountCode;
                                    EmployeeEmail = Employee.Email;
                                }

                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                            }

                            string? Department = "";

                            if (DepartmentId > 0)
                            {
                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                if (!string.IsNullOrEmpty(Department))
                                {
                                    Body += Department + ", ";
                                }
                            }

                            Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Issue Request";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                        else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                        {
                            _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                            _AtAssetIssueDetail.ApprovedDate = HCoreHelper.GetDateTime();
                            _AtAssetIssueDetail.ApprovedById = _Request.UserReference.AccountId;
                            AssetDetails.StatusId = AssetStatus.Issued;

                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                            string? Body = "This asset has been issued to <b>";

                            string? EmployeeName = "";
                            string? EmployeeCode = "";
                            string? EmployeeEmail = "";

                            if (AccountId > 0)
                            {
                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                        .Select(x => new
                                                        {
                                                            Name = x.DisplayName,
                                                            AccountCode = x.AccountCode,
                                                            Email = x.EmailAddress
                                                        }).FirstOrDefaultAsync();
                                if (Employee != null)
                                {
                                    EmployeeName = Employee.Name;
                                    EmployeeCode = Employee.AccountCode;
                                    EmployeeEmail = Employee.Email;
                                }

                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                            }

                            string? Department = "";

                            if (DepartmentId > 0)
                            {
                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                if (!string.IsNullOrEmpty(Department))
                                {
                                    Body += Department + ", ";
                                }
                            }

                            Body += Division + ", " + Location + ", " + SubLocation + "</b> by the approval of <b>Super Admin</b>";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Issued";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }
                        else
                        {
                            _AtAssetIssueDetail.StatusId = AssetStatus.AssetApproval.PendingDepartmentHeadApproval;

                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                            string? Body = "An issue request has been raised for this asset to <b>";

                            string? EmployeeName = "";
                            string? EmployeeCode = "";

                            if (AccountId > 0)
                            {
                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                        .Select(x => new
                                                        {
                                                            Name = x.DisplayName,
                                                            AccountCode = x.AccountCode,
                                                        }).FirstOrDefaultAsync();
                                if (Employee != null)
                                {
                                    EmployeeName = Employee.Name;
                                    EmployeeCode = Employee.AccountCode;
                                }

                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                            }

                            string? Department = "";

                            if (DepartmentId > 0)
                            {
                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                if (!string.IsNullOrEmpty(Department))
                                {
                                    Body += Department + ", ";
                                }
                            }

                            Body += Division + ", " + Location + ", " + SubLocation + "</b>. The request has been sent for approval to the <b>Department Head</b>";

                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                _AtAssetHistory = new AtAssetHistory();
                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                _AtAssetHistory.Status = "Issue Request";
                                _AtAssetHistory.Activity = Body;
                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                await _HCoreContextLogging.SaveChangesAsync();
                            }
                        }

                        _AtAssetIssueDetail.CreatedById = _Request.UserReference.AccountId;
                        _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                        _HCoreContext.AtAssetIssueDetails.Add(_AtAssetIssueDetail);
                        await _HCoreContext.SaveChangesAsync();

                        AssetDetails.RemainingQuantity = AssetDetails.TotalQuantity - _AtAssetIssueDetail.Quantity;
                        AssetDetails.ModifiedById = _Request.UserReference.AccountId;
                        AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.SaveChangesAsync();

                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Aseet issue request saved successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Invalid asset details");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("IssueAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ApproveAssetIssue(OReference _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset key required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset id required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetIssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval)).FirstOrDefaultAsync();
                    if (AssetIssueDetails != null)
                    {
                        var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == AssetIssueDetails.AssetId).FirstOrDefaultAsync();
                        if (AssetDetails != null)
                        {
                            int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                            if (StatusId == AssetStatus.AssetApproval.Rejected)
                            {
                                AssetIssueDetails.StatusId = AssetStatus.AssetApproval.Rejected;
                                AssetIssueDetails.Comment = _Request.Comment;
                                AssetIssueDetails.ApprovedDate = HCoreHelper.GetDateTime();
                                AssetIssueDetails.ApprovedById = _Request.UserReference.AccountId;
                                AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                AssetIssueDetails.ModifiedById = _Request.UserReference.AccountId;

                                AssetDetails.RemainingQuantity = AssetDetails.RemainingQuantity + AssetIssueDetails.Quantity;
                                AssetDetails.ModifiedById = _Request.UserReference.AccountId;
                                AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.SaveChangesAsync();

                                string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId && x.DivisionId == AssetIssueDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId && x.ParentId == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                string? Body = "The issue request for this asset to <b>";

                                string? EmployeeName = "";
                                string? EmployeeCode = "";

                                if (AssetIssueDetails.EmployeeId > 0)
                                {
                                    var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                            .Select(x => new
                                                            {
                                                                Name = x.DisplayName,
                                                                AccountCode = x.AccountCode,
                                                            }).FirstOrDefaultAsync();
                                    if (Employee != null)
                                    {
                                        EmployeeName = Employee.Name;
                                        EmployeeCode = Employee.AccountCode;
                                    }

                                    Body += EmployeeName + "(" + EmployeeCode + "), ";
                                }

                                string? Department = "";

                                if (AssetIssueDetails.DepartmentId > 0)
                                {
                                    Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    if (!string.IsNullOrEmpty(Department))
                                    {
                                        Body += Department + ", ";
                                    }
                                }

                                Body += Division + ", " + Location + ", " + SubLocation + "</b> has been rejected by the <b>" + _Request.UserReference.UserRoleName + "</b>";

                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    _AtAssetHistory = new AtAssetHistory();
                                    _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                    _AtAssetHistory.Status = "Issue Request Rejected";
                                    _AtAssetHistory.Activity = Body;
                                    _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                    _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                    await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }

                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATUPDATE", "Asset issue request rejected");
                            }
                            else
                            {
                                var _AssetConfig = await _HCoreContext.AtAssetRequisitions.Where(x => x.AssetId == AssetIssueDetails.AssetId && x.ApprovalTypeId == AssetApproval.IssueApproval).FirstOrDefaultAsync();
                                if (_AssetConfig != null)
                                {
                                    if (_Request.UserReference.UserRoleId == UserRole.HeadOfDept)
                                    {
                                        AssetIssueDetails.StatusId = AssetStatus.AssetApproval.PendingDivisionHeadApproval;
                                        _AssetConfig.IsDeptHeadApproved = 1;

                                        string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                        string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId && x.DivisionId == AssetIssueDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                        string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId && x.ParentId == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                        string? Body = "The issue request for this asset has been approved by the <b>Department Head</b> and sent for approval to <b>Division Head</b> for <b>";

                                        string? EmployeeName = "";
                                        string? EmployeeCode = "";

                                        if (AssetIssueDetails.EmployeeId > 0)
                                        {
                                            var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                    .Select(x => new
                                                                    {
                                                                        Name = x.DisplayName,
                                                                        AccountCode = x.AccountCode,
                                                                    }).FirstOrDefaultAsync();
                                            if (Employee != null)
                                            {
                                                EmployeeName = Employee.Name;
                                                EmployeeCode = Employee.AccountCode;
                                            }

                                            Body += EmployeeName + "(" + EmployeeCode + "), ";
                                        }

                                        string? Department = "";

                                        if (AssetIssueDetails.DepartmentId > 0)
                                        {
                                            Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                            if (!string.IsNullOrEmpty(Department))
                                            {
                                                Body += Department + ", ";
                                            }
                                        }

                                        Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Issue Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                                    {
                                        AssetIssueDetails.StatusId = AssetStatus.AssetApproval.PendingHRApproval;
                                        _AssetConfig.IsDivHeadApproved = 1;

                                        string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                        string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId && x.DivisionId == AssetIssueDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                        string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId && x.ParentId == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                        string? Body = "The issue request for this asset has been approved by the <b>Division Head</b> and sent for approval to <b>HR</b> for <b>";

                                        string? EmployeeName = "";
                                        string? EmployeeCode = "";

                                        if (AssetIssueDetails.EmployeeId > 0)
                                        {
                                            var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                    .Select(x => new
                                                                    {
                                                                        Name = x.DisplayName,
                                                                        AccountCode = x.AccountCode,
                                                                    }).FirstOrDefaultAsync();
                                            if (Employee != null)
                                            {
                                                EmployeeName = Employee.Name;
                                                EmployeeCode = Employee.AccountCode;
                                            }

                                            Body += EmployeeName + "(" + EmployeeCode + "), ";
                                        }

                                        string? Department = "";

                                        if (AssetIssueDetails.DepartmentId > 0)
                                        {
                                            Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                            if (!string.IsNullOrEmpty(Department))
                                            {
                                                Body += Department + ", ";
                                            }
                                        }

                                        Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Issue Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.HR)
                                    {
                                        AssetIssueDetails.StatusId = AssetStatus.AssetApproval.PendingAdminApproval;
                                        _AssetConfig.IsHrApproved = 1;

                                        string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                        string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId && x.DivisionId == AssetIssueDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                        string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId && x.ParentId == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                        string? Body = "The issue request for this asset has been approved by the <b>HR</b> and sent for approval to <b>Admin</b> for <b>";

                                        string? EmployeeName = "";
                                        string? EmployeeCode = "";
                                        string? EmployeeEmail = "";

                                        if (AssetIssueDetails.EmployeeId > 0)
                                        {
                                            var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                    .Select(x => new
                                                                    {
                                                                        Name = x.DisplayName,
                                                                        AccountCode = x.AccountCode,
                                                                        Email = x.EmailAddress
                                                                    }).FirstOrDefaultAsync();
                                            if (Employee != null)
                                            {
                                                EmployeeName = Employee.Name;
                                                EmployeeCode = Employee.AccountCode;
                                                EmployeeEmail = Employee.Email;
                                            }

                                            Body += EmployeeName + "(" + EmployeeCode + "), ";
                                        }

                                        string? Department = "";

                                        if (AssetIssueDetails.DepartmentId > 0)
                                        {
                                            Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                            if (!string.IsNullOrEmpty(Department))
                                            {
                                                Body += Department + ", ";
                                            }
                                        }

                                        Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Issue Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                                    {
                                        AssetIssueDetails.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;
                                        _AssetConfig.IsAdminApproved = 1;

                                        string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                        string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId && x.DivisionId == AssetIssueDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                        string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId && x.ParentId == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                        string? Body = "The issue request for this asset has been approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b> for <b>";

                                        string? EmployeeName = "";
                                        string? EmployeeCode = "";

                                        if (AssetIssueDetails.EmployeeId > 0)
                                        {
                                            var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                    .Select(x => new
                                                                    {
                                                                        Name = x.DisplayName,
                                                                        AccountCode = x.AccountCode,
                                                                    }).FirstOrDefaultAsync();
                                            if (Employee != null)
                                            {
                                                EmployeeName = Employee.Name;
                                                EmployeeCode = Employee.AccountCode;
                                            }

                                            Body += EmployeeName + "(" + EmployeeCode + "), ";
                                        }

                                        string? Department = "";

                                        if (AssetIssueDetails.DepartmentId > 0)
                                        {
                                            Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                            if (!string.IsNullOrEmpty(Department))
                                            {
                                                Body += Department + ", ";
                                            }
                                        }

                                        Body += Division + ", " + Location + ", " + SubLocation + "</b>";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Issue Request";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }
                                    else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                                    {
                                        AssetIssueDetails.StatusId = AssetStatus.Issued;
                                        _AssetConfig.IsSuperAdminApproved = 1;

                                        AssetDetails.StatusId = AssetStatus.Issued;
                                        AssetDetails.ModifiedById = _Request.UserReference.AccountId;
                                        AssetDetails.ModifiedDate = HCoreHelper.GetDateTime();

                                        var AssetReturnDetails = await _HCoreContext.AtAssetReturnDetails.Where(x => x.AssetId == AssetDetails.Id && x.StatusId == AssetStatus.Returned).FirstOrDefaultAsync();
                                        if (AssetReturnDetails != null)
                                        {
                                            AssetReturnDetails.StatusId = AssetStatus.Issued;
                                            AssetReturnDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                            AssetReturnDetails.ModifiedById = _Request.UserReference.AccountId;
                                            await _HCoreContext.SaveChangesAsync();
                                        }

                                        var AssetLocationDetails = await _HCoreContext.AtAssetLocations.Where(x => x.AssetId == AssetIssueDetails.AssetId).FirstOrDefaultAsync();
                                        if (AssetLocationDetails != null)
                                        {
                                            AssetLocationDetails.SubLocationId = AssetIssueDetails.SubLocationId;
                                            AssetLocationDetails.LocationId = AssetIssueDetails.LocationId;
                                            AssetLocationDetails.DivisionId = AssetIssueDetails.DivisionId;
                                        }
                                        else
                                        {
                                            _AtAssetLocation = new AtAssetLocation();
                                            _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetLocation.AssetId = AssetIssueDetails.AssetId;
                                            _AtAssetLocation.DivisionId = AssetIssueDetails.DivisionId;
                                            _AtAssetLocation.LocationId = AssetIssueDetails.LocationId;
                                            _AtAssetLocation.SubLocationId = AssetIssueDetails.SubLocationId;
                                            _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                                            _AtAssetLocation.CreatedById = _Request.UserReference.AccountId;
                                            _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                                            await _HCoreContext.SaveChangesAsync();
                                        }

                                        string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                        string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId && x.DivisionId == AssetIssueDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                        string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId && x.ParentId == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                        string? Body = "This asset has been issued to <b>";

                                        string? EmployeeName = "";
                                        string? EmployeeCode = "";

                                        if (AssetIssueDetails.EmployeeId > 0)
                                        {
                                            var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                    .Select(x => new
                                                                    {
                                                                        Name = x.DisplayName,
                                                                        AccountCode = x.AccountCode,
                                                                    }).FirstOrDefaultAsync();
                                            if (Employee != null)
                                            {
                                                EmployeeName = Employee.Name;
                                                EmployeeCode = Employee.AccountCode;
                                            }

                                            Body += EmployeeName + "(" + EmployeeCode + "), ";
                                        }

                                        string? Department = "";

                                        if (AssetIssueDetails.DepartmentId > 0)
                                        {
                                            Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                            if (!string.IsNullOrEmpty(Department))
                                            {
                                                Body += Department + ", ";
                                            }
                                        }

                                        Body += Division + ", " + Location + ", " + SubLocation + "</b> by the approval of <b>Super Admin</b>";

                                        using (_HCoreContextLogging = new HCoreContextLogging())
                                        {
                                            _AtAssetHistory = new AtAssetHistory();
                                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                            _AtAssetHistory.Status = "Issued";
                                            _AtAssetHistory.Activity = Body;
                                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                            await _HCoreContextLogging.SaveChangesAsync();
                                        }
                                    }

                                    _AssetConfig.ModifiedDate = HCoreHelper.GetDateTime();
                                    _AssetConfig.ModifiedById = _Request.UserReference.AccountId;

                                    AssetIssueDetails.Comment = _Request.Comment;
                                    AssetIssueDetails.ApprovedDate = HCoreHelper.GetDateTime();
                                    AssetIssueDetails.ApprovedById = _Request.UserReference.AccountId;
                                    AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    AssetIssueDetails.ModifiedById = _Request.UserReference.AccountId;

                                    await _HCoreContext.SaveChangesAsync();

                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATUPDATE", "Asset issue request approved");
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset requisition config details not found for this asset");
                                }
                            }

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset details not found");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset issue details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ApproveAssetIssue", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> TransferAsset(OAssetOperations.TransferAsset.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.AssetKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset key required");
                }
                long? DivisionId = 0;
                if (!string.IsNullOrEmpty(_Request.DivisionKey))
                {
                    DivisionId = HCoreHelper.GetDivisionId(_Request.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                    if (DivisionId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid division");
                    }
                }
                long? DepartmentId = 0;
                if (!string.IsNullOrEmpty(_Request.DepartmentKey))
                {
                    DepartmentId = HCoreHelper.GetDepartmentId(_Request.DepartmentKey, _Request.UserReference);
                    if (DepartmentId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid department");
                    }
                }
                long? LocationId = 0;
                if (!string.IsNullOrEmpty(_Request.LocationKey))
                {
                    LocationId = HCoreHelper.GetLocationId(_Request.LocationKey, _Request.UserReference);
                    if (LocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid location");
                    }
                }
                long? SubLocationId = 0;
                if (!string.IsNullOrEmpty(_Request.SubLocationKey))
                {
                    SubLocationId = HCoreHelper.GetSubLocationId(_Request.SubLocationKey, (long)LocationId, _Request.UserReference);
                    if (SubLocationId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid sublocation");
                    }
                }
                long? AccountId = 0;
                if (!string.IsNullOrEmpty(_Request.EmployeeKey))
                {
                    AccountId = HCoreHelper.GetAccountId(_Request.EmployeeKey, _Request.UserReference);
                    if (AccountId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT003", "Invalid status");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Guid == _Request.AssetKey || x.AssetBarcode == _Request.AssetKey).FirstOrDefaultAsync();
                    if (AssetDetails != null)
                    {
                        var _IsExists = await _HCoreContext.AtAssetTransferDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXISTS", "A request for transfer is already exists for this asset. Please approve it instead of creating new request");
                        }

                        if (AssetDetails.StatusId == AssetStatus.Disposed)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATDISPOSED", "This asset is disposed you cannot transfer to anyone");
                        }

                        if (AssetDetails.AssetTypeId == AssetType.WithoutBarcode)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0404", "This asset cannot be be transfered.");
                        }

                        var AssetIssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == AssetDetails.Id && (x.StatusId == AssetStatus.Issued || x.StatusId == AssetStatus.Returned)).FirstOrDefaultAsync();
                        if (AssetIssueDetails != null)
                        {
                            if (AssetIssueDetails.EmployeeId == AccountId)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0408", "You cannot transfer the asset to the same person whom it is currently assigned.");
                            }

                            bool _IsReturnPending = await _HCoreContext.AtAssetReturnDetails.AnyAsync(x => x.AssetId == AssetDetails.Id && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval));
                            if (_IsReturnPending)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0404", "This asset cannot be be transfered, because there is a retun for this asset which is approval pending. Please approve it then try to transfer");
                            }

                            _AssetRequisition = new AtAssetRequisition();
                            _AssetRequisition.Guid = HCoreHelper.GenerateGuid();
                            _AssetRequisition.AssetId = AssetDetails.Id;
                            _AssetRequisition.ApprovalTypeId = AssetApproval.TransferApproval;

                            if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                            {
                                _AssetRequisition.IsDeptHeadApproved = 1;
                                _AssetRequisition.IsDivHeadApproved = 1;
                                _AssetRequisition.IsHrApproved = 0;
                                _AssetRequisition.IsAdminApproved = 0;
                                _AssetRequisition.IsSuperAdminApproved = 0;
                            }
                            else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                            {
                                _AssetRequisition.IsDeptHeadApproved = 1;
                                _AssetRequisition.IsDivHeadApproved = 1;
                                _AssetRequisition.IsHrApproved = 1;
                                _AssetRequisition.IsAdminApproved = 1;
                                _AssetRequisition.IsSuperAdminApproved = 0;
                            }
                            else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                            {
                                _AssetRequisition.IsDeptHeadApproved = 1;
                                _AssetRequisition.IsDivHeadApproved = 1;
                                _AssetRequisition.IsHrApproved = 1;
                                _AssetRequisition.IsAdminApproved = 1;
                                _AssetRequisition.IsSuperAdminApproved = 1;
                            }
                            else
                            {
                                _AssetRequisition.IsDeptHeadApproved = 0;
                                _AssetRequisition.IsDivHeadApproved = 0;
                                _AssetRequisition.IsHrApproved = 0;
                                _AssetRequisition.IsAdminApproved = 0;
                                _AssetRequisition.IsSuperAdminApproved = 0;
                            }

                            _AssetRequisition.CreatedDate = HCoreHelper.GetDateTime();
                            _AssetRequisition.CreatedById = _Request.UserReference.AccountId;
                            await _HCoreContext.AtAssetRequisitions.AddAsync(_AssetRequisition);
                            await _HCoreContext.SaveChangesAsync();

                            string? Reference = HCoreHelper.GenerateGuid();

                            AssetIssueDetails.IsTransfered = 1;
                            AssetIssueDetails.TransferedTo = AccountId;
                            AssetIssueDetails.TransferedBy = _Request.UserReference.AccountId;
                            AssetIssueDetails.TransferedDate = HCoreHelper.GetDateTime();
                            AssetIssueDetails.TransferReference = Reference;
                            AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                            AssetIssueDetails.ModifiedById = _Request.UserReference.AccountId;
                            await _HCoreContext.SaveChangesAsync();

                            _AssetTransferDetail = new AtAssetTransferDetail();
                            _AssetTransferDetail.Guid = AssetIssueDetails.TransferReference;
                            _AssetTransferDetail.AssetId = AssetDetails.Id;
                            _AssetTransferDetail.AssetIssueId = AssetIssueDetails.Id;
                            _AssetTransferDetail.SourceId = RegistrationSource.WebApplication;
                            _AssetTransferDetail.OrganizationId = AssetIssueDetails.OrganizationId;
                            _AssetTransferDetail.DivisionId = (long)DivisionId;
                            if (DepartmentId > 0)
                            {
                                _AssetTransferDetail.DepartmentId = DepartmentId;
                            }
                            if (AccountId > 0)
                            {
                                _AssetTransferDetail.EmployeeId = AccountId;
                            }
                            _AssetTransferDetail.LocationId = (long)LocationId;
                            _AssetTransferDetail.SubLocationId = (long)SubLocationId;
                            _AssetTransferDetail.Remark = _Request.Remark;
                            _AssetTransferDetail.OsName = _Request.OsName;
                            _AssetTransferDetail.HostName = _Request.HostName;
                            _AssetTransferDetail.IpAddress = _Request.IpAddress;
                            _AssetTransferDetail.Quantity = AssetIssueDetails.Quantity;

                            if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                            {
                                _AssetTransferDetail.StatusId = AssetStatus.AssetApproval.PendingHRApproval;

                                string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                string? Body = "The transfer request for this asset has been raised and approved by the <b>Division Head</b> and sent for approval to <b>HR</b> to <b>";

                                string? EmployeeName = "";
                                string? EmployeeCode = "";
                                string? EmployeeEmail = "";

                                if (AccountId > 0)
                                {
                                    var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                            .Select(x => new
                                                            {
                                                                Name = x.DisplayName,
                                                                AccountCode = x.AccountCode,
                                                                Email = x.EmailAddress
                                                            }).FirstOrDefaultAsync();
                                    if (Employee != null)
                                    {
                                        EmployeeName = Employee.Name;
                                        EmployeeCode = Employee.AccountCode;
                                        EmployeeEmail = Employee.Email;
                                    }

                                    Body += EmployeeName + "(" + EmployeeCode + "), ";
                                }

                                string? Department = "";

                                if (DepartmentId > 0)
                                {
                                    Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    if (!string.IsNullOrEmpty(Department))
                                    {
                                        Body += Department + ", ";
                                    }
                                }

                                Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                if (AssetIssueDetails.EmployeeId > 0)
                                {
                                    var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                          .Select(x => new
                                                          {
                                                              DisplayName = x.DisplayName,
                                                              AccountCode = x.AccountCode,
                                                          }).FirstOrDefaultAsync();
                                    if (EmployeeDetails != null)
                                    {
                                        Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                    }
                                }
                                if (AssetIssueDetails.DepartmentId > 0)
                                {
                                    string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    Body += DepartmentName + ", ";
                                }

                                string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    _AtAssetHistory = new AtAssetHistory();
                                    _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                    _AtAssetHistory.Status = "Transfer Request";
                                    _AtAssetHistory.Activity = Body;
                                    _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                    _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                    await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }
                            else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                            {
                                _AssetTransferDetail.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;

                                string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                string? Body = "The transfer request for this asset has been raised and approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b> to <b>";

                                string? EmployeeName = "";
                                string? EmployeeCode = "";
                                string? EmployeeEmail = "";

                                if (AccountId > 0)
                                {
                                    var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                            .Select(x => new
                                                            {
                                                                Name = x.DisplayName,
                                                                AccountCode = x.AccountCode,
                                                                Email = x.EmailAddress
                                                            }).FirstOrDefaultAsync();
                                    if (Employee != null)
                                    {
                                        EmployeeName = Employee.Name;
                                        EmployeeCode = Employee.AccountCode;
                                        EmployeeEmail = Employee.Email;
                                    }

                                    Body += EmployeeName + "(" + EmployeeCode + "), ";
                                }

                                string? Department = "";

                                if (DepartmentId > 0)
                                {
                                    Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    if (!string.IsNullOrEmpty(Department))
                                    {
                                        Body += Department + ", ";
                                    }
                                }

                                Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                if (AssetIssueDetails.EmployeeId > 0)
                                {
                                    var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                          .Select(x => new
                                                          {
                                                              DisplayName = x.DisplayName,
                                                              AccountCode = x.AccountCode,
                                                          }).FirstOrDefaultAsync();
                                    if (EmployeeDetails != null)
                                    {
                                        Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                    }
                                }
                                if (AssetIssueDetails.DepartmentId > 0)
                                {
                                    string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    Body += DepartmentName + ", ";
                                }

                                string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    _AtAssetHistory = new AtAssetHistory();
                                    _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                    _AtAssetHistory.Status = "Transfer Request";
                                    _AtAssetHistory.Activity = Body;
                                    _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                    _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                    await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }
                            else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                            {
                                _AssetTransferDetail.StatusId = AssetStatus.Transfered;
                                _AssetTransferDetail.ApprovedDate = HCoreHelper.GetDateTime();
                                _AssetTransferDetail.ApprovedById = _Request.UserReference.AccountId;

                                AssetDetails.StatusId = AssetStatus.Issued;
                                AssetIssueDetails.StatusId = AssetStatus.Transfered;

                                _AtAssetIssueDetail = new AtAssetIssueDetail();
                                _AtAssetIssueDetail.Guid = AssetIssueDetails.TransferReference;
                                _AtAssetIssueDetail.AssetId = AssetDetails.Id;
                                _AtAssetIssueDetail.SourceId = RegistrationSource.WebApplication;
                                _AtAssetIssueDetail.OrganizationId = _AssetTransferDetail.OrganizationId;
                                _AtAssetIssueDetail.DivisionId = _AssetTransferDetail.DivisionId;
                                if (_AssetTransferDetail.DepartmentId > 0)
                                {
                                    _AtAssetIssueDetail.DepartmentId = _AssetTransferDetail.DepartmentId;
                                }
                                if (_AssetTransferDetail.EmployeeId > 0)
                                {
                                    _AtAssetIssueDetail.EmployeeId = _AssetTransferDetail.EmployeeId;
                                }
                                _AtAssetIssueDetail.LocationId = _AssetTransferDetail.LocationId;
                                _AtAssetIssueDetail.SubLocationId = _AssetTransferDetail.SubLocationId;
                                _AtAssetIssueDetail.Remark = _AssetTransferDetail.Remark;
                                _AtAssetIssueDetail.OsName = _AssetTransferDetail.OsName;
                                _AtAssetIssueDetail.HostName = _AssetTransferDetail.HostName;
                                _AtAssetIssueDetail.IpAddress = _AssetTransferDetail.IpAddress;

                                _AtAssetIssueDetail.Quantity = _AssetTransferDetail.Quantity;
                                _AtAssetIssueDetail.IsTransfered = 0;
                                _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                                _AtAssetIssueDetail.CreatedById = _Request.UserReference.AccountId;
                                _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.AtAssetIssueDetails.AddAsync(_AtAssetIssueDetail);

                                string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                string? Body = "This asset has been transferred to <b>";

                                string? EmployeeName = "";
                                string? EmployeeCode = "";

                                if (AccountId > 0)
                                {
                                    var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                            .Select(x => new
                                                            {
                                                                Name = x.DisplayName,
                                                                AccountCode = x.AccountCode,
                                                            }).FirstOrDefaultAsync();
                                    if (Employee != null)
                                    {
                                        EmployeeName = Employee.Name;
                                        EmployeeCode = Employee.AccountCode;
                                    }

                                    Body += EmployeeName + "(" + EmployeeCode + "), ";
                                }

                                string? Department = "";

                                if (DepartmentId > 0)
                                {
                                    Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    if (!string.IsNullOrEmpty(Department))
                                    {
                                        Body += Department + ", ";
                                    }
                                }

                                Body += Division + ", " + Location + ", " + SubLocation + "</b> by the approval of <b>Super Admin</b> from <b>";

                                if (AssetIssueDetails.EmployeeId > 0)
                                {
                                    var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                          .Select(x => new
                                                          {
                                                              DisplayName = x.DisplayName,
                                                              AccountCode = x.AccountCode,
                                                          }).FirstOrDefaultAsync();
                                    if (EmployeeDetails != null)
                                    {
                                        Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                    }
                                }
                                if (AssetIssueDetails.DepartmentId > 0)
                                {
                                    string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    Body += DepartmentName + ", ";
                                }

                                string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    _AtAssetHistory = new AtAssetHistory();
                                    _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                    _AtAssetHistory.Status = "Issued";
                                    _AtAssetHistory.Activity = Body;
                                    _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                    _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                    await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }
                            else
                            {
                                _AssetTransferDetail.StatusId = AssetStatus.AssetApproval.PendingDepartmentHeadApproval;

                                string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == LocationId && x.DivisionId == DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == SubLocationId && x.ParentId == LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                string? Body = "An transfer request has been raised for this asset to <b>";

                                string? EmployeeName = "";
                                string? EmployeeCode = "";
                                string? EmployeeEmail = "";

                                if (AccountId > 0)
                                {
                                    var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AccountId)
                                                            .Select(x => new
                                                            {
                                                                Name = x.DisplayName,
                                                                AccountCode = x.AccountCode,
                                                                Email = x.EmailAddress
                                                            }).FirstOrDefaultAsync();
                                    if (Employee != null)
                                    {
                                        EmployeeName = Employee.Name;
                                        EmployeeCode = Employee.AccountCode;
                                        EmployeeEmail = Employee.Email;
                                    }

                                    Body += EmployeeName + "(" + EmployeeCode + "), ";
                                }

                                string? Department = "";

                                if (DepartmentId > 0)
                                {
                                    Department = await _HCoreContext.AtDepartments.Where(x => x.Id == DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    if (!string.IsNullOrEmpty(Department))
                                    {
                                        Body += Department + ", ";
                                    }
                                }

                                Body += Division + ", " + Location + ", " + SubLocation + "</b>. The request has been sent for approval to the <b>Department Head</b> from <b>";

                                if (AssetIssueDetails.EmployeeId > 0)
                                {
                                    var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                          .Select(x => new
                                                          {
                                                              DisplayName = x.DisplayName,
                                                              AccountCode = x.AccountCode,
                                                          }).FirstOrDefaultAsync();
                                    if (EmployeeDetails != null)
                                    {
                                        Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                    }
                                }
                                if (AssetIssueDetails.DepartmentId > 0)
                                {
                                    string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                    Body += DepartmentName + ", ";
                                }

                                string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    _AtAssetHistory = new AtAssetHistory();
                                    _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                    _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                    _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                    _AtAssetHistory.Status = "Transfer Request";
                                    _AtAssetHistory.Activity = Body;
                                    _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                    _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                    await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                    await _HCoreContextLogging.SaveChangesAsync();
                                }
                            }

                            _AssetTransferDetail.CreatedById = _Request.UserReference.AccountId;
                            _AssetTransferDetail.CreatedDate = HCoreHelper.GetDateTime();
                            await _HCoreContext.AtAssetTransferDetails.AddAsync(_AssetTransferDetail);
                            await _HCoreContext.SaveChangesAsync();

                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "AT0200", "Aseet transfer details saved successfully");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "This asset was not assigned.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Invalid asset details");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("TrasnsferAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ApproveAssetTransfer(OReference _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset key required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT001", "Asset id required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetTransferDetails = await _HCoreContext.AtAssetTransferDetails.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && (x.StatusId == AssetStatus.AssetApproval.PendingDepartmentHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingDivisionHeadApproval || x.StatusId == AssetStatus.AssetApproval.PendingHRApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval || x.StatusId == AssetStatus.AssetApproval.PendingSuperAdminApproval)).FirstOrDefaultAsync();
                    if (AssetTransferDetails != null)
                    {
                        var AssetIssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.Id == AssetTransferDetails.AssetIssueId).FirstOrDefaultAsync();
                        if (AssetIssueDetails != null)
                        {
                            var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == AssetTransferDetails.AssetId).FirstOrDefaultAsync();
                            if (AssetDetails != null)
                            {
                                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                                if (StatusId == AssetStatus.AssetApproval.Rejected)
                                {
                                    AssetIssueDetails.StatusId = AssetStatus.Issued;
                                    AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    AssetIssueDetails.ModifiedById = _Request.UserReference.AccountId;

                                    AssetTransferDetails.StatusId = AssetStatus.AssetApproval.Rejected;
                                    AssetTransferDetails.ApprovedDate = HCoreHelper.GetDateTime();
                                    AssetTransferDetails.ApprovedById = _Request.UserReference.AccountId;
                                    AssetTransferDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                    AssetTransferDetails.ModifiedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.SaveChangesAsync();

                                    string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetTransferDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                    string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.LocationId && x.DivisionId == AssetTransferDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.SubLocationId && x.ParentId == AssetTransferDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                    string? Body = "The transfer request for this asset to <b>";

                                    string? EmployeeName = "";
                                    string? EmployeeCode = "";

                                    if (AssetTransferDetails.EmployeeId > 0)
                                    {
                                        var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetTransferDetails.EmployeeId)
                                                                .Select(x => new
                                                                {
                                                                    Name = x.DisplayName,
                                                                    AccountCode = x.AccountCode,
                                                                }).FirstOrDefaultAsync();
                                        if (Employee != null)
                                        {
                                            EmployeeName = Employee.Name;
                                            EmployeeCode = Employee.AccountCode;
                                        }

                                        Body += EmployeeName + "(" + EmployeeCode + "), ";
                                    }

                                    string? Department = "";

                                    if (AssetTransferDetails.DepartmentId > 0)
                                    {
                                        Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetTransferDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                        if (!string.IsNullOrEmpty(Department))
                                        {
                                            Body += Department + ", ";
                                        }
                                    }

                                    Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                    if (AssetIssueDetails.EmployeeId > 0)
                                    {
                                        var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                              .Select(x => new
                                                              {
                                                                  DisplayName = x.DisplayName,
                                                                  AccountCode = x.AccountCode,
                                                              }).FirstOrDefaultAsync();
                                        if (EmployeeDetails != null)
                                        {
                                            Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                        }
                                    }
                                    if (AssetIssueDetails.DepartmentId > 0)
                                    {
                                        string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                        Body += DepartmentName + ", ";
                                    }

                                    string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                    string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                    Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b> has been rejected by the <b>" + _Request.UserReference.UserRoleName + "</b>";

                                    using (_HCoreContextLogging = new HCoreContextLogging())
                                    {
                                        _AtAssetHistory = new AtAssetHistory();
                                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                        _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                        _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                        _AtAssetHistory.Status = "Transfer Requested Rejected";
                                        _AtAssetHistory.Activity = Body;
                                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                        _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                        await _HCoreContextLogging.SaveChangesAsync();
                                    }

                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATUPDATE", "Asset transfer rejected");
                                }
                                else
                                {
                                    var _AssetConfig = await _HCoreContext.AtAssetRequisitions.Where(x => x.AssetId == AssetIssueDetails.AssetId && x.ApprovalTypeId == AssetApproval.TransferApproval).FirstOrDefaultAsync();
                                    if (_AssetConfig != null)
                                    {
                                        if (_Request.UserReference.UserRoleId == UserRole.HeadOfDept)
                                        {
                                            AssetTransferDetails.StatusId = AssetStatus.AssetApproval.PendingDivisionHeadApproval;
                                            _AssetConfig.IsDeptHeadApproved = 1;

                                            string? Body = "The transfer request for this asset has been approved by the <b>Department Head</b> and sent for approval to <b>Division Head</b> for <b>";

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetTransferDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.LocationId && x.DivisionId == AssetTransferDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.SubLocationId && x.ParentId == AssetTransferDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";

                                            if (AssetTransferDetails.EmployeeId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetTransferDetails.EmployeeId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (AssetTransferDetails.DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetTransferDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                            if (AssetIssueDetails.EmployeeId > 0)
                                            {
                                                var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                      .Select(x => new
                                                                      {
                                                                          DisplayName = x.DisplayName,
                                                                          AccountCode = x.AccountCode,
                                                                      }).FirstOrDefaultAsync();
                                                if (EmployeeDetails != null)
                                                {
                                                    Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                }
                                            }
                                            if (AssetIssueDetails.DepartmentId > 0)
                                            {
                                                string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                Body += DepartmentName + ", ";
                                            }

                                            string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Transfer Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else if (_Request.UserReference.UserRoleId == UserRole.HeadOfDiv)
                                        {
                                            AssetTransferDetails.StatusId = AssetStatus.AssetApproval.PendingHRApproval;
                                            _AssetConfig.IsDivHeadApproved = 1;

                                            string? Body = "The transfer request for this asset has been approved by the <b>Division Head</b> and sent for approval to <b>HR</b> for <b>";

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetTransferDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.LocationId && x.DivisionId == AssetTransferDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.SubLocationId && x.ParentId == AssetTransferDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";

                                            if (AssetTransferDetails.EmployeeId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetTransferDetails.EmployeeId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (AssetTransferDetails.DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetTransferDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                            if (AssetIssueDetails.EmployeeId > 0)
                                            {
                                                var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                      .Select(x => new
                                                                      {
                                                                          DisplayName = x.DisplayName,
                                                                          AccountCode = x.AccountCode,
                                                                      }).FirstOrDefaultAsync();
                                                if (EmployeeDetails != null)
                                                {
                                                    Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                }
                                            }
                                            if (AssetIssueDetails.DepartmentId > 0)
                                            {
                                                string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                Body += DepartmentName + ", ";
                                            }

                                            string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Transfer Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else if (_Request.UserReference.UserRoleId == UserRole.HR)
                                        {
                                            AssetTransferDetails.StatusId = AssetStatus.AssetApproval.PendingAdminApproval;
                                            _AssetConfig.IsHrApproved = 1;

                                            string? Body = "The transfer request for this asset has been approved by the <b>HR</b> and sent for approval to <b>Admin</b> for <b>";

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetTransferDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.LocationId && x.DivisionId == AssetTransferDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.SubLocationId && x.ParentId == AssetTransferDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";

                                            if (AssetTransferDetails.EmployeeId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetTransferDetails.EmployeeId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (AssetTransferDetails.DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetTransferDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                            if (AssetIssueDetails.EmployeeId > 0)
                                            {
                                                var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                      .Select(x => new
                                                                      {
                                                                          DisplayName = x.DisplayName,
                                                                          AccountCode = x.AccountCode,
                                                                      }).FirstOrDefaultAsync();
                                                if (EmployeeDetails != null)
                                                {
                                                    Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                }
                                            }
                                            if (AssetIssueDetails.DepartmentId > 0)
                                            {
                                                string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                Body += DepartmentName + ", ";
                                            }

                                            string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Transfer Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else if (_Request.UserReference.UserRoleId == UserRole.Admin)
                                        {
                                            AssetTransferDetails.StatusId = AssetStatus.AssetApproval.PendingSuperAdminApproval;
                                            _AssetConfig.IsAdminApproved = 1;

                                            string? Body = "The transfer request for this asset has been approved by the <b>Admin</b> and sent for approval to <b>Super Admin</b> for <b>";

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetTransferDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.LocationId && x.DivisionId == AssetTransferDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.SubLocationId && x.ParentId == AssetTransferDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";

                                            if (AssetTransferDetails.EmployeeId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetTransferDetails.EmployeeId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (AssetTransferDetails.DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetTransferDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b> from <b>";

                                            if (AssetIssueDetails.EmployeeId > 0)
                                            {
                                                var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                      .Select(x => new
                                                                      {
                                                                          DisplayName = x.DisplayName,
                                                                          AccountCode = x.AccountCode,
                                                                      }).FirstOrDefaultAsync();
                                                if (EmployeeDetails != null)
                                                {
                                                    Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                }
                                            }
                                            if (AssetIssueDetails.DepartmentId > 0)
                                            {
                                                string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                Body += DepartmentName + ", ";
                                            }

                                            string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Transfer Request";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }
                                        else if (_Request.UserReference.UserRoleId == UserRole.SuperAdmin)
                                        {
                                            AssetTransferDetails.StatusId = AssetStatus.Transfered;
                                            _AssetConfig.IsSuperAdminApproved = 1;

                                            AssetDetails.StatusId = AssetStatus.Issued;
                                            AssetIssueDetails.StatusId = AssetStatus.Transfered;

                                            _AtAssetIssueDetail = new AtAssetIssueDetail();
                                            _AtAssetIssueDetail.Guid = AssetIssueDetails.TransferReference;
                                            _AtAssetIssueDetail.AssetId = AssetDetails.Id;
                                            _AtAssetIssueDetail.SourceId = RegistrationSource.WebApplication;
                                            _AtAssetIssueDetail.OrganizationId = AssetIssueDetails.OrganizationId;
                                            _AtAssetIssueDetail.DivisionId = AssetIssueDetails.DivisionId;
                                            if (AssetTransferDetails.DepartmentId > 0)
                                            {
                                                _AtAssetIssueDetail.DepartmentId = AssetTransferDetails.DepartmentId;
                                            }
                                            if (AssetTransferDetails.EmployeeId > 0)
                                            {
                                                _AtAssetIssueDetail.EmployeeId = AssetTransferDetails.EmployeeId;
                                            }
                                            _AtAssetIssueDetail.LocationId = AssetTransferDetails.LocationId;
                                            _AtAssetIssueDetail.SubLocationId = AssetTransferDetails.SubLocationId;
                                            _AtAssetIssueDetail.Remark = AssetTransferDetails.Remark;
                                            _AtAssetIssueDetail.OsName = AssetTransferDetails.OsName;
                                            _AtAssetIssueDetail.HostName = AssetTransferDetails.HostName;
                                            _AtAssetIssueDetail.IpAddress = AssetTransferDetails.IpAddress;
                                            _AtAssetIssueDetail.Quantity = AssetTransferDetails.Quantity;
                                            _AtAssetIssueDetail.IsTransfered = 0;
                                            _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                                            _AtAssetIssueDetail.CreatedById = _Request.UserReference.AccountId;
                                            _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtAssetIssueDetails.AddAsync(_AtAssetIssueDetail);

                                            var AssetReturnDetails = await _HCoreContext.AtAssetReturnDetails.Where(x => x.AssetId == AssetIssueDetails.Id).FirstOrDefaultAsync();
                                            if (AssetReturnDetails != null)
                                            {
                                                if (AssetReturnDetails.StatusId == AssetStatus.Returned)
                                                {
                                                    AssetReturnDetails.StatusId = AssetStatus.Issued;
                                                    AssetReturnDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                                    AssetReturnDetails.ModifiedById = _Request.UserReference.AccountId;
                                                }
                                            }

                                            string? Division = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetTransferDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? Location = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.LocationId && x.DivisionId == AssetTransferDetails.DivisionId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocation = await _HCoreContext.AtLocations.Where(x => x.Id == AssetTransferDetails.SubLocationId && x.ParentId == AssetTransferDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();

                                            string? Body = "This asset has been transferred to <b>";

                                            string? EmployeeName = "";
                                            string? EmployeeCode = "";

                                            if (AssetTransferDetails.EmployeeId > 0)
                                            {
                                                var Employee = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetTransferDetails.EmployeeId)
                                                                        .Select(x => new
                                                                        {
                                                                            Name = x.DisplayName,
                                                                            AccountCode = x.AccountCode,
                                                                        }).FirstOrDefaultAsync();
                                                if (Employee != null)
                                                {
                                                    EmployeeName = Employee.Name;
                                                    EmployeeCode = Employee.AccountCode;
                                                }

                                                Body += EmployeeName + "(" + EmployeeCode + "), ";
                                            }

                                            string? Department = "";

                                            if (AssetTransferDetails.DepartmentId > 0)
                                            {
                                                Department = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetTransferDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                if (!string.IsNullOrEmpty(Department))
                                                {
                                                    Body += Department + ", ";
                                                }
                                            }

                                            Body += Division + ", " + Location + ", " + SubLocation + "</b> by the approval of <b>Super Admin</b> from <b>";

                                            if (AssetIssueDetails.EmployeeId > 0)
                                            {
                                                var EmployeeDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == AssetIssueDetails.EmployeeId)
                                                                      .Select(x => new
                                                                      {
                                                                          DisplayName = x.DisplayName,
                                                                          AccountCode = x.AccountCode,
                                                                      }).FirstOrDefaultAsync();
                                                if (EmployeeDetails != null)
                                                {
                                                    Body += EmployeeDetails.DisplayName + "(" + EmployeeDetails.AccountCode + "), ";
                                                }
                                            }
                                            if (AssetIssueDetails.DepartmentId > 0)
                                            {
                                                string? DepartmentName = await _HCoreContext.AtDepartments.Where(x => x.Id == AssetIssueDetails.DepartmentId).Select(x => x.DepartmentName).FirstOrDefaultAsync();
                                                Body += DepartmentName + ", ";
                                            }

                                            string? DivisionName = await _HCoreContext.AtDivisions.Where(x => x.Id == AssetIssueDetails.DivisionId).Select(x => x.DivisionName).FirstOrDefaultAsync();
                                            string? LocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.LocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            string? SubLocationName = await _HCoreContext.AtLocations.Where(x => x.Id == AssetIssueDetails.SubLocationId).Select(x => x.LocationName).FirstOrDefaultAsync();
                                            Body += DivisionName + ", " + LocationName + ", " + SubLocationName + "</b>";

                                            using (_HCoreContextLogging = new HCoreContextLogging())
                                            {
                                                _AtAssetHistory = new AtAssetHistory();
                                                _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                                                _AtAssetHistory.OrganizationId = _Request.UserReference.OrganizationId;
                                                _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                                                _AtAssetHistory.Status = "Transferred";
                                                _AtAssetHistory.Activity = Body;
                                                _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                                                _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                                                await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                                                await _HCoreContextLogging.SaveChangesAsync();
                                            }
                                        }

                                        _AssetConfig.ModifiedDate = HCoreHelper.GetDateTime();
                                        _AssetConfig.ModifiedById = _Request.UserReference.AccountId;

                                        AssetIssueDetails.ApprovedDate = HCoreHelper.GetDateTime();
                                        AssetIssueDetails.Comment = _Request.Comment;
                                        AssetIssueDetails.ApprovedById = _Request.UserReference.AccountId;
                                        AssetIssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                        AssetIssueDetails.ModifiedById = _Request.UserReference.AccountId;

                                        AssetTransferDetails.Comment = _Request.Comment;
                                        AssetTransferDetails.ApprovedDate = HCoreHelper.GetDateTime();
                                        AssetTransferDetails.ApprovedById = _Request.UserReference.AccountId;
                                        AssetTransferDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                        AssetTransferDetails.ModifiedById = _Request.UserReference.AccountId;

                                        await _HCoreContext.SaveChangesAsync();

                                        var AssetLocationDetails = await _HCoreContext.AtAssetLocations.Where(x => x.AssetId == AssetIssueDetails.Id).FirstOrDefaultAsync();
                                        if (AssetLocationDetails != null)
                                        {
                                            AssetLocationDetails.SubLocationId = (long)AssetTransferDetails.SubLocationId;
                                            AssetLocationDetails.LocationId = (long)AssetTransferDetails.LocationId;
                                            AssetLocationDetails.DivisionId = (long)AssetTransferDetails.DivisionId;
                                        }
                                        else
                                        {
                                            _AtAssetLocation = new AtAssetLocation();
                                            _AtAssetLocation.Guid = HCoreHelper.GenerateGuid();
                                            _AtAssetLocation.AssetId = AssetTransferDetails.Id;
                                            _AtAssetLocation.DivisionId = (long)AssetTransferDetails.DivisionId;
                                            _AtAssetLocation.LocationId = (long)AssetTransferDetails.LocationId;
                                            _AtAssetLocation.SubLocationId = (long)AssetTransferDetails.SubLocationId;
                                            _AtAssetLocation.StatusId = HelperStatus.Default.Active;
                                            _AtAssetLocation.CreatedById = _Request.UserReference.AccountId;
                                            _AtAssetLocation.CreatedDate = HCoreHelper.GetDateTime();
                                            await _HCoreContext.AtAssetLocations.AddAsync(_AtAssetLocation);
                                            await _HCoreContext.SaveChangesAsync();
                                        }

                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, "ATUPDATE", "Asset transfer approved");
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset requisition config details not found for this asset");
                                    }
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset details not found");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "This asset is not issued to anyone");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Please create a transfer request first.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ApproveAssetTransfer", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> IssueAssetToMaintenance(OAssetOperations.Maintenance.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.AssetKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT001", "Please enter the barcode");
                }

                int? MaintenaceTypeId = 0;
                if (string.IsNullOrEmpty(_Request.MaintenanceType))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT002", "Please select maintenance type.");
                }
                else
                {
                    MaintenaceTypeId = HCoreHelper.GetSystemHelperId(_Request.MaintenanceType, _Request.UserReference);
                    if (MaintenaceTypeId < 1 || MaintenaceTypeId == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT002", "Invalid maintenance type selected.");
                    }
                }

                long? VendorId = 0;
                if (string.IsNullOrEmpty(_Request.VendorKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT003", "Please select vendor");
                }
                else
                {
                    VendorId = HCoreHelper.GetAccountId(_Request.VendorKey, _Request.UserReference);
                    if (VendorId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT002", "Invalid vendor selected.");
                    }
                }

                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT004", "Please enter description.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Guid == _Request.AssetKey && (x.StatusId != AssetStatus.Disposed || x.StatusId != AssetStatus.Expired)).FirstOrDefaultAsync();
                    if (AssetDetails != null)
                    {
                        string? MaintenanceTypes = "";
                        if (MaintenaceTypeId == MaintenanceType.Repaire)
                        {
                            MaintenanceTypes = MaintenanceType.RepaireS;
                        }
                        else
                        {
                            MaintenanceTypes = MaintenanceType.MaintenanceS;
                        }

                        bool _IsExists = await _HCoreContext.AtAssetMaintenances.AnyAsync(x => x.AssetId == AssetDetails.Id && x.StatusId == MaintenanceStatus.Assigned);
                        if (_IsExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATEXISTS", "There's already a request present for repaire and maintenance for this asset.");
                        }

                        _AtAssetMaintenance = new AtAssetMaintenance();
                        _AtAssetMaintenance.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetMaintenance.AssetId = AssetDetails.Id;
                        _AtAssetMaintenance.VendorId = (long)VendorId;
                        _AtAssetMaintenance.OrganizationId = _Request.UserReference.OrganizationId;
                        _AtAssetMaintenance.MaintenanceType = (int)MaintenaceTypeId;
                        _AtAssetMaintenance.Description = _Request.Description;
                        _AtAssetMaintenance.RequestDate = (DateTime)_Request.RequestDate;
                        _AtAssetMaintenance.ExpectedDate = _Request.ExpectedDate;
                        _AtAssetMaintenance.ReceiptNo = HCoreHelper.GenerateAccountCode(6);
                        _AtAssetMaintenance.ReceiptDate = HCoreHelper.GetDateTime();
                        _AtAssetMaintenance.StatusId = MaintenanceStatus.Assigned;
                        _AtAssetMaintenance.CreatedDate = HCoreHelper.GetDateTime();
                        _AtAssetMaintenance.CreatedById = _Request.UserReference.AccountId;
                        await _HCoreContext.AtAssetMaintenances.AddAsync(_AtAssetMaintenance);
                        await _HCoreContext.SaveChangesAsync();

                        var VendorDetails = await _HCoreContext.AtAccounts.Where(x => x.Id == VendorId)
                                            .Select(x => new
                                            {
                                                VendorName = x.DisplayName,
                                                MobileNo = x.MobileNumber,
                                                EmailAddress = x.EmailAddress,
                                                Address = x.Address
                                            }).FirstOrDefaultAsync();

                        var _Response = new
                        {
                            ReceiptNo = _AtAssetMaintenance.ReceiptNo,
                            ReceiptDate = _AtAssetMaintenance.ReceiptDate.ToString(),
                            CompanyName = _Request.UserReference.OrganizationDisplayName,
                            AssetBarcode = AssetDetails.AssetBarcode,
                            AssetSerialNo = AssetDetails.AssetSerialNumber,
                            AssetName = AssetDetails.AssetName,
                            Description = _AtAssetMaintenance.Description,
                            VendorName = VendorDetails.VendorName,
                            MobileNo = VendorDetails.MobileNo,
                            EmailAddress = VendorDetails.EmailAddress,
                            Address = VendorDetails.Address,
                            ExpectedDate = _AtAssetMaintenance.ExpectedDate.ToString(),
                            UserName = _Request.UserReference.DisplayName,
                        };

                        if (_Request.Activity == true)
                        {
                            long? AccountId = HCoreHelper.GetAccountId(_Request.AccountKey, _Request.UserReference);
                            var IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == AssetDetails.Id && x.EmployeeId == AccountId && x.StatusId == AssetStatus.Issued).FirstOrDefaultAsync();
                            if (IssueDetails != null)
                            {
                                IssueDetails.StatusId = AssetStatus.Returned;
                                IssueDetails.TransferReference = _AtAssetMaintenance.Guid;
                                IssueDetails.ModifiedById = _Request.UserReference.AccountId;
                                IssueDetails.ModifiedDate = HCoreHelper.GetDateTime();
                                await _HCoreContext.SaveChangesAsync();
                            }
                        }

                        string? Vendor = await _HCoreContext.AtAccounts.Where(x => x.Id == VendorId).Select(x => x.DisplayName).FirstOrDefaultAsync();

                        string? Body = "";
                        Body = "<div>";
                        Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                        Body += "Maintenace Details: -";
                        Body += "</label>";
                        Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                        Body += "<div>";
                        Body += "Vendor: - {{Vendor}}";
                        Body += "</div>";
                        Body += "<div>";
                        Body += "Requested Date: - {{RequestedDate}}";
                        Body += "</div>";
                        Body += "<div>";
                        Body += "Expected Date: - {{ExpectedDate}}";
                        Body += "</div>";
                        Body += "</div>";
                        Body += "</div>";
                        StringBuilder? Activty = new StringBuilder(Body);
                        Activty.Replace("{{Vendor}}", Vendor);
                        Activty.Replace("{{RequestedDate}}", _Request.RequestDate.ToString());
                        Activty.Replace("{{ExpectedDate}}", _Request.ExpectedDate.ToString());

                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            _AtAssetHistory = new AtAssetHistory();
                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                            _AtAssetHistory.OrganizationId = AssetDetails.OrganizationId;
                            _AtAssetHistory.AssetBarcode = AssetDetails.AssetBarcode;
                            _AtAssetHistory.Status = "Issued To Repaire & Maintenance";
                            _AtAssetHistory.Activity = Activty.ToString();
                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                            await _HCoreContextLogging.SaveChangesAsync();
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "ATSAVE", "Asset issued to " + MaintenanceTypes + "");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Asset details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("IssueAssetToMaintenance", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ReturnAssetFromMaintenance(OAssetOperations.Maintenance.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREF", "Reference key required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREF", "Reference id required");
                }
                if (string.IsNullOrEmpty(_Request.Remark))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREF", "Please fill the remark.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = await _HCoreContext.AtAssetMaintenances.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (Details != null)
                    {
                        Details.Remark = _Request.Remark;
                        Details.ReceivedDate = _Request.ReceivedDate;
                        Details.Amount = _Request.Amount;
                        Details.StatusId = MaintenanceStatus.Completed;
                        Details.ModifiedById = _Request.UserReference.AccountId;
                        Details.ModifiedDate = HCoreHelper.GetDateTime();
                        await _HCoreContext.SaveChangesAsync();

                        if (_Request.IssueDetails != null)
                        {
                            string? TransferRemark = "";
                            long? DivisionId = HCoreHelper.GetDivisionId(_Request.IssueDetails.DivisionKey, _Request.UserReference.OrganizationId, _Request.UserReference);
                            long? DepartmentId = HCoreHelper.GetDepartmentId(_Request.IssueDetails.DepartmentKey, _Request.UserReference);
                            long? LocationId = HCoreHelper.GetLocationId(_Request.IssueDetails.LocationKey, _Request.UserReference);
                            long? SubLocationId = HCoreHelper.GetSubLocationId(_Request.IssueDetails.SubLocationKey, (long)LocationId, _Request.UserReference);
                            long? AccountId = HCoreHelper.GetAccountId(_Request.IssueDetails.EmployeeKey, _Request.UserReference);
                            long? SubDivisionId = await _HCoreContext.AtDivisions.Where(x => x.ParentId == DivisionId).Select(x => x.Id).FirstOrDefaultAsync();

                            var IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.TransferReference == Details.Guid && x.StatusId == AssetStatus.Returned).FirstOrDefaultAsync();
                            if (IssueDetails != null)
                            {
                                TransferRemark = IssueDetails.Remark;
                                IssueDetails.IsTransfered = 1;
                                IssueDetails.TransferedBy = _Request.UserReference.AccountId;
                                IssueDetails.TransferedDate = HCoreHelper.GetDateTime();
                                IssueDetails.TransferedTo = (long)AccountId;
                                IssueDetails.StatusId = AssetStatus.Transfered;
                                await _HCoreContext.SaveChangesAsync();
                            }

                            _AtAssetIssueDetail = new AtAssetIssueDetail();
                            _AtAssetIssueDetail.Guid = Details.Guid;
                            _AtAssetIssueDetail.AssetId = Details.AssetId;
                            _AtAssetIssueDetail.OrganizationId = _Request.UserReference.OrganizationId;
                            _AtAssetIssueDetail.DivisionId = (long)DivisionId;
                            _AtAssetIssueDetail.DepartmentId = (long)DepartmentId;
                            _AtAssetIssueDetail.LocationId = (long)LocationId;
                            _AtAssetIssueDetail.SubLocationId = (long)SubLocationId;
                            _AtAssetIssueDetail.EmployeeId = (long)AccountId;
                            _AtAssetIssueDetail.Remark = TransferRemark;
                            _AtAssetIssueDetail.Quantity = 1;
                            _AtAssetIssueDetail.IsTransfered = 0;
                            _AtAssetIssueDetail.StatusId = AssetStatus.Issued;
                            _AtAssetIssueDetail.CreatedById = _Request.UserReference.AccountId;
                            _AtAssetIssueDetail.CreatedDate = HCoreHelper.GetDateTime();
                            _HCoreContext.AtAssetIssueDetails.Add(_AtAssetIssueDetail);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        string? Vendor = await _HCoreContext.AtAccounts.Where(x => x.Id == Details.VendorId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                        string? AssetBarcode = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == Details.AssetId).Select(x => x.AssetBarcode).FirstOrDefaultAsync();

                        string? Body = "";
                        Body = "<div>";
                        Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                        Body += "Maintenace Details: -";
                        Body += "</label>";
                        Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                        Body += "<div>";
                        Body += "Vendor: - {{Vendor}}";
                        Body += "</div>";
                        Body += "<div>";
                        Body += "Requested Date: - {{RequestedDate}}";
                        Body += "</div>";
                        Body += "<div>";
                        Body += "Expected Date: - {{ExpectedDate}}";
                        Body += "</div>";
                        Body += "<div>";
                        Body += "Recieved Date: - {{RecievedDate}}";
                        Body += "</div>";
                        Body += "<div>";
                        Body += "Amount: - {{Amount}}";
                        Body += "</div>";
                        Body += "</div>";
                        Body += "</div>";
                        StringBuilder? Activty = new StringBuilder(Body);
                        Activty.Replace("{{Vendor}}", Vendor);
                        Activty.Replace("{{RequestedDate}}", _Request.RequestDate.ToString());
                        Activty.Replace("{{ExpectedDate}}", _Request.ExpectedDate.ToString());
                        Activty.Replace("{{RecievedDate}}", Details.ReceivedDate.ToString());
                        Activty.Replace("{{Amount}}", _Request.Amount.ToString());

                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            _AtAssetHistory = new AtAssetHistory();
                            _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                            _AtAssetHistory.OrganizationId = Details.OrganizationId;
                            _AtAssetHistory.AssetBarcode = AssetBarcode;
                            _AtAssetHistory.Status = "Returned From Repaire & Maintenance";
                            _AtAssetHistory.Activity = Activty.ToString();
                            _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                            _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                            await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                            await _HCoreContextLogging.SaveChangesAsync();
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATUPDATE", "Asset recieved from maintenance");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ReturnAssetFromMaintenance", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> MultipleBarcodePrint(OAssetOperations.MultipleBarcodePrinting.Request _Request)
        {
            try
            {
                if (_Request.BarcodeDetails == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT001", "Barcode required");
                }
                if (string.IsNullOrEmpty(_Request.PrinterName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT001", "Please select Printer");
                }
                foreach (var Barcode in _Request.BarcodeDetails)
                {
                    string? ExAssetId = "";
                    using (_HCoreContext = new HCoreContext())
                    {
                        ExAssetId = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == Barcode.AssetBarcode).Select(x => x.ExAssetId).FirstOrDefaultAsync();
                    }
                    ManagePrinters? _ManagePrinters = new ManagePrinters();
                    await _ManagePrinters.PrintBarcode(Barcode.AssetBarcode, ExAssetId, _Request.PrinterName);
                }

                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "AT001", "Barcodes printed successfully");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("MultipleBarcodePrint", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> RePrintBarcode(OAssetOperations.RePrintBarcode _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.Barcode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT001", "Please enter barcode");
                }
                if (string.IsNullOrEmpty(_Request.ExAssetId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT001", "Ex Asset Id required");
                }
                if (string.IsNullOrEmpty(_Request.PrinterName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT001", "Please select Printer");
                }

                ManagePrinters? _ManagePrinters = new ManagePrinters();
                await _ManagePrinters.PrintBarcode(_Request.Barcode, _Request.ExAssetId.ToString(), _Request.PrinterName);

                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "AT001", "Barcode printed successfully");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("RePrintBarcode", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> DisposeAsset(OAssetOperations.Dispose _Request)
        {
            try
            {
                if (_Request.AssetId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Asset id required");
                }
                if (string.IsNullOrEmpty(_Request.AssetKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Please enter the reason for disposing this asset");
                }
                if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Asset key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var BarcodeDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == _Request.AssetId && x.Guid == _Request.AssetKey).FirstOrDefaultAsync();
                    if (BarcodeDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Details not found");
                    }

                    if (BarcodeDetails.StatusId == AssetStatus.Disposed)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATDISPOSED", "This asset is already disposed");
                    }

                    var Details = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == BarcodeDetails.Id).FirstOrDefaultAsync();
                    if (Details != null)
                    {
                        if (Details.StatusId != AssetStatus.Returned)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0004", "This asset is assigned to someone. Please make a return before disposing it.");
                        }
                        else
                        {
                            Details.StatusId = AssetStatus.Disposed;
                            Details.ModifiedById = _Request.UserReference.AccountId;
                            Details.ModifiedDate = HCoreHelper.GetDateTime();
                        }
                    }

                    var AssetReturnDetails = await _HCoreContext.AtAssetReturnDetails.Where(x => x.AssetId == BarcodeDetails.Id).FirstOrDefaultAsync();
                    if (AssetReturnDetails != null)
                    {
                        if (AssetReturnDetails.StatusId == AssetStatus.AssetReturn.ApprovalPending)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0404", "Please approve the asset return request before disposing it");
                        }
                        else if (AssetReturnDetails.StatusId == AssetStatus.Issued)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0004", "This asset is assigned to someone. Please make a return before disposing it.");
                        }
                        else
                        {
                            AssetReturnDetails.StatusId = AssetStatus.Disposed;
                            AssetReturnDetails.ModifiedDate = HCoreHelper.GetDateTime();
                            AssetReturnDetails.ModifiedById = _Request.UserReference.AccountId;
                        }
                    }

                    BarcodeDetails.StatusId = AssetStatus.Disposed;
                    BarcodeDetails.ModifiedById = _Request.UserReference.AccountId;
                    BarcodeDetails.ModifiedDate = HCoreHelper.GetDateTime();
                    await _HCoreContext.SaveChangesAsync();

                    string? DisposedBy = await _HCoreContext.AtAccounts.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.DisplayName).FirstOrDefaultAsync();

                    string? Body = "";
                    Body = "<div>";
                    Body += "<label style=\"font-size: 14px; font-style: inherit; font-weight: bolder;font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                    Body += "Disposal Details: -";
                    Body += "</label>";
                    Body += "<div style=\"font -size: 13px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;\">";
                    Body += "<div>";
                    Body += "Disposed On: - {{DisposedOn}}";
                    Body += "</div>";
                    Body += "<div>";
                    Body += "Disposed By: - {{DisposedBy}}";
                    Body += "</div>";
                    Body += "</div>";
                    Body += "</div>";
                    StringBuilder? Activty = new StringBuilder(Body);
                    Activty.Replace("{{DisposedOn}}", BarcodeDetails.ModifiedDate.ToString());
                    Activty.Replace("{{DisposedBy}}", DisposedBy);

                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {
                        _AtAssetDisposeLog = new AtAssetDisposeLog();
                        _AtAssetDisposeLog.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetDisposeLog.AssetBarcode = BarcodeDetails.AssetBarcode;
                        _AtAssetDisposeLog.Comment = _Request.Comment;
                        _AtAssetDisposeLog.DisposedDate = HCoreHelper.GetDateTime();
                        _AtAssetDisposeLog.DisposedById = _Request.UserReference.AccountId;
                        await _HCoreContextLogging.AtAssetDisposeLogs.AddAsync(_AtAssetDisposeLog);
                        await _HCoreContextLogging.SaveChangesAsync();

                        _AtAssetHistory = new AtAssetHistory();
                        _AtAssetHistory.Guid = HCoreHelper.GenerateGuid();
                        _AtAssetHistory.OrganizationId = BarcodeDetails.OrganizationId;
                        _AtAssetHistory.AssetBarcode = BarcodeDetails.AssetBarcode;
                        _AtAssetHistory.Status = "Disposed";
                        _AtAssetHistory.Activity = Activty.ToString();
                        _AtAssetHistory.ActivityDate = HCoreHelper.GetDateTime();
                        _AtAssetHistory.UserId = _Request.UserReference.AccountId;
                        await _HCoreContextLogging.AtAssetHistories.AddAsync(_AtAssetHistory);
                        await _HCoreContextLogging.SaveChangesAsync();
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATDISPOSE", "Asset disposed successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DisposeAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> ValidateBarcode(OReference _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var BarcodeDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                         .Select(x => new
                                         {
                                             AssetId = x.Id,
                                             AssetKey = x.Guid,
                                             AssetName = x.AssetName,
                                             AssetBarcode = x.AssetBarcode,
                                             AssetSerialNo = x.AssetSerialNumber,
                                             AssetType = x.AssetType.SystemName,
                                             AssetTypeName = x.AssetType.Name,
                                             AssetClassName = x.AssetClass.ClassName,
                                             AssetCategoryName = x.AssetCategory.CategoryName,
                                             AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,
                                             ExAssetId = x.ExAssetId,
                                             AssetMake = x.AssetMake,
                                             AssetModel = x.AssetModel,
                                             TotalQuantity = x.TotalQuantity,
                                             StatusId = x.StatusId,
                                             StatusCode = x.Status.SystemName,
                                             StatusName = x.Status.Name,
                                             CompanyId = x.OrganizationId,
                                             CompanyCode = x.Organization.AccountCode,
                                             CompanyName = x.Organization.DisplayName,
                                             RemainingQuantity = x.RemainingQuantity,
                                         }).FirstOrDefaultAsync();
                    if (BarcodeDetails != null)
                    {
                        OAssetOperations.BarcodeDetails _BarcodeDetails = new OAssetOperations.BarcodeDetails();
                        _BarcodeDetails.AssetId = BarcodeDetails.AssetId;
                        _BarcodeDetails.AssetKey = BarcodeDetails.AssetKey;
                        _BarcodeDetails.AssetName = BarcodeDetails.AssetName;
                        _BarcodeDetails.AssetBarcode = BarcodeDetails.AssetBarcode;
                        _BarcodeDetails.AssetSerialNo = BarcodeDetails.AssetSerialNo;
                        _BarcodeDetails.AssetTypeCode = BarcodeDetails.AssetType;
                        _BarcodeDetails.AssetTypeName = BarcodeDetails.AssetTypeName;
                        _BarcodeDetails.AssetClassName = BarcodeDetails.AssetClassName;
                        _BarcodeDetails.AssetCategoryName = BarcodeDetails.AssetCategoryName;
                        _BarcodeDetails.AssetSubCategoryName = BarcodeDetails.AssetSubCategoryName;
                        _BarcodeDetails.ExAssetId = BarcodeDetails.ExAssetId;
                        _BarcodeDetails.AssetMake = BarcodeDetails.AssetMake;
                        _BarcodeDetails.AssetModel = BarcodeDetails.AssetModel;
                        _BarcodeDetails.StatusId = BarcodeDetails.StatusId;
                        _BarcodeDetails.StatusCode = BarcodeDetails.StatusCode;
                        _BarcodeDetails.StatusName = BarcodeDetails.StatusName;
                        _BarcodeDetails.CompanyId = BarcodeDetails.CompanyId;
                        _BarcodeDetails.CompanyCode = BarcodeDetails.CompanyCode;
                        _BarcodeDetails.CompanyName = BarcodeDetails.CompanyName;
                        _BarcodeDetails.ReprintCount = 0;
                        _BarcodeDetails.RemainingQuantity = BarcodeDetails.RemainingQuantity;

                        //_BarcodeDetails.DepreciationMethodId = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode).Select(x => x.DepreciationMethodId).FirstOrDefaultAsync();
                        //_BarcodeDetails.DepreciationMethodCode = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode).Select(x => x.DepreciationMethod.SystemName).FirstOrDefaultAsync();
                        //_BarcodeDetails.DepreciationMethodName = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode).Select(x => x.DepreciationMethod.Name).FirstOrDefaultAsync();
                        //_BarcodeDetails.DepreciationRate = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode).Select(x => x.DepreciationRate).FirstOrDefaultAsync();
                        //if(_BarcodeDetails.DepreciationMethodId == HCoreConstant.Helpers.Depreciation_Methods.Straight_line)
                        //{
                        //    _BarcodeDetails.YearlyDepreciationCost = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode && x.DepreciationTypeId == HCoreConstant.Helpers.DepreciationTypes.Yearly).Select(x => x.DepreciationExpense).FirstOrDefaultAsync();
                        //    _BarcodeDetails.MonthlyDepreciationCost = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode && x.DepreciationTypeId == HCoreConstant.Helpers.DepreciationTypes.Monthly).Select(x => x.DepreciationExpense).FirstOrDefaultAsync();
                        //    _BarcodeDetails.DailyDepreciationCost = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetBarcode == _BarcodeDetails.AssetBarcode && x.DepreciationTypeId == HCoreConstant.Helpers.DepreciationTypes.Daily).Select(x => x.DepreciationExpense).FirstOrDefaultAsync();
                        //}

                        using (_HCoreContextLogging = new HCoreContextLogging())
                        {
                            _BarcodeDetails.IsPrinted = await _HCoreContextLogging.AtAssetPrintLogs.AnyAsync(x => x.AssetId == _BarcodeDetails.AssetId && x.OrganizationId == _Request.UserReference.OrganizationId);
                            _BarcodeDetails.ReprintCount = await _HCoreContextLogging.AtAssetPrintLogs.Where(x => x.AssetId == _BarcodeDetails.AssetId && x.OrganizationId == _Request.UserReference.OrganizationId).CountAsync();
                        }

                        var Details = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == BarcodeDetails.AssetId && x.StatusId == AssetStatus.Issued)
                                      .Select(x => new
                                      {
                                          DepartmentId = x.DepartmentId,
                                          DepartmentKey = x.Department.Guid,
                                          DepartmentName = x.Department.DepartmentName,

                                          DivisionId = x.DivisionId,
                                          DivisionKey = x.Division.Guid,
                                          DivisionName = x.Division.DivisionName,

                                          LocationId = x.LocationId,
                                          LocationKey = x.Location.Guid,
                                          LocationName = x.Location.LocationName,

                                          SubLocationId = x.SubLocationId,
                                          SubLocationKey = x.SubLocation.Guid,
                                          SubLocationName = x.SubLocation.LocationName,

                                          EmployeeId = x.EmployeeId,
                                          EmployeeKey = x.Employee.Guid,
                                          EmployeeName = x.Employee.DisplayName,

                                          CreateDate = x.CreatedDate,
                                      }).FirstOrDefaultAsync();

                        if (Details != null)
                        {
                            _BarcodeDetails.IsIssued = true;
                            _BarcodeDetails.DivisionId = Details.DivisionId;
                            _BarcodeDetails.DivisionKey = Details.DivisionKey;
                            _BarcodeDetails.DivisionName = Details.DivisionName;
                            _BarcodeDetails.DepartmentId = Details.DepartmentId;
                            _BarcodeDetails.DepartmentKey = Details.DepartmentKey;
                            _BarcodeDetails.DepartmentName = Details.DepartmentName;
                            _BarcodeDetails.LocationId = Details.LocationId;
                            _BarcodeDetails.LocationKey = Details.LocationKey;
                            _BarcodeDetails.LocationName = Details.LocationName;
                            _BarcodeDetails.SubLocationId = Details.SubLocationId;
                            _BarcodeDetails.SubLocationKey = Details.SubLocationKey;
                            _BarcodeDetails.SubLocationName = Details.SubLocationName;
                            _BarcodeDetails.EmployeeId = Details.EmployeeId;
                            _BarcodeDetails.EmployeeKey = Details.EmployeeKey;
                            _BarcodeDetails.EmployeeName = Details.EmployeeName;
                            _BarcodeDetails.IssuedDate = Details.CreateDate;
                        }
                        else
                        {
                            _BarcodeDetails.IsIssued = false;
                        }

                        var _PurchaseDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == BarcodeDetails.AssetId)
                                               .Select(x => new
                                               {
                                                   VendorName = x.Vendor.DisplayName,
                                                   PoNumber = x.PoNumber,
                                                   InvoiceNumber = x.InvoiceNumber,
                                                   RecievedDate = x.RecievedDate,
                                                   WarrentyStartDate = x.WarrantyStartDate,
                                                   WarrentyEndDate = x.WarrantyEndDate,
                                                   ExpiresIn = x.ExpiresIn,
                                                   ExpiryDate = x.ExpiryDate,
                                                   PurchasePrice = x.PurchasePrice,
                                                   AdditionalPrice = x.AdditionalPrice,
                                                   IsAmcApplicable = x.IsAmcApplicable,
                                                   IsInsurances = x.IsInsurance,
                                               }).FirstOrDefaultAsync();

                        if (_PurchaseDetails != null)
                        {
                            _BarcodeDetails.VendorName = _PurchaseDetails.VendorName;
                            _BarcodeDetails.PoNumber = _PurchaseDetails.PoNumber;
                            _BarcodeDetails.InvoiceNumber = _PurchaseDetails.InvoiceNumber;
                            _BarcodeDetails.RecievedDate = _PurchaseDetails.RecievedDate;
                            _BarcodeDetails.WarrentyStartDate = _PurchaseDetails.WarrentyStartDate;
                            _BarcodeDetails.WarrentyEndDate = _PurchaseDetails.WarrentyEndDate;
                            _BarcodeDetails.ExpiresIn = _PurchaseDetails.ExpiresIn;
                            _BarcodeDetails.ExpiryDate = _PurchaseDetails.ExpiryDate;
                            _BarcodeDetails.PurchasePrice = _PurchaseDetails.PurchasePrice;
                            _BarcodeDetails.AdditionalPrice = _PurchaseDetails.AdditionalPrice;
                            if (_PurchaseDetails.IsAmcApplicable == 1)
                            {
                                _BarcodeDetails.IsAmc = true;
                            }
                            else
                            {
                                _BarcodeDetails.IsAmc = false;
                            }
                            if (_PurchaseDetails.IsInsurances == 1)
                            {
                                _BarcodeDetails.IsInsurance = true;
                            }
                            else
                            {
                                _BarcodeDetails.IsInsurance = false;
                            }
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BarcodeDetails, "ATSUCCESS", "Barcode validated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATINV", "Invalid barcode");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ValidateBarcode", _Exception, _Request.UserReference);
            }
        }

        internal async Task UploadAssets(OAssetOperations.BulkUpload.Request _Request)
        {
            try
            {
                if (_Request.AssetDetails.Count > 0)
                {
                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {
                        var _Assets = await _HCoreContextLogging.AtAssetUploads.Where(x => x.StatusId == AssetStatus.Processed).ToListAsync();
                        if (_Assets.Count > 0)
                        {
                            _HCoreContextLogging.AtAssetUploads.RemoveRange(_Assets);
                            await _HCoreContextLogging.SaveChangesAsync();
                        }
                        foreach (var Asset in _Request.AssetDetails)
                        {
                            _AtAssetUpload = new AtAssetUpload();
                            _AtAssetUpload.Guid = HCoreHelper.GenerateGuid();
                            _AtAssetUpload.OrganizationId = _Request.UserReference.OrganizationId;
                            _AtAssetUpload.AssetType = Asset.AssetType;
                            _AtAssetUpload.TotalQuantity = Asset.TotalQuantity;
                            _AtAssetUpload.AssetBarcode = Asset.Barcode;
                            _AtAssetUpload.AssetClass = Asset.AssetClass;
                            _AtAssetUpload.AssetCategory = Asset.AssetCategory;
                            _AtAssetUpload.AssetSubCategory = Asset.AssetSubCategory;
                            _AtAssetUpload.AssetName = Asset.AssetName;
                            _AtAssetUpload.AssetSerialNo = Asset.AssetSerialNo;
                            _AtAssetUpload.ExAssetId = Asset.ExAssetId;
                            _AtAssetUpload.AssetMake = Asset.AssetMake;
                            _AtAssetUpload.AssetModel = Asset.AssetModel;
                            _AtAssetUpload.Division = Asset.Division;
                            _AtAssetUpload.Location = Asset.Location;
                            _AtAssetUpload.SubLocation = Asset.SubLocation;
                            _AtAssetUpload.AssetVendor = Asset.VendorKey;
                            _AtAssetUpload.PoNumber = Asset.PoNumber;
                            _AtAssetUpload.InvoiceNumber = Asset.InvoiceNumber;
                            _AtAssetUpload.ReceivedDate = Asset.RecievedDate;
                            _AtAssetUpload.StartDate = Asset.StartDate;
                            _AtAssetUpload.EndDate = Asset.EndDate;
                            _AtAssetUpload.ExpiresIn = Asset.ExpireInYears;

                            if (Asset.RecievedDate != null)
                            {
                                _AtAssetUpload.ExpiryDate = Asset.RecievedDate.Value.AddYears((int)Asset.ExpireInYears);
                            }
                            else if (Asset.RecievedDate == null && Asset.StartDate != null)
                            {
                                _AtAssetUpload.ExpiryDate = Asset.StartDate.Value.AddYears((int)Asset.ExpireInYears);
                            }
                            else
                            {
                                _AtAssetUpload.ExpiryDate = HCoreHelper.GetDateTime().AddYears((int)Asset.ExpireInYears);
                            }

                            _AtAssetUpload.PurchasePrice = Asset.PurchasePrice;
                            _AtAssetUpload.AdditionalPrice = Asset.AdditionalPrice;
                            _AtAssetUpload.SalvageValue = Asset.SalvageValue;

                            if (Asset.IsAmc == true)
                            {
                                _AtAssetUpload.IsAmc = 1;
                            }
                            else
                            {
                                _AtAssetUpload.IsAmc = 0;
                            }

                            if (Asset.IsInsurance == true)
                            {
                                _AtAssetUpload.IsInsurance = 1;
                            }
                            else
                            {
                                _AtAssetUpload.IsInsurance = 0;
                            }

                            _AtAssetUpload.StatusId = AssetStatus.InProcess;
                            _AtAssetUpload.CreatedById = _Request.UserReference.AccountId;
                            _AtAssetUpload.CreatedDate = HCoreHelper.GetDateTime();
                            await _HCoreContextLogging.AtAssetUploads.AddAsync(_AtAssetUpload);
                        }

                        await _HCoreContextLogging.SaveChangesAsync();
                        await _HCoreContextLogging.DisposeAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UploadAssets", _Exception, _Request.UserReference);
            }
        }
    }
}
