﻿using Akka.Actor;
using DocumentFormat.OpenXml.Wordprocessing;
using HCore.Data.Logging;
using HCore.Data.Logging.Models;
using HCore.Data.Models;
using HCore.Helper;
using HCore.RKBS.Core.Object.Assets;
using HCore.RKBS.Core.Operations.Assets;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Framework.Assets
{
    public class FrameworkAsset
    {
        HCoreContext? _HCoreContext;
        HCoreContextLogging? _HCoreContextLogging;
        ManageDownload? _ManageDownload;
        OAsset.AssetDepreciation.Response? _AssetDepreciation;

        internal async Task<OResponse> GetAsset(OReference _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _Request.AssetBarcode || (x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey))
                                       .Select(x => new OAsset.Details
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,
                                           AssetMake = x.AssetMake,
                                           AssetModel = x.AssetModel,
                                           ExAssetId = x.ExAssetId,
                                           Quantity = x.TotalQuantity,
                                           SalvageValue = x.SalvageValue,

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (AssetDetails != null)
                    {
                        OAsset.IssueDetails? _IssueDetails = new OAsset.IssueDetails();
                        OAsset.Vendor? _VendorDetails = new OAsset.Vendor();
                        OAsset.LocationDetails? _LocationDetails = new OAsset.LocationDetails();
                        List<OAsset.Amc>? _AmcDetails = new List<OAsset.Amc>();
                        List<OAsset.Insurance>? _InsuranceDetails = new List<OAsset.Insurance>();

                        _IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.AssetId == AssetDetails.AssetId && x.StatusId == AssetStatus.Issued)
                                        .Select(x => new OAsset.IssueDetails
                                        {
                                            DivisionId = x.DivisionId,
                                            DivisionKey = x.Division.Guid,
                                            DivisionName = x.Division.DivisionName,

                                            LocationId = x.LocationId,
                                            LocationKey = x.Location.Guid,
                                            LocationName = x.Location.LocationName,

                                            SubLocationId = x.SubLocationId,
                                            SubLocationKey = x.SubLocation.Guid,
                                            SubLocationName = x.SubLocation.LocationName,

                                            DepartmentId = x.DepartmentId,
                                            DepartmentKey = x.Department.Guid,
                                            DepartmentName = x.Department.DepartmentName,

                                            EmployeeId = x.EmployeeId,
                                            EmployeeKey = x.Employee.Guid,
                                            EmployeeName = x.Employee.DisplayName,
                                            EmployeeCode = x.Employee.AccountCode,

                                            Remark = x.Remark,
                                            Quantity = x.Quantity,
                                            OsName = x.OsName,
                                            HostName = x.HostName,
                                            IpAddress = x.IpAddress,

                                            IssuedOn = x.CreatedDate,
                                            IssuedByName = x.CreatedBy.DisplayName,
                                        }).FirstOrDefaultAsync();

                        AssetDetails.IssueDetails = _IssueDetails;

                        _VendorDetails = await _HCoreContext.AtAssetVendorDetails.Where(x => x.AssetId == AssetDetails.AssetId)
                                         .Select(x => new OAsset.Vendor
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             VendorId = x.VendorId,
                                             VendorKey = x.Vendor.Guid,
                                             VendorName = x.Vendor.DisplayName,
                                             PoNumber = x.PoNumber,
                                             InvoiceNumber = x.InvoiceNumber,
                                             RecievedDate = x.RecievedDate,
                                             StartDate = x.WarrantyStartDate,
                                             EndDate = x.WarrantyEndDate,
                                             ExpireInYears = x.ExpiresIn,
                                             ExpiryDate = x.ExpiryDate,
                                             Currency = x.Currency,
                                             Price = x.PurchasePrice,
                                             AdditionalCharges = x.AdditionalPrice,
                                             IsAmcApplicable = x.IsAmcApplicable,
                                             IsInsurances = x.IsInsurance,
                                         }).FirstOrDefaultAsync();

                        if (_VendorDetails != null)
                        {
                            if (_VendorDetails.IsAmcApplicable == 1)
                            {
                                _VendorDetails.IsAmc = true;
                                _AmcDetails = await _HCoreContext.AtAssetAmcDetails.Where(x => x.VendorDetailsId == _VendorDetails.ReferenceId)
                                              .Select(x => new OAsset.Amc
                                              {
                                                  VendorId = x.VendorId,
                                                  VendorKey = x.Vendor.Guid,
                                                  VendorName = x.Vendor.DisplayName,
                                                  StartDate = x.StartDate,
                                                  EndDate = x.EndDate,
                                                  Amount = x.Price,
                                              }).ToListAsync();

                                _VendorDetails.AmcDetails = _AmcDetails;
                            }
                            else
                            {
                                _VendorDetails.IsAmc = false;
                            }

                            if (_VendorDetails.IsInsurances == 1)
                            {
                                _VendorDetails.IsInsurance = true;
                                _InsuranceDetails = await _HCoreContext.AtAssetInsuranceDetails.Where(x => x.VendorDetailsId == _VendorDetails.ReferenceId)
                                                    .Select(x => new OAsset.Insurance
                                                    {
                                                        VendorId = x.VendorId,
                                                        VendorKey = x.Vendor.Guid,
                                                        VendorName = x.Vendor.DisplayName,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        Amount = x.Price,
                                                    }).ToListAsync();

                                _VendorDetails.InsuranceDetails = _InsuranceDetails;
                            }
                            else
                            {
                                _VendorDetails.IsInsurance = false;
                            }
                        }

                        AssetDetails.VendorDetails = _VendorDetails;

                        _LocationDetails = await _HCoreContext.AtAssetLocations.Where(x => x.AssetId == AssetDetails.AssetId)
                                           .Select(x => new OAsset.LocationDetails
                                           {
                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.LocationId,
                                               LocationKey = x.Location.Guid,
                                               LocationName = x.Location.LocationName,

                                               SubLocationId = x.SubLocationId,
                                               SubLocationKey = x.SubLocation.Guid,
                                               SubLocationName = x.SubLocation.LocationName,
                                           }).FirstOrDefaultAsync();

                        AssetDetails.LocationDetails = _LocationDetails;

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, AssetDetails, "AT0200", "Asset details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT0404", "Asset details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                           .Select(x => new OAsset.List
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,
                                               ExAssetId = x.ExAssetId,

                                               RecievedDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.RecievedDate).FirstOrDefault(),

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               AssetClassId = x.AssetClass.Id,
                                               AssetClassKey = x.AssetClass.Guid,
                                               AssetClassName = x.AssetClass.ClassName,

                                               AssetCategoryId = x.AssetCategory.Id,
                                               AssetCategoryKey = x.AssetCategory.Guid,
                                               AssetCategoryName = x.AssetCategory.CategoryName,

                                               AssetSubCategoryId = x.AssetSubCategory.Id,
                                               AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                               AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               CompanyId = x.OrganizationId,
                                               CompanyCode = x.Organization.AccountCode,
                                               CompanyName = x.Organization.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.List> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                       .Select(x => new OAsset.List
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,
                                           ExAssetId = x.ExAssetId,

                                           RecievedDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.RecievedDate).FirstOrDefault(),

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           CompanyId = x.OrganizationId,
                                           CompanyCode = x.Organization.AccountCode,
                                           CompanyName = x.Organization.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDisposedAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetDisposedAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Disposed)
                                           .Select(x => new OAsset.List
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.List> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Disposed)
                                       .Select(x => new OAsset.List
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDisposedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetExpiredAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetExpiredAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Expired)
                                           .Select(x => new OAsset.List
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.List> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId == AssetStatus.Expired)
                                       .Select(x => new OAsset.List
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetExpiredAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetBarcode(OReference _Request)
        {
            try
            {
                OAsset.Barcode _Barcode = new OAsset.Barcode();
                _Barcode.SerialNumber = await HCoreHelper.GenerateSerialNumber(_Request.UserReference.OrganizationId);
                _Barcode.AssetBarcode = HCoreHelper.GenerateBarcode(_Barcode.SerialNumber);

                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Barcode, "AT0200", "Barcode and serial number loaded");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetBarcode", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetBulkAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetBulkAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetUploads.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OAsset.UploadAssets
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNo,
                                               AssetType = x.AssetType,
                                               AssetClass = x.AssetClass,
                                               AssetCategory = x.AssetCategory,
                                               AssetSubCategory = x.AssetSubCategory,
                                               CreateDate = x.CreatedDate,
                                               StatusId = x.StatusId,
                                               Comment = x.Comment,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.UploadAssets> _Assets = await _HCoreContextLogging.AtAssetUploads.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OAsset.UploadAssets
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNo,
                                           AssetType = x.AssetType,
                                           AssetClass = x.AssetClass,
                                           AssetCategory = x.AssetCategory,
                                           AssetSubCategory = x.AssetSubCategory,
                                           StatusId = x.StatusId,
                                           CreateDate = x.CreatedDate,
                                           Comment = x.Comment,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    if (_Assets.Count > 0)
                    {
                        foreach (var Asset in _Assets)
                        {
                            if (Asset.StatusId == AssetStatus.InProcess)
                            {
                                Asset.StatusCode = "asset.inprocess";
                                Asset.StatusName = "InProcess";
                            }
                            else if (Asset.StatusId == AssetStatus.Processed)
                            {
                                Asset.StatusCode = "asset.processed";
                                Asset.StatusName = "Processed";
                            }
                            else
                            {
                                Asset.StatusCode = "asset.failed";
                                Asset.StatusName = "Failed";
                            }
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBulkAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetIssuedAsset(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Asset id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Asset key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepartmentDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OAsset.IssueAsset.Details
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           Remark = x.Remark,
                                           Quantity = x.Quantity,

                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,

                                           OrganizationId = x.OrganizationId,
                                           OrganizationKey = x.Organization.Guid,
                                           OrganizationName = x.Organization.DisplayName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.LocationId,
                                           LocationKey = x.Location.Guid,
                                           LocationName = x.Location.LocationName,

                                           SubLocationId = x.SubLocationId,
                                           SubLocationKey = x.SubLocation.Guid,
                                           SubLocationName = x.SubLocation.LocationName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,

                                           OsName = x.OsName,
                                           HostName = x.HostName,
                                           IpAddress = x.IpAddress,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (DepartmentDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, DepartmentDetails, "AT0200", "Asset details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Asset details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetIssuedAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetIssuedAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetIssuedAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                           .Select(x => new OAsset.IssueAsset.List
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,
                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNo = x.Asset.AssetSerialNumber,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               LocationId = x.LocationId,
                                               LocationKey = x.Location.Guid,
                                               LocationName = x.Location.LocationName,

                                               SubLocationId = x.SubLocationId,
                                               SubLocationKey = x.SubLocation.Guid,
                                               SubLocationName = x.SubLocation.LocationName,

                                               EmployeeId = x.EmployeeId,
                                               EmployeeKey = x.Employee.Guid,
                                               EmployeeName = x.Employee.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ApprovedDate = x.ApprovedDate,
                                               ApprovedById = x.ApprovedById,
                                               ApprovedByKey = x.ApprovedBy.Guid,
                                               ApprovedByName = x.ApprovedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,

                                               Comment = x.Comment,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.IssueAsset.List> _Categoryes = await _HCoreContext.AtAssetIssueDetails.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                       .Select(x => new OAsset.IssueAsset.List
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           LocationId = x.LocationId,
                                           LocationKey = x.Location.Guid,
                                           LocationName = x.Location.LocationName,

                                           SubLocationId = x.SubLocationId,
                                           SubLocationKey = x.SubLocation.Guid,
                                           SubLocationName = x.SubLocation.LocationName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ApprovedDate = x.ApprovedDate,
                                           ApprovedById = x.ApprovedById,
                                           ApprovedByKey = x.ApprovedBy.Guid,
                                           ApprovedByName = x.ApprovedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,

                                           Comment = x.Comment,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetIssuedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetUnIssuedAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetUnIssuedAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                List<OAsset.UnIssuedAssets> _UnIssuedAssets = new List<OAsset.UnIssuedAssets>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.RemainingQuantity > 0 && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OAsset.UnIssuedAssets
                                           {
                                               AssetId = x.Id,
                                               AssetKey = x.Guid,
                                               AssetName = x.AssetName,
                                               AssetBarcode = x.AssetBarcode,
                                               AssetSerialNo = x.AssetSerialNumber,

                                               DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                               DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                               DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                               LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                               LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                               LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                               SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                               SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                               SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                               AssetTypeId = x.AssetTypeId,
                                               AssetTypeCode = x.AssetType.SystemName,
                                               AssetTypeName = x.AssetType.Name,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.UnIssuedAssets> _Assets = await _HCoreContext.AtAssetRegisters.Where(x => x.RemainingQuantity > 0 && x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OAsset.UnIssuedAssets
                                       {
                                           AssetId = x.Id,
                                           AssetKey = x.Guid,
                                           AssetName = x.AssetName,
                                           AssetBarcode = x.AssetBarcode,
                                           AssetSerialNo = x.AssetSerialNumber,

                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetUnIssuedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetTransferedAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetTransferedAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetTransferDetails.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId)
                                           .Select(x => new OAsset.TransferAsset.List
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,
                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNo = x.Asset.AssetSerialNumber,
                                               Remark = x.Remark,
                                               Quantity = x.Quantity,

                                               Comment = x.Comment,

                                               TransferedFromId = x.AssetIssue.EmployeeId,
                                               TransferedFromKey = x.AssetIssue.Employee.Guid,
                                               TransferedFromName = x.AssetIssue.Employee.DisplayName,

                                               TransferedById = x.AssetIssue.TransferedBy,
                                               TransferedByKey = x.AssetIssue.TransferedByNavigation.Guid,
                                               TransferedByName = x.AssetIssue.TransferedByNavigation.DisplayName,

                                               DepartmentId = x.DepartmentId,
                                               DepartmentKey = x.Department.Guid,
                                               DepartmentName = x.Department.DepartmentName,

                                               DivisionId = x.DivisionId,
                                               DivisionKey = x.Division.Guid,
                                               DivisionName = x.Division.DivisionName,

                                               EmployeeId = x.EmployeeId,
                                               EmployeeKey = x.Employee.Guid,
                                               EmployeeName = x.Employee.DisplayName,

                                               OsName = x.OsName,
                                               HostName = x.HostName,
                                               IpAddress = x.IpAddress,

                                               ApprovedDate = x.ApprovedDate,
                                               ApprovedById = x.ApprovedById,
                                               ApprovedByKey = x.ApprovedBy.Guid,
                                               ApprovedByName = x.ApprovedBy.DisplayName,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.TransferAsset.List> _Categoryes = await _HCoreContext.AtAssetTransferDetails.Where(x => x.StatusId != AssetStatus.Disposed && x.OrganizationId == _Request.UserReference.OrganizationId)
                                       .Select(x => new OAsset.TransferAsset.List
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,
                                           Remark = x.Remark,
                                           Quantity = x.Quantity,

                                           Comment = x.Comment,

                                           TransferedFromId = x.AssetIssue.EmployeeId,
                                           TransferedFromKey = x.AssetIssue.Employee.Guid,
                                           TransferedFromName = x.AssetIssue.Employee.DisplayName,

                                           TransferedById = x.AssetIssue.TransferedBy,
                                           TransferedByKey = x.AssetIssue.TransferedByNavigation.Guid,
                                           TransferedByName = x.AssetIssue.TransferedByNavigation.DisplayName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,

                                           OsName = x.OsName,
                                           HostName = x.HostName,
                                           IpAddress = x.IpAddress,

                                           ApprovedDate = x.ApprovedDate,
                                           ApprovedById = x.ApprovedById,
                                           ApprovedByKey = x.ApprovedBy.Guid,
                                           ApprovedByName = x.ApprovedBy.DisplayName,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTransferedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetReturnedAsset(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Asset id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL001", "Asset key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DepartmentDetails = await _HCoreContext.AtAssetReturnDetails.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new OAsset.RetrurnAsset.Details
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,

                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,

                                           ReturnById = x.ReturnBy,
                                           ReturnByKey = x.ReturnByNavigation.Guid,
                                           ReturnByName = x.ReturnByNavigation.DisplayName,

                                           ReturnToId = x.ReturnTo,
                                           ReturnToKey = x.ReturnToNavigation.Guid,
                                           ReturnToName = x.ReturnToNavigation.DisplayName,

                                           ReturnCondition = x.ReturnCondition,
                                           ReturnNotes = x.ReturnNotes,
                                           ReturnQuantity = x.ReturnQuantity,
                                           ReturnReference = x.ReturnReference,
                                           ReturnValue = x.ReturnValue,

                                           ApprovedById = x.ReturnApprovedBy,
                                           ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                           ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                           ApprovedDate = x.ReturnApprovedDate,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();
                    if (DepartmentDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, DepartmentDetails, "AT0200", "Asset details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "ATCL0404", "Asset details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetReturnedAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetReturnedAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetReturnedAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetReturnDetails.Where(x => x.Asset.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                           .Select(x => new OAsset.RetrurnAsset.List
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,

                                               AssetId = x.AssetId,
                                               AssetKey = x.Asset.Guid,
                                               AssetName = x.Asset.AssetName,
                                               AssetBarcode = x.Asset.AssetBarcode,
                                               AssetSerialNo = x.Asset.AssetSerialNumber,

                                               ReturnById = x.ReturnBy,
                                               ReturnByKey = x.ReturnByNavigation.Guid,
                                               ReturnByName = x.ReturnByNavigation.DisplayName,

                                               ReturnToId = x.ReturnTo,
                                               ReturnToKey = x.ReturnToNavigation.Guid,
                                               ReturnToName = x.ReturnToNavigation.DisplayName,

                                               ReturnCondition = x.ReturnCondition,
                                               ReturnNotes = x.ReturnNotes,
                                               ReturnQuantity = x.ReturnQuantity,
                                               ReturnReference = x.ReturnReference,
                                               ReturnValue = x.ReturnValue,

                                               ApprovedById = x.ReturnApprovedBy,
                                               ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                               ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                               ApprovedDate = x.ReturnApprovedDate,

                                               CreateDate = x.CreatedDate,
                                               CreatedById = x.CreatedById,
                                               CreatedByKey = x.CreatedBy.Guid,
                                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                                               ModifyDate = x.ModifiedDate,
                                               ModifyById = x.ModifiedById,
                                               ModifyByKey = x.ModifiedBy.Guid,
                                               ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,

                                               Comment = x.Comment,
                                           }).Where(_Request.SearchCondition)
                                           .CountAsync();
                    }

                    List<OAsset.RetrurnAsset.List> _Categoryes = await _HCoreContext.AtAssetReturnDetails.Where(x => x.Asset.OrganizationId == _Request.UserReference.OrganizationId && x.StatusId != AssetStatus.Disposed)
                                       .Select(x => new OAsset.RetrurnAsset.List
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,

                                           AssetId = x.AssetId,
                                           AssetKey = x.Asset.Guid,
                                           AssetName = x.Asset.AssetName,
                                           AssetBarcode = x.Asset.AssetBarcode,
                                           AssetSerialNo = x.Asset.AssetSerialNumber,

                                           ReturnById = x.ReturnBy,
                                           ReturnByKey = x.ReturnByNavigation.Guid,
                                           ReturnByName = x.ReturnByNavigation.DisplayName,

                                           ReturnToId = x.ReturnTo,
                                           ReturnToKey = x.ReturnToNavigation.Guid,
                                           ReturnToName = x.ReturnToNavigation.DisplayName,

                                           ReturnCondition = x.ReturnCondition,
                                           ReturnNotes = x.ReturnNotes,
                                           ReturnQuantity = x.ReturnQuantity,
                                           ReturnReference = x.ReturnReference,
                                           ReturnValue = x.ReturnValue,

                                           ApprovedById = x.ReturnApprovedBy,
                                           ApprovedByKey = x.ReturnApprovedByNavigation.Guid,
                                           ApprovedByName = x.ReturnApprovedByNavigation.DisplayName,
                                           ApprovedDate = x.ReturnApprovedDate,

                                           CreateDate = x.CreatedDate,
                                           CreatedById = x.CreatedById,
                                           CreatedByKey = x.CreatedBy.Guid,
                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                           ModifyDate = x.ModifiedDate,
                                           ModifyById = x.ModifiedById,
                                           ModifyByKey = x.ModifiedBy.Guid,
                                           ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,

                                           Comment = x.Comment,
                                       }).OrderBy(_Request.SortExpression)
                                       .Where(_Request.SearchCondition)
                                       .Skip(_Request.Offset)
                                       .Take(_Request.Limit)
                                       .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Categoryes, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "Asset list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetReturnedAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetMaintenanceAssets(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetMaintenanceAssets(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetMaintenances.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OAsset.Maintenance.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AssetId = x.AssetId,
                                                    AssetKey = x.Asset.Guid,
                                                    AssetName = x.Asset.AssetName,
                                                    AssetBarcode = x.Asset.AssetBarcode,
                                                    AssetSerialNo = x.Asset.AssetSerialNumber,

                                                    Description = x.Description,
                                                    Remark = x.Remark,
                                                    RequestDate = x.RequestDate,
                                                    ExpectedDate = x.ExpectedDate,
                                                    ReceivedDate = x.ReceivedDate,
                                                    Amount = (double?)x.Amount,

                                                    MaintenanceTypeId = x.MaintenanceType,
                                                    MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                                    MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                                    AssetVendorId = x.VendorId,
                                                    AssetVendorKey = x.Vendor.Guid,
                                                    AssetVendorName = x.Vendor.DisplayName,
                                                    MobileNumber = x.Vendor.MobileNumber,

                                                    CreateDate = x.CreatedDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifiedDate,
                                                    ModifyById = x.ModifiedById,
                                                    ModifyByKey = x.ModifiedBy.Guid,
                                                    ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    Comment = x.Comment,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAsset.Maintenance.List> _Assets = await _HCoreContext.AtAssetMaintenances.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                                      .Select(x => new OAsset.Maintenance.List
                                                                      {
                                                                          ReferenceId = x.Id,
                                                                          ReferenceKey = x.Guid,

                                                                          AssetId = x.AssetId,
                                                                          AssetKey = x.Asset.Guid,
                                                                          AssetName = x.Asset.AssetName,
                                                                          AssetBarcode = x.Asset.AssetBarcode,
                                                                          AssetSerialNo = x.Asset.AssetSerialNumber,

                                                                          Description = x.Description,
                                                                          Remark = x.Remark,
                                                                          RequestDate = x.RequestDate,
                                                                          ExpectedDate = x.ExpectedDate,
                                                                          ReceivedDate = x.ReceivedDate,
                                                                          Amount = (double?)x.Amount,

                                                                          MaintenanceTypeId = x.MaintenanceType,
                                                                          MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                                                          MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                                                          AssetVendorId = x.VendorId,
                                                                          AssetVendorKey = x.Vendor.Guid,
                                                                          AssetVendorName = x.Vendor.DisplayName,
                                                                          MobileNumber = x.Vendor.MobileNumber,

                                                                          CreateDate = x.CreatedDate,
                                                                          CreatedById = x.CreatedById,
                                                                          CreatedByKey = x.CreatedBy.Guid,
                                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                          ModifyDate = x.ModifiedDate,
                                                                          ModifyById = x.ModifiedById,
                                                                          ModifyByKey = x.ModifiedBy.Guid,
                                                                          ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                                                          StatusId = x.StatusId,
                                                                          StatusCode = x.Status.SystemName,
                                                                          StatusName = x.Status.Name,
                                                                      }).OrderBy(_Request.SortExpression)
                                                                      .Where(_Request.SearchCondition)
                                                                      .Skip(_Request.Offset)
                                                                      .Take(_Request.Limit)
                                                                      .ToListAsync();

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMaintenanceAssets", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetMaintenanceAsset(OReference _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREF", "Reference key required");
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATREF", "Reference id required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = await _HCoreContext.AtAssetMaintenances.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                  .Select(x => new OAsset.Maintenance.Details
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,

                                      AssetId = x.AssetId,
                                      AssetKey = x.Asset.Guid,
                                      AssetName = x.Asset.AssetName,
                                      AssetBarcode = x.Asset.AssetBarcode,
                                      AssetSerialNo = x.Asset.AssetSerialNumber,

                                      Description = x.Description,
                                      Remark = x.Remark,
                                      RequestDate = x.RequestDate,
                                      ExpectedDate = x.ExpectedDate,
                                      ReceivedDate = x.ReceivedDate,
                                      Amount = (double?)x.Amount,

                                      ReceiptNo = x.ReceiptNo,
                                      ReceiptDate = x.ReceiptDate,

                                      MaintenanceTypeId = x.MaintenanceType,
                                      MaintenanceType = x.MaintenanceTypeNavigation.SystemName,
                                      MaintenanceTypeName = x.MaintenanceTypeNavigation.Name,

                                      AssetVendorId = x.VendorId,
                                      AssetVendorKey = x.Vendor.Guid,
                                      AssetVendorName = x.Vendor.DisplayName,
                                      EmailAddress = x.Vendor.EmailAddress,
                                      MobileNumber = x.Vendor.MobileNumber,
                                      Address = x.Vendor.Address,

                                      CreateDate = x.CreatedDate,
                                      CreatedById = x.CreatedById,
                                      CreatedByKey = x.CreatedBy.Guid,
                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                      ModifyDate = x.ModifiedDate,
                                      ModifyById = x.ModifiedById,
                                      ModifyByKey = x.ModifiedBy.Guid,
                                      ModifyByDisplayName = x.ModifiedBy.DisplayName,

                                      StatusId = x.StatusId,
                                      StatusCode = x.Status.SystemName,
                                      StatusName = x.Status.Name,
                                  }).FirstOrDefaultAsync();

                    if (Details != null)
                    {
                        OAsset.Maintenance.AssetDetails? AssetDetails = new OAsset.Maintenance.AssetDetails();
                        OAsset.Maintenance.IssueDetails? IssueDetails = new OAsset.Maintenance.IssueDetails();

                        bool _Issued = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.AssetId == Details.AssetId && x.StatusId == AssetStatus.Issued);
                        if (_Issued)
                        {
                            Details.IsIssued = true;
                        }
                        else
                        {
                            Details.IsIssued = false;
                        }

                        bool _IsExists = await _HCoreContext.AtAssetIssueDetails.AnyAsync(x => x.AssetId == Details.AssetId && (x.StatusId == AssetStatus.Returned || x.StatusId == AssetStatus.Transfered));
                        if (_IsExists)
                        {
                            Details.IsReturned = true;
                        }
                        else
                        {
                            Details.IsReturned = false;
                        }

                        AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.Id == Details.AssetId && x.Guid == Details.AssetKey)
                                       .Select(x => new OAsset.Maintenance.AssetDetails
                                       {
                                           AssetTypeId = x.AssetTypeId,
                                           AssetTypeCode = x.AssetType.SystemName,
                                           AssetTypeName = x.AssetType.Name,

                                           AssetCategoryId = x.AssetCategory.Id,
                                           AssetCategoryKey = x.AssetCategory.Guid,
                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                           AssetClassId = x.AssetClass.Id,
                                           AssetClassKey = x.AssetClass.Guid,
                                           AssetClassName = x.AssetClass.ClassName,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();

                        Details.AssetDetails = AssetDetails;

                        IssueDetails = await _HCoreContext.AtAssetIssueDetails.Where(x => (x.TransferReference == Details.ReferenceKey && x.StatusId == AssetStatus.Transfered) || (x.AssetId == Details.AssetId && (x.StatusId == AssetStatus.Issued || x.StatusId == AssetStatus.Returned)))
                                       .Select(x => new OAsset.Maintenance.IssueDetails
                                       {
                                           DivisionId = x.DivisionId,
                                           DivisionKey = x.Division.Guid,
                                           DivisionName = x.Division.DivisionName,

                                           DepartmentId = x.DepartmentId,
                                           DepartmentKey = x.Department.Guid,
                                           DepartmentName = x.Department.DepartmentName,

                                           LocationId = x.LocationId,
                                           LocationKey = x.Location.Guid,
                                           LocationName = x.Location.LocationName,

                                           SubLocationId = x.SubLocationId,
                                           SubLocationKey = x.SubLocation.Guid,
                                           SubLocationName = x.SubLocation.LocationName,

                                           EmployeeId = x.EmployeeId,
                                           EmployeeKey = x.Employee.Guid,
                                           EmployeeName = x.Employee.DisplayName,

                                           IssuedDate = x.CreatedDate,

                                           IsTransfered = x.IsTransfered,
                                           TransferedById = x.TransferedBy,
                                           TransferedByKey = x.TransferedByNavigation.Guid,
                                           TransferedByName = x.TransferedByNavigation.DisplayName,
                                           TransferReference = x.TransferReference,
                                           TransferedDate = x.TransferedDate,

                                           StatusId = x.StatusId,
                                           StatusCode = x.Status.SystemName,
                                           StatusName = x.Status.Name,
                                       }).FirstOrDefaultAsync();

                        Details.IssueDetails = IssueDetails;

                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Details, "AT0200", "Details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "AT040", "Details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMaintenanceAsset", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetHistory(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetAssetHistory(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ActivityDate", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetHistories.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OAsset.History
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Status = x.Status,
                                                    Activity = x.Activity,
                                                    UserId = x.UserId,
                                                    ActivityDate = x.ActivityDate,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAsset.History> _Assets = await _HCoreContextLogging.AtAssetHistories.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                   .Select(x => new OAsset.History
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       Status = x.Status,
                                                       Activity = x.Activity,
                                                       UserId = x.UserId,
                                                       ActivityDate = x.ActivityDate,
                                                   }).OrderBy(_Request.SortExpression)
                                                   .Where(_Request.SearchCondition)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToListAsync();

                    foreach (var _DataItem in _Assets)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            _DataItem.UserName = await _HCoreContext.AtAccounts.Where(x => x.Id == _DataItem.UserId).Select(x => x.DisplayName).FirstOrDefaultAsync();
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetHistory", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetAuditReport(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetAssetAuditReport(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }

                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContextLogging.AtAssetAuditLogs.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OAsset.AuditReport
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AssetBarcode = x.AssetBarcode,
                                                    Division = x.Division,
                                                    Location = x.Location,
                                                    SubLocation = x.SubLocation,
                                                    Department = x.Department,
                                                    Employee = x.Employee,
                                                    AssetStatus = x.AssetStatus,
                                                    AuditDivision = x.AuditDivision,
                                                    AuditLocation = x.AuditLocation,
                                                    AuditSubLocation = x.AuditSubLocation,
                                                    AuditStatus = x.AuditStatus,
                                                    CreateDate = x.AuditDate,
                                                    UserId = x.UserId,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAsset.AuditReport> _Assets = await _HCoreContextLogging.AtAssetAuditLogs.Where(x => x.OrganizationId == _Request.UserReference.OrganizationId)
                                                   .Select(x => new OAsset.AuditReport
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       AssetBarcode = x.AssetBarcode,
                                                       Division = x.Division,
                                                       Location = x.Location,
                                                       SubLocation = x.SubLocation,
                                                       Department = x.Department,
                                                       Employee = x.Employee,
                                                       AssetStatus = x.AssetStatus,
                                                       AuditDivision = x.AuditDivision,
                                                       AuditLocation = x.AuditLocation,
                                                       AuditSubLocation = x.AuditSubLocation,
                                                       AuditStatus = x.AuditStatus,
                                                       CreateDate = x.AuditDate,
                                                       UserId = x.UserId,
                                                   }).OrderBy(_Request.SortExpression)
                                                   .Where(_Request.SearchCondition)
                                                   .Skip(_Request.Offset)
                                                   .Take(_Request.Limit)
                                                   .ToListAsync();

                    if (_Assets.Count > 0)
                    {
                        foreach (var _Asset in _Assets)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _Asset.ExAssetId = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _Asset.AssetBarcode).Select(x => x.ExAssetId).FirstOrDefaultAsync();
                            }
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Assets, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetAuditReport", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetDepreciation(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetAssetDepreciation(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Response, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "AssetId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AssetId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetDepreciations.Any() && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                .Select(x => new OAsset.AssetDepreciation.List
                                                {
                                                    AssetId = x.Id,
                                                    AssetKey = x.Guid,
                                                    AssetName = x.AssetName,
                                                    AssetBarcode = x.AssetBarcode,
                                                    AssetSerialNo = x.AssetSerialNumber,
                                                    ExAssetId = x.ExAssetId,

                                                    AssetPrice = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.PurchasePrice).FirstOrDefault(),
                                                    ExpiresIn = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiresIn).FirstOrDefault(),
                                                    ReceivedDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.RecievedDate).FirstOrDefault(),
                                                    ExpiryDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiryDate).FirstOrDefault(),

                                                    AssetClassId = x.AssetClass.Id,
                                                    AssetClassKey = x.AssetClass.Guid,
                                                    AssetClassName = x.AssetClass.ClassName,

                                                    AssetCategoryId = x.AssetCategory.Id,
                                                    AssetCategoryKey = x.AssetCategory.Guid,
                                                    AssetCategoryName = x.AssetCategory.CategoryName,

                                                    AssetSubCategoryId = x.AssetSubCategory.Id,
                                                    AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                                    AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                                    DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                                    DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                                    DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                                    LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                                    LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                                    LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                                    SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                                    SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                                    SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                                    DepartmentId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.DepartmentId).FirstOrDefault(),
                                                    DepartmentKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.Guid).FirstOrDefault(),
                                                    DepartmentName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.DepartmentName).FirstOrDefault(),

                                                    OpeningBookValueOnStartDate = 0,
                                                    ClosingBookValueOnStartDate = 0,
                                                    OpeningBookValueOnEndDate = 0,
                                                    ClosingBookValueOnEndDate = 0,
                                                    DepreciationExpense = 0,
                                                }).Where(_Request.SearchCondition)
                                                .CountAsync();
                    }

                    List<OAsset.AssetDepreciation.List> _AssetDepreciations = await _HCoreContext.AtAssetRegisters.Where(x => x.AtAssetDepreciations.Any() && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                       .Select(x => new OAsset.AssetDepreciation.List
                                                       {
                                                           AssetId = x.Id,
                                                           AssetKey = x.Guid,
                                                           AssetName = x.AssetName,
                                                           AssetBarcode = x.AssetBarcode,
                                                           AssetSerialNo = x.AssetSerialNumber,
                                                           ExAssetId = x.ExAssetId,

                                                           AssetPrice = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.PurchasePrice).FirstOrDefault(),
                                                           ExpiresIn = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiresIn).FirstOrDefault(),
                                                           ReceivedDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.RecievedDate).FirstOrDefault(),
                                                           ExpiryDate = x.AtAssetVendorDetails.Where(a => a.AssetId == x.Id).Select(z => z.ExpiryDate).FirstOrDefault(),

                                                           AssetClassId = x.AssetClass.Id,
                                                           AssetClassKey = x.AssetClass.Guid,
                                                           AssetClassName = x.AssetClass.ClassName,

                                                           AssetCategoryId = x.AssetCategory.Id,
                                                           AssetCategoryKey = x.AssetCategory.Guid,
                                                           AssetCategoryName = x.AssetCategory.CategoryName,

                                                           AssetSubCategoryId = x.AssetSubCategory.Id,
                                                           AssetSubCategoryKey = x.AssetSubCategory.Guid,
                                                           AssetSubCategoryName = x.AssetSubCategory.SubCategoryName,

                                                           DivisionId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.DivisionId).FirstOrDefault(),
                                                           DivisionKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.Guid).FirstOrDefault(),
                                                           DivisionName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Division.DivisionName).FirstOrDefault(),

                                                           LocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.LocationId).FirstOrDefault(),
                                                           LocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.Guid).FirstOrDefault(),
                                                           LocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.Location.LocationName).FirstOrDefault(),

                                                           SubLocationId = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocationId).FirstOrDefault(),
                                                           SubLocationKey = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.Guid).FirstOrDefault(),
                                                           SubLocationName = x.AtAssetLocations.Where(a => a.AssetId == x.Id).Select(z => z.SubLocation.LocationName).FirstOrDefault(),

                                                           DepartmentId = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.DepartmentId).FirstOrDefault(),
                                                           DepartmentKey = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.Guid).FirstOrDefault(),
                                                           DepartmentName = x.AtAssetIssueDetails.Where(a => a.AssetId == x.Id).Select(z => z.Department.DepartmentName).FirstOrDefault(),

                                                           OpeningBookValueOnStartDate = 0,
                                                           ClosingBookValueOnStartDate = 0,
                                                           OpeningBookValueOnEndDate = 0,
                                                           ClosingBookValueOnEndDate = 0,
                                                           DepreciationExpense = 0,
                                                       }).OrderBy(_Request.SortExpression)
                                                       .Where(_Request.SearchCondition)
                                                       .Skip(_Request.Offset)
                                                       .Take(_Request.Limit)
                                                       .ToListAsync();

                    if (_AssetDepreciations.Count > 0)
                    {
                        foreach (var _Asset in _AssetDepreciations)
                        {
                            if (_Request.StartDate == null)
                            {
                                _Request.StartDate = _Asset.ReceivedDate;
                            }
                            if (_Request.EndDate == null)
                            {
                                _Request.EndDate = _Asset.ExpiryDate;
                            }

                            _Asset.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ReceivedDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                            _Asset.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ReceivedDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                            _Asset.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ExpiryDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                            _Asset.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Asset.ExpiryDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();

                            if (_Request.StartDate >= _Asset.ReceivedDate && _Request.EndDate <= _Asset.ExpiryDate)
                            {
                                _Asset.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                            else if (_Request.StartDate >= _Asset.ReceivedDate && _Request.EndDate > _Asset.ExpiryDate)
                            {
                                _Asset.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.StartDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.OpeningBookValueOnEndDate = _Asset.OpeningBookValueOnEndDate;
                                _Asset.ClosingBookValueOnEndDate = _Asset.ClosingBookValueOnEndDate;
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                            else if (_Request.StartDate < _Asset.ReceivedDate && _Request.EndDate <= _Asset.ExpiryDate)
                            {
                                _Asset.OpeningBookValueOnStartDate = _Asset.OpeningBookValueOnStartDate;
                                _Asset.ClosingBookValueOnStartDate = _Asset.ClosingBookValueOnStartDate;
                                _Asset.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                                _Asset.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _Asset.AssetId && x.Date.Value.Date.Date == _Request.EndDate.Value.Date.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                            else
                            {
                                _Asset.OpeningBookValueOnStartDate = _Asset.OpeningBookValueOnStartDate;
                                _Asset.ClosingBookValueOnStartDate = _Asset.ClosingBookValueOnStartDate;
                                _Asset.OpeningBookValueOnEndDate = _Asset.OpeningBookValueOnEndDate;
                                _Asset.ClosingBookValueOnEndDate = _Asset.ClosingBookValueOnEndDate;
                                _Asset.DepreciationExpense = _Asset.OpeningBookValueOnStartDate - _Asset.ClosingBookValueOnEndDate;
                            }
                        }
                    }

                    OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _AssetDepreciations, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "AT0200", "List loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetDepreciation", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetAssetDepreciation(OReference _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    _ManageDownload = new ManageDownload();
                    var _Response = await _ManageDownload.GetAssetDepreciation(_Request);
                    OList.Response _OResponse = HCoreHelper.GetListResponse(0, _Response, 0, 0);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "AT0200", "Asset list loaded successfully");
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ATASTBRCDE", "Please select asset");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _AssetDepreciation = new OAsset.AssetDepreciation.Response();
                    _AssetDepreciation.AssetDetails = await _HCoreContext.AtAssetRegisters.Where(x => x.AssetBarcode == _Request.ReferenceKey && x.OrganizationId == _Request.UserReference.OrganizationId)
                                                      .Select(x => new OAsset.AssetDepreciation.AssetDetails
                                                      {
                                                          AssetId = x.Id,
                                                          AssetKey = x.Guid,
                                                          AssetName = x.AssetName,
                                                          AssetBarcode = x.AssetBarcode,
                                                          AssetSerialNumber = x.AssetSerialNumber,
                                                          ExAssetId = x.ExAssetId,
                                                          AssetType = x.AssetType.Name,
                                                          AssetMake = x.AssetMake,
                                                          AssetModel = x.AssetModel,
                                                          AssetClass = x.AssetClass.ClassName,
                                                          AssetCategory = x.AssetCategory.CategoryName,
                                                          AssetSubCategory = x.AssetSubCategory.SubCategoryName,
                                                          AssetPrice = x.AtAssetVendorDetails.Select(a => a.PurchasePrice).FirstOrDefault(),
                                                          SalvageValue = x.SalvageValue,
                                                          ReceivedDate = x.AtAssetVendorDetails.Select(a => a.RecievedDate).FirstOrDefault(),
                                                          ExpiryDate = x.AtAssetVendorDetails.Select(a => a.ExpiryDate).FirstOrDefault(),
                                                          UsefulLife = x.AtAssetVendorDetails.Select(a => a.ExpiresIn).FirstOrDefault(),
                                                      }).FirstOrDefaultAsync();
                    if (_AssetDepreciation.AssetDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AT0404", "Please select a valid asset");
                    }

                    DateTime StartDate = (DateTime)_AssetDepreciation.AssetDetails.ReceivedDate;
                    DateTime EndDate = (DateTime)_AssetDepreciation.AssetDetails.ExpiryDate;

                    _AssetDepreciation.DepreciationMethodId = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId).Select(x => x.DepreciationMethodId).FirstOrDefaultAsync();
                    _AssetDepreciation.DepreciationMethod = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId).Select(x => x.DepreciationMethod.Name).FirstOrDefaultAsync();
                    _AssetDepreciation.DepreciationRate = await _HCoreContext.AtAssetDepreciationConfigurations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId).Select(x => x.DepreciationRate).FirstOrDefaultAsync();

                    _AssetDepreciation.OpeningBookValueOnReceivedDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == StartDate.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.ClosingBookValueOnReceivedDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == StartDate.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.OpeningBookValueOnExpiryDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == EndDate.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.ClosingBookValueOnExpiryDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == EndDate.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.AccumulatedDepreciation = _AssetDepreciation.OpeningBookValueOnReceivedDate - _AssetDepreciation.ClosingBookValueOnExpiryDate;

                    if (_Request.StartDate == null)
                    {
                        _Request.StartDate = StartDate;
                    }
                    if (_Request.EndDate == null)
                    {
                        _Request.EndDate = EndDate;
                    }

                    _AssetDepreciation.OpeningBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == _Request.StartDate.Value.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.ClosingBookValueOnStartDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == _Request.StartDate.Value.Date).Select(x => x.BookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.OpeningBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == _Request.EndDate.Value.Date).Select(x => x.OpeningBookValue).FirstOrDefaultAsync();
                    _AssetDepreciation.ClosingBookValueOnEndDate = await _HCoreContext.AtAssetDepreciations.Where(x => x.AssetId == _AssetDepreciation.AssetDetails.AssetId && x.Date.Value.Date == _Request.EndDate.Value.Date).Select(x => x.BookValue).FirstOrDefaultAsync();

                    if (_Request.StartDate >= StartDate && _Request.EndDate <= EndDate)
                    {
                        _AssetDepreciation.OpeningBookValueOnStartDate = _AssetDepreciation.OpeningBookValueOnStartDate;
                        _AssetDepreciation.ClosingBookValueOnStartDate = _AssetDepreciation.ClosingBookValueOnStartDate;
                        _AssetDepreciation.OpeningBookValueOnEndDate = _AssetDepreciation.OpeningBookValueOnEndDate;
                        _AssetDepreciation.ClosingBookValueOnEndDate = _AssetDepreciation.ClosingBookValueOnEndDate;
                        _AssetDepreciation.DepreciationExpense = _AssetDepreciation.OpeningBookValueOnStartDate - _AssetDepreciation.ClosingBookValueOnEndDate;
                    }
                    else if (_Request.StartDate >= StartDate && _Request.EndDate > EndDate)
                    {
                        _AssetDepreciation.OpeningBookValueOnStartDate = _AssetDepreciation.OpeningBookValueOnStartDate;
                        _AssetDepreciation.ClosingBookValueOnStartDate = _AssetDepreciation.ClosingBookValueOnStartDate;
                        _AssetDepreciation.OpeningBookValueOnEndDate = _AssetDepreciation.OpeningBookValueOnExpiryDate;
                        _AssetDepreciation.ClosingBookValueOnEndDate = _AssetDepreciation.ClosingBookValueOnExpiryDate;
                        _AssetDepreciation.DepreciationExpense = _AssetDepreciation.OpeningBookValueOnStartDate - _AssetDepreciation.ClosingBookValueOnEndDate;
                    }
                    else if (_Request.StartDate < StartDate && _Request.EndDate <= EndDate)
                    {
                        _AssetDepreciation.OpeningBookValueOnStartDate = _AssetDepreciation.OpeningBookValueOnReceivedDate;
                        _AssetDepreciation.ClosingBookValueOnStartDate = _AssetDepreciation.ClosingBookValueOnReceivedDate;
                        _AssetDepreciation.OpeningBookValueOnEndDate = _AssetDepreciation.OpeningBookValueOnEndDate;
                        _AssetDepreciation.ClosingBookValueOnEndDate = _AssetDepreciation.ClosingBookValueOnEndDate;
                        _AssetDepreciation.DepreciationExpense = _AssetDepreciation.OpeningBookValueOnStartDate - _AssetDepreciation.ClosingBookValueOnEndDate;
                    }
                    else
                    {
                        _AssetDepreciation.OpeningBookValueOnStartDate = _AssetDepreciation.OpeningBookValueOnReceivedDate;
                        _AssetDepreciation.ClosingBookValueOnStartDate = _AssetDepreciation.ClosingBookValueOnReceivedDate;
                        _AssetDepreciation.OpeningBookValueOnEndDate = _AssetDepreciation.OpeningBookValueOnExpiryDate;
                        _AssetDepreciation.ClosingBookValueOnEndDate = _AssetDepreciation.ClosingBookValueOnExpiryDate;
                        _AssetDepreciation.DepreciationExpense = _AssetDepreciation.OpeningBookValueOnStartDate - _AssetDepreciation.ClosingBookValueOnEndDate;
                    }

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AssetDepreciation, "AT0200", "Details loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAssetDepreciation", _Exception, _Request.UserReference);
            }
        }
    }
}
