﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.App
{
    public class OAssetOperations
    {
        public class IssueAsset
        {
            public List<Request>? IssueAssets { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class TransferAsset
        {
            public List<Request>? TransferAssets { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Request
        {
            public string? ReceiptNo { get; set; }
            public string? AssetBarcode { get; set; }
            public string? RfId { get; set; }
            public string? EpcId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DepartmentKey { get; set; }
            public string? LocationKey { get; set; }
            public string? SubLocationKey { get; set; }
            public string? EmployeeKey { get; set; }
            public string? Remark { get; set; }
            public int? Quantity { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? CreateDate { get; set; }
        }

        public class MapAssetBarcode
        {
            public List<Request>? MapAssetBarcodes { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Audit
        {
            public class Request
            {
                public string? AssetBarcode { get; set; }
                public string? Division { get; set; }
                public string? Location { get; set; }
                public string? SubLocation { get; set; }
                public string? Department { get; set; }
                public string? Employee { get; set; }
                public string? AssetStatus { get; set; }
                public string? AuditDivision { get; set; }
                public string? AuditLocation { get; set; }
                public string? AuditSubLocation { get; set; }
                public string? AuditStatus { get; set; }
            }

            public class AuditRequest
            {
                public List<Request>? AuditRequests { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class BarcodeDetails
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? AssetTypeCode { get; set; }
            public string? AssetTypeName { get; set; }
            public string? AssetClassName { get; set; }
            public string? AssetCategoryName { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public string? ExAssetId { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public long? CompanyId { get; set; }
            public string? CompanyCode { get; set; }
            public string? CompanyName { get; set; }
            public bool IsIssued { get; set; }
            public DateTime? IssuedDate { get; set; }
            public int? RemainingQuantity { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public bool IsPrinted { get; set; }
            public int? ReprintCount { get; set; }
            public string? VendorName { get; set; }
            public string? PoNumber { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? WarrentyStartDate { get; set; }
            public DateTime? WarrentyEndDate { get; set; }
            public int? ExpiresIn { get; set; }
            public DateTime? ExpiryDate { get; set; }
            public decimal? PurchasePrice { get; set; }
            public decimal? AdditionalPrice { get; set; }
            public bool IsAmc { get; set; }
            public bool IsInsurance { get; set; }
            public byte? IsAmcApplicable { get; set; }
            public byte? IsInsurances { get; set; }
        }
    }
}
