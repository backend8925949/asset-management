﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.App
{
    public class OAssets
    {
        public class Division
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public List<Department>? Departments { get; set; }
            public List<Location>? Locations { get; set; }
        }

        public class Department
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }

        public class Location
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public long LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public List<SubLocation>? SubLocations { get; set; }
        }

        public class SubLocation
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public long LocationId { get; set; }
            public string? LocationKey { get; set; }
            public long SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }

        public class IssuedAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNumber { get; set; }
            public string? Remark { get; set; }
            public int? AssetTypeId { get; set; }
            public string? AssetTypeCode { get; set; }
            public string? AssetTypeName { get; set; }
            public long? AssetClassId { get; set; }
            public string? AssetClassKey { get; set; }
            public string? AssetClassName { get; set; }
            public long? AssetCategoryId { get; set; }
            public string? AssetCategoryKey { get; set; }
            public string? AssetCategoryName { get; set; }
            public long? AssetSubCategoryId { get; set; }
            public string? AssetSubCategoryKey { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
        }

        public class UnIssuedAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNumber { get; set; }
            public int? Quantity { get; set; }
            public int? AssetTypeId { get; set; }
            public string? AssetTypeCode { get; set; }
            public string? AssetTypeName { get; set; }
            public long? AssetClassId { get; set; }
            public string? AssetClassKey { get; set; }
            public string? AssetClassName { get; set; }
            public long? AssetCategoryId { get; set; }
            public string? AssetCategoryKey { get; set; }
            public string? AssetCategoryName { get; set; }
            public long? AssetSubCategoryId { get; set; }
            public string? AssetSubCategoryKey { get; set; }
            public string? AssetSubCategoryName { get; set; }
        }

        public class Assets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNumber { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeCode { get; set; }
            public string? EmployeeEmail { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Employee
        {
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeCode { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }
    }
}
