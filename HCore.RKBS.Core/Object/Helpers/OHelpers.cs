﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.Helpers
{
    public class OHelpers
    {
        public class Division
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
        }

        public class Department
        {
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }

        public class Location
        {
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
        }

        public class SubLocation
        {
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
        }

        public class Vendor
        {
            public long VendorId { get; set; }
            public string? VendorKey { get; set; }
            public string? Name { get; set; }
            public int TypeId { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
        }

        public class Employee
        {
            public long EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? Name { get; set; }
            public string? EmployeeCode { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }

        public class Class
        {
            public long? ClassId { get; set; }
            public string? ClassKey { get; set; }
            public string? ClassName { get; set; }
        }

        public class Category
        {
            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public long? ClassId { get; set; }
            public string? ClassKey { get; set; }
            public string? ClassName { get; set; }
        }

        public class SubCategory
        {
            public long? ClassId { get; set; }
            public string? ClassKey { get; set; }
            public string? ClassName { get; set; }
            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public long? SubCategoryId { get; set; }
            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }
        }

        public class Request
        {
            public string? EmployeeCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Roles
        {
            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }
        }

        public class Users
        {
            public long? UserId { get; set; }
            public string? UserKey { get; set; }
            public string? UserName { get; set; }
        }
    }
}
