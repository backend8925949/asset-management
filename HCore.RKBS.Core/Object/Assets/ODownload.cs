﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.Assets
{
    public class ODownload
    {
        public class List
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? SerialNo { get; set; }
            public int? AssetTypeId { get; set; }
            public string? AssetTypeName { get; set; }
            public string? AssetTypeCode { get; set; }
            public long? AssetClassId { get; set; }
            public string? AssetClassKey { get; set; }
            public string? AssetClassName { get; set; }
            public long? AssetSubClassId { get; set; }
            public string? AssetSubClassKey { get; set; }
            public string? AssetSubClassName { get; set; }
            public long? AssetCategoryId { get; set; }
            public string? AssetCategoryKey { get; set; }
            public string? AssetCategoryName { get; set; }
            public long? AssetSubCategoryId { get; set; }
            public string? AssetSubCategoryKey { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public string? ExAssetId { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public long? AssetVendorId { get; set; }
            public string? AssetVendorName { get; set; }
            public string? AssetVendorKey { get; set; }
            public string? PoNumber { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? WarrentyStartDate { get; set; }
            public DateTime? WarrentyEndDate { get; set; }
            public int? CurrencyId { get; set; }
            public string? CurrencyKey { get; set; }
            public string? CurrencyName { get; set; }
            public byte? IsAmcApplicable { get; set; }
            public byte? IsInsurance { get; set; }
            public long? OrganizationId { get; set; }
            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? SubDivisionId { get; set; }
            public string? SubDivisionKey { get; set; }
            public string? SubDivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? Remark { get; set; }
            public int? Quantity { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class UploadAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetType { get; set; }
            public string? AssetBarcode { get; set; }
            public string? SerialNo { get; set; }
            public string? AssetClass { get; set; }
            public string? AssetSubClass { get; set; }
            public string? AssetCategory { get; set; }
            public string? AssetSubCategory { get; set; }
            public string? AssetName { get; set; }
            public string? ExAssetId { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public int? TotalQuantity { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? AssetVendor { get; set; }
            public string? PoNumber { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? Currency { get; set; }
            public decimal? Price { get; set; }
            public bool IsAmc { get; set; }
            public bool IsInsurance { get; set; }
            public byte? IsAmcApplicable { get; set; }
            public byte? IsInsurances { get; set; }
            public DateTime? CreateDate { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class IssueAsset
        {
            public class List
            {
                public long? AssetIssueId { get; set; }
                public string? AssetIssueKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public int? AssetTypeId { get; set; }
                public string? AssetTypeName { get; set; }
                public string? AssetTypeCode { get; set; }
                public long? AssetClassId { get; set; }
                public string? AssetClassKey { get; set; }
                public string? AssetClassName { get; set; }
                public long? AssetSubClassId { get; set; }
                public string? AssetSubClassKey { get; set; }
                public string? AssetSubClassName { get; set; }
                public long? AssetCategoryId { get; set; }
                public string? AssetCategoryKey { get; set; }
                public string? AssetCategoryName { get; set; }
                public long? AssetSubCategoryId { get; set; }
                public string? AssetSubCategoryKey { get; set; }
                public string? AssetSubCategoryName { get; set; }
                public string? ExAssetId { get; set; }
                public string? AssetMake { get; set; }
                public string? AssetModel { get; set; }
                public int? Quantity { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? SubLocationId { get; set; }
                public string? SubLocationKey { get; set; }
                public string? SubLocationName { get; set; }
                public long? EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? EmployeeName { get; set; }
                public string? EmployeeCode { get; set; }
                public string? EmployeeEmailAddress { get; set; }
                public string? Remark { get; set; }
                public string? IpAddress { get; set; }
                public string? OsName { get; set; }
                public string? HostName { get; set; }
                public int? PrinterId { get; set; }
                public string? PrinterKey { get; set; }
                public string? PrinterName { get; set; }
                public int? TotalQuantity { get; set; }
                public int? IssuedQuantity { get; set; }
                public int? RemainingQuantity { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class UnIssuedAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public int? AssetTypeId { get; set; }
            public string? AssetTypeName { get; set; }
            public string? AssetTypeCode { get; set; }
            public long? AssetClassId { get; set; }
            public string? AssetClassKey { get; set; }
            public string? AssetClassName { get; set; }
            public long? AssetSubClassId { get; set; }
            public string? AssetSubClassKey { get; set; }
            public string? AssetSubClassName { get; set; }
            public long? AssetCategoryId { get; set; }
            public string? AssetCategoryKey { get; set; }
            public string? AssetCategoryName { get; set; }
            public long? AssetSubCategoryId { get; set; }
            public string? AssetSubCategoryKey { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public string? ExAssetId { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public int? Quantity { get; set; }
            public long? AssetVendorId { get; set; }
            public string? AssetVendorName { get; set; }
            public string? AssetVendorKey { get; set; }
            public string? PoNumber { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? WarrentyStartDate { get; set; }
            public DateTime? WarrentyEndDate { get; set; }
            public int? CurrencyId { get; set; }
            public string? CurrencyKey { get; set; }
            public string? CurrencyName { get; set; }
            public byte? IsAmcApplicable { get; set; }
            public byte? IsInsurance { get; set; }
            public long? OrganizationId { get; set; }
            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? SubDivisionId { get; set; }
            public string? SubDivisionKey { get; set; }
            public string? SubDivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeCode { get; set; }
            public string? Remark { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class RetrurnAsset
        {
            public class List
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int? AssetReturnId { get; set; }
                public string? AssetReturnKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public int? AssetTypeId { get; set; }
                public string? AssetTypeName { get; set; }
                public string? AssetTypeCode { get; set; }
                public long? AssetClassId { get; set; }
                public string? AssetClassKey { get; set; }
                public string? AssetClassName { get; set; }
                public long? AssetSubClassId { get; set; }
                public string? AssetSubClassKey { get; set; }
                public string? AssetSubClassName { get; set; }
                public long? AssetCategoryId { get; set; }
                public string? AssetCategoryKey { get; set; }
                public string? AssetCategoryName { get; set; }
                public long? AssetSubCategoryId { get; set; }
                public string? AssetSubCategoryKey { get; set; }
                public string? AssetSubCategoryName { get; set; }
                public string? ExAssetId { get; set; }
                public string? AssetMake { get; set; }
                public string? AssetModel { get; set; }
                public int? Quantity { get; set; }
                public long? ReturnById { get; set; }
                public string? ReturnByKey { get; set; }
                public string? ReturnByName { get; set; }
                public string? ReturnByEmployeeCode { get; set; }
                public string? ReturnCondition { get; set; }
                public string? ReturnNotes { get; set; }
                public long? ReturnToId { get; set; }
                public string? ReturnToKey { get; set; }
                public string? ReturnToName { get; set; }
                public int? ReturnQuantity { get; set; }
                public decimal? ReturnValue { get; set; }
                public string? ReturnReference { get; set; }
                public long? ApprovedById { get; set; }
                public string? ApprovedByKey { get; set; }
                public string? ApprovedByName { get; set; }
                public DateTime? ApprovedDate { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class TransferAsset
        {
            public class List
            {
                public long? AssetIssueId { get; set; }
                public string? AssetIssueKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public int? AssetTypeId { get; set; }
                public string? AssetTypeName { get; set; }
                public string? AssetTypeCode { get; set; }
                public long? AssetClassId { get; set; }
                public string? AssetClassKey { get; set; }
                public string? AssetClassName { get; set; }
                public long? AssetSubClassId { get; set; }
                public string? AssetSubClassKey { get; set; }
                public string? AssetSubClassName { get; set; }
                public long? AssetCategoryId { get; set; }
                public string? AssetCategoryKey { get; set; }
                public string? AssetCategoryName { get; set; }
                public long? AssetSubCategoryId { get; set; }
                public string? AssetSubCategoryKey { get; set; }
                public string? AssetSubCategoryName { get; set; }
                public string? ExAssetId { get; set; }
                public string? AssetMake { get; set; }
                public string? AssetModel { get; set; }
                public int? Quantity { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? SubLocationId { get; set; }
                public string? SubLocationKey { get; set; }
                public string? SubLocationName { get; set; }
                public long? EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? EmployeeName { get; set; }
                public string? EmployeeCode { get; set; }
                public string? Remark { get; set; }
                public string? IpAddress { get; set; }
                public string? OsName { get; set; }
                public string? HostName { get; set; }
                public byte? IsTransfered { get; set; }
                public string? TransferReference { get; set; }
                public long? TransferedToId { get; set; }
                public string? TransferedToKey { get; set; }
                public string? TransferedToName { get; set; }
                public string? TransferedToEmployeeCode { get; set; }
                public long? TransferedById { get; set; }
                public string? TransferedByKey { get; set; }
                public string? TransferedByName { get; set; }
                public DateTime? TransferedDate { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Maintenance
        {
            public class List
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public int? AssetTypeId { get; set; }
                public string? AssetTypeName { get; set; }
                public string? AssetTypeCode { get; set; }
                public long? AssetClassId { get; set; }
                public string? AssetClassKey { get; set; }
                public string? AssetClassName { get; set; }
                public long? AssetSubClassId { get; set; }
                public string? AssetSubClassKey { get; set; }
                public string? AssetSubClassName { get; set; }
                public long? AssetCategoryId { get; set; }
                public string? AssetCategoryKey { get; set; }
                public string? AssetCategoryName { get; set; }
                public long? AssetSubCategoryId { get; set; }
                public string? AssetSubCategoryKey { get; set; }
                public string? AssetSubCategoryName { get; set; }
                public string? ExAssetId { get; set; }
                public string? AssetMake { get; set; }
                public string? AssetModel { get; set; }
                public int? Quantity { get; set; }
                public int? MaintenanceTypeId { get; set; }
                public string? MaintenanceType { get; set; }
                public string? MaintenanceTypeName { get; set; }
                public long? AssetVendorId { get; set; }
                public string? AssetVendorKey { get; set; }
                public string? AssetVendorName { get; set; }
                public string? MobileNumber { get; set; }
                public string? Description { get; set; }
                public string? Remark { get; set; }
                public DateTime? RequestDate { get; set; }
                public DateTime? ExpectedDate { get; set; }
                public DateTime? ReceivedDate { get; set; }
                public double? Amount { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class AssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Issued_Date { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public int? Issued_Quantity { get; set; }
            public int? Remaining_Quantity { get; set; }
            public string? Remark { get; set; }
            public string? Status { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class IssuedAssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Issued_Date { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public int? Issued_Quantity { get; set; }
            public int? Remaining_Quantity { get; set; }
            public string? Remark { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class UnIssuedAssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class DisposedAssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Issued_Date { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public int? Issued_Quantity { get; set; }
            public int? Remaining_Quantity { get; set; }
            public string? Remark { get; set; }
            public string? Disposed_By { get; set; }
            public string? Disposed_On { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class ExpiredAssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Issued_Date { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public int? Issued_Quantity { get; set; }
            public int? Remaining_Quantity { get; set; }
            public string? Remark { get; set; }
            public string? Expired_On { get; set; }
            public string? Create_Date { get; set; }
        }

        public class AssetUploadListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class TransferredAssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Transfered_Date { get; set; }
            public string? Transferred_To_Division { get; set; }
            public string? Transferred_To_Department { get; set; }
            public string? Transferred_To_Location { get; set; }
            public string? Transferred_To_SubLocation { get; set; }
            public string? Transferred_To_Employee { get; set; }
            public string? Transferred_To_Employee_Code { get; set; }
            public string? Transferred_To_Employee_Email { get; set; }
            public string? Approved_By { get; set; }
            public string? Approved_Date { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public string? Remark { get; set; }
            public string? Status { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class ReturnedAssetListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? Asset_Type { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Issued_Date { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public int? Total_Quantity { get; set; }
            public int? Issued_Quantity { get; set; }
            public int? Remaining_Quantity { get; set; }
            public string? Remark { get; set; }
            public string? Asset_Condition { get; set; }
            public int? Returned_Quantity { get; set; }
            public decimal? Returned_Value { get; set; }
            public string? Return_Reference { get; set; }
            public string? Approved_By { get; set; }
            public string? Approved_Date { get; set; }
            public string? Status { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class AssetMaintenanceListDownload
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Make { get; set; }
            public string? Asset_Model { get; set; }
            public string? Asset_Vendor { get; set; }
            public decimal? Amount { get; set; }
            public string? Maintenance_Type { get; set; }
            public string? Vednor_Mobile_Number { get; set; }
            public string? Description { get; set; }
            public string? Remark { get; set; }
            public string? Request_Date { get; set; }
            public string? Expected_Date { get; set; }
            public string? Received_Date { get; set; }
            public decimal? Maintenance_Amount { get; set; }
            public string? Status { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class StockReport
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_SerialNo { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
            public string? Employee { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Asset_Vendor { get; set; }
            public string? PO_Number { get; set; }
            public string? Invoice_Number { get; set; }
            public string? Asset_Recieved_Date { get; set; }
            public string? Warrenty_Start_Date { get; set; }
            public string? Warrenty_End_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public decimal? Amount { get; set; }
            public string? Currency { get; set; }
            public bool? AMC { get; set; }
            public bool? Insurance { get; set; }
            public string? Status { get; set; }
            public string? User { get; set; }
            public string? Create_Date { get; set; }
        }

        public class AssetHistoryDownload
        {
            public string? Asset_Bracode { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Asset_Name { get; set; }
            public string? Asset_Serial_No { get; set; }
            public string? Status { get; set; }
            public string? User { get; set; }
            public string? Activity { get; set; }
            public string? Activity_Date { get; set; }
        }

        public class AssetLocationDetails
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }

        public class AssetAuditReportDownload
        {
            public string? Asset_Barcode { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public string? Division { get; set; }
            public string? Location { get; set; }
            public string? Sub_Location { get; set; }
            public string? Department { get; set; }
            public string? Employee { get; set; }
            public string? Asset_Status { get; set; }
            public string? Audit_Division { get; set; }
            public string? Audit_Location { get; set; }
            public string? Audit_SubLocation { get; set; }
            public string? Audit_Status { get; set; }
            public string? Audit_Date { get; set; }
            public string? User { get; set; }
        }

        public class AssetDepreciation
        {
            public string? Asset_Name { get; set; }
            public string? Asset_Barcode { get; set; }
            public string? Asset_Serial_No { get; set; }
            public string? SAP_Asset_Code { get; set; }
            public int? Useful_Life { get; set; }
            public decimal? Purchase_Price { get; set; }
            public decimal? Opening_Book_Value_On_Start_Date { get; set; }
            public decimal? Closing_Book_Value_On_Start_Date { get; set; }
            public decimal? Depreciation_Expense { get; set; }
            public decimal? Opening_Book_Value_On_End_Date { get; set; }
            public decimal? Closing_Book_Value_On_End_Date { get; set; }
            public string? Received_Date { get; set; }
            public string? Expiry_Date { get; set; }
            public string? Asset_Class { get; set; }
            public string? Asset_Category { get; set; }
            public string? Asset_Sub_Category { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
        }

        public class AssetDepreciationList
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? ExAssetId { get; set; }
            public long? AssetClassId { get; set; }
            public string? AssetClassKey { get; set; }
            public string? AssetClassName { get; set; }
            public long? AssetCategoryId { get; set; }
            public string? AssetCategoryKey { get; set; }
            public string? AssetCategoryName { get; set; }
            public long? AssetSubCategoryId { get; set; }
            public string? AssetSubCategoryKey { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public decimal? OpeningBookValueOnReceivedDate { get; set; }
            public decimal? ClosingBookValueOnReceivedDate { get; set; }
            public decimal? OpeningBookValueOnExpiryDate { get; set; }
            public decimal? ClosingBookValueOnExpiryDate { get; set; }
            public decimal? OpeningBookValueOnStartDate { get; set; }
            public decimal? ClosingBookValueOnStartDate { get; set; }
            public decimal? OpeningBookValueOnEndDate { get; set; }
            public decimal? ClosingBookValueOnEndDate { get; set; }
            public decimal? DepreciationExpense { get; set; }
            public decimal? AssetPrice { get; set; }
            public int? ExpiresIn { get; set; }
            public DateTime? ReceivedDate { get; set; }
            public DateTime? ExpiryDate { get; set; }
        }

        public class AssetDepreciationDownload
        {
            public class Response
            {
                public string? Asset_Name { get; set; }
                public string? Asset_Barcode { get; set; }
                public string? Asset_Serial_No { get; set; }
                public string? SAP_Asset_Code { get; set; }
                public decimal? Asset_Price { get; set; }
                public decimal? Salvage_Value { get; set; }
                public int? Useful_Life { get; set; }
                public decimal? Opening_Book_Value_On_Received_Date { get; set; }
                public decimal? Closing_Book_Value_On_Received_Date { get; set; }
                public decimal? Opening_Book_Value_On_Expiry_Date { get; set; }
                public decimal? Closing_Book_Value_On_Expiry_Date { get; set; }
                public decimal? Depreciation_Cost_On_Expiry_Date { get; set; }
                public decimal? Opening_Book_Value_On_Start_Date { get; set; }
                public decimal? Closing_Book_Value_On_Start_Date { get; set; }
                public decimal? Opening_Book_Value_On_End_Date { get; set; }
                public decimal? Closing_Book_Value_On_End_Date { get; set; }
                public decimal? Depreciation_Cost_On_End_Date { get; set; }
                public decimal? Depreciation_Rate { get; set; }
                public string? Depreciation_Method { get; set; }
                public string? Received_Date { get; set; }
                public string? Expiry_Date { get; set; }
                public string? Asset_Type { get; set; }
                public string? Asset_Make { get; set; }
                public string? Asset_Model { get; set; }
                public string? Asset_Class { get; set; }
                public string? Asset_Category { get; set; }
                public string? Asset_Sub_Category { get; set; }
            }
        }
    }
}
