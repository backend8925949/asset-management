﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Assets
{
    public class OAssetOperations
    {
        public class Register
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetType { get; set; }
            public string? AssetClass { get; set; }
            public string? AssetCategory { get; set; }
            public string? AssetSubCategory { get; set; }
            public string? AssetName { get; set; }
            public string? ExAssetId { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public int? TotalQuantity { get; set; }
            public decimal? SalvageValue { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? Barcode { get; set; }
            public int? SerialNo { get; set; }
            public Vendor? VendorDetails { get; set; }
            public IssueDetails? IssueDetails { get; set; }
            public LocationDetails? LocationDetails { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Vendor
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? VendorId { get; set; }
            public string? VendorKey { get; set; }
            public string? VendorName { get; set; }
            public string? PoNumber { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public int? ExpireInYears { get; set; }
            public DateTime? ExpiryDate { get; set; }
            public string? Currency { get; set; }
            public decimal? PurchasePrice { get; set; }
            public decimal? AdditionalPrice { get; set; }
            public bool IsAmc { get; set; }
            public bool IsInsurance { get; set; }
            public byte? IsAmcApplicable { get; set; }
            public byte? IsInsurances { get; set; }
            public List<Insurance>? InsuranceDetails { get; set; }
            public List<Amc>? AmcDetails { get; set; }
            public string? StatusCode { get; set; }
        }

        public class Insurance
        {
            public long? VendorId { get; set; }
            public string? VendorKey { get; set; }
            public string? VendorName { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public decimal? Amount { get; set; }
        }

        public class Amc
        {
            public long? VendorId { get; set; }
            public string? VendorKey { get; set; }
            public string? VendorName { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public decimal? Amount { get; set; }
        }

        public class IssueDetails
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeCode { get; set; }
            public string? Remark { get; set; }
            public string? IpAddress { get; set; }
            public string? OsName { get; set; }
            public string? HostName { get; set; }
            public int? Quantity { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? IssuedOn { get; set; }
            public string? IssuedByName { get; set; }
        }

        public class LocationDetails
        {
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
        }

        public class IssueAsset
        {
            public class Request
            {
                public string? AssetKey { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? DepartmentKey { get; set; }
                public string? LocationKey { get; set; }
                public string? SubLocationKey { get; set; }
                public string? EmployeeKey { get; set; }
                public string? Remark { get; set; }
                public string? IpAddress { get; set; }
                public string? OsName { get; set; }
                public string? HostName { get; set; }
                public int? Quantity { get; set; }
                public string? StatusCode { get; set; }
                public bool? IsIssueToEmployee { get; set; }
                public bool? IsIssueToDepartment { get; set; }
                public bool? IsIssueToDivision { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class RetrurnAsset
        {
            public class Request
            {
                public int? AssetReturnId { get; set; }
                public string? AssetReturnKey { get; set; }
                public string? AssetKey { get; set; }
                public string? ReturnBy { get; set; }
                public string? ReturnCondition { get; set; }
                public string? ReturnNotes { get; set; }
                public int? ReturnQuantity { get; set; }
                public decimal? ReturnValue { get; set; }
                public string? ReturnReference { get; set; }
                public string? ApprovedBy { get; set; }
                public string? StatusCode { get; set; }
                public bool? IsReturnFromEmployee { get; set; }
                public bool? IsReturnFromDepartment { get; set; }
                public bool? IsReturnFromDivision { get; set; }
                public string? Comment { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class TransferAsset
        {
            public class Request
            {
                public string? AssetKey { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? DepartmentKey { get; set; }
                public string? LocationKey { get; set; }
                public string? SubLocationKey { get; set; }
                public string? EmployeeKey { get; set; }
                public string? Remark { get; set; }
                public string? IpAddress { get; set; }
                public string? OsName { get; set; }
                public string? HostName { get; set; }
                public string? PrinterKey { get; set; }
                public int? Quantity { get; set; }
                public string? StatusCode { get; set; }
                public bool? IsEmployeeToEmployeeTransfer { get; set; }
                public bool? IsEmployeeToDepartmentTransfer { get; set; }
                public bool? IsEmployeeToDivisionTransfer { get; set; }
                public bool? IsDepartmentToEmployeeTransfer { get; set; }
                public bool? IsDepartmentToDepartmentTransfer { get; set; }
                public bool? IsDepartmentToDivisionTransfer { get; set; }
                public bool? IsDivisionToEmployeeTransfer { get; set; }
                public bool? IsDivisionToDepartmentTransfer { get; set; }
                public bool? IsDivisionToDivisionTransfer { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Maintenance
        {
            public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AssetKey { get; set; }
                public string? AccountKey { get; set; }
                public string? MaintenanceType { get; set; }
                public string? VendorKey { get; set; }
                public string? Description { get; set; }
                public string? Remark { get; set; }
                public DateTime? RequestDate { get; set; }
                public DateTime? ExpectedDate { get; set; }
                public DateTime? ReceivedDate { get; set; }
                public string? StatusCode { get; set; }
                public bool Activity { get; set; }
                public decimal? Amount { get; set; }
                public IssueDetails? IssueDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class IssueDetails
            {
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? SubLocationId { get; set; }
                public string? SubLocationKey { get; set; }
                public string? SubLocationName { get; set; }
                public long? EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? EmployeeName { get; set; }
                public DateTime? IssuedDate { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public byte? IsTransfered { get; set; }
                public string? TransferReference { get; set; }
                public long? TransferedById { get; set; }
                public string? TransferedByKey { get; set; }
                public string? TransferedByName { get; set; }
                public DateTime? TransferedDate { get; set; }
            }
        }

        public class MultipleBarcodePrinting
        {
            public class Request
            {
                public List<PrintRequest>? BarcodeDetails { get; set; }
                public string? PrinterName { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class PrintRequest
            {
                public string? AssetBarcode { get; set; }
            }
        }

        public class RePrintBarcode
        {
            public string? Barcode { get; set; }
            public string? ExAssetId { get; set; }
            public string? PrinterName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class BarcodeDetails
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? AssetTypeCode { get; set; }
            public string? AssetTypeName { get; set; }
            public string? AssetClassName { get; set; }
            public string? AssetCategoryName { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public string? ExAssetId { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public long? CompanyId { get; set; }
            public string? CompanyCode { get; set; }
            public string? CompanyName { get; set; }
            public bool IsIssued { get; set; }
            public DateTime? IssuedDate { get; set; }
            public int? RemainingQuantity { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public bool IsPrinted { get; set; }
            public int? ReprintCount { get; set; }
            public string? VendorName { get; set; }
            public string? PoNumber { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? WarrentyStartDate { get; set; }
            public DateTime? WarrentyEndDate { get; set; }
            public int? ExpiresIn { get; set; }
            public DateTime? ExpiryDate { get; set; }
            public decimal? PurchasePrice { get; set; }
            public decimal? AdditionalPrice { get; set; }
            public bool IsAmc { get; set; }
            public bool IsInsurance { get; set; }
            public byte? IsAmcApplicable { get; set; }
            public byte? IsInsurances { get; set; }
            public int? DepreciationMethodId { get; set; }
            public string? DepreciationMethodCode { get; set; }
            public string? DepreciationMethodName { get; set; }
            public decimal? DepreciationRate { get; set; }
            public decimal? YearlyDepreciationCost { get; set; }
            public decimal? MonthlyDepreciationCost { get; set; }
            public decimal? DailyDepreciationCost { get; set; }
        }

        public class BulkUpload
        {
            public class Request
            {
                public List<AssetUpload>? AssetDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class AssetUpload
            {
                public string? AssetType { get; set; }
                public string? Barcode { get; set; }
                public string? AssetClass { get; set; }
                public string? AssetSubClass { get; set; }
                public string? AssetCategory { get; set; }
                public string? AssetSubCategory { get; set; }
                public string? AssetName { get; set; }
                public string? ExAssetId { get; set; }
                public string? AssetMake { get; set; }
                public string? AssetModel { get; set; }
                public string? AssetSerialNo { get; set; }
                public string? Division { get; set; }
                public string? Location { get; set; }
                public string? SubLocation { get; set; }
                public int? TotalQuantity { get; set; }
                public string? VendorKey { get; set; }
                public string? PoNumber { get; set; }
                public string? InvoiceNumber { get; set; }
                public DateTime? RecievedDate { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public int? ExpireInYears { get; set; }
                public string? Currency { get; set; }
                public decimal? PurchasePrice { get; set; }
                public decimal? AdditionalPrice { get; set; }
                public decimal? SalvageValue { get; set; }
                public bool IsAmc { get; set; }
                public bool IsInsurance { get; set; }
            }
        }

        public class Dispose
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? Comment { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
