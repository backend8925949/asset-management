﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Assets
{
    public class OAssetReport
    {
        public class AssetReport
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public int? AssetTypeId { get; set; }
            public string? AssetTypeName { get; set; }
            public string? AssetTypeCode { get; set; }
            public long? AssetClassId { get; set; }
            public string? AssetClassKey { get; set; }
            public string? AssetClassName { get; set; }
            public long? AssetSubClassId { get; set; }
            public string? AssetSubClassKey { get; set; }
            public string? AssetSubClassName { get; set; }
            public long? AssetCategoryId { get; set; }
            public string? AssetCategoryKey { get; set; }
            public string? AssetCategoryName { get; set; }
            public long? AssetSubCategoryId { get; set; }
            public string? AssetSubCategoryKey { get; set; }
            public string? AssetSubCategoryName { get; set; }
            public string? AssetMake { get; set; }
            public string? AssetModel { get; set; }
            public string? ExAssetId { get; set; }
            public DateTime? RecievedDate { get; set; }
            public DateTime? IssuedDate { get; set; }
            public long? AssetVendorId { get; set; }
            public string? AssetVendorName { get; set; }
            public string? AssetVendorKey { get; set; }
            public long? OrganizationId { get; set; }
            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? SubDivisionId { get; set; }
            public string? SubDivisionKey { get; set; }
            public string? SubDivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class AssetsOverview
        {
            public long? TotalAssets { get; set; }
            public long? IssuedAssets { get; set; }
            public long? UnIssuedAssets { get; set; }
            public long? ReturnedAssets { get; set; }
            public long? TransferedAssets { get; set; }
            public long? DisposedAssets { get; set; }
            public long? ExpiredAssets { get; set; }
            public AssetsReport? AssetsReport { get; set; }
            public AssetsOverview? Overview { get; set; }
            public AMCInsuranceReport? AMCInsuranceReport { get; set; }
            public WarrentyReport? WarrentyReport { get; set; }
        }

        public class AssetsReport
        {
            public long? WithBarcode { get; set; }
            public long? WithoutBarcode { get; set; }
        }

        public class AMCInsuranceReport
        {
            public long? WithAMC { get; set; }
            public long? WithInsurance { get; set; }
            public long? WithAMC_and_Insurance { get; set; }
        }

        public class WarrentyReport
        {
            public long? InWarrenty { get; set; }
            public long? WarrentyExpired { get; set; }
        }

        public class StockReport
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public int? TotalQuantity { get; set; }
            public int? IssuedQuantity { get; set; }
            public int? RemainingQuantity { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationName { get; set; }
            public long? SubLocationId { get; set; }
            public string? SubLocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeCode { get; set; }
            public DateTime? CreateDate { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class IssuedAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetBarcode { get; set; }
            public string? DivisionName { get; set; }
            public string? DivisionKey { get; set; }
            public string? DepartmentName { get; set; }
            public string? DepartmentKey { get; set; }
            public string? LocationName { get; set; }
            public string? LocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public string? SubLocationKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeKey { get; set; }
            public string? Remark { get; set; }
            public int? Quantity { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? Comment { get; set; }
        }

        public class TransferredAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetBarcode { get; set; }
            public string? DivisionName { get; set; }
            public string? DivisionKey { get; set; }
            public string? DepartmentName { get; set; }
            public string? DepartmentKey { get; set; }
            public string? LocationName { get; set; }
            public string? LocationKey { get; set; }
            public string? SubLocationName { get; set; }
            public string? SubLocationKey { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeKey { get; set; }
            public string? Remark { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? Comment { get; set; }
        }

        public class MappedAssets
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetBarcode { get; set; }
            public string? RfId { get; set; }
            public string? EpcId { get; set; }
            public string? Remark { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
        }

        public class Insurance
        {
            public long? VendorId { get; set; }
            public string? VendorKey { get; set; }
            public string? VendorName { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public decimal? Amount { get; set; }
        }

        public class Amc
        {
            public long? VendorId { get; set; }
            public string? VendorKey { get; set; }
            public string? VendorName { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public decimal? Amount { get; set; }
        }

        public class IssueAsset
        {
            public class List
            {
                public long? AssetIssueId { get; set; }
                public string? AssetIssueKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public int? AssetTypeId { get; set; }
                public string? AssetTypeName { get; set; }
                public string? AssetTypeCode { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? SubLocationId { get; set; }
                public string? SubLocationKey { get; set; }
                public string? SubLocationName { get; set; }
                public long? EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? EmployeeName { get; set; }
                public string? Remark { get; set; }
                public string? IpAddress { get; set; }
                public string? OsName { get; set; }
                public string? HostName { get; set; }
                public int? PrinterId { get; set; }
                public string? PrinterKey { get; set; }
                public string? PrinterName { get; set; }
                public int? TotalQuantity { get; set; }
                public int? IssuedQuantity { get; set; }
                public int? RemainingQuantity { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class RetrurnAsset
        {
            public class List
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int? AssetReturnId { get; set; }
                public string? AssetReturnKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public long? ReturnById { get; set; }
                public string? ReturnByKey { get; set; }
                public string? ReturnByName { get; set; }
                public string? ReturnCondition { get; set; }
                public string? ReturnNotes { get; set; }
                public long? ReturnToId { get; set; }
                public string? ReturnToKey { get; set; }
                public string? ReturnToName { get; set; }
                public int? ReturnQuantity { get; set; }
                public decimal? ReturnValue { get; set; }
                public string? ReturnReference { get; set; }
                public long? ApprovedById { get; set; }
                public string? ApprovedByKey { get; set; }
                public string? ApprovedByName { get; set; }
                public DateTime? ApprovedDate { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Maintenance
        {
            public class List
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? AssetId { get; set; }
                public string? AssetKey { get; set; }
                public string? AssetName { get; set; }
                public string? AssetBarcode { get; set; }
                public string? AssetSerialNo { get; set; }
                public int? MaintenanceTypeId { get; set; }
                public string? MaintenanceType { get; set; }
                public string? MaintenanceTypeName { get; set; }
                public long? AssetVendorId { get; set; }
                public string? AssetVendorKey { get; set; }
                public string? AssetVendorName { get; set; }
                public string? MobileNumber { get; set; }
                public string? Description { get; set; }
                public string? Remark { get; set; }
                public DateTime? RequestDate { get; set; }
                public DateTime? ExpectedDate { get; set; }
                public DateTime? ReceivedDate { get; set; }
                public double? Amount { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Assets
        {
            public string? ReceiptNo { get; set; }
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public string? AssetName { get; set; }
            public string? AssetBarcode { get; set; }
            public string? AssetSerialNo { get; set; }
            public string? DivisionKey { get; set; }
            public string? DepartmentKey { get; set; }
            public string? LocationKey { get; set; }
            public string? SubLocationKey { get; set; }
            public string? EmployeeKey { get; set; }
            public int? Quantity { get; set; }
            public long? EmployeeId { get; set; }
            public string? EmployeeName { get; set; }
            public string? EmployeeCode { get; set; }
            public string? EmployeeEmail { get; set; }
            public string? SubLocationName { get; set; }
            public string? LocationName { get; set; }
            public string? DepartmentName { get; set; }
            public string? DivisionName { get; set; }
            public string? OrganizationKey { get; set; }
            public long? TransferredFromEmployeeId { get; set; }
            public string? TransferredFromEmployeeName { get; set; }
            public string? TransferredFromEmployeeCode { get; set; }
            public string? TransferredFromEmployeeEmail { get; set; }
            public string? TransferredFromSubLocationName { get; set; }
            public string? TransferredFromLocationName { get; set; }
            public string? TransferredFromDepartmentName { get; set; }
            public string? TransferredFromDivisionName { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? ReceiptDate { get; set; }
            public bool IsSelected { get; set; }
            public long? UserId { get; set; }
            public string? UserName { get; set; }
            public string? CompanyName { get; set; }
        }
    }
}
