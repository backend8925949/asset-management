﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.BackgroundProcessor
{
    public class OBackgroundProcessor
    {
        public class Request
        {
            public long? AssetId { get; set; }
            public string? AssetKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
