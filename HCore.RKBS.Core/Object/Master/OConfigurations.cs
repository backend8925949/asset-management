﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.Master
{
    public class OConfigurations
    {
        public class EmailConfiguration
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? EmailAddress { get; set; }
                public string? Password { get; set; }
                public string? SmtpServer { get; set; }
                public int? SmtpPort { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? EmailAddress { get; set; }
                public string? Password { get; set; }
                public string? SmtpServer { get; set; }
                public int? SmtpPort { get; set; }
            }
        }

        public class DepreciationConfiguration
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? MethodCode { get; set; }
                public bool IsFromReceivedDate { get; set; }
                public bool IsFromIssuedDate { get; set; }
                public bool IsWithSalvageValue { get; set; }
                public bool IsWithActualValue { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int? MethodId { get; set; }
                public string? MethodCode { get; set; }
                public string? MethodName { get; set; }
                public bool IsFromReceivedDate { get; set; }
                public bool IsFromIssuedDate { get; set; }
                public bool IsWithSalvageValue { get; set; }
                public bool IsWithActualValue { get; set; }
            }
        }

        public class RequisitionConfig
        {
            public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? RoleKey { get; set; }
                public bool IssueRequest { get; set; }
                public bool IssueApproval { get; set; }
                public bool TransferRequest { get; set; }
                public bool TransferApproval { get; set; }
                public bool ReturnRequest { get; set; }
                public bool ReturnApproval { get; set; }
                public bool MaintenanceRequest { get; set; }
                public bool MaintenanceApproval { get; set; }
                public bool PORequest { get; set; }
                public bool POApproval { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class List
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }
                public byte? IsIssueRequest { get; set; }
                public byte? IsIssueApproval { get; set; }
                public byte? IsTransferRequest { get; set; }
                public byte? IsTransferApproval { get; set; }
                public byte? IsReturnRequest { get; set; }
                public byte? IsReturnApproval { get; set; }
                public byte? IsMaintenanceRequest { get; set; }
                public byte? IsMaintenanceApproval { get; set; }
                public byte? IsPORequest { get; set; }
                public byte? IsPOApproval { get; set; }
                public bool IssueRequest { get; set; }
                public bool IssueApproval { get; set; }
                public bool TransferRequest { get; set; }
                public bool TransferApproval { get; set; }
                public bool ReturnRequest { get; set; }
                public bool ReturnApproval { get; set; }
                public bool MaintenanceRequest { get; set; }
                public bool MaintenanceApproval { get; set; }
                public bool PORequest { get; set; }
                public bool POApproval { get; set; }
            }

            public class Details
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? RoleId { get; set; }
                public string? RoleKey { get; set; }
                public string? RoleName { get; set; }
                public byte? IsIssueRequest { get; set; }
                public byte? IsIssueApproval { get; set; }
                public byte? IsTransferRequest { get; set; }
                public byte? IsTransferApproval { get; set; }
                public byte? IsReturnRequest { get; set; }
                public byte? IsReturnApproval { get; set; }
                public byte? IsMaintenanceRequest { get; set; }
                public byte? IsMaintenanceApproval { get; set; }
                public byte? IsPORequest { get; set; }
                public byte? IsPOApproval { get; set; }
                public bool IssueRequest { get; set; }
                public bool IssueApproval { get; set; }
                public bool TransferRequest { get; set; }
                public bool TransferApproval { get; set; }
                public bool ReturnRequest { get; set; }
                public bool ReturnApproval { get; set; }
                public bool MaintenanceRequest { get; set; }
                public bool MaintenanceApproval { get; set; }
                public bool PORequest { get; set; }
                public bool POApproval { get; set; }
            }
        }
    }
}
