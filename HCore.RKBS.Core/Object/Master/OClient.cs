﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.Master
{
    public class OClient
    {
        public class Request
        {
            public long ClientId { get; set; }
            public string? ClientKey { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? CpName { get; set; }
            public string? CpEmailAddress { get; set; }
            public string? CpMobileNumber { get; set; }
            public string? StatusCode { get; set; }
            public AddressDetails? AddressDetails { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class AddressDetails
        {
            public string? Address { get; set; }
            public string? PinCode { get; set; }
            public string? StateKey { get; set; }
            public string? CityKey { get; set; }
        }

        public class Details
        {
            public long ClientId { get; set; }
            public string? ClientKey { get; set; }
            public string? ClientCode { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? CpName { get; set; }
            public string? CpEmailAddress { get; set; }
            public string? CpMobileNumber { get; set; }
            public string? Address { get; set; }
            public string? PinCode { get; set; }
            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class List
        {
            public long ClientId { get; set; }
            public string? ClientKey { get; set; }
            public string? ClientCode { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? CpName { get; set; }
            public string? CpEmailAddress { get; set; }
            public string? CpMobileNumber { get; set; }
            public string? Address { get; set; }
            public string? PinCode { get; set; }
            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
        }
    }
}
