﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Master
{
    public class OUserAccount
    {
        public class UserAccount
        {
            public class Request
            {
                public long? UserId { get; set; }
                public string? UserKey { get; set; }
                public string? UserRole { get; set; }
                public string? FirstName { get; set; }
                public string? LastName { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? UserName { get; set; }
                public string? Password { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? DepartmentKey { get; set; }
                public string? Designation { get; set; }
                public string? StatusCode { get; set; }
                public UserAddress? Address { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class UserAddress
            {
                public string? Address { get; set; }
                public string? PinCode { get; set; }
                public string? StateKey { get; set; }
                public string? CityKey { get; set; }
            }
        }

        public class Details
        {
            public long UserId { get; set; }
            public string? UserKey { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }
            public long? OrganizationId { get; set; }
            public string? OrganizationKey { get; set; }
            public string? OrganizationName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? SubDivisionId { get; set; }
            public string? SubDivisionKey { get; set; }
            public string? SubDivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public string? Designation { get; set; }
            public string? Address { get; set; }
            public string? PinCode { get; set; }
            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class List
        {
            public long UserId { get; set; }
            public string? UserKey { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? Address { get; set; }
            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }
            public long? DivisionId { get; set; }
            public string? DivisionKey { get; set; }
            public string? DivisionName { get; set; }
            public long? SubDivisionId { get; set; }
            public string? SubDivisionKey { get; set; }
            public string? SubDivisionName { get; set; }
            public long? DepartmentId { get; set; }
            public string? DepartmentKey { get; set; }
            public string? DepartmentName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }

        public class UserListDownload
        {
            public string? User_Name { get; set; }
            public string? Email { get; set; }
            public string? Mobile_Number { get; set; }
            public string? Designation { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? User_Role { get; set; }
            public string? Status { get; set; }
            public string? Registered_On { get; set; }
            public string? Registered_By { get; set; }
        }
    }
}
