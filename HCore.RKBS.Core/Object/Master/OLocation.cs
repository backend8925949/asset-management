﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Master
{
    public class OLocation
    {
        public class Save
        {
            public class Request
            {
                public string? LocationName { get; set; }
                public string? OrganizationKey { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? ParentKey { get; set; }
                public string? SubParentKey { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public string? OrganizationKey { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? ParentKey { get; set; }
                public string? SubParentKey { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Details
        {
            public class Response
            {
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public long? SubParentId { get; set; }
                public string? SubParentKey { get; set; }
                public string? SubParentName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class List
        {
            public class Response
            {
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public long? SubParentId { get; set; }
                public string? SubParentKey { get; set; }
                public string? SubParentName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }
    }
}
