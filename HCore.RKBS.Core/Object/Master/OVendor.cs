﻿

using HCore.Helper;

namespace HCore.RKBS.Core.Object.Master
{
    public class OVendor
    {
        public class Save
        {
            public class Request
            {
                public string? TypeCode { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? CpName { get; set; }
                public string? CpEmailAddress { get; set; }
                public string? CpMobileNumber { get; set; }
                public string? CpDesignation { get; set; }
                public string? GstNumber { get; set; }
                public string? PanNumber { get; set; }
                public Addresses? AddressDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long VendorId { get; set; }
                public string? VendorKey { get; set; }
                public string? TypeCode { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? CpName { get; set; }
                public string? CpEmailAddress { get; set; }
                public string? CpMobileNumber { get; set; }
                public string? CpDesignation { get; set; }
                public string? GstNumber { get; set; }
                public string? PanNumber { get; set; }
                public Addresses? AddressDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Details
        {
            public class Response
            {
                public long VendorId { get; set; }
                public string? VendorKey { get; set; }
                public string? VendorCode { get; set; }
                public int TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? CpName { get; set; }
                public string? CpEmailAddress { get; set; }
                public string? CpMobileNumber { get; set; }
                public string? CpDesignation { get; set; }
                public string? GstNumber { get; set; }
                public string? PanNumber { get; set; }
                public string? Address { get; set; }
                public string? PinCode { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class List
        {
            public class Response
            {
                public long VendorId { get; set; }
                public string? VendorKey { get; set; }
                public string? VendorCode { get; set; }
                public int TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? CpName { get; set; }
                public string? CpEmailAddress { get; set; }
                public string? CpMobileNumber { get; set; }
                public string? CpDesignation { get; set; }
                public string? GstNumber { get; set; }
                public string? PanNumber { get; set; }
                public string? Address { get; set; }
                public string? PinCode { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Addresses
        {
            public string? Address { get; set; }
            public string? PinCode { get; set; }
            public string? StateKey { get; set; }
            public string? CityKey { get; set; }
        }

        public class VendorListDownload
        {
            public string? Company_Name { get; set; }
            public string? Company_Code { get; set; }
            public string? Email { get; set; }
            public string? Mobile_Number { get; set; }
            public string? GST_Number { get; set; }
            public string? PAN_Number { get; set; }
            public string? Contact_Person_Name { get; set; }
            public string? Contact_Person_Email { get; set; }
            public string? Contact_Person_Mobile_Number { get; set; }
            public string? Registered_On { get; set; }
            public string? Registered_By { get; set; }
        }
    }
}
