﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Master
{
    public class OClass
    {
        public class Save
        {
            public class Request
            {
                public string? OrganizationKey { get; set; }
                public string? ClassName { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long? ClassId { get; set; }
                public string? ClassKey { get; set; }
                public string? OrganizationKey { get; set; }
                public string? ClassName { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class List
        {
            public class Response
            {
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? ClassId { get; set; }
                public string? ClassKey { get; set; }
                public string? ClassName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Details
        {
            public class Response
            {
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? ClassId { get; set; }
                public string? ClassKey { get; set; }
                public string? ClassName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }
    }
}
