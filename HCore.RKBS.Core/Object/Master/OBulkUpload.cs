﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.Master
{
    public class OBulkUpload
    {

        public class Request
        {
            public List<Employees>? Employees { get; set; }
            public List<Users>? Users { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Employees
        {
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Designation { get; set; }
            public string? AccountCode { get; set; }
            public string? Location { get; set; }
            public string? SubLocation { get; set; }
        }

        public class Users
        {
            public string? UserName { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? Designation { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
