﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Master
{
    public class OEmployee
    {
        public class Save
        {
            public class Request
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? OrganizationKey { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? DepartmentKey { get; set; }
                public string? LocationKey { get; set; }
                public string? SubLocationKey { get; set; }
                public string? Designation { get; set; }
                public string? AccountCode { get; set; }
                public Addresses? AddressDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? DepartmentKey { get; set; }
                public string? LocationKey { get; set; }
                public string? SubLocationKey { get; set; }
                public string? Designation { get; set; }
                public string? AccountCode { get; set; }
                public string? StatusCode { get; set; }
                public Addresses? AddressDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Details
        {
            public class Response
            {
                public long EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? AccountCode { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? SubLocationId { get; set; }
                public string? SubLocationKey { get; set; }
                public string? SubLocationName { get; set; }
                public string? Designation { get; set; }
                public string? Address { get; set; }
                public string? PinCode { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class List
        {
            public class Response
            {
                public long EmployeeId { get; set; }
                public string? EmployeeKey { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
                public string? AccountCode { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? LocationId { get; set; }
                public string? LocationKey { get; set; }
                public string? LocationName { get; set; }
                public long? SubLocationId { get; set; }
                public string? SubLocationKey { get; set; }
                public string? SubLocationName { get; set; }
                public string? Designation { get; set; }
                public string? Address { get; set; }
                public string? PinCode { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Addresses
        {
            public string? Address { get; set; }
            public string? PinCode { get; set; }
            public string? StateKey { get; set; }
            public string? CityKey { get; set; }
        }

        public class EmployeeListDownload
        {
            public string? Employee_Name { get; set; }
            public string? Employee_Code { get; set; }
            public string? Email { get; set; }
            public string? Mobile_Number { get; set; }
            public int? Issued_Assets { get; set; }
            public string? Designation { get; set; }
            public string? Division { get; set; }
            public string? Department { get; set; }
            public string? Location { get; set; }
            public string? Sub_Location { get; set; }
            public string? Status { get; set; }
            public string? Registered_On { get; set; }
            public string? Registered_By { get; set; }
        }
    }
}
