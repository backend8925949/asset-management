﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Object.Master
{
    public class OPrinter
    {
        public class List
        {
            public string? Name { get; set; }
        }

        public class Request
        {
            public long? AssetId { get; set; }
            public string? AssetBarcode { get; set; }
            public string? ExAssetId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class ListRequest
        {
            public List<Asset>? Assets { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Asset
        {
            public long? AssetId { get; set; }
            public string? AssetBarcode { get; set; }
            public string? ExAssetId { get; set; }
        }
    }
}
