﻿using HCore.Helper;

namespace HCore.RKBS.Core.Object.Master
{
    public class OAddress
    {
        public class Save
        {
            public class Request
            {
                public string? AccountKey { get; set; }
                public string? OrganizationKey { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DivisionKey { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? ParentKey { get; set; }
                public string? SubParentKey { get; set; }
                public string? CountryKey { get; set; }
                public string? StateKey { get; set; }
                public string? CityKey { get; set; }
                public string? Address { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long? AddressId { get; set; }
                public string? AddressKey { get; set; }
                public string? Address { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class List
        {
            public class Response
            {
                public long? AddressId { get; set; }
                public string? AddressKey { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountName { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public long? SubParentId { get; set; }
                public string? SubParentKey { get; set; }
                public string? SubParentName { get; set; }
                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public string? Address { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }

        public class Details
        {
            public class Response
            {
                public long? AddressId { get; set; }
                public string? AddressKey { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountName { get; set; }
                public long? OrganizationId { get; set; }
                public string? OrganizationKey { get; set; }
                public string? OrganizationName { get; set; }
                public long? DepartmentId { get; set; }
                public string? DepartmentKey { get; set; }
                public string? DepartmentName { get; set; }
                public long? DivisionId { get; set; }
                public string? DivisionKey { get; set; }
                public string? DivisionName { get; set; }
                public long? SubDivisionId { get; set; }
                public string? SubDivisionKey { get; set; }
                public string? SubDivisionName { get; set; }
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public long? SubParentId { get; set; }
                public string? SubParentKey { get; set; }
                public string? SubParentName { get; set; }
                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public string? Address { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public DateTime? CreateDate { get; set; }
                public long CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }
            }
        }
    }
}
