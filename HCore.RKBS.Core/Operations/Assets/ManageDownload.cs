﻿using Akka.Actor;
using HCore.Helper;
using HCore.RKBS.Core.Framework.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Operations.Assets
{
    public class ManageDownload
    {
        FrameworkDownload? _FrameworkDownload;

        public async Task<object> GetAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetAssets(_Request);
        }

        public async Task<object> GetIssuedAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetIssuedAssets(_Request);
        }

        public async Task<object> GetUnIssuedAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetUnIssuedAssets(_Request);
        }

        public async Task<object> GetTransferedAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetTransferedAssets(_Request);
        }

        public async Task<object> GetDisposedAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetDisposedAssets(_Request);
        }

        public async Task<object> GetExpiredAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetExpiredAssets(_Request);
        }

        public async Task<object> GetReturnedAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetReturnedAssets(_Request);
        }

        public async Task<object> GetBulkAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetBulkAssets(_Request);
        }

        public async Task<object> GetMaintenanceAssets(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetMaintenanceAssets(_Request);
        }

        public async Task<object> GetWithBarcodeAssetsStock(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetWithBarcodeAssetsStock(_Request);
        }

        public async Task<object> GetWithoutBarcodeAssetsStock(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetWithoutBarcodeAssetsStock(_Request);
        }

        public async Task<object> GetAssetHistory(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetAssetHistory(_Request);
        }

        public async Task<object> GetAssetAuditReport(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetAssetAuditReport(_Request);
        }

        public async Task<object> GetAssetDepreciation(OList.Request _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetAssetDepreciation(_Request);
        }

        public async Task<object> GetAssetDepreciation(OReference _Request)
        {
            _FrameworkDownload = new FrameworkDownload();
            return await _FrameworkDownload.GetAssetDepreciation(_Request);
        }
    }
}
