﻿using Akka.Actor;
using HCore.Helper;
using HCore.RKBS.Core.Framework.Assets;
using HCore.RKBS.Core.Object.Assets;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Operations.Assets
{
    public class ManageAssetOperations
    {
        FrameworkAssetOperations? _FrameworkAssetOperations;

        public async Task<OResponse> RegisterAsset(OAssetOperations.Register _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.RegisterAsset(_Request);
        }

        public async Task<OResponse> UpdateAsset(OAssetOperations.Register _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.UpdateAsset(_Request);
        }

        public async Task<OResponse> ReturnAsset(OAssetOperations.RetrurnAsset.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ReturnAsset(_Request);
        }

        public async Task<OResponse> ApproveAssetReturn(OReference _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ApproveAssetReturn(_Request);
        }

        public async Task<OResponse> IssueAsset(OAssetOperations.IssueAsset.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.IssueAsset(_Request);
        }

        public async Task<OResponse> ApproveAssetIssue(OReference _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ApproveAssetIssue(_Request);
        }

        public async Task<OResponse> TransferAsset(OAssetOperations.TransferAsset.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.TransferAsset(_Request);
        }

        public async Task<OResponse> ApproveAssetTransfer(OReference _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ApproveAssetTransfer(_Request);
        }

        public async Task<OResponse> IssueAssetToMaintenance(OAssetOperations.Maintenance.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.IssueAssetToMaintenance(_Request);
        }

        public async Task<OResponse> ReturnAssetFromMaintenance(OAssetOperations.Maintenance.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ReturnAssetFromMaintenance(_Request);
        }

        public async Task<OResponse> MultipleBarcodePrint(OAssetOperations.MultipleBarcodePrinting.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.MultipleBarcodePrint(_Request);
        }

        public async Task<OResponse> RePrintBarcode(OAssetOperations.RePrintBarcode _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.RePrintBarcode(_Request);
        }

        public async Task<OResponse> DisposeAsset(OAssetOperations.Dispose _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.DisposeAsset(_Request);
        }

        public async Task<OResponse> ValidateBarcode(OReference _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ValidateBarcode(_Request);
        }

        public async Task<OResponse> ManageAssetUpload(OAssetOperations.BulkUpload.Request _Request)
        {
            try
            {
                var system = ActorSystem.Create("ActorAssetUpload");
                var greeter = system.ActorOf<ActorAssetUpload>("ActorAssetUpload");
                greeter.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSUCCESS", "Assets will get uploaded within 5 mins.");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ManageAssetUpload", _Exception, null);
            }
        }
    }

    internal class ActorAssetUpload : ReceiveActor
    {
        public ActorAssetUpload()
        {
            Receive<OAssetOperations.BulkUpload.Request>(async _Request =>
            {
                FrameworkAssetOperations _FrameworkAssetOperations = new FrameworkAssetOperations();
                await _FrameworkAssetOperations.UploadAssets(_Request);
            });
        }
    }
}
