﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Assets;

namespace HCore.RKBS.Core.Operations.Assets
{
    public class ManageAssetReport
    {
        FrameworkAssetReport? _FrameworkAssetOperations;

        public async Task<OResponse> GetAssetsOverview(OReference _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetAssetsOverview(_Request);
        }

        public async Task<OResponse> GetAssetIssueReport(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetAssetIssueReport(_Request);
        }

        public async Task<OResponse> GetAssetMaintenanceReport(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetAssetMaintenanceReport(_Request);
        }

        public async Task<OResponse> GetAssetReturnReport(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetAssetReturnReport(_Request);
        }

        public async Task<OResponse> GetWithBarcodeAssetsStock(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetWithBarcodeAssetsStock(_Request);
        }

        public async Task<OResponse> GetWithoutBarcodeAssetsStock(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetWithoutBarcodeAssetsStock(_Request);
        }

        public async Task<OResponse> GetIssuedAssets(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetIssuedAssets(_Request);
        }

        public async Task<OResponse> GetTransferredAssets(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetTransferredAssets(_Request);
        }

        public async Task<OResponse> GetMappedAssets(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetMappedAssets(_Request);
        }

        public async Task<OResponse> GetAssets(OList.Request _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetReport();
            return await _FrameworkAssetOperations.GetAssets(_Request);
        }
    }
}
