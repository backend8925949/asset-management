﻿using Akka.Actor;
using HCore.Helper;
using HCore.RKBS.Core.Framework.App;
using HCore.RKBS.Core.Framework.Assets;
using HCore.RKBS.Core.Framework.BackgroundProcessor;
using HCore.RKBS.Core.Object.Assets;
using HCore.RKBS.Core.Object.BackgroundProcessor;
using static HCore.Helper.HCoreConstant;
using static HCore.RKBS.Core.Operations.BackgroundProcessor.ManageBackgroundProcessor;

namespace HCore.RKBS.Core.Operations.Assets
{
    public class ManageAssets
    {
        FrameworkAsset? _FrameworkAsset;

        public async Task<OResponse> GetAsset(OReference _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAsset(_Request);
        }

        public async Task<OResponse> GetAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAssets(_Request);
        }

        public async Task<OResponse> GetDisposedAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetDisposedAssets(_Request);
        }

        public async Task<OResponse> GetExpiredAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetExpiredAssets(_Request);
        }

        public async Task<OResponse> GetAssetBarcode(OReference _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAssetBarcode(_Request);
        }

        public async Task<OResponse> GetBulkAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetBulkAssets(_Request);
        }

        public async Task<OResponse> GetIssuedAsset(OReference _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetIssuedAsset(_Request);
        }

        public async Task<OResponse> GetIssuedAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetIssuedAssets(_Request);
        }

        public async Task<OResponse> GetUnIssuedAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetUnIssuedAssets(_Request);
        }

        public async Task<OResponse> GetTransferedAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetTransferedAssets(_Request);
        }

        public async Task<OResponse> GetReturnedAsset(OReference _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetReturnedAsset(_Request);
        }

        public async Task<OResponse> GetReturnedAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetReturnedAssets(_Request);
        }

        public async Task<OResponse> GetMaintenanceAsset(OReference _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetMaintenanceAsset(_Request);
        }

        public async Task<OResponse> GetMaintenanceAssets(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetMaintenanceAssets(_Request);
        }

        public async Task<OResponse> GetAssetAuditReport(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAssetAuditReport(_Request);
        }

        public async Task<OResponse> GetAssetHistory(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAssetHistory(_Request);
        }

        public async Task<OResponse> GetAssetDepreciation(OReference _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAssetDepreciation(_Request);
        }

        public async Task<OResponse> GetAssetDepreciation(OList.Request _Request)
        {
            _FrameworkAsset = new FrameworkAsset();
            return await _FrameworkAsset.GetAssetDepreciation(_Request);
        }
    }
}
