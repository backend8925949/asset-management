﻿using Akka.Actor;
using HCore.Helper;
using HCore.RKBS.Core.Framework.App;
using HCore.RKBS.Core.Object.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Operations.App
{
    public class ManageAssetOperations
    {
        FrameworkAssetOperations? _FrameworkAssetOperations;

        public async Task<OResponse> MapAssetBarcode(OAssetOperations.MapAssetBarcode _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.MapAssetBarcode(_Request);
        }

        public async Task<OResponse> IssueAsset(OAssetOperations.IssueAsset _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.IssueAsset(_Request);
        }

        public async Task<OResponse> TransferAsset(OAssetOperations.TransferAsset _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.TransferAsset(_Request);
        }

        public async Task<OResponse> Asset_Audit(OAssetOperations.Audit.AuditRequest _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.Asset_Audit(_Request);
        }

        public async Task<OResponse> ValidateBarcode(OReference _Request)
        {
            _FrameworkAssetOperations = new FrameworkAssetOperations();
            return await _FrameworkAssetOperations.ValidateBarcode(_Request);
        }

        public async Task MappAssetBarcodes()
        {
            try
            {
                var system = ActorSystem.Create("ActorMapAssetBarcodes");
                var greeter = system.ActorOf<ActorMapAssetBarcodes>("ActorMapAssetBarcodes");
                greeter.Tell("mapassetbarcode");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("MappAssetBarcodes", _Exception, null);
            }
        }

        public async Task IssueAssets()
        {
            try
            {
                var system = ActorSystem.Create("ActorManageIssueAsset");
                var greeter = system.ActorOf<ActorManageIssueAsset>("ActorManageIssueAsset");
                greeter.Tell("issueasset");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("IssueAssets", _Exception, null);
            }
        }

        public async Task TransferAssets()
        {
            try
            {
                var system = ActorSystem.Create("ActorManageTransferAsset");
                var greeter = system.ActorOf<ActorManageTransferAsset>("ActorManageTransferAsset");
                greeter.Tell("transferasset");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TransferAssets", _Exception, null);
            }
        }
    }

    internal class ActorMapAssetBarcodes : ReceiveActor
    {
        public ActorMapAssetBarcodes()
        {
            Receive<string>(async _Request =>
            {
                FrameworkAssetOperations _FrameworkAssetOperations = new FrameworkAssetOperations();
                await _FrameworkAssetOperations.MapAssetBarcodes();
            });
        }
    }

    internal class ActorManageIssueAsset : ReceiveActor
    {
        public ActorManageIssueAsset()
        {
            Receive<string>(async _Request =>
            {
                FrameworkAssetOperations _FrameworkAssetOperations = new FrameworkAssetOperations();
                await _FrameworkAssetOperations.IssueAssets();
            });
        }
    }

    internal class ActorManageTransferAsset : ReceiveActor
    {
        public ActorManageTransferAsset()
        {
            ReceiveAsync<string>(async _Request =>
            {
                FrameworkAssetOperations _FrameworkAssetOperations = new FrameworkAssetOperations();
                await _FrameworkAssetOperations.TransferAssets();
            });
        }
    }
}
