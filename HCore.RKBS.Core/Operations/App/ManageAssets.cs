﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Operations.App
{
    public class ManageAssets
    {
        FrameworkAssets? _FrameworkAssets;

        public async Task<OResponse> GetDivisions(OList.Request _Request)
        {
            _FrameworkAssets = new FrameworkAssets();
            return await _FrameworkAssets.GetDivisions(_Request);
        }

        public async Task<OResponse> GetIssuedAssets(OList.Request _Request)
        {
            _FrameworkAssets = new FrameworkAssets();
            return await _FrameworkAssets.GetIssuedAssets(_Request);
        }

        public async Task<OResponse> GetUnIssuedAssets(OList.Request _Request)
        {
            _FrameworkAssets = new FrameworkAssets();
            return await _FrameworkAssets.GetUnIssuedAssets(_Request);
        }

        public async Task<OResponse> GetAssets(OList.Request _Request)
        {
            _FrameworkAssets = new FrameworkAssets();
            return await _FrameworkAssets.GetAssets(_Request);
        }

        public async Task<OResponse> GetEmployees(OList.Request _Request)
        {
            _FrameworkAssets = new FrameworkAssets();
            return await _FrameworkAssets.GetEmployees(_Request);
        }
    }
}
