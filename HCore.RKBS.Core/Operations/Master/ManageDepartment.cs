﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageDepartment
    {
        FrameworkDepartment? _FrameworkDepartment;

        public async Task<OResponse> SaveDepartment(ODepartment.Save.Request _Request)
        {
            _FrameworkDepartment = new FrameworkDepartment();
            return await _FrameworkDepartment.SaveDepartment(_Request);
        }

        public async Task<OResponse> UpdateDepartment(ODepartment.Update.Request _Request)
        {
            _FrameworkDepartment = new FrameworkDepartment();
            return await _FrameworkDepartment.UpdateDepartment(_Request);
        }

        public async Task<OResponse> DeleteDepartment(OReference _Request)
        {
            _FrameworkDepartment = new FrameworkDepartment();
            return await _FrameworkDepartment.DeleteDepartment(_Request);
        }

        public async Task<OResponse> GetDepartment(OReference _Request)
        {
            _FrameworkDepartment = new FrameworkDepartment();
            return await _FrameworkDepartment.GetDepartment(_Request);
        }

        public async Task<OResponse> GetDepartments(OList.Request _Request)
        {
            _FrameworkDepartment = new FrameworkDepartment();
            return await _FrameworkDepartment.GetDepartments(_Request);
        }
    }
}
