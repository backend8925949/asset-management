﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageClass
    {
        FrameworkClass? _FrameworkClass;

        public async Task<OResponse> SaveClass(OClass.Save.Request _Request)
        {
            _FrameworkClass = new FrameworkClass();
            return await _FrameworkClass.SaveClass(_Request);
        }

        public async Task<OResponse> UpdateClass(OClass.Update.Request _Request)
        {
            _FrameworkClass = new FrameworkClass();
            return await _FrameworkClass.UpdateClass(_Request);
        }

        public async Task<OResponse> DeleteClass(OReference _Request)
        {
            _FrameworkClass = new FrameworkClass();
            return await _FrameworkClass.DeleteClass(_Request);
        }

        public async Task<OResponse> GetClass(OReference _Request)
        {
            _FrameworkClass = new FrameworkClass();
            return await _FrameworkClass.GetClass(_Request);
        }

        public async Task<OResponse> GetClasses(OList.Request _Request)
        {
            _FrameworkClass = new FrameworkClass();
            return await _FrameworkClass.GetClasses(_Request);
        }
    }
}
