﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageEmployee
    {
        FrameworkEmployee? _FrameworkEmployee;

        public async Task<OResponse> SaveEmployee(OEmployee.Save.Request _Request)
        {
            _FrameworkEmployee = new FrameworkEmployee();
            return await _FrameworkEmployee.SaveEmployee(_Request);
        }

        public async Task<OResponse> UpdateEmployee(OEmployee.Update.Request _Request)
        {
            _FrameworkEmployee = new FrameworkEmployee();
            return await _FrameworkEmployee.UpdateEmployee(_Request);
        }

        public async Task<OResponse> DeleteEmployee(OReference _Request)
        {
            _FrameworkEmployee = new FrameworkEmployee();
            return await _FrameworkEmployee.DeleteEmployee(_Request);
        }

        public async Task<OResponse> GetEmployee(OReference _Request)
        {
            _FrameworkEmployee = new FrameworkEmployee();
            return await _FrameworkEmployee.GetEmployee(_Request);
        }

        public async Task<OResponse> GetEmployees(OList.Request _Request)
        {
            _FrameworkEmployee = new FrameworkEmployee();
            return await _FrameworkEmployee.GetEmployees(_Request);
        }
    }
}
