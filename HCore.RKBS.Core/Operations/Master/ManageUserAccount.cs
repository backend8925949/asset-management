﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageUserAccount
    {
        FrameworkUserAccount? _FrameworkUserAccount;

        public async Task<OResponse> SaveUser(OUserAccount.UserAccount.Request _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.SaveUser(_Request);
        }

        public async Task<OResponse> UpdateUser(OUserAccount.UserAccount.Request _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.UpdateUser(_Request);
        }

        public async Task<OResponse> DeleteUser(OReference _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.DeleteUser(_Request);
        }

        public async Task<OResponse> GetUser(OReference _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.GetUser(_Request);
        }

        public async Task<OResponse> GetUsers(OList.Request _Request)
        {
            _FrameworkUserAccount = new FrameworkUserAccount();
            return await _FrameworkUserAccount.GetUsers(_Request);
        }
    }
}
