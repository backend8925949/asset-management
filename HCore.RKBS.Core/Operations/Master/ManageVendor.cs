﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageVendor
    {
        FrameworkVendor? _FrameworkVendor;

        public async Task<OResponse> SaveVendor(OVendor.Save.Request _Request)
        {
            _FrameworkVendor = new FrameworkVendor();
            return await _FrameworkVendor.SaveVendor(_Request);
        }

        public async Task<OResponse> UpdateVendor(OVendor.Update.Request _Request)
        {
            _FrameworkVendor = new FrameworkVendor();
            return await _FrameworkVendor.UpdateVendor(_Request);
        }

        public async Task<OResponse> DeleteVendor(OReference _Request)
        {
            _FrameworkVendor = new FrameworkVendor();
            return await _FrameworkVendor.DeleteVendor(_Request);
        }

        public async Task<OResponse> GetVendor(OReference _Request)
        {
            _FrameworkVendor = new FrameworkVendor();
            return await _FrameworkVendor.GetVendor(_Request);
        }

        public async Task<OResponse> GetVendors(OList.Request _Request)
        {
            _FrameworkVendor = new FrameworkVendor();
            return await _FrameworkVendor.GetVendors(_Request);
        }
    }
}
