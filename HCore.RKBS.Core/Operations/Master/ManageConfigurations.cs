﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageConfigurations
    {
        FrameworkConfigurations? _FrameworkConfigurations;

        public async Task<OResponse> SaveEmailConfiguration(OConfigurations.EmailConfiguration.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.SaveEmailConfiguration(_Request);
        }

        public async Task<OResponse> SaveDepreciationConfiguration(OConfigurations.DepreciationConfiguration.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.SaveDepreciationConfiguration(_Request);
        }

        public async Task<OResponse> UpdateEmailConfiguration(OConfigurations.EmailConfiguration.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.UpdateEmailConfiguration(_Request);
        }

        public async Task<OResponse> UpdateDepreciationConfiguration(OConfigurations.DepreciationConfiguration.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.UpdateDepreciationConfiguration(_Request);
        }

        public async Task<OResponse> GetEmailConfiguration(OReference _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.GetEmailConfiguration(_Request);
        }

        public async Task<OResponse> GetDepreciationConfiguration(OReference _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.GetDepreciationConfiguration(_Request);
        }

        public async Task<OResponse> SaveRequisitionConfig(OConfigurations.RequisitionConfig.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.SaveRequisitionConfig(_Request);
        }

        public async Task<OResponse> UpdateRequisitionConfig(OConfigurations.RequisitionConfig.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.UpdateRequisitionConfig(_Request);
        }

        public async Task<OResponse> GetRequisitionConfig(OReference _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.GetRequisitionConfig(_Request);
        }

        public async Task<OResponse> GetRequisitionConfig(OList.Request _Request)
        {
            _FrameworkConfigurations = new FrameworkConfigurations();
            return await _FrameworkConfigurations.GetRequisitionConfig(_Request);
        }
    }
}
