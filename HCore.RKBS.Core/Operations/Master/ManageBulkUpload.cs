﻿using Akka.Actor;
using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageBulkUpload
    {
        public async Task<OResponse> SaveEmployees(OBulkUpload.Request _Request)
        {
            try
            {
                var system = ActorSystem.Create("ActorSaveEmployees");
                var greeter = system.ActorOf<ActorSaveEmployees>("ActorSaveEmployees");
                greeter.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSUCCESS", "Employees data uploaded successfully.");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveEmployees", _Exception, null);
            }
        }
        public async Task<OResponse> SaveUsers(OBulkUpload.Request _Request)
        {
            try
            {
                var system = ActorSystem.Create("ActorSaveUsers");
                var greeter = system.ActorOf<ActorSaveUsers>("ActorSaveUsers");
                greeter.Tell(_Request);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "ATSUCCESS", "Users data uploaded successfully.");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveUsers", _Exception, null);
            }
        }
    }

    internal class ActorSaveEmployees : ReceiveActor
    {
        public ActorSaveEmployees()
        {
            Receive<OBulkUpload.Request>(async _Request =>
            {
                FrameworkBulkUpload _FrameworkBulkUpload = new FrameworkBulkUpload();
                await _FrameworkBulkUpload.SaveEmployees(_Request);
            });
        }
    }

    internal class ActorSaveUsers : ReceiveActor
    {
        public ActorSaveUsers()
        {
            Receive<OBulkUpload.Request>(async _Request =>
            {
                FrameworkBulkUpload _FrameworkBulkUpload = new FrameworkBulkUpload();
                await _FrameworkBulkUpload.SaveUsers(_Request);
            });
        }
    }
}
