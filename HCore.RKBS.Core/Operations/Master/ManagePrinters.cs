﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManagePrinters
    {
        FrameworkPrinters? _FrameworkPrinters;

        public async Task<OResponse> GetPrinters(OList.Request _Request)
        {
            _FrameworkPrinters = new FrameworkPrinters();
            return await _FrameworkPrinters.GetPrinters(_Request);
        }

        public async Task PrintBarcode(string? AssetBarcode, string ExAssetId, string? PrinterName)
        {
            _FrameworkPrinters = new FrameworkPrinters();
            await _FrameworkPrinters.PrintBarcode(AssetBarcode, ExAssetId, PrinterName);
        }

        public async Task<OResponse> SavePrintData(OPrinter.Request _Request)
        {
            _FrameworkPrinters = new FrameworkPrinters();
            return await _FrameworkPrinters.SavePrintData(_Request);
        }

        public async Task<OResponse> SavePrintsData(OPrinter.ListRequest _Request)
        {
            _FrameworkPrinters = new FrameworkPrinters();
            return await _FrameworkPrinters.SavePrintsData(_Request);
        }
    }
}
