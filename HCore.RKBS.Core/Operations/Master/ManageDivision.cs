﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageDivision
    {
        FrameworkDivision? _FrameworkDivision;

        public async Task<OResponse> SaveDivision(ODivision.Save.Request _Request)
        {
            _FrameworkDivision = new FrameworkDivision();
            return await _FrameworkDivision.SaveDivision(_Request);
        }

        public async Task<OResponse> UpdateDivision(ODivision.Update.Request _Request)
        {
            _FrameworkDivision = new FrameworkDivision();
            return await _FrameworkDivision.UpdateDivision(_Request);
        }

        public async Task<OResponse> DeleteDivision(OReference _Request)
        {
            _FrameworkDivision = new FrameworkDivision();
            return await _FrameworkDivision.DeleteDivision(_Request);
        }

        public async Task<OResponse> GetDivision(OReference _Request)
        {
            _FrameworkDivision = new FrameworkDivision();
            return await _FrameworkDivision.GetDivision(_Request);
        }

        public async Task<OResponse> GetDivisions(OList.Request _Request)
        {
            _FrameworkDivision = new FrameworkDivision();
            return await _FrameworkDivision.GetDivisions(_Request);
        }
    }
}
