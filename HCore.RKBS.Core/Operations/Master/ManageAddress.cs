﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageAddress
    {
        FrameworkAddress? _FrameworkLocation;

        public async Task<OResponse> SaveLocation(OAddress.Save.Request _Request)
        {
            _FrameworkLocation = new FrameworkAddress();
            return await _FrameworkLocation.SaveLocation(_Request);
        }

        public async Task<OResponse> UpdateLocation(OAddress.Update.Request _Request)
        {
            _FrameworkLocation = new FrameworkAddress();
            return await _FrameworkLocation.UpdateLocation(_Request);
        }

        public async Task<OResponse> DeleteLocation(OReference _Request)
        {
            _FrameworkLocation = new FrameworkAddress();
            return await _FrameworkLocation.DeleteLocation(_Request);
        }

        public async Task<OResponse> GetLocation(OReference _Request)
        {
            _FrameworkLocation = new FrameworkAddress();
            return await _FrameworkLocation.GetLocation(_Request);
        }

        public async Task<OResponse> GetLocations(OList.Request _Request)
        {
            _FrameworkLocation = new FrameworkAddress();
            return await _FrameworkLocation.GetLocations(_Request);
        }

        public async Task<OResponse> GetSubLocations(OList.Request _Request)
        {
            _FrameworkLocation = new FrameworkAddress();
            return await _FrameworkLocation.GetSubLocations(_Request);
        }
    }
}
