﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageSubCategory
    {
        FrameworkSubCategory? _FrameworkSubCategory;

        public async Task<OResponse> SaveSubCategory(OSubCategory.Save.Request _Request)
        {
            _FrameworkSubCategory = new FrameworkSubCategory();
            return await _FrameworkSubCategory.SaveSubCategory(_Request);
        }

        public async Task<OResponse> UpdateSubCategory(OSubCategory.Update.Request _Request)
        {
            _FrameworkSubCategory = new FrameworkSubCategory();
            return await _FrameworkSubCategory.UpdateSubCategory(_Request);
        }

        public async Task<OResponse> DeleteSubCategory(OReference _Request)
        {
            _FrameworkSubCategory = new FrameworkSubCategory();
            return await _FrameworkSubCategory.DeleteSubCategory(_Request);
        }

        public async Task<OResponse> GetSubCategory(OReference _Request)
        {
            _FrameworkSubCategory = new FrameworkSubCategory();
            return await _FrameworkSubCategory.GetSubCategory(_Request);
        }

        public async Task<OResponse> GetSubCategories(OList.Request _Request)
        {
            _FrameworkSubCategory = new FrameworkSubCategory();
            return await _FrameworkSubCategory.GetSubCategories(_Request);
        }
    }
}
