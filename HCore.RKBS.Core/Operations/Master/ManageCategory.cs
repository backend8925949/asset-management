﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Masters;
using HCore.RKBS.Core.Object.Master;

namespace HCore.RKBS.Core.Operations.Master
{
    public class ManageCategory
    {
        FrameworkCategory? _FrameworkCategory;

        public async Task<OResponse> SaveCategory(OCategory.Save.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return await _FrameworkCategory.SaveCategory(_Request);
        }

        public async Task<OResponse> UpdateCategory(OCategory.Update.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return await _FrameworkCategory.UpdateCategory(_Request);
        }

        public async Task<OResponse> DeleteCategory(OReference _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return await _FrameworkCategory.DeleteCategory(_Request);
        }

        public async Task<OResponse> GetCategory(OReference _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return await _FrameworkCategory.GetCategory(_Request);
        }

        public async Task<OResponse> GetCategories(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return await _FrameworkCategory.GetCategories(_Request);
        }
    }
}
