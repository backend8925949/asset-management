﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using HCore.Helper;
using HCore.RKBS.Core.Framework.Assets;
using HCore.RKBS.Core.Framework.BackgroundProcessor;
using HCore.RKBS.Core.Object.BackgroundProcessor;
using HCore.RKBS.Core.Operations.Assets;
using static HCore.Helper.HCoreConstant;

namespace HCore.RKBS.Core.Operations.BackgroundProcessor
{
    public class ManageBackgroundProcessor
    {
        public async Task RegisterAssets()
        {
            try
            {
                var system = ActorSystem.Create("ActorManageRegisterAssets");
                var greeter = system.ActorOf<ActorManageRegisterAssets>("ActorManageRegisterAssets");
                greeter.Tell("registerasset");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RegisterAssets", _Exception, null);
            }
        }

        public async Task UpdateSession()
        {
            try
            {
                var system = ActorSystem.Create("ActorUpdateSession");
                var greeter = system.ActorOf<ActorUpdateSession>("ActorUpdateSession");
                greeter.Tell("updatesession");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSession", _Exception, null);
            }
        }

        public async Task ExpireAsset()
        {
            try
            {
                var system = ActorSystem.Create("ActorExpireAsset");
                var greeter = system.ActorOf<ActorExpireAsset>("ActorExpireAsset");
                greeter.Tell("expireasset");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ExpireAsset", _Exception, null);
            }
        }

        public async Task ExpireAssetWarrenty()
        {
            try
            {
                var system = ActorSystem.Create("ActorExpireAssetWarrenty");
                var greeter = system.ActorOf<ActorExpireAssetWarrenty>("ActorExpireAssetWarrenty");
                greeter.Tell("expireassetwarrenty");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ExpireAssetWarrenty", _Exception, null);
            }
        }

        public async Task AssetAudit(OBackgroundProcessor.Request _Request)
        {
            try
            {
                var system = ActorSystem.Create("ActorAssetAudit");
                var greeter = system.ActorOf<ActorAssetAudit>("ActorAssetAudit");
                greeter.Tell(_Request);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("AssetAudit", _Exception, _Request.UserReference);
            }
        }

        internal class ActorManageRegisterAssets : ReceiveActor
        {
            public ActorManageRegisterAssets()
            {
                Receive<string>(async _Request =>
                {
                    FrameworkBackgroundProcessor _FrameworkBackgroundProcessor = new FrameworkBackgroundProcessor();
                    await _FrameworkBackgroundProcessor.RegisterAssets();
                });
            }
        }

        internal class ActorUpdateSession : ReceiveActor
        {
            public ActorUpdateSession()
            {
                Receive<string>(async _Request =>
                {
                    FrameworkBackgroundProcessor _FrameworkBackgroundProcessor = new FrameworkBackgroundProcessor();
                    await _FrameworkBackgroundProcessor.UpdateSession();
                });
            }
        }

        internal class ActorExpireAsset : ReceiveActor
        {
            public ActorExpireAsset()
            {
                Receive<string>(async _Request =>
                {
                    FrameworkBackgroundProcessor _FrameworkBackgroundProcessor = new FrameworkBackgroundProcessor();
                    await _FrameworkBackgroundProcessor.ExpireAsset();
                });
            }
        }

        internal class ActorExpireAssetWarrenty : ReceiveActor
        {
            public ActorExpireAssetWarrenty()
            {
                Receive<string>(async _Request =>
                {
                    FrameworkBackgroundProcessor _FrameworkBackgroundProcessor = new FrameworkBackgroundProcessor();
                    await _FrameworkBackgroundProcessor.ExpireAssetWarrenty();
                });
            }
        }

        internal class ActorAssetAudit : ReceiveActor
        {
            public ActorAssetAudit()
            {
                Receive<OBackgroundProcessor.Request> (async _Request =>
                {
                    FrameworkBackgroundProcessor _FrameworkBackgroundProcessor = new FrameworkBackgroundProcessor();
                    await _FrameworkBackgroundProcessor.AssetAudit(_Request);
                });
            }
        }
    }
}
