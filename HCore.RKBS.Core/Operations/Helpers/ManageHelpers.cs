﻿using HCore.Helper;
using HCore.RKBS.Core.Framework.Helpers;
using HCore.RKBS.Core.Object.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.RKBS.Core.Operations.Helpers
{
    public class ManageHelpers
    {
        FrameworkHelpers? _FrameworkHelpers;

        public async Task<OResponse> GetDivisions(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetDivisions(_Request);
        }

        public async Task<OResponse> GetDepartments(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetDepartments(_Request);
        }

        public async Task<OResponse> GetLocations(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetLocations(_Request);
        }

        public async Task<OResponse> GetSubLocations(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetSubLocations(_Request);
        }

        public async Task<OResponse> GetVendors(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetVendors(_Request);
        }

        public async Task<OResponse> GetEmployees(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetEmployees(_Request);
        }

        public async Task<OResponse> GetClasses(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetClasses(_Request);
        }

        public async Task<OResponse> GetCategories(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetCategories(_Request);
        }

        public async Task<OResponse> GetSubCategories(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetSubCategories(_Request);
        }

        public async Task<OResponse> GetEmployee(OHelpers.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetEmployee(_Request);
        }

        public async Task<OResponse> GetDepartment(OReference _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetDepartment(_Request);
        }

        public async Task<OResponse> GetUserRoles(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetUserRoles(_Request);
        }

        public async Task<OResponse> GetUsers(OList.Request _Request)
        {
            _FrameworkHelpers = new FrameworkHelpers();
            return await _FrameworkHelpers.GetUsers(_Request);
        }
    }
}
