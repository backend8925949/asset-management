﻿using HCore.RKBS.Core.Operations.Assets;
using Quartz;
using Quartz.Impl;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using HCore.RKBS.Core.Operations.App;
using HCore.RKBS.Core.Operations.BackgroundProcessor;

namespace HCore.App.RKBS.AppConnect.Jobs
{
    public class JobFactory
    {
        public async Task StartJobAsync()
        {
            ISchedulerFactory _ISchedulerFactory = new StdSchedulerFactory();
            IScheduler _IScheduler = await _ISchedulerFactory.GetScheduler();
            await _IScheduler.Start();

            #region 5 MIN Job
            IJobDetail _IJobDetail_JOB_5_MIN = JobBuilder.Create<JOB_5_MIN>()
                                    .WithIdentity("JOB_5_MIN", "JOB_5_MIN_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_5_MIN = TriggerBuilder.Create()
            .WithIdentity("JOB_5_MIN", "JOB_5_MIN_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_5_MIN)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInMinutes(2).RepeatForever())
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_5_MIN, _ITrigger_JOB_5_MIN);
            #endregion

            #region 2 MIN Job
            IJobDetail _IJobDetail_JOB_2_MIN = JobBuilder.Create<JOB_2_MIN>()
                                    .WithIdentity("JOB_2_MIN", "JOB_2_MIN_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_2_MIN = TriggerBuilder.Create()
            .WithIdentity("JOB_2_MIN", "JOB_2_MIN_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_2_MIN)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInMinutes(2).RepeatForever())
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_2_MIN, _ITrigger_JOB_2_MIN);
            #endregion

            #region 5 HRS Job
            IJobDetail _IJobDetail_JOB_5_HRS = JobBuilder.Create<JOB_5_HRS>()
                                    .WithIdentity("JOB_5_HRS", "JOB_5_HRS_JOB_Group")
                                    .Build();
            ITrigger _ITrigger_JOB_5_HRS = TriggerBuilder.Create()
            .WithIdentity("JOB_5_HRS", "JOB_5_HRS_TRIGGER_Group")
            .ForJob(_IJobDetail_JOB_5_HRS)
            .StartNow()
            .WithSimpleSchedule(x => x.WithIntervalInHours(5).RepeatForever())
            .Build();
            await _IScheduler.ScheduleJob(_IJobDetail_JOB_5_HRS, _ITrigger_JOB_5_HRS);
            #endregion
        }

        public class JOB_5_MIN : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                await Task.Delay(0);

                ManageBackgroundProcessor _ManageBackgroundProcessor = new ManageBackgroundProcessor();
                await _ManageBackgroundProcessor.RegisterAssets();
                await _ManageBackgroundProcessor.ExpireAsset();
                await _ManageBackgroundProcessor.ExpireAssetWarrenty();
            }
        }

        public class JOB_2_MIN : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                await Task.Delay(0);

                HCore.RKBS.Core.Operations.App.ManageAssetOperations _ManageAssetOperations = new HCore.RKBS.Core.Operations.App.ManageAssetOperations();
                await _ManageAssetOperations.MappAssetBarcodes();
                await _ManageAssetOperations.IssueAssets();
                await _ManageAssetOperations.TransferAssets();
            }
        }

        public class JOB_5_HRS : IJob
        {
            public async Task Execute(IJobExecutionContext context)
            {
                await Task.Delay(0);
            }
        }
    }
}
