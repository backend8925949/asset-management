﻿using HCore;
using HCore.Object;
using HCore.Helper;
using Microsoft.AspNetCore.Mvc;

namespace HCore.App.RKBS.WebConnect.App.V1.Rkbs.Core
{
    [Produces("application/json")]
    [Route("api/core/[action]")]
    public class CoreUserAccountAccessController
    {
        ManageUserAccount? _ManageUserAccount;

        [HttpPost]
        [ActionName("signin")]
        public async Task<object> AppUserLogin([FromBody] OAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.AppUserLogin(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("signout")]
        public async Task<object> AppUserLogout([FromBody] OAccount.UserAccount.Request _Request)
        {
            _ManageUserAccount = new ManageUserAccount();
            OResponse _Response = await _ManageUserAccount.AppUserLogout(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
