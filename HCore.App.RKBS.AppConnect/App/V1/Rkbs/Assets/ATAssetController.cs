﻿using Microsoft.AspNetCore.Mvc;
using HCore.RKBS.Core.Operations.App;
using HCore.Helper;

namespace HCore.App.RKBS.AppConnect.App.V1.Rkbs.Assets
{
    [Produces("application/json")]
    [Route("api/app/asset/[action]")]
    public class ATAssetController
    {
        ManageAssets? _ManageAssets;

        [HttpPost]
        [ActionName("getdivisions")]
        public async Task<object> GetDivisions([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetDivisions(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getissuedassets")]
        public async Task<object> GetIssuedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetIssuedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getunissuedassets")]
        public async Task<object> GetUnIssuedAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetUnIssuedAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getassets")]
        public async Task<object> GetAssets([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetAssets(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("getemployees")]
        public async Task<object> GetEmployees([FromBody] OList.Request _Request)
        {
            _ManageAssets = new ManageAssets();
            OResponse _Response = await _ManageAssets.GetEmployees(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
