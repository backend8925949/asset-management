﻿using HCore.Helper;
using Microsoft.AspNetCore.Mvc;
using HCore.RKBS.Core.Operations.App;
using HCore.RKBS.Core.Object.App;

namespace HCore.App.RKBS.AppConnect.App.V1.Rkbs.Assets
{
    [Produces("application/json")]
    [Route("api/app/asset/operations/[action]")]
    public class ATAssetOperationsController
    {
        ManageAssetOperations? _ManageAssetOperations;

        [HttpPost]
        [ActionName("mapassetbarcode")]
        public async Task<object> MapAssetBarcode([FromBody] OAssetOperations.MapAssetBarcode _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.MapAssetBarcode(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("issueasset")]
        public async Task<object> IssueAsset([FromBody] OAssetOperations.IssueAsset _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.IssueAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("transferasset")]
        public async Task<object> TransferAsset([FromBody] OAssetOperations.TransferAsset _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.TransferAsset(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("assetaudit")]
        public async Task<object> Asset_Audit([FromBody] OAssetOperations.Audit.AuditRequest _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.Asset_Audit(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("validatebarcode")]
        public async Task<object> ValidateBarcode([FromBody] OReference _Request)
        {
            _ManageAssetOperations = new ManageAssetOperations();
            OResponse _Response = await _ManageAssetOperations.ValidateBarcode(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
