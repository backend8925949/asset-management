using HCore.App.RKBS.AppConnect.Core;
using HCore.Helper;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using static HCore.Helper.HCoreConstant;

var _WebApplicationBuilder = WebApplication.CreateBuilder(args);
_WebApplicationBuilder.Services.AddMvc(options => options.EnableEndpointRouting = false);
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson(opt =>
{
    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
});
_WebApplicationBuilder.Services.AddControllers().AddNewtonsoftJson();
_WebApplicationBuilder.Services.Configure<KestrelServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});

_WebApplicationBuilder.Services.Configure<IISServerOptions>(options =>
{
    options.AllowSynchronousIO = true;
});

System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

var _IApplicationBuilder = _WebApplicationBuilder.Build();
_IApplicationBuilder.Use(async (context, next) =>
{
    if (string.IsNullOrEmpty(HCoreConstant.HostName))
    {
        HCoreConstant.HostName = context.Request.Host.Host;
        HCoreXConfiguration.LoadConfiguration(context.Request.Host.Host);
        HCoreHelper.LogData(LogType.Security, "IIS", "", context.Request.Host.Host, null);
    }
    await next.Invoke();
});
_IApplicationBuilder.UseCors(_Builder => _Builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
_IApplicationBuilder.UseRouting();
_IApplicationBuilder.HCoreXAuth();
_IApplicationBuilder.UseMvc();
_IApplicationBuilder.Run();


